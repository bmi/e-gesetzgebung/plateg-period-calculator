// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

export const de = {
  zeit: {
    generalErrorMsg:
      'Es ist ein unerwarteter Fehler aufgetreten. Bitte wenden Sie sich an den Support. Wir entschuldigen uns für entstandene Umstände. {{fehlercode}}',
    header: {
      help: {
        linkHelp: 'Hilfe',
        title: 'Zeitplanung',
        contentlight:
          '<p>In der Anwendung Zeitplanung können Sie Zeitplanungen anlegen und diese bearbeiten. Unterstützt werden Sie dabei durch Vorlagen, die alle wesentlichen Phasen und Termine enthalten. Bestehende Vorlagen können Sie für Ihren individuellen Bedarf anpassen.</p>' +
          '<p>In der Anwendung werden Sie durch spezifische Hilfetexte zu den wichtigsten Begrifflichkeiten unterstützt.</p>' +
          '<h2>Bedienung und Barrierefreiheit der Kalendersicht</h2>' +
          '<p>Sie können die Zeitachse des Kalenders horizontal bewegen, indem Sie im Bereich der Datumseinträge ihren Mauszeiger gedrückt halten und den Bereich nach links oder rechts ziehen. Falls Ihre Maus ein Scrollrad besitzt, drücken Sie die Umschalttaste (Shift-Taste) und scrollen Sie horizontal.</p>' +
          '<p>Die Kalendersicht ist nicht barrierefrei zugänglich. Alle relevanten Informationen und Funktionalitäten finden Sie in der barrierefreien Tabellensicht.</p>',
        content:
          '<p>In der Anwendung Zeitplanung können Sie Zeitplanungen zu einem Regelungsvorhaben anlegen und diese bearbeiten. Die Anwendung dient der internen Planung und hat keine Bindungswirkung innerhalb des eigenen Hauses oder für andere Verfassungsorgane. Unterstützt werden Sie durch Vorlagen, die alle wesentlichen Phasen und Termine enthalten. Dabei werden Sitzungswochen von Bundestag und Bundesrat berücksichtigt. Die einzelnen Zeitangaben der Phasen und Termine sind Näherungswerte und können im Einzelfall abweichen. Bestehende Vorlagen können Sie für Ihren individuellen Bedarf anpassen.</p>' +
          '<p>Zeitplanungen und Zeitplanungsvorlagen können archiviert werden. Sie behalten weiterhin Lese-Zugriff auf archivierte Zeitplanungen und -vorlagen.</p>' +
          '<p>In der Anwendung werden Sie durch spezifische Hilfetexte zu den wichtigsten Begrifflichkeiten unterstützt.</p>' +
          '<h2>Bedienung und Barrierefreiheit der Kalendersicht</h2>' +
          '<p>Sie können die Zeitachse des Kalenders horizontal bewegen, indem Sie im Bereich der Datumseinträge ihren Mauszeiger gedrückt halten und den Bereich nach links oder rechts ziehen.</p>' +
          '<p>Falls Ihre Maus ein Scrollrad besitzt, drücken Sie die Umschalttaste (Shift-Taste) und scrollen Sie horizontal.</p>' +
          '<p>Die Kalendersicht ist nicht barrierefrei zugänglich. Alle relevanten Informationen und Funktionalitäten finden Sie in der barrierefreien Tabellensicht.</p>',
      },
      linkHome: 'Startseite elektronische Zeitplanung',
      btnNewZeitPlanungsVorlage: 'Neue Zeitplanungsvorlage anlegen',
      btnNewZeitPlanung: 'Neue Zeitplanung anlegen',
      exportButton: 'Datenexport (PDF)',
      breadcrumbs: {
        projectName: 'Zeitplanung',
        meineZeitplanungen: 'Zeitplanungen',
        billigungsanfragen: 'Billigungsanfragen',
        zeitplanungsvorlagen: 'Eigene Vorlagen',
        systemvorlagen: 'Systemvorlagen',
        archiv: 'Archiv',
      },
    },
    myZeitHome: {
      title: 'Zeitplanung',
      archivedLabel: 'Archiviert',
      contactPersonTitle: 'Konakt',
      tabs: {
        sorters: {
          bearbeitetAsc: 'Zuletzt bearbeitet (älteste zuerst)',
          bearbeitetDesc: 'Zuletzt bearbeitet (neueste zuerst)',
          terminAsc: 'Nächstes Element (nächstes zuerst)',
          terminDesc: 'Nächstes Element (letztes zuerst)',
          gesendetAsc: 'Gesendet am (älteste zuerst)',
          gesendetDesc: 'Gesendet am (neueste zuerst)',
        },
        filter: {
          displayNames: {
            regelungsvorhaben: 'Regelungsvorhaben',
            typ: 'Zeitplanung',
            vorhabenart: 'Vorhabentyp',
            originalTab: 'Ursprung',
            erstelltVon: 'Erstellerin oder Ersteller',
            rueckmeldung: 'Rückmeldung',
          },
          labelContent: 'Filtern nach',
        },
        zugriffsrechte: {
          FEDERFUEHRER: 'Federführung',
          GAST: 'Beobachtung',
          MITARBEITER: 'Mitarbeit',
          ERSTELLER: 'Ersteller',
          LESER: 'Lesen',
          SCHREIBER: 'Schreiben',
          ZPV_BESITZER: 'Besitzerin/Besitzer',
          ZPV_BEOBACHTER: 'Beobachtung',
          KEINE: 'Keine',
        },
        meineZeitplanungen: {
          tabNav: 'Zeitplanungen',
          text: 'Hier können Sie neue Zeitplanungen zu einem Regelungsvorhaben zur internen Planung eines Regelungsvorhabens anlegen sowie bestehende Zeitplanungen bearbeiten. Beim Anlegen einer Zeitplanung können Sie eine Vorlage auswählen, die bereits alle wesentlichen Phasen, Termine und Hinweise für Ihr Regelungsvorhaben enthält. Falls Ihre Zeitplanung von jemand anderem gebilligt werden muss, können Sie von hier eine Billigung anfragen.',
          imgText1: 'Neue Zeitplanungen anlegen',
          imgText2: 'Zeitplanungen gemeinsam bearbeiten',
          imgText3: 'Billigungen anfragen',
          table: {
            menuItemRegelungsvorhaben: 'Regelungsvorhaben',
            menuItemZeitplanung: 'Zeitplanung',
            menuItemNaechstesElement: 'Nächstes Element',
            menuItemBeplanterZeitraum: 'Beplanter Zeitraum',
            menuItemErsteller: 'Erstellt',
            menuItemZuletztBearbeitet: 'Zuletzt bearbeitet',
            zugriffsrecht: 'Meine Zugriffsrechte',
            zugriffsrechtInfo: {
              title: 'Meine Zugriffsrechte für Zeitplanungen',
              drawerText: `
              <p>Die Zugriffsrechte für eine Zeitplanung richten sich nach den Zugriffsrechten des zugehörigen Regelungsvorhabens. Sie können für jede Zeitplanung die Berechtigungen einsehen. Die Zugriffsrechte werden beim zugehörigen Regelungsvorhaben verwaltet. </p>
              </br>
              <h2>Mögliche Zugriffsrechte</h2>

              <p><b>Federführung</b>: Sie können eine Zeitplanung zu dem Regelungsvorhaben anlegen. Sie können Phasen und Termine bearbeiten und die Bearbeitung für andere sperren. Sie können Elemente hinzufügen oder löschen. Sie können die Billigung der Zeitplanung anfragen sowie die Zeitplanung archivieren. Sie können die externen Kalender einsehen und verwalten.</p>
              <p><b>Mitarbeit</b>: Sie können die Zeitplanung einsehen, Phasen und Termine bearbeiten sowie Elemente hinzufügen oder löschen. Sie können die externen Kalender einsehen und verwalten.</p>
              <p><b>Beobachtung</b>: Sie können die Zeitplanung einsehen. Sie können die externen Kalender einsehen und verwalten. </p>`,
            },
            menuItemAktionen: 'Aktionen',
            beschreibungLabel: 'Beschreibung der Zeitplanung',
            actions: {
              freigeben: 'Berechtigungen',
              zeichnungAnfragen: 'Billigung anfragen',
              vorlageSpeichern: 'Als Zeitplanungsvorlage speichern unter...',
              archivieren: 'Archivieren',
            },
          },
          archiveConfirmModal: {
            title: 'Hinweis',
            content: `<p>Hiermit verschieben Sie die Zeitplanung ins Archiv. Eine Archivierung gilt für alle Personen mit Berechtigungen an der Zeitplanung. Eine Archivierung können Sie nicht rückgängig machen.</p>
            <p>Im Archiv kann die Zeitplanung noch eingesehen, aber nicht mehr bearbeitet werden.</p>
            <p>Billigungsanfragen für diese Zeitplanung werden ebenfalls ins Archiv verschoben. Falls sie noch offen sind, werden sie zurückgezogen.</p>
            <p>Eine Veraktung der Zeitplanung muss unabhängig davon manuell vorgenommen werden.</p>
            <p>Alle Zeitplanungen eines Regelungsvorhabens werden automatisch archiviert, wenn das übergeordnete Regelungsvorhaben selbst archiviert wird.</p>
            <p>Möchten Sie diese Zeitplanung wirklich archivieren?</p>`,
            confirm: 'Nein, nicht archivieren',
            cancel: 'Ja, archivieren',
            archiveSuccess: 'Sie haben die Zeitplanung „{{titel}}“ archiviert.',
          },
        },
        billigungsanfragen: {
          tabNav: 'Billigungsanfragen',
          text: 'Hier erhalten Sie Anfragen von anderen Nutzenden mit der „Bitte um Billigung“ Ihrer Zeitplanungen. In der Übersicht können Sie die Anfragen verfolgen und beantworten. Sie sehen hier außerdem Billigungsanfragen, die Sie an andere Nutzende gestellt haben.',
          imgText1: 'Billigungsanfragen erhalten',
          imgText2: 'Billigungsanfragen beantworten',
          table: {
            menuItemRegelungsvorhaben: 'Regelungsvorhaben',
            menuItemZeitplanung: 'Zeitplanung',
            menuItemGesendet: 'Gesendet am',
            menuItemAnfrage: 'Anfrage',
            menuItemAktionen: 'Aktionen',
            anmerkungenLabel: 'Anmerkungen',
            status: {
              BILLIGUNG: 'Gebilligt',
              BILLIGUNG_UNTER_VORBEHALT: 'Gebilligt u.V.',
              KEINE_BILLIGUNG: 'Abgelehnt',
              NICHT_BEARBEITET: 'Offen',
              ZURUECKGEZOGEN: 'Zurückgezogen',
            },
            actions: {
              archivieren: 'Archivieren',
              antworten: 'Beantworten',
              zurueckziehen: 'Billigungsanfrage zurückziehen',
            },
          },
          archiveConfirmModal: {
            title: 'Hinweis',
            content:
              'Eine Archivierung können Sie nicht rückgängig machen. Möchten Sie diese Billigungsanfrage wirklich archivieren?',
            confirm: 'Nein, nicht archivieren',
            cancel: 'Ja, archivieren',
            archiveSuccess: 'Sie haben die Billigungsanfrage zur Zeitplanung „{{titel}}“ archiviert',
          },
          cancelConfirmModal: {
            title: 'Hinweis',
            content: 'Möchten Sie diese Billigungsanfrage wirklich zurückziehen?',
            cancel: 'Nein, nicht zurückziehen',
            confirm: 'Ja, zurückziehen',
            cancelSuccess: 'Sie haben die Billigungsanfrage zur Zeitplanung „{{titel}}“ zurückgezogen',
          },
        },
        zeitplanungsvorlagen: {
          openConfirmModal: {
            title: 'Hinweis',
            content:
              'Sie öffnen eine Zeitplanungsvorlage, die auf den Vorgaben des HdR, der GGO und des Grundgesetzes basiert. Sie kann nicht überschrieben werden. Wenn Sie die Vorlage ändern möchten, müssen Sie sie zuerst unter einem neuen Namen speichern. Benutzen Sie dazu die Funktion "Speichern unter" oben rechts.',
            confirm: 'Ok, gelesen',
            confirmDontShowAgain: 'Nicht mehr anzeigen',
          },
          tabNav: 'Eigene Vorlagen',
          text: 'Hier finden Sie Ihre selbst erstellten Zeitplanungsvorlagen im Überblick. Sie können diese Vorlagen nach Belieben bearbeiten und sie anderen Personen freigeben.',
          imgText1: 'Eigene Vorlagen erstellen und bearbeiten',
          table: {
            menuItemVorhabentyp: 'Vorhabentyp',
            menuItemZeitplanungsvorlage: 'Zeitplanungsvorlage',
            menuItemDauer: 'Dauer',
            menuItemZuletztBearbeitet: 'Zuletzt bearbeitet',
            menuItemErstelltAm: 'Erstellt am',
            menuItemErsteller: 'Erstellt',
            zugriffsrecht: 'Meine Zugriffsrechte',
            zugriffsrechtInfo: {
              title: 'Meine Zugriffsrechte für Zeitplanungsvorlagen',
              drawerText: `
              <h2>Mögliche Zugriffsrechte</h2>
                        <ul>
                          <li>„Besitzerin/Besitzer“: Sie können die Zeitplanungsvorlage öffnen, bearbeiten und kopieren sowie archivieren.</li>
                          <li>„Beobachtung“: Sie können die Zeitplanungsvorlage einsehen und eine Kopie als neue Zeitplanungsvorlage speichern.</li>
                        </ul>
                        <h2>Zugriffsrechte ändern</h2>
                        <p>Öffnen Sie im Aktionen-Menü (Drei-Punkte-Menü) den Dialog „Berechtigungen“. Dort können Sie die Zugriffsrechte einsehen und bearbeiten. </p>`,
            },
            menuItemAktionen: 'Aktionen',
            monate: 'Monate',
            beschreibungLabel: 'Beschreibung der Vorlage',
            actions: {
              freigeben: 'Berechtigungen',
              vorlageSpeichern: 'Kopie als eigene Vorlage speichern',
              archivieren: 'Archivieren',
              msgs: {
                archiveSuccess: 'Sie haben die Zeitplanungsvorlage „{{titel}}“ archiviert',
              },
            },
            vorhabenart: {
              GESETZ: 'Gesetz',
              RECHTSVERORDNUNG: 'Rechtsverordnung',
              VERWALTUNGSVORSCHRIFT: 'Verwaltungsvorschrift',
            },
            sorters: {
              erstelltAsc: 'Erstellt (älteste zuerst)',
              erstelltDesc: 'Erstellt (neueste zuerst)',
              bearbeitetAsc: 'Zuletzt bearbeitet (älteste zuerst)',
              bearbeitetDesc: 'Zuletzt bearbeitet (neueste zuerst)',
              dauerAsc: 'Dauer (kürzeste zuerst)',
              dauerDesc: 'Dauer (längste zuerst)',
            },
          },
        },
        systemvorlagen: {
          openConfirmModal: {
            title: 'Hinweis',
            content:
              'Sie öffnen eine Zeitplanungsvorlage, die auf den Vorgaben des HdR, der GGO und des Grundgesetzes basiert. Sie kann nicht überschrieben werden. Wenn Sie die Vorlage ändern möchten, müssen Sie sie zuerst unter einem neuen Namen speichern. Benutzen Sie dazu die Funktion "Speichern unter" oben rechts.',
            confirm: 'Ok, gelesen',
            confirmDontShowAgain: 'Nicht mehr anzeigen',
          },
          tabNav: 'Systemvorlagen',
          text: 'Die E-Gesetzgebung bietet Vorlagen an, die bereits alle wesentlichen Phasen und Termine für Ihr Regelungsvorhaben enthalten. Sie können als Grundlage für Ihre Planung dienen, sind aber nicht bindend. Sie können diese Vorlagen auch anpassen und als neue eigene Vorlagen abspeichern.',
          imgText1: 'Systemvorlagen einsehen und verwenden',
          table: {
            menuItemVorhabentyp: 'Vorhabentyp',
            menuItemZeitplanungsvorlage: 'Zeitplanungsvorlage',
            menuItemDauer: 'Dauer',
            menuItemZuletztBearbeitet: 'Zuletzt bearbeitet',
            menuItemErstelltAm: 'Erstellt am',
            menuItemErsteller: 'Erstellt',
            zugriffsrecht: 'Meine Zugriffsrechte',
            zugriffsrechtInfo: {
              title: 'Meine Zugriffsrechte für Zeitplanungsvorlagen',
              drawerText: `
              <h2>Mögliche Zugriffsrechte</h2>
                        <ul>
                          <li>„Besitzerin/Besitzer“: Sie können die Zeitplanungsvorlage öffnen, bearbeiten und kopieren sowie archivieren.</li>
                          <li>„Beobachtung“: Sie können die Zeitplanungsvorlage einsehen und eine Kopie als neue Zeitplanungsvorlage speichern.</li>
                        </ul>
                        <h2>Zugriffsrechte ändern</h2>
                        <p>Öffnen Sie im Aktionen-Menü (Drei-Punkte-Menü) den Dialog „Berechtigungen“. Dort können Sie die Zugriffsrechte einsehen und bearbeiten. </p>`,
            },
            menuItemAktionen: 'Aktionen',
            monate: 'Monate',
            beschreibungLabel: 'Beschreibung der Vorlage',
            actions: {
              freigeben: 'Berechtigungen',
              vorlageSpeichern: 'Kopie als eigene Vorlage speichern',
              archivieren: 'Archivieren',
              msgs: {
                archiveSuccess: 'Sie haben die Zeitplanungsvorlage „{{titel}}“ archiviert',
              },
            },
            vorhabenart: {
              GESETZ: 'Gesetz',
              RECHTSVERORDNUNG: 'Rechtsverordnung',
              VERWALTUNGSVORSCHRIFT: 'Verwaltungsvorschrift',
            },
            sorters: {
              erstelltAsc: 'Erstellt (älteste zuerst)',
              erstelltDesc: 'Erstellt (neueste zuerst)',
              bearbeitetAsc: 'Zuletzt bearbeitet (älteste zuerst)',
              bearbeitetDesc: 'Zuletzt bearbeitet (neueste zuerst)',
              dauerAsc: 'Dauer (kürzeste zuerst)',
              dauerDesc: 'Dauer (längste zuerst)',
            },
          },
        },
        archiv: {
          tabNav: 'Archiv',
          text: 'Zeitplanungen, Billigungsanfragen und Zeitplanungsvorlagen, die Sie nicht mehr bearbeiten möchten, können Sie ins Archiv sortieren. Alle Nutzenden haben nach der Archivierung nur noch einen Lese-Zugriff. Eine Archivierung kann nicht rückgängig gemacht werden.',
          imgText1: 'Alle Vorgänge zu einer Zeitplanung archivieren',
          imgText2: 'Lese-Zugriff auf alle archivierten Vorgänge bleibt erhalten',
          table: {
            menuItemRegelungsvorhaben: 'Regelungsvorhaben',
            menuItemName: 'Name des Objekts',
            menuItemVorhabentyp: 'Vorhabentyp',
            vorhabenart: {
              GESETZ: 'Gesetz',
              RECHTSVERORDNUNG: 'Verordnung',
              VERWALTUNGSVORSCHRIFT: 'Vorschrift',
            },
            menuItemArchiviertAm: 'Archiviert',
            menuItemUrspruenglicherTab: 'Ursprung',
            sorter: {
              archivedAsc: 'Archiviert (älteste zuerst)',
              archivedDesc: 'Archiviert (neueste zuerst)',
            },
            tabs: {
              MEINE_ZEITPLANUNGEN: 'Zeitplanungen',
              BILLIGUNGEN: 'Billigungsanfragen',
              ZEITPLANUNGSVORLAGEN: 'Zeitplanungsvorlagen',
            },
          },
        },
      },
      freigabe: {
        success: 'Sie haben die Berechtigungen für die Zeitplanungsvorlage „{{title}}“ aktualisiert.',
        successZeitplanung: 'Sie haben die Berechtigungen für die Zeitplanung „{{title}}“ aktualisiert.',
        successRv: 'Sie haben die Berechtigungen für das Regelungvorhaben „{{title}}“ aktualisiert.',
      },
      freigebenDialog: {
        title: 'Zeitplanungsvorlage Leserecht freigeben',
        titleZeitplanung: 'Zeitplanung Rechte freigeben',
        subtitle: 'Zeitplanungsvorlage: {{title}}',
        subtitleZeitplanung: 'Zeitplanung: {{title}}',
        addAdressaten: 'Adressatinnen und Adressaten hinzufügen',
        placeholder: 'Bitte ausfüllen',
        errorAdressaten: 'Bitte fügen Sie einen Adressaten hinzu',
        errorMailadress: 'Bitte geben Sie eine gültige E-Mailadresse an',
        errorParticipant: 'Sie können die Vorlage nicht für sich selbst freigeben',
        errorType: 'Bitte geben Sie die Art des Elements an.',
        notes: 'Anmerkungen',
        firstPageNext: 'Eingaben prüfen',
        secondPageSubtitle: 'Eingaben prüfen',
        nameOfVorlage: 'Name der Zeitplanungsvorlage:',
        nameOfZeitplanung: 'Name der Zeitplanung:',
        adressaten: 'Adressatinnen und Adressaten:',
        rights: 'Rechte:',
        rightsLESERECHTE: 'Leserechte',
        rightsSCHREIBRECHTE: 'Schreibrechte',
        notesSecondPage: 'Anmerkungen:',
        secondPageNext: 'Zeitplanungsvorlage jetzt freigeben',
        secondPageNextZeitplanung: 'Zeitplanung jetzt freigeben',
        successSingle: 'Sie haben die Zeitplanungsvorlage „{{title}}“ an 1 Person freigegeben.',
        successMultiple: 'Sie haben die Zeitplanungsvorlage „{{title}}“ an {{numPersonen}} Personen freigegeben.',
        successSingleZeitplanung: 'Sie haben die Zeitplanung „{{title}}“ an 1 Person freigegeben.',
        successMultipleZeitplanung: 'Sie haben die Zeitplanung „{{title}}“ an {{numPersonen}} Personen freigegeben.',
      },
      billigungsanfragenDialog: {
        title: 'Billigung anfragen',
        cancelBtnText: 'Abbrechen',
        prevBtnText: 'Vorheriger Schritt',
        nextBtnText: 'Nächster Schritt',
        successMsg: 'Sie haben die Billigung der Zeitplanung „{{title}}“ angefragt.',
        page1: {
          nextBtnText: 'Eingaben prüfen',
          subtitle: 'Zeitplanung: {{title}}',
          searchLabel: 'Suche nach Adressatinnen und Adressaten',
          searchDrawerTitle: 'Suche nach Adressatinnen und Adressaten',
          searchDrawerText: `<p>Hier können Sie die Adressdatenbank durchsuchen. Tippen Sie dazu einfach den Namen, das Ressort oder eine E-Mail-Adresse ein. Sie können hier auch E-Mail-Listen verwenden, die Sie über die Einstellungen verwalten können.</p>
            <p>Sie können mehrere Suchbegriffe eintragen. Trennen Sie diese dazu einfach durch Leerzeichen. Die Begriffe werden dann für die Suche kombiniert. Die Treffer enthalten alle angegebenen Suchbegriffe. Es wird eine Und-Verknüpfung der Begriffe verwendet.</p>
            <p>Sie können nur Personen auswählen, die für die E-Gesetzgebung registriert sind.</p>`,
          searchHint: '(z. B. Name, Ressort, E-Mail-Adresse, E-Mail-Liste)',
          addressLabel: 'Adressatinnen und Adressaten',
          addressDrawerTitle: 'Adressatinnen und Adressaten',
          addressDrawerText:
            'Sofern Sie bereits eine oder mehrere Personen ausgewählt haben, sehen Sie hier die ausgewählten Adressatinnen und Adressaten.',
          errorAdressaten: 'Bitte fügen Sie eine Adressatin oder einen Adressaten hinzu.',
          errorMailadress: 'Bitte geben Sie eine gültige E-Mailadresse an.',
          errorParticipant: 'Sie können keine Billigungsanfrage an sich selbst senden.',
          notes: 'Anmerkungen',
          roleNote:
            'Bitte bedenken Sie, dass Sie mit der Billigungsanfrage der Person für die Beantwortung die Rolle “Beobachtung” für das Regelungsvorhaben zuweisen und sie damit Leseberechtigungen für das Regelungsvorhaben und seine untergeordneten Elemente (einschließlich aller Zeitplanungen) erhält.',
        },
        page2: {
          subtitle: 'Eingaben prüfen',
          nameOfZeitplanung: 'Name der Zeitplanung:',
          adressaten: 'Adressatinnen und Adressaten:',
          notes: 'Anmerkungen:',
          submit: 'Anfrage jetzt absenden',
        },
      },
      anfrageBeantwortenDialog: {
        title: 'Billigungsanfrage beantworten',
        cancelBtnText: 'Abbrechen',
        prevBtnText: 'Vorheriger Schritt',
        nextBtnText: 'Nächster Schritt',
        successMsg: {
          BILLIGUNG: 'Sie haben die Zeitplanung „{{title}}“ gebilligt.',
          BILLIGUNG_UNTER_VORBEHALT: 'Sie haben die Zeitplanung „{{title}}“ unter Vorbehalt gebilligt.',
          KEINE_BILLIGUNG: 'Sie haben die Billigung der Zeitplanung „{{title}}“ abgelehnt.',
        },
        page1: {
          nextBtnText: 'Eingaben prüfen',
          subtitle: 'Zeitplanung: {{title}}',
          answer: {
            title: 'Bitte wählen Sie:',
            error: 'Bitte wählen Sie eine Antwort aus',
            options: {
              BILLIGUNG: {
                title: 'Billigen',
                info: 'Wählen Sie Billigen, wenn Sie der Zeitplanung ohne Vorbehalte zustimmen.',
              },
              BILLIGUNG_UNTER_VORBEHALT: {
                title: 'Billigen unter Vorbehalt',
                info: 'Wählen Sie „Billigen unter Vorbehalt“, wenn Sie der Zeitplanung vorbehaltlich etwaiger Anpassungen zustimmen. Ihre Anpassungsbedarfe können Sie in dem untenstehenden Textfeld einfügen.',
              },
              KEINE_BILLIGUNG: {
                title: 'Billigung ablehnen',
                info: 'Wählen Sie „Billigen ablehnen“, wenn Sie der Zeitplanung in dieser Form nicht zustimmen möchten. Ihre Anpassungsbedarfe können Sie in dem untenstehenden Textfeld einfügen.',
              },
            },
          },
          notes: 'Anmerkungen',
        },
        page2: {
          subtitle: 'Eingaben prüfen',
          nameOfZeitplanung: 'Name der Zeitplanung:',
          type: 'Art der Billigung:',
          adressat: 'Adressatin oder Adressat:',
          notes: 'Anmerkungen:',
          submit: 'Antwort jetzt absenden',
          answer: {
            BILLIGUNG: {
              title: 'Billigen',
            },
            BILLIGUNG_UNTER_VORBEHALT: {
              title: 'Billigen unter Vorbehalt',
            },
            KEINE_BILLIGUNG: {
              title: 'Billigung ablehnen',
            },
          },
        },
      },
      archivierenDialog: {
        text: 'Eine Archivierung können Sie nicht rückgängig machen. Möchten Sie diese Vorlage wirklich archivieren?',
        okButtonText: 'Ja, archivieren',
        cancelButtonText: 'Nein, nicht archivieren',
      },
    },
    newZeitplanung: {
      modal: {
        title: 'Neue Zeitplanung anlegen',
        page1: {
          title: 'Schritt 1 von 2: Zeitplanungsvorgaben auswählen',
          item1: {
            info: {
              title: 'Erklärung zu: Regelungsvorhaben',
              text: 'Durch die Auswahl des Regelungsvorhabens werden Ihnen mögliche passende Zeitplanungsvorlagen durch das System vorgeschlagen.',
            },
            label: 'Regelungsvorhaben',
            placeholder: 'Bitte auswählen',
            requiredMsg: 'Bitte wählen Sie ein Regelungsvorhaben aus',
          },
          item2: {
            info: {
              title: 'Erklärung zu: Zeitplanungsvorlage',
              text: 'Die Zeitplanungsvorlagen der Anwendung „Zeitplanung“ enthalten alle Phasen, die für Ihr Vorhaben relevant sind. Die Vorlagen unterscheiden sich durch unterschiedliche Zeitdauer der einzelnen Phasen. Die Zeiträume leiten sich aus der rechtlichen Grundlage (insbesondere der <a href="{{linkGGO}}" target="_blank">GGO</a>) sowie der gängigen Praxis ab. Unter dem Tab „Zeitplanungsvorlagen“ können Sie bestehende Vorlagen einsehen, auf ihrer Grundlage eigene Vorlagen erstellen und diese bearbeiten. Zeitplanungen von bestehenden Regelungsvorhaben können Sie ebenfalls als Vorlage speichern und als Vorlage für weitere Regelungsvorhaben nutzen.',
            },
            label: 'Zeitplanungsvorlage',
            placeholder: 'Bitte auswählen',
            requiredMsg: 'Bitte wählen Sie ein Zeitplanungsvorlage aus',
          },
        },
        page2: {
          title: 'Schritt 2 von 2: Zeitplanung benennen',
          nextBtn: 'Eingaben prüfen',
          item1: {
            info: {
              title: 'Erklärung zu: Titel',
              text: 'Der Titel der Zeitplanung wird automatisch durch das System generiert. Bei Bedarf können Sie den Titel anpassen. ',
            },
            requiredMsg: 'Bitte geben Sie den Titel an',
            errorLength: 'Maximale Länge von {{maxChars}} Zeichen für den Titel wurde überschritten.',
            label: 'Titel',
          },
          item2: {
            info: {
              title: 'Erklärung zu: Beschreibung',
              text: 'Hier können Sie eine aussagekräftige Beschreibung Ihrer Zeitplanung hinzufügen. Die Beschreibung wird auf der Startseite der Anwendung „Zeitplanung“ unter den Daten der jeweiligen Zeitplanung angezeigt. Sie ist für alle Personen sichtbar, die auf diese Zeitplanung zugreifen dürfen.',
            },
            errorLength: 'Maximale Länge von {{maxChars}} Zeichen für die Beschreibung wurde überschritten.',
            label: 'Beschreibung',
          },
          item3: {
            info: {
              title: 'Erklärung zu: Beginn der Zeitplanung',
              text: 'Hier können Sie für die Zeitplanung ein Startdatum festlegen. Wenn Sie eine Vorlage ausgewählt haben, berechnet das System ausgehend vom Startdatum einen ersten Vorschlag für eine regelkonforme Zeitplanung. Wenn Sie eine Vorlage ausgewählt haben, beginnt die Zeitplanung mit der „Vorphase“.',
            },
            requiredMsg: 'Bitte geben Sie den Beginn der Zeitplanung ein.',
            label: 'Beginn der Zeitplanung (tt.mm.jjjj)',
          },
        },
        page3: {
          noAnswer: 'Keine Angabe',
          title: 'Eingaben prüfen',
          nextBtn: 'Zeitplanung jetzt anlegen',
          definitionTitles: {
            regelungsvorhaben: 'Dazugehöriges Regelungsvorhaben',
            verwendeteZeitplanungsvorlage: 'Verwendete Zeitplanungsvorlage',
            titelDerZeitplanung: 'Titel der Zeitplanung',
            beschreibung: 'Beschreibung',
            datum: 'Beginn der Zeitplanung',
          },
          hinweis: {
            title: 'Berücksichtigte Sitzungswochen',
            contentGesetze:
              'Bitte beachten Sie, dass in Ihrer Zeitplanung nur die bereits im System hinterlegten Sitzungswochen des Bundestags und Bundesrats berücksichtigt werden. Diese können Sie sich in der Kalenderansicht oder bei der Bearbeitung von Phasen und Terminen anzeigen lassen.',
            contentRvVv:
              'Bitte beachten Sie, dass in Ihrer Zeitplanung nur die bereits im System hinterlegten Sitzungswochen des Bundesrats berücksichtigt werden. Diese können Sie sich in der Kalenderansicht oder bei der Bearbeitung von Phasen und Terminen anzeigen lassen.',
          },
          successMsg: {
            msg: 'Sie haben die neue {{typ}} Zeitplanung „{{titel}}“ angelegt.',
            INTERN: 'interne',
            UEBERGREIFEND: 'externe',
          },
        },
      },
    },
    newZeitplanungsvorlage: {
      modal: {
        copyZeitplanungsvorlage: {
          title: 'Kopie als eigene Vorlage speichern',
          page1: {
            title: 'Schritt 1 von 1: Zeitplanungsvorlage benennen',
          },
          page2: {
            nextBtn: 'Kopie jetzt anlegen',
          },
        },
        speichernAlsZeitplanungsvorlage: {
          title: 'Als Zeitplanungsvorlage speichern unter...',
          page1: {
            title: 'Schritt 1 von 2: Regelungsvorhabentyp auswählen',
          },

          page2: {
            title: 'Schritt 2 von 2: Zeitplanungsvorlage benennen',
            nextBtn: 'Eingaben prüfen',
          },
          page3: {
            nextBtn: 'Zeitplanungsvorlage jetzt anlegen',
          },
        },
        title: 'Neue Zeitplanungsvorlage anlegen',
        cancelBtnText: 'Abbrechen',
        nextBtnText: 'Nächster Schritt',
        prevBtnText: 'Vorheriger Schritt',
        requiredInfo: 'Pflichtfelder sind mit einem * gekennzeichnet.',
        requiredMsg: 'Bitte füllen Sie das Pflichtfeld aus',
        page1: {
          title: 'Schritt 1 von 3: Zeitplanungsvorgaben auswählen',
          item1: {
            info: {
              title: 'Erklärung zu: Zeitplanungsvorlage',
              text: 'An dieser Stelle haben Sie die Möglichkeit eine Zeitplanungsvorlage auszuwählen, auf deren Basis Sie Ihre neue Vorlage erstellen möchten. Wählen können Sie aus systemseitig bereitgestellten Vorlagen, von Ihnen angelegten Vorlagen und für Sie freigegebenen Vorlagen. Wenn Sie hier keine Vorlage auswählen, beginnen Sie mit einer leeren Vorlage, in der sich noch keine Elemente befinden. ',
            },
            label: 'Zeitplanungsvorlage',
            placeholder: 'Bitte auswählen',
          },
        },
        page2: {
          title: 'Schritt 2 von 3: Zeitplanungsvorgaben machen',
          info: {
            title: 'Erklärung zu: Vorhabentyp',
            text: 'Wählen Sie hier den passenden Vorhabentyp zu Ihrem Vorhaben aus.',
          },
          item1: {
            requiredMsg: 'Bitte geben Sie den Vorhabentyp an',
            label: 'Bitte wählen Sie den Vorhabentyp aus',
            options: {
              GESETZ: 'Gesetz',
              RECHTSVERORDNUNG: 'Verordnung',
              VERWALTUNGSVORSCHRIFT: 'Vorschrift',
            },
          },
        },
        page3: {
          title: 'Schritt 3 von 3: Zeitplanungsvorlage benennen',
          nextBtn: 'Eingaben prüfen',
          item1: {
            info: {
              title: 'Erklärung zu: Titel',
              text: 'Hier können Sie einen aussagekräftigen Titel Ihrer Zeitplanungsvorlage hinzufügen. Er ist für alle Personen sichtbar, die auf diese Zeitplanungsvorlage zugreifen dürfen.',
            },
            requiredMsg: 'Bitte geben Sie den Titel an',
            errorLength: 'Maximale Länge von {{maxChars}} Zeichen für den Titel wurde überschritten.',
            label: 'Titel',
          },
          item2: {
            info: {
              title: 'Erklärung zu: Beschreibung',
              text: 'Hier können Sie eine aussagekräftige Beschreibung Ihrer Zeitplanungsvorlage hinzufügen. Die Beschreibung wird auf der Startseite der Anwendung „Zeitplanung“ unter den Daten der jeweiligen Zeitplanungsvorlage angezeigt. Sie ist für alle Personen sichtbar, die auf diese Zeitplanungsvorlage zugreifen dürfen.',
            },
            errorLength: 'Maximale Länge von {{maxChars}} Zeichen für die Beschreibung wurde überschritten.',
            label: 'Beschreibung',
          },
        },
        page4: {
          noAnswer: 'Keine Angabe',
          title: 'Eingaben prüfen',
          nextBtn: 'Zeitplanungsvorlage jetzt anlegen',
          definitionTitles: {
            zeitplanungsvorlagenart: 'Zeitplanungsvorlagenart',
            vorhabentyp: 'Vorhabentyp',
            verwendeteZeitplanungsvorlage: 'Verwendete Zeitplanungsvorlage',
            titelDerZeitplanung: 'Titel der Zeitplanung',
            titelDerZeitplanungsVorlage: 'Titel der Zeitplanungsvorlage',
            beschreibung: 'Beschreibung',
          },
        },
      },
    },
    newElementGeneral: {
      newElementBtn: 'Element hinzufügen',
      expandAllRows: 'Alle Phasen öffnen',
      closeAllRows: 'Alle Phasen schließen',
      breadcrumbTitle: '',
      submit: 'Eingaben prüfen',
      cancel: 'Abbrechen',
      placeholder: {
        input: 'Bitte ausfüllen',
        select: 'Bitte auswählen',
      },
      kalendarDrawer: {
        title: 'Externe Kalender sowie Termine und Phasen',
        buttonText: 'Externe Kalender sowie Termine und Phasen anzeigen',
        errorMessage: 'Kalenderdaten konnten nicht abgerufen werden!',
        stand: 'Stand: ',
        bundesratContentTitle: 'Plenarsitzung des Bundesrates',
        bundesweiteFeiertageTitle: 'Gesetzliche bundesweite Feiertage',
        sitzungswochenBundestags: 'Sitzungswochen des Deutschen Bundestags',
        termineBundesrats: 'Termine des Bundesrats',
        schulferienBundesland: 'Schulferien nach Bundesland',
        gesetzlicheFeiertage: 'Gesetzliche Feiertage',
        plenarsitzung: 'Plenarsitzung',
        eigeneTermineUndPhasen: 'Termine und Phasen dieser Zeitplanung',
        nurIn: 'nur in',
        erweiterbar: 'erweiterbar',
        selectLabel: 'Angezeigte Informationen wählen',
        introText:
          'Hier können Sie bereits geplante Phasen und Termine nachschlagen. Außerdem können Sie externe Kalender auswählen. Falls ein Kalender für das Element relevant ist, wird er direkt angezeigt.',
      },
      action: {
        neuesElement: {
          breadcrumbTitle: 'Neues Element hinzufügen',
          mainTitle: 'Neues Element hinzufügen',
          msgs: {
            success: 'Sie haben erfolgreich {{type}} {{name}} angelegt.',
          },
        },
        elementBearbeiten: {
          breadcrumbTitle: '{{type}} „{{name}}“',
          breadcrumbActionTitle: 'Bearbeiten',
          mainTitle: '{{type}} bearbeiten',
          msgs: {
            success: 'Sie haben erfolgreich {{type}} {{name}} geändert.',
          },
        },
        uebersicht: {
          breadcrumbTitle: '{{type}} „{{name}}“',
        },
      },
      confirmModal: {
        title: 'Möchten Sie das Hinzufügen des neuen Elements wirklich abbrechen?',
        btnCancelConfirmOk: 'Ja, abbrechen',
        btnCancelConfirmNo: 'Nein, nicht abbrechen',
      },
      titleAndOrder: {
        title: 'Titel und Reihenfolge',
        type: {
          elementType: {
            title: 'Art des Elements',
            error: 'Bitte geben Sie die Art des Elements an.',
            options: {
              termin: 'Termin',
              phase: 'Phase',
            },
            hint: {
              title: 'Art des Elements',
              content:
                '<p>Wählen Sie „Termin“, wenn Sie einen Termin mit einem Zeitpunkt anlegen möchten. Wählen Sie „Phase“, wenn Sie einen Zeitraum planen möchten.</p>',
            },
          },
          specificType: {
            phase: {
              error: 'Bitte geben Sie die Art der Phase an.',
              title: 'Art der Phase',
              options: {
                copy: 'Vordefinierte Phase (mit Regeln und Hinweisen)',
                custom: 'Benutzerdefinierte Phase (ohne Regeln und Hinweise)',
              },
              hint: {
                title: 'Art der Phase',
                content:
                  '<p>Wählen Sie „Vordefinierte Phase (mit Regeln und Hinweisen)“, wenn Sie auf eine Liste mit Standard-Phasen zurückgreifen möchten. Diese Standard-Phasen finden sich auch in den Systemvorlagen wieder. Bei diesen sind bereits Regeln enthalten, die konform mit dem Rechtsetzungsprozess sind. Bei Abweichungen erhalten Sie Hinweise. Sie können Anpassungen vornehmen; nur der Name ist nicht editierbar.</p><p>Wählen Sie „Benutzerdefinierte Phase (ohne Regeln und Hinweise)“, wenn Sie eine Phase vollständig neu anlegen möchten. Hier legen Sie selbst den Namen und die ggf. dazugehörige Phase fest.</p>',
              },
            },
            termin: {
              error: 'Bitte geben Sie die Art des Termins an.',
              title: 'Art des Termins',
              options: {
                copy: 'Vordefinierter Termin (mit Regeln und Hinweisen)',
                custom: 'Benutzerdefinierter Termin (ohne Regeln und Hinweise)',
              },
              hint: {
                title: 'Art des Termins',
                content:
                  '<p>Wählen Sie „Vordefinierter Termin (mit Regeln und Hinweisen)“, wenn Sie auf eine Liste mit Standard-Terminen zurückgreifen möchten. Diese Standard-Termine finden sich auch in den Systemvorlagen wieder. Bei den Standard-Terminen sind bereits Regeln enthalten, die konform mit dem Rechtsetzungsprozess sind. Bei Abweichungen erhalten Sie Hinweise. Sie können Anpassungen vornehmen; nur der Name ist nicht editierbar.</p><p>Wählen Sie „Benutzerdefinierter Termin (ohne Regeln und Hinweise)“, wenn Sie einen Termin vollständig neu anlegen möchten. Hier können Sie selbst den Namen frei wählen sowie die dazugehörige Phase und die Position in der Zeitplanung festlegen.</p>',
              },
            },
          },
        },
        mainTitle: {
          error: 'Bitte geben Sie den Titel des neuen Elements an.',
          phase: {
            title: 'Titel der Phase',
          },
          termin: {
            title: 'Titel des Termins',
          },
        },
        position: {
          title: '{{type}} an folgender Stelle einfügen',
          firstElement: 'Am Anfang der Zeitplanung',
          firstElementInPhase: 'Am Anfang dieser Phase',
          error: 'Bitte geben Sie die Position des neuen Elements an.',
          msg: {
            title: 'Reihenfolge von Elementen',
            content:
              'Bitte beachten Sie die vorgegebene Reihenfolge in der GGO, dem GG sowie der Anwendung <a href="#/evir" target="_blank"><em>Verfahrensassistent</em></a>',
          },
          hint: {
            phase: {
              title: 'Phase an folgender Stelle einfügen',
              content:
                '<p>Wählen Sie hier die passende Position der Phase in der Zeitplanungsvorlage.</p><p>Wenn Sie eine vordefinierte Phase erstellen oder bearbeiten, wird Ihnen automatisch die im Rechtsetzungsprozess vorgesehene Platzierung vorgeschlagen. Das ist erkennbar durch den Eintrag „... (gemäß Rechtsetzungsprozess)“ beim passenden Element. Sie können eine andere Position wählen.</p><p>Erstellen oder bearbeiten Sie eine benutzerdefinierte Phase, wird Ihnen standardmäßig die Position „Am Beginn der Zeitplanung“ vorgeschlagen. Sie können eine andere Position wählen.</p>',
            },
            termin: {
              title: 'Termin an folgender Stelle einfügen',
              content:
                '<p>Wählen Sie hier die passende Position des Termins in der Zeitplanung.</p><p>Wenn Sie mit einem vordefinierten Termin arbeiten, wird Ihnen automatisch die im Rechtsetzungsprozess vorgesehene Platzierung vorgeschlagen. Das ist erkennbar durch den Eintrag „... (gemäß Rechtsetzungsprozess)“ beim passenden Element. Sie können eine andere Position wählen.</p><p>Erstellen oder bearbeiten Sie einen benutzerdefinierten Termin, wird Ihnen standardmäßig die Position „Am Beginn der Zeitplanung“ vorgeschlagen. Sie können eine andere Position wählen.</p>',
            },
          },
        },
      },
      moreInformation: {
        title: 'Weitere Informationen',
        properties: {
          title: 'Eigenschaften',
          hint: {
            title: 'Eigenschaften',
            content:
              'Sie können jedem Element zwei Eigenschaften hinzufügen. Wählen Sie „Diesen Termin als „wichtig“ markieren“, wenn dieses Element besondere Beachtung benötigt. Wählen Sie „Die Bearbeitung dieses Termins für andere sperren“, wenn Sie Änderungen an diesem Element durch andere Personen verhindern möchten, etwa bei einer Freigabe.',
          },
          options: {
            important: {
              label: 'Diesen Termin als „wichtig“ markieren.',
            },
            lock: {
              termin: {
                label: 'Die Bearbeitung dieses Termins für andere sperren.',
                labelChild: 'Andere dürfen diesen Termin nicht bearbeiten, weil die übergeordnete Phase gesperrt ist.',
              },
              phase: {
                label: 'Die Bearbeitung dieser Phase für andere sperren.',
                labelChild: 'Andere dürfen diese Phase nicht bearbeiten, weil die übergeordnete Phase gesperrt ist.',
              },
            },
          },
        },
        commentBox: {
          title: 'Kommentar',
          hint: ' (maximal {{maxChars}} Zeichen)',
          error: 'Die maximal zulässige Anzahl von {{maxChars}} Zeichen wurde überschritten.',
          info: {
            title: 'Kommentar',
            content:
              'Sie können dem neuen Element einen Kommentar hinzufügen. Dieser Kommentar ist dann für alle Personen sichtbar, die Zugriff auf diese Zeitplanung haben oder erhalten, etwa durch Freigaben.',
          },
        },
      },
      startDate: {
        title: 'Datum (tt.mm.jjjj)',
        error: 'Bitte geben Sie das Datum des neuen Elements an.',
        validationError:
          'Das {{elementType}} darf nicht vor dem {{type}} ({{prevDate}}) des vorherigen Elements liegen.',
        info: {
          title: 'Datum',
          content:
            'Wählen Sie hier das Datum des Termins. Sie können externe Kalender einblenden, die Sie bei der Terminfindung unterstützen. Haben Sie einen vordefinierten Termin ausgewählt, kann Ihnen das System passende Einträge vorschlagen. Diese sind visuell hervorgehoben.',
        },
        validationErrorElement: {
          beginn:
            'Das {{dateType}} ({{newDate}}) erstes Elements ist früher als das Startdatum ({{parentDate}}) der Hauptphase. Bitte ändern Sie als erstes das Startdatum der Hauptphase.',
          ende: 'Das {{dateType}} ({{newDate}}) letztes Elements ist später als das Enddatum ({{parentDate}}) der Hauptphase. Bitte ändern Sie als erstes das Enddatum der Hauptphase.',
        },
        validationErrorParentPhase: {
          beginn:
            'Das Startdatum ({{newDate}}) der Hauptphase ist später als das Startdatum ({{childDate}}) des erstes Elements. Bitte ändern Sie als erstes das Startdatum des ersten Elements: {{childTitle}}.',
          ende: 'Das Enddatum ({{newDate}}) der Hauptphase ist früher als das Enddatum ({{childDate}}) des letzten Elements. Bitte ändern Sie als erstes das Enddatum des letzten Elements: {{childTitle}}.',
        },
        infobox: {
          title: 'Unzulässiger Wochentag',
          content: 'Kabinettsitzungen finden immer mittwochs statt.',
        },
      },
      period: {
        title: 'Start- und Enddatum (tt.mm.jjjj)',
        error: 'Bitte geben Sie das {{type}} des neuen Elements an.',
        closeBtn: 'Schließen',
        info: {
          title: 'Zeitraum',
          content:
            'Wählen Sie hier das Start- und Enddatum der Phase. Sie können externe Kalender einblenden, die Sie bei der Terminfindung unterstützen. Haben Sie eine vordefinierte Phase ausgewählt, kann Ihnen das System passende Einträge vorschlagen. Diese sind visuell hervorgehoben.',
        },
        hinweis: {
          title: 'Bearbeitung der Dauer',
          content_G_RV:
            'Die Frist für die Ressortabstimmung beträgt in der Regel vier Wochen. Wenn Sie die Frist verkürzen möchten, müssen Sie vorab die Zustimmung aller Beteiligten einholen. Wenn sie die Frist verlängern möchten, muss dies vorab von einem Ressort im Rahmen der Beteiligung beantragt werden.',
          content_VV: 'Planen Sie, wenn möglich, mit einer Dauer von {{min}} bis {{max}} Wochen.',
        },
        startLabel: 'Startdatum (tt.mm.jjjj)',
        endLabel: 'Enddatum (tt.mm.jjjj)',
      },
      errorDateFormat: 'Bitte geben Sie ein gültiges Datum im Format „tt.mm.jjjj“ ein.',
      reposition: {
        title: 'Datumseinträge aller nachfolgenden Elemente verschieben',
        info: {
          title: 'Datumseinträge aller nachfolgenden Elemente verschieben',
          content:
            'Wenn Sie ein Datum auf einen zeitlich späteren Zeitpunkt verlegen, können Sie mithilfe dieses Schalters beeinflussen, ob sich diese Änderung auch auf alle nachfolgenden Einträge auswirken soll. ' +
            'Ist dieser Schalter aktiv, verschieben sich alle nachfolgenden Elemente entsprechend den enthaltenen Regeln, der Reihenfolge sowie den unterschiedlichen Dauern ebenfalls nach hinten. Wenn der Schalter nicht aktiv ist, können sich Elemente ggf. überschneiden oder Lücken entstehen. ' +
            'Verlegen Sie die Datumsangabe auf einen zeitlich früheren Zeitpunkt, hat das keine Auswirkungen auf vorhergehende Termine. Es kommt dann ggf. zu Überschneidungen.',
        },
      },
      elementPruefen: {
        title: 'Eingaben prüfen',
        breadcrumbTitle: 'Eingaben prüfen',
        submit: 'Element jetzt speichern',
        submitEdit: 'Änderungen jetzt speichern',
        edit: {
          overview: {
            false: 'Eingaben bearbeiten',
            true: '{{type}} bearbeiten',
          },
        },
        backLink: {
          meineZeitplanungen: 'Zurück zur Zeitplanung',
          zeitplanungsvorlagen: 'Zurück zur Zeitplanungsvorlage',
          systemvorlagen: 'Zurück zur Systemvorlage',
        },
        titleOrder: {
          title: 'Titel und Reihenfolge',
          items: {
            title: {
              label: 'Titel',
            },
            type: {
              phase: {
                label: 'Art der Phase',
                copyLabel: 'Vordefinierte Phase (mit Regeln und Hinweisen)',
                newLabel: 'Benutzerdefinierte Phase (ohne Regeln und Hinweise)',
              },
              termin: {
                label: 'Art des Termins',
                copyLabel: 'Vordefinierter Termin (mit Regeln und Hinweisen)',
                newLabel: 'Benutzerdefinierter Termin (ohne Regeln und Hinweise)',
              },
            },
            phase: {
              label: 'Zugeordnete Phase',
              noPhase: 'Keine Zugeordnete Phase',
            },
            order: {
              label: 'Reihenfolge',
              firstInVorlage: 'Am Anfang der Zeitplanung',
              firstInPhase: 'Am Anfang der Phase',
            },
          },
        },
        duration: {
          title: 'Dauer',
          items: {
            months: 'Monate',
            weeks: 'Wochen',
            days: 'Tage',
          },
          durationLabels: {
            months: ['Monate', 'Monat'],
            weeks: ['Wochen', 'Woche'],
            days: ['Tage', 'Tag'],
          },
        },
        date: {
          title: 'Frist',
          items: {
            date: 'Datum',
          },
        },
        plannendPeriod: {
          title: 'Geplanter Zeitraum',
          items: {
            from: 'Von',
            to: 'Bis',
          },
        },
        repositionElements: {
          title: 'Datumseinträge aller nachfolgenden Elemente verschieben',
        },
        moreInformation: {
          title: 'Weitere Informationen',
          properties: {
            title: 'Eigenschaften',
            items: {
              important: {
                label: 'Dieser Termin ist als „wichtig“ markiert.',
              },
              lock: {
                termin: {
                  label: 'Andere dürfen diesen Termin nicht bearbeiten.',
                  labelChild:
                    'Andere dürfen diesen Termin nicht bearbeiten, weil die übergeordnete Phase gesperrt ist.',
                },
                phase: {
                  label: 'Andere dürfen diese Phase nicht bearbeiten.',
                  labelChild: 'Andere dürfen diese Phase nicht bearbeiten, weil die übergeordnete Phase gesperrt ist.',
                },
              },
            },
          },
          comment: {
            title: 'Kommentar',
          },
        },
      },
    },
    zeitplanungenTable: {
      linkZeitplanungen: 'Zeitplanungen',
      noParticipantError: {
        title: 'Achtung, es ist ein Fehler aufgetreten.',
        text: 'Sie sind kein Teilnehmender der Zeitplanung.',
        buttonText: 'Startseite elektronische Zeitplanung',
      },
      tabs: {
        kalendersicht: {
          tabNav: 'Kalendersicht',
          removeExternal: 'Kalender ausblenden',
          detailsModal: {
            title: {
              termin: 'Termin',
              phase: 'Phase',
              external: 'Externer Kalendereintrag',
            },
            timeLine: {
              termin: 'Frist',
              phase: 'Geplanter Zeitraum',
              external: {
                termin: 'Datum',
                phase: 'Geplanter Zeitraum',
              },
            },
            kommentar: 'Kommentar',
            btnBearbeiten: {
              termin: 'Termin bearbeiten',
              phase: 'Phase bearbeiten',
            },
            titleMenu: {
              edit: 'Bearbeiten',
              delete: 'Löschen',
            },
          },
          scrollConfirmModal: {
            title: 'Hinweis',
            content: `<p>Sie können die Zeitachse des Kalenders horizontal bewegen, indem Sie im Bereich der Datumseinträge ihren Mauszeiger gedrückt halten und den Bereich nach links oder rechts ziehen.</p>
              <p>Falls Ihre Maus ein Scrollrad besitzt, drücken Sie die Umschalttaste (Shift-Taste) und Sie scrollen horizontal.</p>
              <p>Die Kalendersicht ist nicht barrierefrei zugänglich. Alle relevanten Informationen und Funktionalitäten finden Sie in der barrierefreien Tabellensicht.</p>`,
            confirm: 'Ok, gelesen',
            confirmDontShowAgain: 'Nicht mehr anzeigen',
          },
          scaleSelect: {
            options: {
              year: 'Jahr',
              halfYear: 'Halbjahr',
              quarterYear: 'Quartal',
            },
          },
          daySelect: {
            todayBtn: 'Heute',
          },
          selectCalendar: {
            trigger: 'Externe Kalender verwalten',
            modal: {
              title: 'Externe Kalender verwalten',
              cancel: 'Abbrechen',
              confirm: 'Ausgewählte Kalender jetzt einblenden',
              heading: 'Kalender auswählen und in der Kalendersicht einblenden',
              selects: {
                placeholder: 'Bitte auswählen',
                feiertage: {
                  label: 'Feiertage und Schulferien',
                  bundesEntry: 'Bundesweite Feiertage',
                  allEntries: 'Feiertage aller Bundesländer',
                },
                ferien: {
                  allEntries: 'Schulferien aller Bundesländer',
                },
                sitzungen: {
                  label: 'Sitzungskalender',
                  allEntries: 'Alle Sitzungen',
                },
                selectedLabels: {
                  feiertageSchulferien: 'Ausgewählte Feiertage und Schulferien',
                  noFeiertageSchulferien: 'Es wurden noch keine Feiertage oder Schulferien ausgewählt.',
                  sitzungen: 'Ausgewählte Sitzungskalender',
                  noSitzungen: 'Es wurden noch keine Sitzungskalender ausgewählt.',
                },
                successToast: 'Die ausgewählten Kalender wurden erfolgreich in der Zeitplanung eingeblendet.',
              },
            },
          },
        },
        tabellensicht: {
          tabNav: 'Tabellensicht',
          tableHeads: {
            phasenUndTermine: 'Phasen und Termine',
            geplanterZeitraum: 'Geplanter Zeitraum',
            frist: 'Zeitpunkt',
            eigenschaften: 'Eigenschaften',
            aktionen: 'Aktionen',
          },
          actions: {
            bearbeiten: 'Bearbeiten',
            important: 'Als "wichtig" markieren',
            notImportant: 'Nicht mehr als "wichtig" markieren',
            gesperrt: 'Bearbeiten für andere freigeben',
            nichtGesperrt: 'Bearbeiten für andere sperren',
            delete: 'Löschen',
          },
          ariaLabels: {
            buttonOpened: 'Untergeordnete Termine und Phasen werden in den nächsten Zeilen angezeigt',
            buttonClosed: 'Untergeordnete Termine und Phasen einblenden',
          },
        },
        kommentare: {
          tabNav: 'Kommentare',
          newComment: {
            writeAnswer: 'Antwort verfassen',
            editComment: 'Kommentar bearbeiten',
            writeComment: 'Kommentar verfassen',
            saveBtn: 'Speichern',
            addBtn: 'Hinzufügen',
            cancelBtn: 'Abbrechen',
          },
          allComments: {
            commentSgl: 'Kommentar',
            commentPl: 'Kommentare',
            timeLabel: 'Uhr',
          },
          actionBtns: {
            deleteConfirm: 'Möchten Sie den Kommentar löschen?',
            answerBtn: 'Antworten',
            deleteBtn: 'Löschen',
            editBtn: 'Bearbeiten',
            cancelBtn: 'Abbrechen',
          },
        },
      },
      header: {
        btnSpeichern: 'Als Zeitplanungsvorlage speichern unter...',
        lastSavedAt: 'Zuletzt gespeichert {{dateTime}} Uhr',
      },
      billigungsanfragenStatus: {
        title: 'Status der Billigungsanfrage',
        status: {
          BILLIGUNG: 'Gebilligt',
          BILLIGUNG_UNTER_VORBEHALT: 'Gebilligt unter Vorbehalt',
          KEINE_BILLIGUNG: 'Abgelehnt',
          NICHT_BEARBEITET: 'Offen',
          ZURUECKGEZOGEN: 'Zurückgezogen',
        },
        person: 'durch {{person}}&nbsp;',
        datumAm: '· am {{datum}}',
        datumSeit: '· seit {{datum}}',
      },
    },
    zeitplanungsvorlagenTable: {
      linkZeitplanungsvorlagen: 'Zeitplanungsvorlagen',
      linkArchiv: 'Archiv',
      dropdownTitle: 'Anzeigen des Zeitplans',
      tabs: {
        kalendersicht: {
          tabNav: 'Kalendersicht',
        },
        tabellensicht: {
          tabNav: 'Tabellensicht',
          types: {
            phase: 'Phase',
            termin: 'Termin',
          },
          terminDauer: '1 Tag',
          tableHeads: {
            phasenUndTermine: 'Phasen und Termine',
            dauer: 'Dauer',
            eigenschaften: 'Eigenschaften',
            aktionen: 'Aktionen',
          },
          actions: {
            bearbeiten: 'Bearbeiten',
            important: 'Als „wichtig“ markieren',
            unimportant: 'Nicht mehr als „wichtig“ markieren',
            delete: 'Löschen',
            msgs: {
              important: 'Der Termin „{{name}}“ wird als „wichtig“ markiert.',
              unimportant: 'Der Termin „{{name}}“ wird nicht mehr als „wichtig“ markiert.',
              locked: '{{type}} „{{name}}“ ist für andere Benutzer gesperrt.',
              unlocked: '{{type}} „{{name}}“ ist für andere Benutzer freigegeben.',
            },
          },
          deleteModal: {
            title: 'Hinweis: Löschen von Element',
            btnOk: 'Ok, löschen',
            btnCancel: 'Abbrechen',
            textGGO:
              'Die GGO gibt die Phasen und Termine aus denen ein Rechtsetzungsprozess besteht vor. Das von Ihnen ausgewählte Element muss laut GGO durchlaufen werden. Möchten Sie das Element trotzdem löschen?',
            text: 'Möchten Sie dieses Element wirklich löschen?',
            deleteMsg: 'Sie haben {{type}} {{name}} erfolgreich gelöscht.',
            type: {
              phase: 'Phase',
              termin: 'Termin',
            },
          },
          ariaLabels: {
            buttonOpened: 'Untergeordnete Termine und Phasen werden in den nächsten Zeilen angezeigt',
            buttonClosed: 'Untergeordnete Termine und Phasen einblenden',
          },
          infos: {
            G_PRUEFUNG_DER_KABINETTVORLAGE_DURCH_BKAMT_UND_ANDERE_RESSORT:
              'Zwischen der Zustellung der Vorlage an die Staatssekretärin oder den Staatssekretär des Bundeskanzleramtes und die Bundesministerinnen und Bundesminister und der Beratung soll mindestens eine Woche liegen (§ 21 Absatz 3 <a target="_blank" href="https://www.bundesregierung.de/breg-de/service/archiv/alt-inhalte/geschaeftsordnung-der-bundesregierung-459846">GO BReg</a>).',
            G_STANDARDFRIST_BIS_ZUM_INKRAFTTRETEN:
              'Gemäß <a target="_blank" href="https://www.gesetze-im-internet.de/gg/BJNR000010949.html#BJNR000010949BJNE010701377">Artikel 82 Absatz 2 GG</a> tritt das Gesetz 14 Tage nach Verkündung im Bundesgesetzblatt in Kraft, wenn kein anderes Datum im Gesetzestext selbst festgelegt wurde.',
            G_VORPHASE:
              'Die Vorphase umfasst neben ersten konzeptionellen Überlegungen, die Ergänzung der Vorhabensplanung sowie die Erstellung des Referentenentwurfs. Die Dauer der Vorphase ist abhängig vom Gegenstand des Regelungsvorhabens.',
            G_KONZEPTIONELLE_UEBERLEGUNGEN:
              'Im Rahmen der konzeptionellen Überlegungen erfolgt die grundlegende organisatorische und fachlich, inhaltliche Planung des Regelungsvorhabens. ',
            G_PRUEFUNG_DER_POLITISCHEN_INHALTLICHEN_UND_ZEITLICHEN_RAHMENBEDINGUNGEN:
              'Die politischen, inhaltlichen und zeitlichen Rahmenbedingungen sind vor Beginn der Arbeiten am Regelungsentwurf zu klären. Sie variieren je nach Gegenstand des Regelungsvorhabens.',
            G_PRUEFUNG_VON_REGELUNGSALTERNATIVEN:
              'Die Notwendigkeit einer Regelung ist zu evaluieren. Es ist zu prüfen, ob sich Ziele des Regelungsvorhabens auch anders – z. B. durch alternative Formen der Problembewältigung oder problemorientierte Auslegung/Anwendung der vorhandenen rechtlichen Regelungen – erreichen lassen. Die Anwendung <a href="#/evor" target="_blank">Vorbereitung</a> bietet Ihnen Unterstützung bei der Prüfung von Regelungsalternativen.',
            G_PRUEFUNG_VON_GESETZESFOLGEN_INKL_ERFUELLUNGSAUFWAND:
              '<p>Die Ressorts sind verpflichtet, die voraussichtlichen Gesetzesfolgen einer Regelung darzustellen (<a href="{{baseUrl}}/arbeitshilfen/download/34#page=33" target="_blank">§ 43 Absatz 1 Nr. 5</a> und <a href="{{baseUrl}}/arbeitshilfen/download/34#page=34" target="_blank">§ 44 Absatz 1 GGO</a>). Diese umfassen sowohl die beabsichtigten Wirkungen als auch die unbeabsichtigten Nebenwirkungen (<a href="{{baseUrl}}/arbeitshilfen/download/34#page=34" target="_blank">§ 44 Absatz 1 Satz 2 GGO</a>).</p><p>Bei der Prüfung der Gesetzesfolgen unterstützt Sie die Anwendung <a href="#/egfa" target="_blank">Gesetzesfolgenabschätzung</a>.</p>',
            G_ERGAENZUNG_DER_VORHABENPLANUNG_DER_BREG:
              'Die Ergänzung der Vorhabenplanung der Bundesregierung sollte bis spätestens montags vier Wochen vor der geplanten Kabinettbefassung erfolgen. Gegebenenfalls erfolgt in diesem Schritt auch die Ergänzung des ELVER/IntraplanB.',
            G_ERSTELLUNG_DES_REFERENTENENTWURFS:
              'Liegen ausreichend fundierte Informationen zum Regelungsvorhaben vor, ist der erste Entwurf auszuarbeiten. Der Referentenentwurf besteht aus dem Vorblatt, dem Gesetzentwurf und der Begründung. Zu beachten sind <a href="{{baseUrl}}/arbeitshilfen/download/34#page=30" target="_blank">§§ 42</a>, <a href="{{baseUrl}}/arbeitshilfen/download/34#page=33" target="_blank">43</a>, <a href="{{baseUrl}}/arbeitshilfen/download/34#page=34" target="_blank">44 GGO</a> sowie einschlägige Hausanordnungen.',
            G_HAUSABSTIMMUNG:
              'Im Rahmen der Hausabstimmung sind alle Arbeitseinheiten eines Hauses rechtzeitig zu beteiligen, deren Zuständigkeiten berührt sind (<a href="{{baseUrl}}/arbeitshilfen/download/34#page=14" target="_blank">§ 15 GGO</a>). Einschlägige Hausanordnungen sind zu beachten. Gemäß <a href="https://www.verwaltung-innovativ.de/DE/Gesetzgebung/Projekt_eGesetzgebung/Handbuecher_Arbeitshilfen_Leitfaeden/Hb_vorbereitung_rechts_u_verwaltungsvorschriften/hb_vorbereitung_rechtsvorschriften_node.html" target="_blank">HVRV</a> nimmt die Hausabstimmung in der Regel zwei bis vier Wochen in Anspruch.',
            G_BETEILIGUNG_ORGANISATORISCH_UND_FACHLICH_BETROFFENER_REFERATE:
              'Eine Hausabstimmung können Sie mit der Anwendung <a href="#/hra" target="_blank">Abstimmung</a> anlegen. Sofern vorhanden, sind beim Anlegen der Hausabstimmung einschlägige Hausanordnungen zu beachten. Nach <a href="{{baseUrl}}/arbeitshilfen/download/34#page=14" target="_blank">§ 15 Absatz 1 GGO</a> sind alle Arbeitseinheiten eines Hauses rechtzeitig zu beteiligen, deren Zuständigkeiten durch das Vorhaben berührt sind.',
            G_EROERTERUNG_UND_UMSETZUNG_DER_AENDERUNGSWUENSCHE_1:
              'Planen Sie ausreichend Zeit zur Erörterung und Umsetzung der Änderungswünsche aus der Hausabstimmung ein.',
            G_BEFASSUNG_HAUSLEITUNG:
              'Die Befassung der Hausleitung muss mindestens 1 Woche vor der Ressortabstimmung eingeplant werden.  Nach der Billigung des hausabgestimmten Entwurfes durch die Hausleitung erfolgt unter Umständen eine Vorabunterrichtung der Koalitionsfraktionen.',
            G_VERSAND_DES_HAUSENTWURFS_AN_HAUSLEITUNG:
              'Nachdem die betroffenen Referate einbezogen wurden ist der hausabgestimmte Entwurf an die Hausleitung zu übermitteln. Mit diesem Schritt wird die Billigung des Entwurfs und die Zustimmung zur Einleitung der Ressortabstimmung bei  der Hausleitung angefordert. Der Dienstweg ist zu beachten. Das Kabinettreferat ist im Rahmen des Dienstwegs gegebenenfalls einzubeziehen.',
            G_GGF_VORABUNTERRICHTUNG_DER_KOALITIONSFRAKTIONEN:
              'Nach der Billigung durch die Hausleitung ist den Koalitionsfraktionen gegebenenfalls Gelegenheit zur Stellungnahme zu geben.',
            G_RESSORTABSTIMMUNG_UND_WEITERE_BETEILIGUNGEN:
              'Aus <a href="{{baseUrl}}/arbeitshilfen/download/34#page=39" target="_blank">§ 50 GGO</a> ergibt sich eine regelmäßige Dauer der Ressortabstimmung von 4 Wochen. Eine Verkürzung auf unter 4 Wochen bzw. eine Verlängerung auf maximal 8 Wochen ist auf Antrag möglich.',
            G_RECHTZEITIGE_BETEILIGUNG_DER_BETROFFENEN_INSTITUTIONEN:
              'Der von der Hausleitung des federführenden Ressorts gebilligte Regelungsentwurf ist an an alle Ressorts, den Normenkontrollrat, die Bundesbeauftragte oder den Bundesbeauftragten für Wirtschaftlichkeit in der Verwaltung, das Bundeskanzleramt sowie, wenn deren Aufgaben berührt werden, die Beauftragten der Bundesregierung, die Bundesbeauftragten sowie die Koordinatorinnen und Koordinatoren der Bundesregierung zu übermitteln.',
            G_BEACHTUNG_DER_VORHABENPLANUNG_DER_BUNDESREGIERUNG:
              'Ggf. muss die Vorhabenplanung der Bundesregierung aktualisiert werden. Hierzu gehört auch das Nachholen der Aktualisierung von ELVER bzw. IntraplanB.',
            G_BETEILIGUNG_DER_LAENDER_DER_KOMMUNALEN_SPITZENVERBAENDE_DER_FACHKREISE:
              'Gemäß <a href="{{baseUrl}}/arbeitshilfen/download/34#page=37" target="_blank">§ 47 GGO</a> sind die Länder, kommunale Spitzenverbände, Verbände sowie Fachkreise zu beteiligen.',
            G_KENNTNISGABE_DES_ENTWURFS_AN_DIE_GESCHAEFTSSTELLEN_DER_FRAKTIONEN_DES_DEUTSCHEN_BUNDESTAGES_UND_DES_BUNDESRATES:
              'Gemäß <a href="{{baseUrl}}/arbeitshilfen/download/34#page=38" target="_blank">§ 48 Absatz 2 GGO</a> sollte die Kenntnisgabe zeitgleich zur Länder- und Verbändebeteiligung stattfinden. Empfängerinnen und Empfänger der Kenntnisgabe sind die Geschäftsstellen der Fraktionen.',
            G_EROERTERUNG_UND_UMSETZUNG_DER_AENDERUNGSWUENSCHE_2:
              'Planen Sie ausreichend Zeit zur Erörterung und Umsetzung der Änderungswünsche aus der Ressortabstimmung ein.',
            G_KABINETTVORLAGE:
              'Die Beschlüsse der Bundesregierung werden durch schriftliche Kabinettvorlagen vorbereitet. <a href="{{baseUrl}}/arbeitshilfen/download/34#page=18" target="_blank">§§ 22</a>, <a href="{{baseUrl}}/arbeitshilfen/download/34#page=19" target="_blank">23</a>, <a href="{{baseUrl}}/arbeitshilfen/download/34#page=39" target="_blank">51 GGO</a> sind zu beachten.',
            G_VORBEREITUNG_DER_KABINETTVORLAGE:
              'Im Rahmen der Vorbereitung der Kabinettvorlage sind  <a href="{{baseUrl}}/arbeitshilfen/download/34#page=18" target="_blank">§§ 22</a> und <a href="{{baseUrl}}/arbeitshilfen/download/34#page=39" target="_blank">51 GGO</a> zu beachten.',
            G_VORLAGE_DER_NOTWENDIGEN_DOKUMENTE_AUF_DIENSTWEG_AN_HAUSLEITUNG:
              'Die Kabinettvorlage ist spätestens freitags, 10 Tage vor der geplanten Kabinetteinbringung an die Hausleitung zu übermitteln. In begründeten Fällen ist auch eine Einreichung bis freitags, 12 Uhr, vor der Kabinettsitzung möglich.',
            G_UEBERSENDUNG_DER_KABINETTVORLAGE_AN_DEN_CHEFBK:
              'Die Übersendung von Kabinettsvorlagen hat so zeitig zu erfolgen, dass für eine sachliche Prüfung vor der Beratung noch ausreichend Zeit bleibt. Zwischen der Zustellung der Vorlage an die Staatssekretärin oder den Staatssekretär des Bundeskanzleramtes und die Bundesministerinnen und Bundesminister und der Beratung soll mindestens eine Woche liegen (§ 21 Absatz 3 <a href="https://www.bundesregierung.de/breg-de/themen/geschaeftsordnung-der-bundesregierung-459846" target="_blank">GO BReg</a>).',
            G_KABINETTBESCHLUSS:
              'Der Kabinettbeschluss wird während einer Kabinettsitzung vorgenommen; diese findet mittwochs statt.',
            G_ZULEITUNG_AN_BUNDESRAT_DURCH_BKAMT:
              'Vorlagen der Bundesregierung sind zunächst dem Bundesrat zuzuleiten. Der Bundesrat ist berechtigt, innerhalb von sechs Wochen Stellung zu nehmen. Der Bundesrat hat die Möglichkeit, eine Fristverlängerung auf neun Wochen zu verlangen. Wird ein Vorhaben durch die Bundesregierung ausnahmsweise als eilbedürftig bezeichnet, kann die Bundesregierung die Vorlage nach drei bzw. sechs Wochen - letzteres in Fällen der Fristverlängerung - dem Bundestag zuleiten und muss die Stellungnahme des Bundesrates unverzüglich nachreichen (<a href="https://www.gesetze-im-internet.de/gg/art_76.html" target="_blank">Artikel 76 Absatz 2 Satz 4 GG</a>). Bei Vorlagen zur Änderung des Grundgesetzes oder zur Übertragung von Hoheitsrechten ist eine Bezeichnung als besonders eilbedürftig ausgeschlossen (<a href="https://www.gesetze-im-internet.de/gg/art_76.html" target="_blank">Artikel 76 Absatz 2 Satz 5 GG</a>).',
            G_ERSTE_BEFASSUNG_BUNDESRAT:
              '<p>Der Bundesrat ist berechtigt, gemäß <a href="https://www.gesetze-im-internet.de/gg/art_76.html" target="_blank">Artikel 76 Absatz 2 Satz 2 GG</a> innerhalb von sechs Wochen Stellung zu nehmen. Bei Fristverlängerung, GG-Änderung oder Übertragung von Hoheitsrechten beträgt die Frist neun Wochen, <a href="https://www.gesetze-im-internet.de/gg/art_76.html" target="_blank">Artikel 76 Absatz 2 Satz 3 und 5 GG</a>. Bei Änderungsvorlagen zum Haushalt beträgt die Frist gemäß <a href="https://www.gesetze-im-internet.de/gg/art_110.html" target="_blank">Artikel 110 Absatz 3 GG</a> drei Wochen. Gesetzesvorlagen zum Haushalt werden gleichzeitig dem Bundesrat zugestellt und in den Bundestag eingebracht.</p>' +
              '<p>Stimmt der Bundesrat einer fristverkürzten Beratung zu, ist eine Dauer von weniger als sechs bzw. neun oder drei Wochen möglich.</p>',
            G_ANNAHME_UND_VERTEILUNG_GESETZENTWURF_BUNDESREGIERUNG:
              'Der erste Schritt für die Einleitung der Beratungen ist die Verteilung der Vorlage an die Fachausschüsse (<a href="https://www.bundesrat.de/DE/aufgaben/recht/go/go-node.html#doc4353624bodyText4" target="_blank">§ 36 GO BR</a>). Dabei wird festgelegt, welcher Ausschuss die Federführung hat (<a href="https://www.bundesrat.de/DE/aufgaben/recht/go/go-node.html#doc4353624bodyText4" target="_blank">§ 36 Absatz 1 Satz 1 GO BR</a>).',
            G_AUSSCHUSSVERFAHREN_1: `Die beteiligten Ausschüsse beraten die Vorlagen parallel. Der Federführer fasst am Ende alle Empfehlungen in einer Empfehlungsdrucksache zusammen (<a target="_blank" href="https://www.bundesrat.de/DE/aufgaben/recht/go/go-node.html">§ 45 GO BR</a>). <br /><br />
              Hinweis: Die Vorlage wird grundsätzlich in der dritten Woche vor dem Plenum in den Ausschüssen beraten (= Ausschusswoche).`,
            G_BERATUNG_IM_PLENUM_1:
              'Im Plenum wird über die Vorlage beraten und über die Empfehlungsdrucksache abgestimmt (siehe hierzu auch <a target="_blank" href="https://www.bundesrat.de/DE/aufgaben/recht/go/go-node.html#:~:text=%C2%A7%2030%20%2D%20Abstimmungsregeln" >§ 30 Absatz 1 GO BR</a>). Die Plenarsitzung findet grundsätzlich freitags statt.',
            G_ZULEITUNG_AN_BKAMT: `Die Beschlüsse des Bundesrates (Stellungnahmen im ersten Durchgang) werden in der Regel noch am Plenartag (spätestens aber am darauffolgenden Montag) über das Planungs- und Kabinettmanagement-Programm (PKP) an das Bundeskanzleramt übermittelt. <br /><br />
               Darüber hinaus werden diese Beschlüsse notifiziert, d. h. mit Schreiben der Präsidentin bzw. des Präsidenten des Bundesrates in Papierform an das Bundeskanzleramt übersandt. Die Notifizierung erfolgt in der Regel am Montag nach der Plenarsitzung.`,
            G_GEGENAEUSSERUNG_DER_BREG_ZUR_STELLUNGNAHME_DES_BUNDESRATES:
              'Wenn der Bundesrat eine Stellungnahme zum Gesetzentwurf der Bundesregierung verfasst hat, kann die Bundesregierung eine Gegenäußerung zu dieser Stellungnahme verfassen. Die Gegenäußerung der Bundesregierung zur Stellungnahme des Bundesrates ist dem Bundeskanzleramt als Kabinettvorlage zu übermitteln (<a href="{{baseUrl}}/arbeitshilfen/download/34#page=40" target="_blank">§ 53 Absatz 1 GGO</a>).',
            G_ZULEITUNG_AN_BUNDESTAG:
              'Nach der Befassung durch den Bundesrat wird der Gesetzentwurf, die Stellungnahme des Bundesrates sowie die Gegenäußerung der Bundesregierung dem Bundestag zugeleitet. Im Falle von Eilbedürftigkeit kann dies drei Wochen nach Zuleitung an den Bundesrat geschehen. Die Eilbedürftigkeit muss im Kabinett beschlossen werden. Die Stellungnahme des Bundesrates muss in diesem Fall umgehend nachgereicht werden (<a href="https://www.gesetze-im-internet.de/gg/art_76.html" target="_blank">Artikel 76 Absatz 2 Satz 4 GG</a>). Im Rahmen der Zuleitung an den Bundestag sind die Mindestfristen für die Verteilung der Bundestagsdrucksachen nach der GO-BT zu beachten.',
            G_BEFASSUNG_BUNDESTAG:
              'Der Sitzungskalender des Bundestages ist zwingend zu beachten. Der Bundestag befasst sich in drei Lesungen mit dem Gesetzentwurf. Die Befassung im Bundestag nimmt in der Regel mindestens drei Wochen in Anspruch. Zwischen den Lesungen findet die Arbeit in den Fraktionen und in unterschiedlichen Ausschüssen statt.',
            G_ERSTE_LESUNG:
              'Die erste Lesung im Plenum beginnt gemäß <a href="https://www.bundestag.de/parlament/aufgaben/rechtsgrundlagen/go_btg/go08-245176" target="_blank">§ 78 Absatz 5 GO-BT</a> frühestens am dritten Tage nach der Verteilung der Bundestagsdrucksache (<a href="https://www.bundestag.de/parlament/aufgaben/rechtsgrundlagen/go_btg/go11-245172" target="_blank">§ 123 GO-BT</a>), soweit die GO-BT nichts anderes vorschreibt oder zulässt. Ein Abweichen von dieser Frist kann gemäß <a href="https://www.bundestag.de/parlament/aufgaben/rechtsgrundlagen/go_btg/go12-245174" target="_blank">§ 126 GO-BT</a> mit Zweidrittelmehrheit der anwesenden Mitglieder des Bundestages beschlossen werden, was in der Praxis regelmäßig das Einvernehmen der Fraktionen voraussetzt. Praktisch wird statt eines Einzelbeschlusses überwiegend von der Frist abgesehen durch einen im Ältestenrat bei Aufstellung der Tagesordnung einvernehmlich erklärten Fristverzicht.',
            G_UEBERWEISUNG_AN_AUSSCHUSS:
              'Die erste Lesung endet mit der Überweisung des Gesetzentwurfs an einen federführenden Ausschuss und ggf. weitere Ausschüsse zur Mitberatung (<a href="https://www.bundestag.de/parlament/aufgaben/rechtsgrundlagen/go_btg/go08-245176" target="_blank">§ 80 Absatz 1 GO-BT</a>)',
            G_BEFASSUNG_IM_AUSSCHUSS:
              'Der Sitzungskalender der Ausschüsse ist zwingend zu beachten. Die Befassung findet meist mittwochs statt. Die GO-BT verpflichtet die Ausschüsse zur baldigen Erledigung ihrer Aufgaben (<a href="https://www.bundestag.de/parlament/aufgaben/rechtsgrundlagen/go_btg/go07-245166" target="_blank">§ 62 Absatz 1 Satz 1 GO-BT</a>).',
            G_GGF_ANHOERUNGEN:
              'In öffentlichen Anhörungen können die Ausschüsse Expertinnen und Experten einladen und befragen; bei überwiesenen Vorlagen ist der federführende Ausschuss auf Verlangen eines Viertels seiner Mitglieder zur Anhörung verpflichtet (<a href="https://www.bundestag.de/parlament/aufgaben/rechtsgrundlagen/go_btg/go07-245166" target="_blank">§70 Absatz 1 Satz 2 erster Halbsatz GO-BT</a>).',
            G_GGF_ENTWURF_VON_FORMULIERUNGSVORSCHLAEGEN_BEI_AENDERUNGSWUENSCHEN:
              'Formulierungshilfen für Änderungsanträge werden in der Regel in den Ressorts verfasst, im Kabinett abgestimmt und beschlossen und anschließend den Koalitionsfraktionen übermittelt, die sodann die Möglichkeit haben die Formulierungshilfen als Änderungsanträge in den Ausschuss einzubringen. Die Einbringung von Formulierungshilfen kann die Phase der Befassung des Bundestages verlängern.',
            G_ABSCHLUSS_DER_AUSSCHUSSBEFASSUNG_DURCH_BESCHLUSSEMPFEHLUNGEN:
              'Nach Abschluss der Willensbildung in den Fraktionen (in der Regel ist dafür eine Fraktionssitzung erforderlich) endet die Befassung in den Ausschüssen mit Anfertigung einer Beschlussempfehlung. Diese ist regelmäßig mit dem Bericht des federführenden Ausschusses verbunden (<a href="https://www.bundestag.de/parlament/aufgaben/rechtsgrundlagen/go_btg/go07-245166" target="_blank">§§ 63 Absatz 1, 66 GO-BT</a>). Die Beschlussempfehlung wird vom federführenden Ausschuss an die Annahmestelle des Parlamentssekretariats übermittelt und dort als Bundestagsdrucksache aufbereitet.',
            G_ZWEITE_LESUNG:
              'Die zweite Lesung des Gesetzentwurfs in der vom Ausschuss vorgelegten Fassung im Plenum beginnt gemäß <a href="https://www.bundestag.de/parlament/aufgaben/rechtsgrundlagen/go_btg/go08-245176" target="_blank">§ 81 Absatz 1 Satz 2 GO-BT</a> frühestens am zweiten Tage nach Verteilung der Beschlussempfehlung und des Ausschussberichts, früher nur, wenn auf Antrag einer Fraktion oder fünf von Hundert der Mitglieder des Bundestages zwei Drittel der anwesenden Mitglieder des Bundestages es beschließen, was in der Praxis regelmäßig das Einvernehmen der Fraktionen voraussetzt; bei Gesetzentwürfen der Bundesregierung, die für dringlich erklärt worden sind (<a href="https://www.gesetze-im-internet.de/gg/art_81.html" target="_blank">Artikel 81 GG</a>), kann die Fristverkürzung mit der Mehrheit der Mitglieder des Bundestages beschlossen werden. Alle Abgeordneten können Änderungsanträge stellen (<a href="https://www.bundestag.de/parlament/aufgaben/rechtsgrundlagen/go_btg/go08-245176" target="_blank">§ 82 Absatz 1 Satz 2 erster Halbsatz GOBT</a>).',
            G_DRITTE_LESUNG:
              'Wird der Gesetzentwurf in der zweiten Lesung unverändert angenommen, folgen die dritte Lesung und die Schlussabstimmung im Plenum regelmäßig ohne Debatte unmittelbar anschließend (<a href="https://www.bundestag.de/parlament/aufgaben/rechtsgrundlagen/go_btg/go08-245176" target="_blank">§ 84 Satz 1 Buchstabe a GO-BT</a>). Ansonsten erfolgen sie am zweiten Tage nach Verteilung der Bundestagsdrucksache mit den in zweiter Lesung beschlossenen Änderungen, früher nur, wenn auf Antrag einer Fraktion oder von fünf von Hundert der Mitglieder des Bundestages zwei Drittel der anwesenden Mitglieder des Bundestages es beschließen, was in der Praxis regelmäßig das Einvernehmen der Fraktionen voraussetzt; bei Gesetzentwürfen der Bundesregierung, die für dringlich erklärt worden sind (<a href="https://www.gesetze-im-internet.de/gg/art_81.html" target="_blank">Artikel 81 GG</a>), kann die Fristverkürzung mit der Mehrheit der Mitglieder des Bundestages beschlossen werden (<a href="https://www.bundestag.de/parlament/aufgaben/rechtsgrundlagen/go_btg/go08-245176" target="_blank">§ 84 Satz 1 Buchstabe b GO-BT</a>).',
            G_WEITERLEITUNG_AN_BUNDESRAT:
              'Nach der dritten Lesung und der Schlussabstimmung wird das Gesetz an den Bundesrat zur Beratung weitergeleitet. Die Zuleitung an den Bundesrat hat gemäß <a href="https://www.gesetze-im-internet.de/gg/art_77.html" target="_blank">Artikel 77 Absatz 1 Satz</a> 2 GG unverzüglich zu erfolgen, regelmäßig erfolgt sie drei Wochen vor der Plenarsitzung im Bundesrat, um ihm ausreichend Beratungszeit einzuräumen.',
            G_ZWEITE_BEFASSUNG_BUNDESRAT:
              'Der Bundesrat hat grundsätzlich drei Wochen Zeit zur Beratung des Gesetzesbeschlusses des Bundestages. Stimmt der Bundesrat einer fristverkürzten Beratung zu, ist eine Dauer von weniger als drei Wochen möglich. Bei einem Zustimmungsgesetz kann der Bundesrat dem Gesetz zustimmen, nicht zustimmen oder den Vermittlungsausschuss (VA) anrufen, vgl. <a target="_blank" href="https://www.gesetze-im-internet.de/gg/art_77.html" >Artikel 77 Absatz 2</a> und <a href="https://www.gesetze-im-internet.de/gg/art_78.html" target="_blank">Artikel 78 GG</a>. Bei einem Einspruchsgesetz kann der Bundesrat entweder den VA anrufen oder den VA nicht anrufen, vgl. <a target="_blank" href="https://www.gesetze-im-internet.de/gg/art_77.html" >Artikel 77 Absatz 2</a> und <a href="https://www.gesetze-im-internet.de/gg/art_78.html" target="_blank">Artikel 78 GG</a>.',
            G_ANNAHME_UND_INTERNE_VERTEILUNG_GESETZBESCHLUSS:
              'Im so genannten zweiten Durchgang wird grundsätzlich nur an den federführenden Ausschuss zugewiesen. In bestimmten Fällen, z. B. bei Fraktionsinitiativen oder bei schwerwiegenden Änderungen im Verhältnis zum ersten Durchgang, können sich weitere Ausschüsse an der Beratung beteiligen.',
            G_AUSSCHUSSVERFAHREN_2: `Die beteiligten Ausschüsse beraten die Vorlagen parallel. Der Federführer fasst am Ende alle Empfehlungen in einer Empfehlungsdrucksache zusammen (<a target="_blank" href="https://www.bundesrat.de/DE/aufgaben/recht/go/go-node.html">§ 45 GO BR</a>). <br /><br />
               Hinweis: Die Vorlage wird grundsätzlich in der dritten Woche vor dem Plenum in den Ausschüssen beraten (= Ausschusswoche).`,
            G_BERATUNG_IM_PLENUM_2:
              'Im Plenum wird über die Vorlage beraten und über die Empfehlungsdrucksache abgestimmt (siehe hierzu auch <a target="_blank" href="https://www.bundesrat.de/DE/aufgaben/recht/go/go-node.html#:~:text=%C2%A7%2030%20%2D%20Abstimmungsregeln">§ 30 Absatz 1 GO BR</a>). Die Plenarsitzung findet grundsätzlich freitags statt.',
            G_VERTEILUNG: `Die Beschlüsse des Bundesrates werden noch am Plenartag (spätestens aber am darauffolgenden Montag) über das Planungs- und Kabinettmanagement-Programm (PKP) an das Bundeskanzleramt übermittelt. <br /><br />
              Darüber hinaus werden diese Beschlüsse notifiziert, d.h. mit Schreiben der Präsidentin bzw. des Präsidenten des Bundesrates in Papierform an den Bundestag und das Bundeskanzleramt übersandt. Die Notifizierung erfolgt in der Regel am Montag nach der Plenarsitzung.`,
            G_ERSTELLUNG_DER_URSCHRIFT: 'Diese Phase nimmt ungefähr 2 Wochen in Anspruch.',
            G_VERANLASSUNG_DER_URSCHRIFT_DES_GESETZES_BEI_DER_SCHRIFTLEITUNG_DES_BUNDESGESETZBLATTES:
              'Das federführende Ressort veranlasst bei der Schriftleitung des Bundesgesetzblattes die Herstellung der Urschrift, sobald es vom Bundeskanzleramt über das Zustandekommen des Gesetzes unterrichtet wurde (<a href="{{baseUrl}}/arbeitshilfen/download/34#page=44" target="_blank">§ 58 GGO</a> sowie einschlägige Hausanordnungen).',
            G_GGF_BEREINIGUNG_VON_DRUCKFEHLERN_ODER_OFFENSICHTLICHEN_UNRICHTIGKEITEN_IM_BERICHTIGUNGSVERFAHREN:
              'Nach Verabschiedung ist zur formlosen Berichtigung von Druckfehlern und offenbaren Unrichtigkeiten die Einwilligung der Präsidentin oder des Präsidenten des Deutschen Bundestages und der Präsidentin oder des Präsidenten des Bundesrates einzuholen. Das Bundeskanzleramt ist über die Einleitung des Berichtigungsverfahrens zu informieren (<a href="{{baseUrl}}/arbeitshilfen/download/34#page=47" target="_blank">§ 61 Abs. 2 GGO</a>).',
            G_GEGENZEICHNUNG_DURCH_BK_UND_MITGLIEDER_DER_BREG:
              'Das federführende Ressort veranlasst, dass die Urschrift (Artikel 82 Absatz 1 Satz 1 <a href="https://www.gesetze-im-internet.de/gg/BJNR000010949.html" target="_blank">GG</a> in Verbindung mit Artikel 58 Satz 1 <a href="https://www.gesetze-im-internet.de/gg/BJNR000010949.html" target="_blank">GG</a>) von den zuständigen Mitgliedern der Bundesregierung gegengezeichnet wird (§ 58 Absatz 3 Satz 1, 2 sowie Absatz 5 und 6 GGO in Verbindung mit § 14 <a href="https://www.bundesregierung.de/breg-de/themen/geschaeftsordnung-der-bundesregierung-459846" target="_blank">GO BReg</a>). Die Gegenzeichnung der Bundesregierung erfolgt durch die federführende Ministerin oder den federführenden Minister, die Bundeskanzlerin oder den Bundeskanzler und die Ministerinnen und Minister der Ressorts, deren Belange wesentlich betroffen sind. ',
            G_AUSFERTIGUNG_DURCH_BPR:
              'Das Bundeskanzleramt leitet die Urschrift nach Gegenzeichnung durch die Bundeskanzerlin oder den Bundeskanzler an das Bundespräsidialamt weiter. Die Bundespräsidentin oder der Bundespräsident bestätigt die Verfassungskonformität des Gesetzes, indem sie oder er die Urschrift unterzeichnet (<a href="{{baseUrl}}/arbeitshilfen/download/34#page=46" target="_blank">§ 59 GGO</a>).',
            G_VERKUENDUNG:
              'Die Verkündung des ausgefertigten Gesetzes erfolgt im Bundesgesetzblatt (<a href="{{baseUrl}}/arbeitshilfen/download/34#page=46" target="_blank">§ 60 GGO</a>, Artikel 82 Absatz 1 <a href="https://www.gesetze-im-internet.de/gg/BJNR000010949.html" target="_blank">GG</a>).',
            G_INKRAFTTRETEN_DES_GESETZES:
              'Gemäß <a href="https://www.gesetze-im-internet.de/gg/BJNR000010949.html#BJNR000010949BJNE010701377" target="_blank">Artikel 82 Absatz 2 GG</a> tritt das Gesetz 14 Tage nach Verkündung im Bundesgesetzblatt in Kraft, wenn kein anderes Datum im Gesetzestext selbst festgelegt wurde.',

            RV_ZULEITUNG_AN_BKAMT:
              '<p>Die Beschlüsse des Bundesrates werden noch am Plenartag (spätestens aber am darauffolgenden Montag) über das Planungs- und Kabinettmanagement-Programm (PKP) an das Bundeskanzleramt übermittelt.</p>' +
              '<p>Darüber hinaus werden diese Beschlüsse notifiziert, d.h. mit Schreiben der Präsidentin bzw. des Präsidenten des Bundesrates in Papierform an den Bundestag und das Bundeskanzleramt übersandt. Die Notifizierung erfolgt in der Regel am Montag nach der Plenarsitzung.</p>',
            RV_ANNAHME_UND_ZUWEISUNG:
              'Der erste Schritt für die Einleitung der Beratungen ist die Verteilung der Vorlage an die Fachausschüsse (<a target="_blank" href="https://www.bundesrat.de/DE/aufgaben/recht/go/go-node.html#doc4353624bodyText4">§ 36 GO BR</a>). Dabei wird festgelegt, welcher Ausschuss die Federführung hat (<a target="_blank" href="https://www.bundesrat.de/DE/aufgaben/recht/go/go-node.html#doc4353624bodyText4">§ 36 Absatz 1 Satz 1 GO BR</a>).',
            RV_BERATUNG_IM_PLENUM:
              'Im Plenum wird über die Vorlage beraten und über die Empfehlungsdrucksache abgestimmt (siehe hierzu auch <a target="_blank" href="https://www.bundesrat.de/DE/aufgaben/recht/go/go-node.html#:~:text=%C2%A7%2030%20%2D%20Abstimmungsregeln">§ 30 Absatz 1 GO BR</a>). Die Plenarsitzung findet grundsätzlich freitags statt.',
            RV_PRUEFUNG_DER_KABINETTVORLAGE_DURCH_BKAMT_UND_ANDERE_RESSORT:
              'Zwischen der Zustellung der Vorlage an die Staatssekretärin oder den Staatssekretär des Bundeskanzleramtes und die Bundesministerinnen und Bundesminister und der Beratung soll mindestens eine Woche liegen (§ 21 Absatz 3 <a target="_blank" href="https://www.bundesregierung.de/breg-de/service/archiv/alt-inhalte/geschaeftsordnung-der-bundesregierung-459846">GO BReg</a>).',
            RV_VORPHASE:
              'Die Vorphase umfasst neben ersten konzeptionellen Überlegungen, die Ergänzung der Vorhabenplanung sowie die Erstellung des Referentenentwurfs. Die Dauer der Vorphase ist abhängig vom Gegenstand des Regelungsvorhabens.',
            RV_KONZEPTIONELLE_UEBERLEGUNGEN:
              'Im Rahmen der konzeptionellen Überlegungen gilt es den Anlass des Tätigwerdens, die politischen, inhaltlichen und zeitlichen Rahmenbedingungen sowie Regelungsalternativen und Gesetzesfolgen zu prüfen.',
            RV_PRUEFUNG_DES_ANLASSES_DES_TAETIGWERDENS:
              'Anlässe können z.B. gesetzliche Verpflichtungen, gerichtliche Entscheidungen, (Teil-)Nichtigkeit einer bestehenden Rechtsverordnung, eine Weisung der Hausleitung, die Umsetzung des Koalitionsvertrages oder ein ressortabgestimmtes Eckpunktepapier sein. ',
            RV_PRUEFUNG_DER_POLITISCHEN_INHALTLICHEN_UND_ZEITLICHEN_RAHMENBEDINGUNGEN:
              'Die politischen, inhaltlichen und zeitlichen Rahmenbedingungen sind vor Beginn der Arbeiten am Regelungsentwurf zu klären. Sie variieren je nach Gegenstand des Regelungsvorhabens.',
            RV_PRUEFUNG_VON_FOLGEN_INKL_ERFUELLUNGSAUFWAND:
              'Die Ressorts sind verpflichtet die voraussichtlichen Folgen einer Regelung darzustellen (<a href="{{baseUrl}}/arbeitshilfen/download/34#page=33" target="_blank">§ 43 Absatz 1 Nr. 5</a> und <a href="{{baseUrl}}/arbeitshilfen/download/34#page=34" target="_blank">§ 44 Absatz 1 GGO</a>). Diese umfassen sowohl die beabsichtigten Wirkungen als auch die unbeabsichtigten Nebenwirkungen (<a href="{{baseUrl}}/arbeitshilfen/download/34#page=34" target="_blank">§ 44 Absatz 1 Satz 2 GGO</a>).',
            RV_ERSTELLUNG_DES_REFERENTENENTWURFS:
              'Für Entwürfe von Rechtsverordnungen gelten die Bestimmungen der GGO über die Vorbereitung und Fassung der Gesetzentwürfe entsprechend (<a href="{{baseUrl}}/arbeitshilfen/download/34#page=48" target="_blank">§ 62 Absatz 2 GGO</a>). Anforderungen an die Form und den Inhalt einer Rechtsverordnung sind dem Handbuch der Rechtsförmlichkeit Teil E (ab Rn. 761) zu entnehmen. ',
            RV_HAUSABSTIMMUNG:
              'Im Rahmen der Hausabstimmung sind alle Arbeitseinheiten eines Hauses rechtzeitig zu beteiligen, deren Zuständigkeiten berührt sind (<a href="{{baseUrl}}/arbeitshilfen/download/34#page=14" target="_blank">§ 15 GGO</a>). Einschlägige Hausanordnungen sind zu beachten. Gemäß <a href="https://www.verwaltung-innovativ.de/DE/Gesetzgebung/Projekt_eGesetzgebung/Handbuecher_Arbeitshilfen_Leitfaeden/Hb_vorbereitung_rechts_u_verwaltungsvorschriften/hb_vorbereitung_rechtsvorschriften_node.html" target="_blank">HVRV</a> nimmt die Hausabstimmung in der Regel zwei bis vier Wochen in Anspruch.',
            RV_BETEILIGUNG_ORGANISATORISCH_UND_FACHLICH_BETROFFENER_REFERATE:
              'Eine Hausabstimmung können Sie mit der Anwendung <a href="#/hra" target="_blank">Abstimmung</a> anlegen. Sofern vorhanden, sind beim Anlegen der Hausabstimmung einschlägige Hausanordnungen zu beachten. Nach <a href="{{baseUrl}}/arbeitshilfen/download/34#page=14" target="_blank">§ 15 Absatz 1 GGO</a> sind alle Arbeitseinheiten eines Hauses rechtzeitig zu beteiligen, deren Zuständigkeiten durch das Vorhaben berührt sind.',
            RV_EROERTERUNG_UND_UMSETZUNG_DER_AENDERUNGSWUENSCHE_1:
              'Planen Sie ausreichend Zeit zur Erörterung und Umsetzung der Änderungswünsche aus der Hausabstimmung ein.',
            RV_BEFASSUNG_HAUSLEITUNG:
              'Die Befassung der Hausleitung muss mindestens 1 Woche vor der Ressortabstimmung eingeplant werden. ',
            RV_VERSAND_DES_HAUSENTWURFS_AN_HAUSLEITUNG:
              'Nachdem die betroffenen Referate einbezogen wurden, ist der hausabgestimmte Entwurf an die Hausleitung zu übermitteln. Mit diesem Schritt wird die Billigung des Entwurfs und die Zustimmung zur Einleitung der Ressortabstimmung bei  der Hausleitung angefordert. Der Dienstweg ist zu beachten. Das Kabinettreferat ist im Rahmen des Dienstwegs gegebenenfalls einzubeziehen.',
            RV_GGF_VORABUNTERRICHTUNG_DER_KOALITIONSFRAKTIONEN:
              'Nach der Billigung des hausabgestimmten Entwurfes durch die Hausleitung erfolgt unter Umständen eine Vorabunterrichtung der Koalitionsfraktionen.',
            RV_RESSORTABSTIMMUNG_UND_WEITERE_BETEILIGUNGEN:
              'Aus <a href="{{baseUrl}}/arbeitshilfen/download/34#page=39" target="_blank">§ 50 GGO</a> ergibt sich eine regelmäßige Dauer der Ressortabstimmung von 4 Wochen. Eine Verkürzung auf unter 4 Wochen bzw. eine Verlängerung auf maximal 8 Wochen ist auf Antrag möglich.',
            RV_RECHTZEITIGE_BETEILIGUNG_DER_BETROFFENEN_INSTITUTIONEN:
              'Der von der Hausleitung des federführenden Ressorts gebilligte Regelungsentwurf ist an alle Ressorts, den Normenkontrollrat, die Bundesbeauftragte oder den Bundesbeauftragten für Wirtschaftlichkeit in der Verwaltung, das Bundeskanzleramt sowie, wenn deren Aufgaben berührt werden, die Beauftragten der Bundesregierung, die Bundesbeauftragten sowie die Koordinatorinnen und Koordinatoren der Bundesregierung zu übermitteln.',
            RV_BEACHTUNG_DER_VORHABENPLANUNG_DER_BUNDESREGIERUNG:
              'Ggf. muss die Vorhabenplanung der Bundesregierung aktualisiert werden. Hierzu gehört auch das Nachholen der Aktualisierung von ELVER bzw. IntraplanB.',
            RV_BETEILIGUNG_DER_LAENDER_DER_KOMMUNALEN_SPITZENVERBAENDE_DER_FACHKREISE:
              'Gemäß <a href="{{baseUrl}}/arbeitshilfen/download/34#page=37" target="_blank">§ 47 GGO</a> sind die Länder, kommunale Spitzenverbände, Verbände sowie Fachkreise zu beteiligen.',
            RV_KENNTNISGABE_DES_ENTWURFS_AN_DIE_GESCHAEFTSSTELLEN_DER_FRAKTIONEN_DES_DEUTSCHEN_BUNDESTAGES_UND_DES_BUNDESRATES:
              'Gemäß <a href="{{baseUrl}}/arbeitshilfen/download/34#page=38" target="_blank">§ 48 Absatz 2 GGO</a> sollte die Kenntnisgabe zeitgleich zur Länder- und Verbändebeteiligung stattfinden. Empfängerinnen und Empfänger der Kenntnisgabe sind die Geschäftsstellen der Fraktionen.',
            RV_EROERTERUNG_UND_UMSETZUNG_DER_AENDERUNGSWUENSCHE_2:
              'Planen Sie ausreichend Zeit zur Erörterung und Umsetzung der Änderungswünsche aus der Ressortabstimmung ein.',
            RV_KABINETTVORLAGE:
              'Die Beschlüsse der Bundesregierung werden durch schriftliche Kabinettvorlagen vorbereitet. <a href="{{baseUrl}}/arbeitshilfen/download/34#page=18" target="_blank">§§ 22</a>, <a href="{{baseUrl}}/arbeitshilfen/download/34#page=19" target="_blank">23</a>, <a href="{{baseUrl}}/arbeitshilfen/download/34#page=39" target="_blank">51 GGO</a> sind zu beachten.',
            RV_VORBEREITUNG_DER_KABINETTVORLAGE:
              'Im Rahmen der Vorbereitung der Kabinettvorlage sind <a href="{{baseUrl}}/arbeitshilfen/download/34#page=18" target="_blank">§§ 22</a> und <a href="{{baseUrl}}/arbeitshilfen/download/34#page=39" target="_blank">51 GGO</a> zu beachten.',
            RV_VORLAGE_DER_NOTWENDIGEN_DOKUMENTE_AUF_DIENSTWEG_AN_HAUSLEITUNG:
              'Die Kabinettvorlage ist spätestens freitags, 10 Tage vor der geplanten Kabinetteinbringung an die Hausleitung zu übermitteln. In begründeten Fällen ist auch eine Einreichung bis freitags, 12 Uhr, vor der Kabinettsitzung möglich.',
            RV_UEBERSENDUNG_DER_KABINETTVORLAGE_AN_DEN_CHEFBK:
              'Die Übersendung von Kabinettsvorlagen hat so zeitig zu erfolgen, dass für eine sachliche Prüfung vor der Beratung noch ausreichend Zeit bleibt. Zwischen der Zustellung der Vorlage an die Staatssekretärin oder den Staatssekretär des Bundeskanzleramtes und die Bundesministerinnen und Bundesminister und der Beratung soll mindestens eine Woche liegen (§ 21 Absatz 3 <a href="https://www.bundesregierung.de/breg-de/themen/geschaeftsordnung-der-bundesregierung-459846" target="_blank">GO BReg</a>).',
            RV_KABINETTBESCHLUSS:
              'Der Kabinettbeschluss wird während einer Kabinettsitzung vorgenommen; diese findet mittwochs statt.',
            RV_ZULEITUNG_AN_BUNDESRAT_DURCH_BKAMT_ODER_CHEFBK:
              'Bestimmte Rechtsverordnungen benötigen die Zustimmung des Bundesrates (Artikel 80 Absatz 2 <a href="https://www.gesetze-im-internet.de/gg/BJNR000010949.html" target="_blank">GG</a>, <a href="{{baseUrl}}/arbeitshilfen/download/34#page=49" target="_blank">§ 64 GGO</a>).',
            RV_BEFASSUNG_BUNDESRAT:
              'Der Bundesrat ist für den Zustimmungs-, Ablehnungs- oder Maßgabebeschluss an keine Frist gebunden. Analog zu Gesetzentwürfen hat der Bundesrat nach ständiger Praxis auch für Rechtsverordnungen sechs Wochen Zeit für seine Beratungen. In der Praxis ist aber sowohl eine Beratungsdauer von weniger (im Falle der Zustimmung des Bundesrates zu einer fristverkürzten Behandlung) als auch von mehr als sechs Wochen (z.B. bei Vertagung) möglich.',
            RV_ANNAHME_UND_INTERNE_VERTEILUNG_ENTWURF:
              'Der erste Schritt für die Einleitung der Beratungen ist die Verteilung der Vorlage an die Fachausschüsse (§ 36 <a href="https://www.bundesregierung.de/breg-de/themen/geschaeftsordnung-der-bundesregierung-459846" target="_blank">GO BReg</a>). Dabei wird festgelegt, welcher Ausschuss die Federführung hat (§ 36 Absatz 1 Satz 1 <a href="https://www.bundesregierung.de/breg-de/themen/geschaeftsordnung-der-bundesregierung-459846" target="_blank">GO BReg</a>).',
            RV_AUSSCHUSSVERFAHREN:
              '<p>Die beteiligten Ausschüsse beraten die Vorlagen parallel. Der Federführer fasst am Ende alle Empfehlungen in einer Empfehlungsdrucksache zusammen (<a a href="https://www.bundesrat.de/DE/aufgaben/recht/go/go-node.html"  target="_blank">§ 45 GO BR</a>). </p> ' +
              '<p>Hinweis: Die Vorlage wird grundsätzlich in der dritten Woche vor dem Plenum in den Ausschüssen beraten (= Ausschusswoche).</p>',
            RV_BERATUNG_UND_BESCHLUSS_IM_PLENUM:
              'Im Plenum wird über die Stellungnahme beraten und abgestimmt. Die Plenarsitzung findet freitags statt.',
            RV_SPEZIELLE_PHASE_BEI_MASSGABEBESCHLUESSEN_DES_BUNDESRATES:
              'Hat der Bundesrat der Rechtsverordnung nach Maßgabe von Änderungen zugestimmt, ist für den weiteren Verlauf <a href="{{baseUrl}}/arbeitshilfen/download/34#page=49" target="_blank">§ 65 GGO</a> zu beachten. Die Phase dauert in der Regel zwischen sechs und 12 Wochen.',
            RV_AUSFERTIGUNG_UND_VORBEREITUNG_DER_VERKUENDUNG_DER_RECHTSVERORDNUNG:
              'Eine Rechtsverordnung ist erst auszufertigen, nachdem die ermächtigende Gesetzesbestimmung in Kraft getreten ist (<a href="{{baseUrl}}/arbeitshilfen/download/34#page=50" target="_blank">§ 66 Abs. 1 GGO</a>). Die Phase dauert in der Regel mindestens drei Wochen.',

            RV_UEBERSENDUNG_AN_DIE_SCHRIFTLEITUNG_DES_BUNDESGESETZBLATTES_ODER_DES_BUNDESANZEIGERS:
              'Wenn der Wortlaut einer Rechtsverordnung endgültig feststeht übersendet das federführende Bundesministerium der Schriftleitung des Bundesgesetzblattes oder der Schriftleitung des Bundesanzeigers den Verordnungstext (<a href="{{baseUrl}}/arbeitshilfen/download/34#page=50" target="_blank">§ 66 Abs. 2 GGO</a>).',
            RV_HERSTELLUNG_DER_URSCHRIFT: 'Die Herstellung der Urschrift nimmt in der Regel 2 Wochen in Anspruch.',
            RV_BILLIGUNGSANFRAGEN:
              '<p>Wird die Rechtsverordnung von der Bundesregierung erlassen, wird sie von der Bundeskanzlerin oder vom Bundeskanzler oder von der zur Vertretung berechtigten Person und von dem federführenden Mitglied der Bundesregierung unterzeichnet (<a href="{{baseUrl}}/arbeitshilfen/download/34#page=50" target="_blank">§ 67 Abs. 2 GGO</a>).</p><p>Wird die Rechtsverordnung von einem Bundesministerium erlassen, wird sie von dem zuständigen Mitglied der Bundesregierung unterzeichnet. Sind weitere Bundesministerien beteiligt, wird die Rechtsverordnung auch von den jeweils zuständigen Mitgliedern der Bundesregierung unterzeichnet (<a href="{{baseUrl}}/arbeitshilfen/download/34#page=50" target="_blank">§ 67 Abs. 3 GGO</a>).</p> <p>Bei Rechtsverordnungen, die das Einvernehmen mit einem oder mehreren Bundesministerien in der Eingangsformel zum Ausdruck bringen, unterbleibt die Unterzeichnung durch die jeweils zuständigen Mitglieder der Bundesregierung (<a href="{{baseUrl}}/arbeitshilfen/download/34#page=50" target="_blank">§ 67 Abs. 4 GGO</a>).</p>',
            RV_VERANLASSUNG_DER_URSCHRIFT_DES_GESETZES_BEI_DER_SCHRIFTLEITUNG_DES_BUNDESGESETZBLATTES_ODER_DES_BUNDESANZEIGERS:
              'Sobald die endgültige Fassung verabschiedet ist, also alle notwendigen Billigungen erfolgt sind, veranlasst das federführende Bundesministerium bei der Schriftleitung des Bundesgesetzblattes oder des Bundesanzeigers die Herstellung der Urschrift.',
            RV_GGF_BEREINIGUNG_VON_DRUCKFEHLERN_ODER_OFFENSICHTLICHEN_UNRICHTIGKEITEN_IM_BERICHTIGUNGSVERFAHREN:
              'Nach Verabschiedung ist zur formlosen Berichtigung von Druckfehlern und offenbaren Unrichtigkeiten die Einwilligung der Präsidentin oder des Präsidenten des Deutschen Bundestages und der Präsidentin oder des Präsidenten des Bundesrates einzuholen. Das Bundeskanzleramt ist über die Einleitung des Berichtigungsverfahrens zu informieren (<a href="{{baseUrl}}/arbeitshilfen/download/34#page=47" target="_blank">§ 61 Abs. 2 GGO</a>).',
            RV_VERKUENDUNG_DER_RECHTSVERORDNUNG:
              'Rechtsverordnungen werden entweder im Bundesgesetzblatt Teil I (§ 76 Abs. 1 2.), im Bundesanzeiger (§ 76 Abs. 3 1.) oder in anderen Amtsblättern veröffentlicht. Diese Phase nimmt ca. 2 Wochen in Anspruch.',
            RV_INKRAFTTRETEN_DER_RECHTSVERORDNUNG:
              'Gemäß Artikel 82 Absatz 2 <a href="https://www.gesetze-im-internet.de/gg/BJNR000010949.html" target="_blank">GG</a> tritt die Verordnung 14 Tage nach Verkündung im Bundesgesetzblatt in Kraft, wenn kein anderes Datum im Verordnungstext selbst festgelegt wurde.',
            RV_ZEICHNUNGEN:
              'Je nach Verordnungsermächtigung müssen einzelne oder mehrere Bundesministerinen und Bundesminister die Rechtsverordnung zeichnen.',
            RV_STANDARDFRIST_BIS_ZUM_INKRAFTTRETEN:
              'Gemäß <a href="https://www.gesetze-im-internet.de/gg/BJNR000010949.html#BJNR000010949BJNE010701377" target="_blank">Artikel 82 Absatz 2 GG</a> tritt die Verordnung 14 Tage nach Verkündung im Bundesgesetzblatt in Kraft, wenn kein anderes Datum im Verordnungstext selbst festgelegt wurde.',
            VV_ZULEITUNG_AN_BKAMT:
              '<p>Die Beschlüsse des Bundesrates werden noch am Plenartag (spätestens aber am darauffolgenden Montag) über das Planungs- und Kabinettmanagement-Programm (PKP) an das Bundeskanzleramt übermittelt.</p>' +
              '<p>Darüber hinaus werden diese Beschlüsse notifiziert, d.h. mit Schreiben der Präsidentin bzw. des Präsidenten des Bundesrates in Papierform an den Bundestag und das Bundeskanzleramt übersandt. Die Notifizierung erfolgt in der Regel am Montag nach der Plenarsitzung.</p>',
            VV_ANNAHME_ZUWEISUNG:
              'Der erste Schritt für die Einleitung der Beratungen ist die Verteilung der Vorlage an die Fachausschüsse (<a target="_blank" href="https://www.bundesrat.de/DE/aufgaben/recht/go/go-node.html#doc4353624bodyText4">§ 36 GO BR</a>). Dabei wird festgelegt, welcher Ausschuss die Federführung hat (<a href="https://www.bundesrat.de/DE/aufgaben/recht/go/go-node.html#doc4353624bodyText4" target="_blank">§ 36 Absatz 1 Satz 1 GO BR</a>).',
            VV_VORLAGE_DER_NOTWENDIGEN_DOKUMENTE_AUF_DIENSTWEG_AN_HAUSLEITUNG:
              'Gemäß <a href="{{baseUrl}}/arbeitshilfen/download/34#page=18" target="_blank">§§ 22</a>, <a href="{{baseUrl}}/arbeitshilfen/download/34#page=39" target="_blank">51 GGO</a> ist die Kabinettvorlage spätestens freitags, 10 Tage vor der geplanten Kabinetteinbringung an die Hausleitung zu übermitteln. In begründeten Fällen ist auch eine Einreichung bis freitags, 12 Uhr, vor der Kabinettsitzung möglich.',
            VV_PRUEFUNG_DER_KABINETTVORLAGE_DURCH_BKAMT_UND_ANDERE_RESSORT:
              'Zwischen der Zustellung der Vorlage an die Staatssekretärin oder den Staatssekretär des Bundeskanzleramtes und die Bundesministerinnen und Bundesminister und der Beratung soll mindestens eine Woche liegen (§ 21 Absatz 3 <a target="_blank" href="https://www.bundesregierung.de/breg-de/service/archiv/alt-inhalte/geschaeftsordnung-der-bundesregierung-459846">GO BReg</a>).',
            VV_VORPHASE:
              'Die Vorphase umfasst neben ersten konzeptionellen Überlegungen, die Notwendigkeitsprüfung sowie die Prüfung der Rahmenbedingungen.',
            VV_KONZEPTIONELLE_UEBERLEGUNGEN:
              'Beachten Sie für den Aufbau der Vorlage <a href="{{baseUrl}}/arbeitshilfen/download/34#page=52" target="_blank">§ 70 GGO</a>.',
            VV_NOTWENDIGKEITSPRUEFUNG:
              'Eine Notwendigkeitsprüfung ist gemäß § 2 <a href="https://www.verwaltung-innovativ.de/DE/Gesetzgebung/Projekt_eGesetzgebung/Handbuecher_Arbeitshilfen_Leitfaeden/Hb_vorbereitung_rechts_u_verwaltungsvorschriften/Annexe/Annex_4/Annex4_node.html" target="_blank">VwVR</a> (Verwaltungsvorschriften des Bundes) vor Erlass der Verwaltungsvorschrift durchzuführen.',
            VV_PRUEFUNG_DER_RAHMENBEDINGUNGEN:
              '<p>Es sollten unter anderem die folgenden Aspekte geprüft werden:</p><p><ul><li>erlassende Stelle</li><li>Mitwirkung Bundesrat</li><li>Folgenabschätzung (hierbei kann Sie die Anwendung <a href="#/egfa" target="_blank">Gesetzesfolgenabschätzung</a> unterstützen </li><li>Zeitrahmen</li><li>Finanzbedarf</li><li>Notwendigkeit von höherrangigem Recht/ Anforderungen des höherrangigen Rechts</li></ul></p>',
            VV_EINBINDUNG_DER_HAUSLEITUNG:
              'Die Befassung der Hausleitung muss mindestens 1 Woche vor der Ressortabstimmung eingeplant werden.  Nach der Billigung des hausabgestimmten Entwurfes durch die Hausleitung erfolgt unter Umständen eine Vorabunterrichtung der Koalitionsfraktionen.',
            VV_ENTSCHEIDUNG_DER_HAUSLEITUNG_1:
              'Die Hausleitung hat die Entscheidung zu treffen, ob das Vorhaben in Angriff genommen werden soll. Hierfür wird ihr die Grundsatzvorlage übermittelt. ',
            VV_PRUEFUNG_DER_REFERATSMATERIALIEN_UND_SONSTIGER_MATERIALIEN:
              'Die Prüfung kann unter anderem Materialien aus nachgeordneten Bereichen, Referaten des eigenen Hauses, den Ressorts des Bundes, der Wissenschaft sowie der Länder umfassen.',
            VV_ERARBEITUNG_DES_ENTWURF_DES_REFERATS:
              'Basierend auf der Sichtung des Materials wird der Entwurf des Referates ausgearbeitet. Anforderungen an Form und Inhalte ergeben sich aus §§ 3 bis 5 und § 7 <a href="https://www.verwaltung-innovativ.de/DE/Gesetzgebung/Projekt_eGesetzgebung/Handbuecher_Arbeitshilfen_Leitfaeden/Hb_vorbereitung_rechts_u_verwaltungsvorschriften/Annexe/Annex_4/Annex4_node.html" target="_blank">VwVR</a> sowie <a href="{{baseUrl}}/arbeitshilfen/download/34#page=52" target="_blank">§ 70 Absatz 1 GGO</a> (auch i.V.m. <a href="{{baseUrl}}/arbeitshilfen/download/34#page=14" target="_blank">§ 44 GGO</a>).',
            VV_HAUSABSTIMMUNG:
              'Die betroffenen Referate des Hauses sind gemäß <a href="{{baseUrl}}/arbeitshilfen/download/34#page=14" target="_blank">§ 15 GGO</a> einzubinden. Im Rahmen der Hausabstimmung sind alle Arbeitseinheiten eines Hauses rechtzeitig zu beteiligen, deren Zuständigkeiten berührt sind (<a href="{{baseUrl}}/arbeitshilfen/download/34#page=14" target="_blank">§ 15 GGO</a>).',
            VV_BETEILIGUNG_ORGANISATORISCH_UND_FACHLICH_BETROFFENER_REFERATE:
              'Die betroffenen Referate des Hauses sind gemäß <a href="{{baseUrl}}/arbeitshilfen/download/34#page=14" target="_blank">§ 15 GGO</a> einzubinden.',
            VV_PRUEFUNG_SOWIE_GGF_EROERTERUNG_UND_UMSETZUNG_DER_AENDERUNGSWUENSCHE:
              'Planen Sie ausreichend Zeit zur Erörterung und Umsetzung der Änderungswünsche aus der Hausabstimmung ein.',
            VV_ERSTELLUNG_ENTWURF_DES_HAUSES:
              'Beachten Sie bei der Erstellung des Entwurfs § 70 Absatz 1 i.V.m. <a href="{{baseUrl}}/arbeitshilfen/download/34#page=39" target="_blank">§ 49 Absatz 1 GGO</a>. Den Entwurf können Sie im Editor erstellen (Verlinkung zu Anwendung).',
            VV_RESSORTABSTIMMUNG_UND_WEITERE_BETEILIGUNGEN:
              'Aus <a href="{{baseUrl}}/arbeitshilfen/download/34#page=39" target="_blank">§ 50 GGO</a> ergibt sich eine regelmäßige Dauer der Ressortabstimmung von 4 Wochen. Eine Verkürzung auf unter 4 Wochen bzw. eine Verlängerung auf maximal 8 Wochen ist auf Antrag möglich.',
            VV_RECHTZEITIGE_BETEILIGUNG_DER_BETROFFENEN_INSTITUTIONEN:
              'Der von der Hausleitung gebilligte Regelungsentwurf ist an alle Ressorts, den Normenkontrollrat, die Bundesbeauftragte oder den Bundesbeauftragten für Wirtschaftlichkeit in der Verwaltung, das Bundeskanzleramt sowie, wenn deren Aufgaben berührt werden, die Beauftragten der Bundesregierung, die Bundesbeauftragten sowie die Koordinatorinnen und Koordinatoren der Bundesregierung zu übermitteln.',
            VV_BETEILIGUNG_DER_LAENDER_DER_KOMMUNALEN_SPITZENVERBAENDE_DER_FACHKREISE_UND_VERBAENDE:
              'Beachten Sie § 70 Absatz 1 i.V.m. <a href="{{baseUrl}}/arbeitshilfen/download/34#page=37" target="_blank">§ 47 GGO</a>.',
            VV_PRUEFUNG_GGF_EROERTERUNG_UND_UMSETZUNG_DER_AENDERUNGSWUENSCHE:
              'Planen Sie ausreichend Zeit ein für die Prüfung sowie ggf. Erörterung und Umsetzung der Änderungswünsche.',
            VV_ENTSCHEIDUNG_DER_HAUSLEITUNG_2:
              'Bevor der Entwurf der überarbeitete Entwurf des Hauses erstellt wird ist eine Entscheidung der Hausleitung erforderlich.',
            VV_ERSTELLUNG_UEBERARBEITETER_ENTWURF_DES_HAUSES:
              'Beachten Sie § 70 i.V.m. <a href="{{baseUrl}}/arbeitshilfen/download/34#page=39" target="_blank">§ 49 Absatz 1 GGO</a>.',
            VV_SCHLUSSABSTIMMUNG_MIT_DEN_RESSORTS:
              'Der Entwurf des Hauses wird zur Schlussabstimmung an die Ressorts gegeben.',
            VV_EINARBEITUNG_DER_AENDERUNGSWUENSCHE_DER_RESSORTS:
              'Planen Sie ausreichend Zeit ein für die Prüfung sowie ggf. Erörterung und Umsetzung der Änderungswünsche.',
            VV_ENTSCHEIDUNG_DER_HAUSLEITUNG:
              'Vor der Erarbeitung der Kabinettvorlage ist eine Entscheidung der Hausleitung erforderlich. Ist für den Erlass der Allgemeinen Verwaltungsvorschrift eine Zustimmung des Bundesrates nicht erforderlich, folgt der Erlass/die Bekanntmachung durch hausinterne oder allgemeine Veröffentlichung im GMBl./Bundesanzeiger (vgl. Artikel 86 Satz 1 <a href="https://www.gesetze-im-internet.de/gg/BJNR000010949.html" target="_blank">GG</a>). Ist der Bundesrat einzubinden (Artikel 84 Absatz 2, Artikel 85 Absatz 2 <a href="https://www.gesetze-im-internet.de/gg/BJNR000010949.html" target="_blank">GG</a>), folgt die Erstellung der Kabinettvorlage.',
            VV_KABINETTVORLAGE:
              'Diese Phase umfasst sowohl die hausinterne Vorbereitung der Kabinettvorlage als auch die Übersendung derselben an die oder den ChefBK.',
            VV_HAUSINTERNE_VORBEREITUNG_DER_KABINETTVORLAGE:
              'Bei der Erstellung der Kabinettvorlage sind §§ 15, 16 Absatz 4 und § 20 Absatz 2 <a href="https://www.bundesregierung.de/breg-de/themen/geschaeftsordnung-der-bundesregierung-459846" target="_blank">GO BReg</a>, <a href="{{baseUrl}}/arbeitshilfen/download/34#page=39" target="_blank">§ 51 GGO</a> i.V.m. §§ 22, <a href="{{baseUrl}}/arbeitshilfen/download/34#page=19" target="_blank">23</a> <a href="{{baseUrl}}/arbeitshilfen/download/34#page=19" target="_blank">GGO</a> zu beachten. Zur Vorlage an das Kabinett oder den Bundesrat sind Angaben über die Auswirkungen auf die öffentlichen Haushalte beizufügen, wenn und soweit sie nicht schon im Rahmen der Begründung eines Gesetzes oder einer Verordnung gemacht worden sind.',
            VV_UEBERSENDUNG_DER_KABINETTVORLAGE_AN_DEN_CHEFBK:
              'Die Übersendung von Kabinettsvorlagen hat so zeitig zu erfolgen, dass für eine sachliche Prüfung vor der Beratung noch ausreichend Zeit bleibt. Zwischen der Zustellung der Vorlage an die Staatssekretärin oder den Staatssekretär des Bundeskanzleramtes, die Bundesministerinnen und die Bundesminister und der Beratung soll mindestens eine Woche liegen (§ 21 Absatz 3 <a href="https://www.bundesregierung.de/breg-de/themen/geschaeftsordnung-der-bundesregierung-459846" target="_blank">GO BReg</a>). ',
            VV_KABINETTBESCHLUSS:
              'Der Kabinettbeschluss wird während einer Kabinettsitzung vorgenommen; diese findet mittwochs statt.',
            VV_ZULEITUNG_AN_BUNDESRAT_DURCH_BKAMT:
              'Die Beteiligung des Bundesrates ist in <a target="_blank" href="https://www.gesetze-im-internet.de/gg/art_84.html">Art. 84 Abs. 2</a> sowie in <a href="https://www.gesetze-im-internet.de/gg/art_85.html" target="_blank">Art. 85 Abs. 2 GG</a> geregelt. Für allgemeine Verwaltungsvorschriften nach <a href="https://www.gesetze-im-internet.de/gg/art_86.html" target="_blank">Art. 86 GG</a> ist keine Zustimmung des Bundesrates erforderlich. Die allgemeinen Verwaltungsvorschriften nach <a href="https://www.gesetze-im-internet.de/gg/art_108.html" target="_blank">Artikel 108 Absatz 7 GG</a> bedürfen der Zustimmung des Bundesrates, soweit sie für die Verwaltung der Landesfinanzbehörden oder der Gemeinden und Gemeindeverbände gelten.',
            VV_BEFASSUNG_BUNDESRAT:
              'Der Bundesrat ist für den Zustimmungs-, Ablehnungs- oder Maßgabebeschluss an keine Frist gebunden. Analog zu Gesetzentwürfen hat der Bundesrat nach ständiger Praxis auch für Verwaltungsvorschriften sechs Wochen Zeit für seine Beratungen. In der Praxis ist aber sowohl eine Beratungsdauer von weniger (im Falle der Zustimmung des Bundesrates zu einer fristverkürzten Behandlung) als auch von mehr als sechs Wochen (z.B. bei Vertagung) möglich.',
            VV_ZUWEISUNG_DER_VORLAGE_AN_DIE_FACHAUSSCHUESSE_DES_BUNDESRATES_UND_FESTLEGUNG_DES_FEDERFUEHRENDEN_AUSSCHUSSES:
              'Bevor die Ausschussberatungen stattfinden wird die Vorlage den Fachausschüssen zugewiesen und der federführende Ausschuss bestimmt.',
            VV_AUSSCHUSSVERFAHREN:
              '<p>Die beteiligten Ausschüsse beraten die Vorlagen parallel. Der Federführer fasst am Ende alle Empfehlungen in einer Empfehlungsdrucksache zusammen (<a href="https://www.bundesrat.de/DE/aufgaben/recht/go/go-node.html" target="_blank">§ 45 GO BR</a>). </p>' +
              '<p>Hinweis: Die Vorlage wird grundsätzlich in der dritten Woche vor dem Plenum in den Ausschüssen beraten (= Ausschusswoche).</p>',
            VV_BERATUNG_UND_BESCHLUSS_DES_BUNDESRATSPLENUMS:
              'Der Bundesrat tritt regelmäßig freitags in Abständen von drei bis vier Wochen zu einer Plenarsitzung zusammen.',
            VV_ERLASS_BEKANNTMACHUNG_DURCH_ALLGEMEINE_VEROEFFENTLICHUNG_IM_GMBL_BUNDESANZEIGER:
              '<p>Die Veröffentlichung von allgemeinen Verwaltungsvorschriften ist in <a href="{{baseUrl}}/arbeitshilfen/download/34#page=52" target="_blank">§ 71</a> i.V.m. <a href="{{baseUrl}}/arbeitshilfen/download/34#page=51" target="_blank">§ 68</a> und <a href="{{baseUrl}}/arbeitshilfen/download/34#page=56" target="_blank">§ 76</a> (siehe insbesondere § 76 Absatz 3 Nummer 2 und Absatz 4 Nummer 1) GGO geregelt.</p><p>Soweit sie hierfür geeignet sind auch sonstige Verwaltungsvorschriften bekanntzumachen, um die Transparenz der Verwaltung zu erhöhen. Die Art der Bekanntmachung richtet sich dann nach dem Kreis der Adressatinnen und Adressaten und der sonst von der Verwaltungsvorschrift Betroffenen. Verwaltungsvorschriften, die ein Verhalten vorschreiben, das Auswirkungen auf Dritte haben kann, sind in der Regel so bekanntzumachen, dass sie allgemein zugänglich sind. Regeln Verwaltungsvorschriften nur das behördeninterne Verhalten von Mitarbeitenden, so reicht eine hausinterne Sammlung aus, die den Mitarbeitenden zur Verfügung steht und deren Fortschreibung auf den jeweils neuesten Stand über die behördeninternen Informationswege erfolgt.</p>',
            VV_BERATUNG_IM_PLENUM:
              'Im Plenum wird über die Vorlage beraten und über die Empfehlungsdrucksache abgestimmt (siehe hierzu auch <a href="https://www.bundesrat.de/DE/aufgaben/recht/go/go-node.html#:~:text=%C2%A7%2030%20%2D%20Abstimmungsregeln" target="_blank">§ 30 Absatz 1 GO BR</a>). Die Plenarsitzung findet grundsätzlich freitags statt.',
          },
          eigenschaften: {
            abstand: {
              title: 'Abweichung von vorgegebener Reihenfolge.',
            },
            wichtig: {
              title: 'Dieser Termin ist als „wichtig“ markiert.',
            },
            gesperrt: {
              termin: {
                title: 'Andere dürfen diesen Termin nicht bearbeiten.',
                titleChild: 'Andere dürfen diesen Termin nicht bearbeiten, weil die übergeordnete Phase gesperrt ist.',
              },
              phase: {
                title: 'Andere dürfen diese Phase und ggf. ihre untergeordneten Phasen nicht bearbeiten.',
                titleChild: 'Andere dürfen diese Phase nicht bearbeiten, weil die übergeordnete Phase gesperrt ist.',
              },
            },
          },
          newElement: {
            placeholder: {
              input: 'Bitte ausfüllen',
              select: 'Bitte auswählen',
            },
            sections: {
              duration: {
                title: 'Dauer*',
                hint: {
                  title: 'Dauer',
                },
                error: 'Die Dauer einer Phase muss länger als 0 Tage sein.',
                nanError: 'Buchstaben sind als Eingabe nicht zulässig.',
                infoText:
                  'Die Dauer wird in Wochen zuzüglich einzelner Tage angegeben. Es ist eine tagbasierte oder kombinierte wochen- und tagbasierte Eingabe möglich. Wenn Sie Ihre Eingabe speichern, wird diese in Wochen zuzüglich einzelner Tage umgerechnet.',
                items: {
                  months: {
                    title: 'Monate',
                    error:
                      'Bitte vergeben Sie bei der Angabe der Dauer in Monaten einen ganzzahligen Wert zwischen 0 und 12.',
                  },
                  weeks: {
                    title: 'Wochen',
                    error:
                      'Bitte vergeben Sie bei der Angabe der Dauer in Wochen einen ganzzahligen Wert zwischen 0 und 52.',
                  },
                  days: {
                    title: 'Tage',
                    error:
                      'Bitte vergeben Sie bei der Angabe der Dauer in Tagen einen ganzzahligen Wert zwischen 0 und 365.',
                  },
                },
                hinweis: {
                  title: 'Bearbeitung der Dauer',
                  content: 'Planen Sie, wenn möglich, mit einer Dauer von {{min}} bis {{max}} Wochen.',
                },
                errorsList: {
                  parentDurationToShort:
                    'Die Dauer ({{phaseDuration}}) der Hauptphase ist kürzer als die Summe der Dauer ({{childrenDuration}}) aller Unterelemente. Bitte verkürzen Sie als erstes die Dauer der Unterelemente.',
                  childrenDurationTooLong:
                    'Die Gesamtdauer ({{childrenDuration}}) aller Unterelemente ist länger als die Dauer ({{phaseDuration}}) der Hauptphase. Bitte ändern Sie als erstes die Dauer der Hauptphase.',
                },
              },
            },
          },
        },
        kommentare: {
          tabNav: 'Kommentare',
        },
      },
      header: {
        addUser: 'Nutzer hinzufügen',
        btnSpeichern: 'Kopie als eigene Vorlage speichern',
        rights: {
          label: 'Rechte',
          read: 'Leserechte',
          write: 'Schreibrechte',
          revoke: 'Rechte entziehen',
          revokeRightsGeneralModal: {
            title: 'Möchten Sie {{name}} die {{rechteType}} entziehen?',
            confirm: 'Nein, nicht entziehen',
            cancel: 'Ja, entziehen',
            archiveSuccess: 'Sie haben {{name}} die {{rechteType}} entzogen.',
            radioGroup: {
              partialRevoke: 'Schreibrechte entziehen, Leserechte behalten',
              revokeAll: 'Schreibrechte und Leserechte entziehen',
            },
          },
        },
      },
    },
    deleteModal: {
      title: 'Hinweis: Löschen von Element',
      btnOk: 'Ok, löschen',
      btnCancel: 'Abbrechen',
      textGGO:
        'Die GGO gibt die Phasen und Termine aus denen ein Rechtsetzungsprozess besteht vor. Das von Ihnen ausgewählte Element muss laut GGO durchlaufen werden. Möchten Sie das Element trotzdem löschen?',
      text: 'Möchten Sie dieses Element wirklich löschen?',
      deleteMsg: 'Sie haben {{type}} {{name}} erfolgreich gelöscht.',
      type: {
        phase: 'Phase',
        termin: 'Termin',
      },
      zeitplanungAnpassen: {
        label: 'Datumseinträge aller nachfolgenden Elemente nach vorne verschieben',
        infoTitle: 'Erklärung zu: Datumseinträge aller nachfolgenden Elemente nach vorne verschieben',
        info: 'Ist dieser Schalter aktiv, verschieben sich alle Datumseinträge der nachfolgenden Elemente entsprechend den enthaltenen Regeln, der Reihenfolge sowie den unterschiedlichen Dauern zeitlich nach vorne.',
      },
    },
  },
};
