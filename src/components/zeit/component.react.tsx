// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Layout, Spin } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { AjaxError } from 'rxjs/ajax';

import {
  ErrorController,
  HeaderComponent,
  HeaderController,
  InfoComponent,
  LoadingStatusController,
} from '@plateg/theme';
import { Loading } from '@plateg/theme/src/components/icons/Loading';
import { GlobalDI } from '@plateg/theme/src/shares';

import { MainRoutesLightZeit } from '../zeit-light/main/component.react';
import { MainRoutesZeit } from './main/component.react';
import { ZeitHelpComponent } from './main/home-tabs-zeit/help/component.react';

interface ZeitHelpLinkProps {
  isLight?: boolean;
}

export function ZeitHelpLink(props: ZeitHelpLinkProps): React.ReactElement {
  const { t } = useTranslation();
  return (
    <InfoComponent title={t('zeit.header.help.title')} buttonText={t('zeit.header.help.linkHelp')}>
      <ZeitHelpComponent isLight={props.isLight} />
    </InfoComponent>
  );
}
interface ZEITPropsInterface {
  isLight?: boolean;
}

const { Header, Content } = Layout;

export function LayoutWrapperZeit(props: ZEITPropsInterface): React.ReactElement {
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const headerController = GlobalDI.getOrRegister('ezeitHeaderController', () => new HeaderController());
  const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  const [loadingStatus, setLoadingStatus] = useState<boolean>(false);

  useEffect(() => {
    const subs = loadingStatusController.subscribeLoadingStatus().subscribe({
      next: (state: boolean) => {
        setLoadingStatus(state);
      },
      error: (error: AjaxError) => {
        console.log('Error sub', error);
        errorCtrl.displayErrorMsg(error, 'zeit.generalErrorMsg');
      },
    });
    loadingStatusController.initLoadingStatus();
    return () => subs.unsubscribe();
  }, []);

  return (
    <Spin
      {...{ role: loadingStatus ? 'progressbar' : undefined }}
      aria-busy={loadingStatus}
      spinning={loadingStatus}
      size="large"
      tip="Laden..."
      wrapperClassName="loading-screen"
      indicator={<Loading />}
    >
      <Layout
        style={{ height: '100%', display: 'unset' }}
        className={`site-layout-background ${loadingStatus === true ? 'wrapper-spinner-in-progress' : ''}`}
      >
        <HeaderComponent ctrl={headerController} headerId="zeit-header" />

        <Content className="main-content-area has-drawer">
          <div className="ant-layout-content">{props.isLight ? <MainRoutesLightZeit /> : <MainRoutesZeit />}</div>
        </Content>
      </Layout>
    </Spin>
  );
}
