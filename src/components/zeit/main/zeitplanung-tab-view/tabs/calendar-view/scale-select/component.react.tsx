// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './scale-select.less';

import { Radio, RadioChangeEvent } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';
interface ScaleSelectComponentProps {
  setSelectedScale: (selectedScale: number) => void;
  defaultScale?: number;
}

export function ScaleSelectComponent(props: ScaleSelectComponentProps): React.ReactElement {
  const { t } = useTranslation();

  const onScaleChange = (e: RadioChangeEvent) => {
    props.setSelectedScale(e.target.value as number);
  };

  return (
    <div className="calendar-scale-select">
      <Radio.Group
        defaultValue={props.defaultScale}
        onChange={onScaleChange}
        className="calendar-scale-radios"
        name="status"
      >
        <Radio className="calendar-scale-radio" id="zeit-calendar-scale-year-radio" value={12}>
          {t('zeit.zeitplanungenTable.tabs.kalendersicht.scaleSelect.options.year')}
        </Radio>
        <Radio className="calendar-scale-radio" id="zeit-calendar-scale-halfYear-radio" value={6}>
          {t('zeit.zeitplanungenTable.tabs.kalendersicht.scaleSelect.options.halfYear')}
        </Radio>
        <Radio className="calendar-scale-radio" id="zeit-calendar-scale-quarterYear-radio" value={3}>
          {t('zeit.zeitplanungenTable.tabs.kalendersicht.scaleSelect.options.quarterYear')}
        </Radio>
      </Radio.Group>
    </div>
  );
}
