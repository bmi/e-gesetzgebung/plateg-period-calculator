// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './day-select.less';

import { Button } from 'antd';
import addMonths from 'date-fns/addMonths';
import subMonths from 'date-fns/subMonths';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { LeftOutlined, RightOutlined } from '@plateg/theme';

import { VisibleTime } from '../component.react';
interface DaySelectComponentProps {
  visibleTime: VisibleTime;
  selectedScale: number;
  setCustomStartDay: (customStartDay: Date) => void;
}

export function DaySelectComponent(props: DaySelectComponentProps): React.ReactElement {
  const { t } = useTranslation();

  const offset = props.selectedScale === 3 ? 1 : props.selectedScale / 2;

  return (
    <div className="calendar-day-select">
      <Button
        onClick={() => props.setCustomStartDay(subMonths(props.visibleTime?.start, offset))}
        type="text"
        className="calendar-day-select-previous"
        id="zeit-calendar-day-select-previous"
      >
        <LeftOutlined />
      </Button>
      <Button
        onClick={() => props.setCustomStartDay(new Date(new Date().toDateString()))}
        type="text"
        className="calendar-day-select-today"
        id="zeit-calendar-day-select-today"
      >
        {t('zeit.zeitplanungenTable.tabs.kalendersicht.daySelect.todayBtn')}
      </Button>
      <Button
        onClick={() => props.setCustomStartDay(addMonths(props.visibleTime?.start, offset))}
        type="text"
        className="calendar-day-select-next"
        id="zeit-calendar-day-select-next"
      >
        <RightOutlined />
      </Button>
    </div>
  );
}
