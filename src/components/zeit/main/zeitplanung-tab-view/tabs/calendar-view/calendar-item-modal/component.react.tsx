// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button } from 'antd';
import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useHistory, useRouteMatch } from 'react-router-dom';

import { PhaseEntityDTO, TerminEntityDTO } from '@plateg/rest-api';
import { DropdownMenu, GlobalDI, HinweisComponent, IconImportant, IconLocked } from '@plateg/theme';
import { ModalWrapper } from '@plateg/theme/src/components/modal-wrapper/component.react';
import { DropdownMenuItem } from '@plateg/theme/src/components/table-component/table-sub-components/dropdown-button-component/component.react';

import { routes } from '../../../../../../../shares/routes';
import { DeleteConfirmModalPropsInterface } from '../../../../general-zeitelement/delete-confirm-modal/component.react';
import { CalendarItemController } from '../calendar-item/controller';

interface CalendarItemModalComponentProps {
  itemData: PhaseEntityDTO | TerminEntityDTO;
  clickX: number;
  clickY: number;
  isReadOnly: boolean;
  itemId: string;
  setIsModalVisible: (flag: boolean) => void;
  setDeleteConfirmModalProps: (openConfirmModalProps: DeleteConfirmModalPropsInterface) => void;
  isExternalElement?: boolean;
  isLight?: boolean;
}

interface CalendarModalCoordinatesInterface {
  coordX: number;
  coordY: number;
}
export function CalendarItemModalComponent(props: CalendarItemModalComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const history = useHistory();

  const routeMatcher = useRouteMatch<{
    archiv?: string;
    vorlageId: string;
    action: string;
    elementId?: string;
    elementAction?: string;
  }>(`/zeit/:archiv?/*/:vorlageId/(${routes.TABELLENSICHT}|${routes.KALENDERSICHT})`);

  const isArchive = routeMatcher?.params.archiv;
  const tab = routeMatcher?.url.includes(routes.MEINE_ZEITPLANUNGEN)
    ? routes.MEINE_ZEITPLANUNGEN
    : routes.ZEITPLANUNGSVORLAGEN;
  const vorlageId = routeMatcher?.params.vorlageId ?? '';
  const parentTabLinkUrl = `/zeit/${isArchive ? routes.ARCHIV : tab}/${vorlageId}`;

  const editUrl = `${parentTabLinkUrl}/${routes.KALENDERSICHT}/${routes.ELEMENT_BEARBEITEN}/${props.itemId}`;

  const сalendarItemController = GlobalDI.getOrRegister('сalendarItemController', () => new CalendarItemController());
  const [modalCoordinates, setModalCoordinates] = useState<CalendarModalCoordinatesInterface>({
    coordX: 0,
    coordY: 0,
  });

  const itemType = props.itemData.termin ? 'termin' : 'phase';
  const propertiesList = [];
  if (props.itemData?.hinweise?.length) {
    propertiesList.push(
      <HinweisComponent
        mode="warning"
        title={t(`zeit.zeitplanungsvorlagenTable.tabs.tabellensicht.eigenschaften.abstand.title`)}
        content={<></>}
      />,
    );
  }
  if (props.itemData.termin && (props.itemData as TerminEntityDTO).wichtig) {
    propertiesList.push(
      <>
        <IconImportant />
        {t(`zeit.newElementGeneral.elementPruefen.moreInformation.properties.items.important.label`)}
      </>,
    );
  }
  if (props.itemData.gesperrt) {
    propertiesList.push(
      <>
        <IconLocked />
        {t(
          `zeit.zeitplanungsvorlagenTable.tabs.tabellensicht.eigenschaften.gesperrt.${itemType}.${props.itemData.uebergeordneteElementeId == null ? 'title' : 'titleChild'}`,
        )}
      </>,
    );
  }
  const properties = propertiesList.length > 0 && (
    <>
      <dt>{t('zeit.newElementGeneral.elementPruefen.moreInformation.properties.title')}</dt>
      <dd>
        {propertiesList.map((prop, index) => {
          return (
            <div className="property-item" key={`property-item-${index}`}>
              {prop}
            </div>
          );
        })}
      </dd>
    </>
  );

  const comment = props.itemData.kommentar ? (
    <>
      <dt>{t(`zeit.zeitplanungenTable.tabs.kalendersicht.detailsModal.kommentar`)}</dt>
      <dd>{props.itemData.kommentar}</dd>
    </>
  ) : null;

  const titleMenu = () => {
    if (props.isExternalElement) {
      return <></>;
    }
    const items: DropdownMenuItem[] = [
      {
        element: t(`zeit.zeitplanungenTable.tabs.kalendersicht.detailsModal.titleMenu.edit`),
        disabled: () => props.isReadOnly,
        onClick: () => {
          history.push(editUrl);
        },
      },
      {
        element: t(`zeit.zeitplanungenTable.tabs.kalendersicht.detailsModal.titleMenu.delete`),
        onClick: () => {
          props.setIsModalVisible(false);
          props.setDeleteConfirmModalProps({
            isVisible: true,
            target: {
              id: props.itemId,
              titel: props.itemData.titel,
              isTermin: props.itemData.termin,
              infoKey: props.itemData.bauplan,
              parentId: vorlageId,
            },
          });
        },
        disabled: () => props.isReadOnly,
      },
    ];
    return <DropdownMenu items={items} elementId={'title-menu'} />;
  };

  const modalContentRef = useRef<HTMLDivElement>();

  useEffect(() => {
    if (modalContentRef && modalContentRef.current) {
      const modalHeight = modalContentRef.current.clientHeight;
      setModalCoordinates(сalendarItemController.calculateCoordinates(props.clickX, props.clickY, modalHeight));
    }
  }, []);

  return (
    <ModalWrapper
      title={
        <div className="modal-title">
          <h3 className="title-text">
            {t(
              `zeit.zeitplanungenTable.tabs.kalendersicht.detailsModal.title.${
                props.isExternalElement ? 'external' : itemType
              }`,
            )}
          </h3>
          {titleMenu()}
        </div>
      }
      mask={false}
      open={true}
      cancelText={null}
      onCancel={() => props.setIsModalVisible(false)}
      footer={
        !props.isExternalElement && (
          <Button id="ezeit-zeitplanungenTableKalendarBearbeiten-btn" type="primary" disabled={props.isReadOnly}>
            <Link id="ezeit-zeitplanungenTableKalendarBearbeiten-link" to={editUrl}>
              {t(`zeit.zeitplanungenTable.tabs.kalendersicht.detailsModal.btnBearbeiten.${itemType}`)}
            </Link>
          </Button>
        )
      }
      style={{ left: modalCoordinates.coordX, top: modalCoordinates.coordY }}
      className={`calendar-item-details ${props.isExternalElement ? 'external-calendar-item-details' : ''}`}
      destroyOnClose={true}
      transitionName=""
    >
      <div className="calendar-item-details-content" ref={modalContentRef}>
        <h3>{props.itemData.titel}</h3>
        <dl>
          <dt>
            {t(
              `zeit.zeitplanungenTable.tabs.kalendersicht.detailsModal.timeLine.${
                props.isExternalElement ? 'external.' : ''
              }${itemType}`,
            )}
          </dt>
          <dd>{сalendarItemController.getPeriod(props.itemData)}</dd>
          {properties}
          {comment}
        </dl>
      </div>
    </ModalWrapper>
  );
}
