// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './calendar-item.less';

import { Tooltip } from 'antd';
import { format } from 'date-fns';
import React, { useEffect, useState } from 'react';
import { ReactCalendarItemRendererProps } from 'react-calendar-timeline';

import { PhaseEntityDTO, TerminEntityDTO } from '@plateg/rest-api';
import { ExclamationCircleFilled } from '@plateg/theme';

import { DeleteConfirmModalPropsInterface } from '../../../../general-zeitelement/delete-confirm-modal/component.react';
import { CalendarItemModalComponent } from '../calendar-item-modal/component.react';
import { CalendarItem } from '../controller.react';

interface CalendarItemComponentProps {
  itemProps: ReactCalendarItemRendererProps<CalendarItem>;
  isReadOnly: boolean;
  setDeleteConfirmModalProps: (openConfirmModalProps: DeleteConfirmModalPropsInterface) => void;
  isLight?: boolean;
}

export function CalendarItemComponent(props: CalendarItemComponentProps): React.ReactElement {
  const [isModalVisible, setIsModalVisible] = useState<boolean>(false);
  const [clickX, setClickX] = useState<number>(0);
  const [clickY, setClickY] = useState<number>(0);
  const dateFormat = 'dd.MM.yyyy';
  const itemData: PhaseEntityDTO | TerminEntityDTO = props.itemProps.item.itemData.dto;
  const [itemTitle, setItemTitle] = useState(<></>);

  useEffect(() => {
    let date;
    if (itemData.termin) {
      date = format(new Date((itemData as TerminEntityDTO).datum), dateFormat);
    } else {
      const phase = itemData as PhaseEntityDTO;
      date = `${format(new Date(phase.beginn), dateFormat) || ''}-
            ${format(new Date(phase.ende), dateFormat) || ''}`;
    }

    const title = (
      <>
        {date} <span className="item-name">· {itemData.titel}</span>{' '}
      </>
    );

    setItemTitle(title);
  }, [props.itemProps]);

  return (
    <>
      <Tooltip overlayClassName="calendar-item-tooltip" placement="top" title={itemTitle}>
        <div
          {...props.itemProps.getItemProps({})}
          title=""
          onClick={(e) => {
            setClickX(e.clientX);
            setClickY(e.clientY);
            setIsModalVisible(true);
          }}
          className={`${isModalVisible ? 'rct-item selected' : 'rct-item'} ${
            props.itemProps.item.isExternalElement ? 'external-calendar-item' : ''
          } ${itemData.wichtig && 'wichtig'}`}
        >
          <div className="rct-item-content">
            {itemData?.hinweise?.length > 0 && (
              <>
                <ExclamationCircleFilled /> &nbsp;
              </>
            )}
            {itemTitle}
          </div>
        </div>
      </Tooltip>
      {isModalVisible && (
        <CalendarItemModalComponent
          itemData={itemData}
          clickX={clickX}
          clickY={clickY}
          isReadOnly={props.isReadOnly}
          itemId={props.itemProps.item.itemData.base.id}
          setIsModalVisible={setIsModalVisible}
          setDeleteConfirmModalProps={props.setDeleteConfirmModalProps}
          isExternalElement={props.itemProps.item.isExternalElement}
          isLight={props.isLight}
        />
      )}
    </>
  );
}
