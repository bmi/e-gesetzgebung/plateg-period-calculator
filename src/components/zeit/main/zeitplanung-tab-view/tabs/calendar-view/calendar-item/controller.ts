// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { format } from 'date-fns';

import { PhaseEntityDTO, TerminEntityDTO } from '@plateg/rest-api';
export class CalendarItemController {
  private readonly dateFormat = 'dd.MM.yyyy';
  public readonly screenWidth = window.innerWidth;
  public readonly screenHeight = window.innerHeight;
  public readonly modalWidth = 380;

  /**
   * Get correct time period depending on item type
   * @param item {PhaseEntityDTO | TerminEntityDTO}
   * @returns
   */
  public getPeriod(item: PhaseEntityDTO | TerminEntityDTO): string {
    if (item.termin) {
      return format(new Date((item as TerminEntityDTO).datum), this.dateFormat);
    } else {
      return `${format(new Date((item as PhaseEntityDTO).beginn), this.dateFormat) || ''} - ${
        format(new Date((item as PhaseEntityDTO).ende), this.dateFormat) || ''
      }`;
    }
  }

  /**
   * To calculate correct position of details modal
   * @param clickX {number}
   * @param clickY {number}
   * @returns Object with calculated coordinates X and Y
   */
  public calculateCoordinates(clickX: number, clickY: number, modalHeight: number): { coordX: number; coordY: number } {
    const fullHeight = modalHeight + 178; // summary header + content paddings + footer
    let coordX = clickX;
    // Show modal on left, if not enough space on right
    if (this.screenWidth - this.modalWidth < coordX) {
      coordX = clickX - this.modalWidth;
    }

    // Align modal vertically, to have center according to clicked item
    let coordY = clickY - fullHeight / 2;
    // If modal cutted by border bottom, show correct it to show full height
    if (coordY > this.screenHeight - fullHeight) {
      coordY = this.screenHeight - fullHeight;
    }
    // If modal cutted by top bottom, show correct it to show full height
    if (coordY < 0) {
      coordY = 0;
    }

    return {
      coordX,
      coordY,
    };
  }
}
