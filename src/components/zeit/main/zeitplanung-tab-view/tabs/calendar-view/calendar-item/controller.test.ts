// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import chai, { expect } from 'chai';
import chaiString from 'chai-string';

import { PhaseEntityDTO, TerminEntityDTO } from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme/src/shares';

import { CalendarItemController } from './controller';
chai.use(chaiString);

const сalendarItemController = GlobalDI.getOrRegister('сalendarItemController', () => new CalendarItemController());

const testPhase: PhaseEntityDTO = {
  termin: false,
  wichtig: false,
  titel: 'Vorphase',
  vorherigeElementeIds: [],
  zeitplanungAnpassen: false,
  beginn: '2021-08-16',
  ende: '2021-12-05',
  untergeordneteElemente: [],
  datum: '2021-12-05',
};
const testTermin: TerminEntityDTO = {
  termin: true,
  wichtig: true,
  titel: 'Vorlage der notwendigen Dokumente auf Dienstweg an Hausleitung',
  vorherigeElementeIds: ['21b5b88f-ef56-4e31-85be-aef10c782594'],
  kommentar: 'Lorem ipsum dolor sit amet, consec',
  zeitplanungAnpassen: false,
  datum: '2021-08-16',
};

describe('TEST: getPeriod - get correct time period depending on item type', () => {
  it('get time period for phase', () => {
    const textPeriod = сalendarItemController.getPeriod(testPhase);
    expect(textPeriod).to.equal('16.08.2021 - 05.12.2021');
  });
  it('get time period for termin', () => {
    const textPeriod = сalendarItemController.getPeriod(testTermin);
    expect(textPeriod).to.equal('16.08.2021');
  });
});

describe('TEST: calculateCoordinates - get correct modals coordinates', () => {
  it('modal does not cutted by top border', () => {
    const coordinates = сalendarItemController.calculateCoordinates(400, 114, 352);
    expect(coordinates.coordX).to.equal(400);
    expect(coordinates.coordY).to.equal(0);
  });
  it('modal does not cutted by bottom border', () => {
    const coordinates = сalendarItemController.calculateCoordinates(400, 662, 352);
    expect(coordinates.coordX).to.equal(400);
    // WindowHeight(768) - modalHeight(530) = 238
    expect(coordinates.coordY).to.equal(238);
  });
  it('Show modal on left, if not enough space on right', () => {
    const coordinates = сalendarItemController.calculateCoordinates(919, 323, 352);
    // WindowWidth(1024) - modalWidth(380) = 644 < 919 => coordX = clickX(919) - modalWidth(380) = 539
    expect(coordinates.coordX).to.equal(539);
  });
  it('Show modal on left, if not enough space on right', () => {
    const coordinates = сalendarItemController.calculateCoordinates(919, 323, 352);
    // WindowWidth(1024) - modalWidth(380) = 644 < 919 => coordX = clickX(919) - modalWidth(380) = 539
    expect(coordinates.coordX).to.equal(539);
  });
});
