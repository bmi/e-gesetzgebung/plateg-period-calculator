// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button, Tooltip } from 'antd';
import i18n from 'i18next';
import React from 'react';
import { ReactCalendarItemRendererProps, TimelineItemBase } from 'react-calendar-timeline';

import {
  KalenderType,
  PhaseEntityResponseDTO,
  SitzungenDTO,
  TerminEntityResponseDTO,
  ZeitplanungselementEntityResponseDTOZeitplanungselementEntityDTO,
} from '@plateg/rest-api';
import { CloseOutlined } from '@plateg/theme';

import { DeleteConfirmModalPropsInterface } from '../../../general-zeitelement/delete-confirm-modal/component.react';
import { CalendarItemComponent } from './calendar-item/component.react';
import { CalGroup, KalenderUnionDTO } from './component.react';

export interface CalendarItem extends TimelineItemBase<Date> {
  id: string;
  group: string;
  start_time: Date;
  end_time: Date;
  itemData: PhaseEntityResponseDTO | TerminEntityResponseDTO;
  isExternalElement?: boolean;
}

export class CalendarController {
  private dirtyFlag = 0;
  private setDirtyFlag: ((dirtyFlag: number) => void) | undefined;
  private readonly openGroups = new Set<string>();

  public initalizeIsDirtyFlag = (setDirtyFlag: (dirtyFlag: number) => void): void => {
    this.setDirtyFlag = setDirtyFlag;
  };

  public renderCustomItem(
    props: ReactCalendarItemRendererProps<CalendarItem>,
    isReadOnly: boolean,
    setDeleteConfirmModalProps: (openConfirmModalProps: DeleteConfirmModalPropsInterface) => void,
    isLight: boolean,
  ): React.ReactNode {
    return (
      <CalendarItemComponent
        itemProps={props}
        isReadOnly={isReadOnly}
        setDeleteConfirmModalProps={setDeleteConfirmModalProps}
        isLight={isLight}
      />
    );
  }

  public createItems(liste: ZeitplanungselementEntityResponseDTOZeitplanungselementEntityDTO[]): CalendarItem[] {
    const itemList = liste.map((item) => {
      return item.dto.termin
        ? this.createTerminItem(item as TerminEntityResponseDTO)
        : this.createPhaseItem(item as PhaseEntityResponseDTO);
    });
    return itemList.flat();
  }

  private createPhaseItem(item: PhaseEntityResponseDTO) {
    let itemList: CalendarItem[] = [
      {
        id: item.base.id,
        group: item.base.id,
        start_time: new Date(item.dto.beginn as string),
        end_time: new Date(`${item.dto.ende || ''} 23:59:59`),
        itemData: item,
      },
    ];
    if (item.dto.untergeordneteElemente && item.dto.untergeordneteElemente.length > 0) {
      itemList = itemList.concat(this.createItems(item.dto.untergeordneteElemente));
    }
    return itemList;
  }

  private createTerminItem(item: TerminEntityResponseDTO) {
    return {
      id: item.base.id,
      group: item.base.id,
      start_time: new Date(item.dto.datum as string),
      end_time: new Date(`${item.dto.datum as string} 23:59:59`),
      itemData: item,
    };
  }

  public createGroups(
    liste: ZeitplanungselementEntityResponseDTOZeitplanungselementEntityDTO[],
    level: number,
  ): { id: string; title: React.ReactElement }[] {
    const nestedGroupList = liste.map((item) => {
      if (item.dto.termin || (item as PhaseEntityResponseDTO).dto.untergeordneteElemente?.length === 0) {
        return this.createGroupNoChilds(item, level);
      } else {
        return this.createGroupWithChilds(item as PhaseEntityResponseDTO, level);
      }
    });
    return nestedGroupList.flat();
  }

  private createGroupWithChilds(item: PhaseEntityResponseDTO, level: number) {
    const isOpen = this.openGroups.has(item.base.id);
    const title = (
      <div
        onClick={() => this.toggleGroup(item.base.id)}
        style={{ cursor: 'pointer', paddingLeft: level * 20 }}
        className={isOpen ? 'calendar-arrow-down' : 'calendar-arrow'}
        title={item.dto.titel}
      >
        {item.dto.titel}
      </div>
    );
    let groupList = [{ id: item.base.id, title: title }];
    if (isOpen) {
      groupList = groupList.concat(this.createGroups(item?.dto?.untergeordneteElemente || [], level + 1));
    }
    return groupList;
  }

  private createGroupNoChilds(item: ZeitplanungselementEntityResponseDTOZeitplanungselementEntityDTO, level: number) {
    const title = (
      <div
        style={{ paddingLeft: 10 + level * 20 }}
        title={item.dto.titel}
        className={`regular-group ${item.dto.wichtig && 'wichtig'}`}
      >
        {item.dto.titel}
      </div>
    );
    return { id: item.base.id, title: title, isWichtig: item.dto.wichtig };
  }

  private hideExternalCalendar = (
    calendar: KalenderUnionDTO,
    externalCalendars: KalenderUnionDTO[],
    setExternalCalendars: (externalCalendars: KalenderUnionDTO[]) => void,
  ) => {
    setExternalCalendars(
      externalCalendars.filter((cal) => {
        return cal.id !== calendar.id;
      }),
    );
  };

  public createExternalItemsAndGroups = (
    calendars: KalenderUnionDTO[],
    setExternalCalendars: (externalCalendars: KalenderUnionDTO[]) => void,
  ) => {
    const groups: CalGroup[] = calendars.map((cal, index) => {
      let title = cal.titel;
      if (cal.typ === KalenderType.Sitzungen) {
        const calendarType = (cal as SitzungenDTO).initiantType as string;
        title += ` ${calendarType[0]}${calendarType.toLowerCase().slice(1)}`;
      }

      return {
        title: (
          <div
            className={`external-calendar ${index === 0 ? 'first-external-calendar' : ''} ${
              index + 1 === calendars.length ? 'last-external-calendar' : ''
            }`}
            title={title}
          >
            <div>
              <span>{title}</span>
              <Tooltip
                title={i18n.t('zeit.zeitplanungenTable.tabs.kalendersicht.removeExternal')}
                trigger="hover focus"
              >
                <Button
                  className="remove-calendar-button"
                  onClick={() => this.hideExternalCalendar(cal, calendars, setExternalCalendars)}
                >
                  <CloseOutlined />
                </Button>
              </Tooltip>
            </div>
          </div>
        ),
        id: cal.id,
        isExternalGroup: true,
      };
    });

    const items = calendars
      .map((cal) => {
        return cal.kalendereintraege.map((entry, index) => {
          return {
            id: `${entry.id}-${index}`,
            group: cal.id,
            start_time: new Date(entry.beginn),
            end_time: new Date(`${entry.ende} 23:59:59`),
            itemData: {
              base: { id: entry.id },
              dto: {
                ...entry,
                termin: new Date(entry.beginn).toDateString() === new Date(entry.ende).toDateString(),
                hinweise: [],
                datum: new Date(entry.beginn),
              },
            },
            isExternalElement: true,
          };
        });
      })
      .flat();
    return { groups, items };
  };

  private toggleGroup(id: string) {
    if (this.openGroups.has(id)) {
      this.openGroups.delete(id);
    } else {
      this.openGroups.add(id);
    }
    this.setDirtyFlag?.(1 - this.dirtyFlag);
    this.dirtyFlag = 1 - this.dirtyFlag;
  }

  public getZeitplanungStart(
    zeitplanungElements: ZeitplanungselementEntityResponseDTOZeitplanungselementEntityDTO[],
  ): Date {
    const element = zeitplanungElements?.[0];
    if (element?.dto?.termin) {
      return new Date((element as TerminEntityResponseDTO).dto.datum as string);
    } else {
      const beginn = (element as PhaseEntityResponseDTO)?.dto?.['beginn'];
      return beginn ? new Date(beginn) : new Date();
    }
  }
}
