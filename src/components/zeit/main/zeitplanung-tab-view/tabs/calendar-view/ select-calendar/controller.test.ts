// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import chai, { assert, expect } from 'chai';
import chaiString from 'chai-string';
import i18n from 'i18next';
import sinon from 'sinon';

import { BundeslandType, Configuration, KalenderType, SitzungType, ZeitplanungControllerApi } from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme/src/shares';

import { KalenderUnionDTO } from '../component.react';
import { SelectExternalCalendarController } from './controller';

chai.use(chaiString);

describe('TEST: SelectExternalCalendarController', () => {
  const ctrl = GlobalDI.getOrRegister('selectExternalCalendarController', () => new SelectExternalCalendarController());
  const zeitplanungRestController = GlobalDI.getOrRegister(
    'zeitplanungRestController',
    () => new ZeitplanungControllerApi(new Configuration()),
  );
  describe('TEST: getCalendarCall', () => {
    let getKalenderStub: sinon.SinonStub;

    before(() => {
      getKalenderStub = sinon.stub(zeitplanungRestController, 'getKalender');
    });
    after(() => {
      getKalenderStub.restore();
    });

    it('check if REST API is called', () => {
      ctrl.getCalendarCall();
      sinon.assert.calledOnce(getKalenderStub);
    });
  });

  describe('TEST: getTreeStructure', () => {
    const cals: KalenderUnionDTO[] = [
      {
        titel: 'Feiertage Brandenburg',
        bundeslandType: BundeslandType.Bb,
        typ: KalenderType.Feiertage,
        kalendereintraege: [
          {
            id: '1',
            titel: 'Neujahr in Brandenburg',
            beginn: '2022-01-01',
            ende: '2022-01-02',
          },
          {
            id: '2',
            titel: 'Karfreitag in Brandenburg',
            beginn: '2022-04-15',
            ende: '2022-04-16',
          },
        ],
      },
      {
        titel: 'Feiertage Berlin',
        bundeslandType: BundeslandType.Be,
        typ: KalenderType.Feiertage,
        kalendereintraege: [
          {
            id: '3',
            titel: 'Neujahr in Berlin',
            beginn: '2022-01-01',
            ende: '2022-01-02',
          },
          {
            id: '4',
            titel: 'Karfreitag in Berlin',
            beginn: '2022-04-15',
            ende: '2022-04-16',
          },
        ],
      },
      {
        titel: 'Feiertage Deutschalnd',
        typ: KalenderType.Feiertage,
        kalendereintraege: [
          {
            id: '5',
            titel: 'Neujahr in Deutschland',
            beginn: '2022-01-01',
            ende: '2022-01-02',
          },
          {
            id: '6',
            titel: 'Karfreitag in Deutschland',
            beginn: '2022-04-15',
            ende: '2022-04-16',
          },
        ],
      },
      {
        titel: 'Ferien Brandenburg',
        typ: KalenderType.Ferien,
        bundeslandType: BundeslandType.Bb,
        kalendereintraege: [
          {
            id: '7',
            titel: 'Neujahr in Deutschland',
            beginn: '2022-01-01',
            ende: '2022-01-02',
          },
          {
            id: '8',
            titel: 'Karfreitag in Deutschland',
            beginn: '2022-04-15',
            ende: '2022-04-16',
          },
        ],
      },
      {
        titel: 'Sitzungen',
        typ: KalenderType.Sitzungen,
        sitzungType: SitzungType.Plenar,
        kalendereintraege: [
          {
            id: '9',
            titel: 'Plenar 1',
            beginn: '2022-01-01',
            ende: '2022-01-02',
          },
          {
            id: '10',
            titel: 'Plenar 2',
            beginn: '2022-04-15',
            ende: '2022-04-16',
          },
        ],
        initiantType: 'BUNDESRAT',
      },
    ];

    it('correct structure for KalenderType.Feiertage should be returned', () => {
      const result = ctrl.getTreeStructure(KalenderType.Feiertage, cals);
      expect(result?.length).to.eql(2);
      expect(result[0].title).to.eql(cals[2].titel);
      expect(result[0].key).to.eql(cals[2].id);
      expect(result[1].title).to.eql(
        i18n.t(`zeit.zeitplanungenTable.tabs.kalendersicht.selectCalendar.modal.selects.feiertage.allEntries`),
      );
      expect(result[1].children?.length).to.eql(2);
      expect(result[1].children?.[0].title).to.eql(cals[0].titel);
      expect(result[1].children?.[0].key).to.eql(cals[0].id);
      expect(result[1].children?.[1].title).to.eql(cals[1].titel);
      expect(result[1].children?.[1].key).to.eql(cals[1].id);
    });

    it('correct structure for KalenderType.Ferien should be returned', () => {
      const result = ctrl.getTreeStructure(KalenderType.Ferien, cals);
      expect(result[0].title).to.eql(
        i18n.t(`zeit.zeitplanungenTable.tabs.kalendersicht.selectCalendar.modal.selects.ferien.allEntries`),
      );
      expect(result[0].children?.length).to.eql(1);
      expect(result[0].children?.[0].title).to.eql(cals[3].titel);
      expect(result[0].children?.[0].key).to.eql(cals[3].id);
    });

    it('correct structure for KalenderType.Sitzungen should be returned', () => {
      const result = ctrl.getTreeStructure(KalenderType.Sitzungen, cals);
      expect(result[0].title).to.eql(
        i18n.t(`zeit.zeitplanungenTable.tabs.kalendersicht.selectCalendar.modal.selects.sitzungen.allEntries`),
      );
      expect(result[0].children?.length).to.eql(1);
      expect(result[0].children?.[0].title).to.eql(cals[4].titel + ' Bundesrat');
      expect(result[0].children?.[0].key).to.eql(cals[4].id);
    });
  });
});
