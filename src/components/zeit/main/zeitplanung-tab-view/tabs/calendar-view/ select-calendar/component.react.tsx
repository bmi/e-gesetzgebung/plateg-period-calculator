// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button, Form } from 'antd';
import { CheckboxValueType } from 'antd/es/checkbox/Group';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { KalenderDTO, KalenderType } from '@plateg/rest-api';
import { CalendarOutlined, displayMessage, GeneralFormWrapper, ModalComponent } from '@plateg/theme';

import { KalenderUnionDTO } from '../component.react';
import { FormValuesExternCal } from './select-calendar-checkbox/component.react';
import { SelectCalendarContent } from './select-calendar-content/component.react';

interface SelectCalendarProps {
  initialCalenders: KalenderUnionDTO[];
  externalCalendars: KalenderUnionDTO[];
  setExternalCalendars: (externalCalendars: KalenderUnionDTO[]) => void;
}
export function SelectCalendar(props: SelectCalendarProps): React.ReactElement {
  const [form] = Form.useForm<FormValuesExternCal>();
  const { t } = useTranslation();
  const [modalIsVisible, setModalIsVisible] = useState(false);

  const [alleSchulferien, setAlleSchulferien] = useState<CheckboxValueType[]>([]);
  const [alleFeiertage, setAlleFeiertage] = useState<CheckboxValueType[]>([]);
  const [alleBundesFeiertage, setAlleBundesFeiertage] = useState<CheckboxValueType[]>([]);
  const [alleSitzungen, setAlleSitzungen] = useState<CheckboxValueType[]>([]);

  useEffect(() => {
    if (props.externalCalendars.length > 0) {
      const alle_bundes_feiertag = props.externalCalendars
        .filter((item) => item.titel === 'Bundesweite Feiertage' && item.typ === KalenderType.Feiertage)
        .map((item) => item.id);
      setAlleBundesFeiertage(alle_bundes_feiertag);

      const alle_feiertage = props.externalCalendars
        .filter((item) => item.titel !== 'Bundesweite Feiertage' && item.typ === KalenderType.Feiertage)
        .map((item) => item.id);
      setAlleFeiertage(alle_feiertage);

      const alle_schulferien = props.externalCalendars
        .filter((item) => item.typ === KalenderType.Ferien)
        .map((item) => item.id);
      setAlleSchulferien(alle_schulferien);

      const alle_sitzungen = props.externalCalendars
        .filter((item) => item.typ === KalenderType.Sitzungen)
        .map((item) => item.id);
      setAlleSitzungen(alle_sitzungen);
    } else {
      setAlleSchulferien([]);
      setAlleFeiertage([]);
      setAlleBundesFeiertage([]);
      setAlleSitzungen([]);
    }
  }, [props.externalCalendars]);

  const onFinish = () => {
    props.setExternalCalendars(
      props.initialCalenders.filter((cal: KalenderDTO) => {
        return [...alleSchulferien, ...alleFeiertage, ...alleBundesFeiertage, ...alleSitzungen]?.includes(cal.id);
      }),
    );
    setModalIsVisible(false);
    displayMessage(
      t('zeit.zeitplanungenTable.tabs.kalendersicht.selectCalendar.modal.selects.successToast'),
      'success',
    );
  };

  const formInitialValues = {
    freizeit: Array.from(
      new Set(
        props.externalCalendars
          .filter((cal) => {
            return cal.typ === KalenderType.Feiertage || cal.typ === KalenderType.Ferien;
          })
          .map((cal) => {
            return cal.id;
          }),
      ),
    ),
    sitzungen: Array.from(
      new Set(
        props.externalCalendars
          .filter((cal) => {
            return cal.typ === KalenderType.Sitzungen;
          })
          .map((cal) => {
            return cal.id;
          }),
      ),
    ),
  };

  const pickedItems = props.initialCalenders.filter((item) =>
    [...alleSchulferien, ...alleFeiertage, ...alleBundesFeiertage, ...alleSitzungen]?.includes(item.id),
  );

  return (
    <>
      <Button
        onClick={() => setModalIsVisible(true)}
        id="ezeit-show-select-calendar-modal-btn"
        type="text"
        className="new-zeit-element-btn"
      >
        <CalendarOutlined />
        {t('zeit.zeitplanungenTable.tabs.kalendersicht.selectCalendar.trigger')}
      </Button>
      <ModalComponent
        isVisible={modalIsVisible}
        setIsVisible={setModalIsVisible}
        title={t('zeit.zeitplanungenTable.tabs.kalendersicht.selectCalendar.modal.title')}
        footerLeft={[
          <Button onClick={() => setModalIsVisible(false)}>
            {t('zeit.zeitplanungenTable.tabs.kalendersicht.selectCalendar.modal.cancel')}
          </Button>,
        ]}
        footerRight={[
          <Button
            disabled={pickedItems.length === 0}
            type="primary"
            onClick={() => {
              form.submit();
            }}
          >
            {t('zeit.zeitplanungenTable.tabs.kalendersicht.selectCalendar.modal.confirm')}
          </Button>,
        ]}
        content={
          <GeneralFormWrapper form={form} layout="vertical" onFinish={onFinish} initialValues={formInitialValues}>
            <SelectCalendarContent
              alleSitzungen={alleSitzungen}
              setAlleSitzungen={setAlleSitzungen}
              alleBundesFeiertage={alleBundesFeiertage}
              alleFeiertage={alleFeiertage}
              alleSchulferien={alleSchulferien}
              setAlleBundesFeiertage={setAlleBundesFeiertage}
              setAlleFeiertage={setAlleFeiertage}
              setAlleSchulferien={setAlleSchulferien}
              externalCalendars={props.externalCalendars}
              initialCalenders={props.initialCalenders}
              form={form}
              pickedItems={pickedItems}
            />
          </GeneralFormWrapper>
        }
      />
    </>
  );
}
