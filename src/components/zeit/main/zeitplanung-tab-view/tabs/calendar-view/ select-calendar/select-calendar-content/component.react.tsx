// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './select-calendar-content.less';

import { FormInstance } from 'antd';
import { CheckboxValueType } from 'antd/es/checkbox/Group';
import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { KalenderType } from '@plateg/rest-api';
import { CloseOutlined } from '@plateg/theme';

import { KalenderUnionDTO } from '../../component.react';
import {
  FormValuesExternCal,
  getInitiantTitle,
  SelectCalendarCheckbox,
} from '../select-calendar-checkbox/component.react';

type SetStateCheckboxValueType = React.Dispatch<React.SetStateAction<CheckboxValueType[]>>;

interface Props {
  alleBundesFeiertage: CheckboxValueType[];
  setAlleBundesFeiertage: SetStateCheckboxValueType;

  alleSchulferien: CheckboxValueType[];
  setAlleSchulferien: SetStateCheckboxValueType;

  alleFeiertage: CheckboxValueType[];
  setAlleFeiertage: SetStateCheckboxValueType;

  alleSitzungen: CheckboxValueType[];
  setAlleSitzungen: SetStateCheckboxValueType;

  form: FormInstance<FormValuesExternCal>;
  initialCalenders: KalenderUnionDTO[];
  externalCalendars: KalenderUnionDTO[];

  pickedItems: KalenderUnionDTO[];
}

export function SelectCalendarContent(props: Props): React.ReactElement {
  const { t } = useTranslation();

  const onClickRemoveCalendarItem = (itemId: string) => {
    if (props.alleSchulferien.includes(itemId)) {
      props.setAlleSchulferien((items) => items.filter((id) => id !== itemId));
    }
    if (props.alleFeiertage.includes(itemId)) {
      props.setAlleFeiertage((items) => items.filter((id) => id !== itemId));
    }
    if (props.alleBundesFeiertage.includes(itemId)) {
      props.setAlleBundesFeiertage((items) => items.filter((id) => id !== itemId));
    }
    if (props.alleSitzungen.includes(itemId)) {
      props.setAlleSitzungen((items) => items.filter((id) => id !== itemId));
    }
  };

  const pickedItemsFerienFeiertage = props.pickedItems.filter(
    (item) => item.typ === KalenderType.Feiertage || item.typ === KalenderType.Ferien,
  );

  const pickedItemsSitzungen = props.pickedItems.filter((item) => item.typ === KalenderType.Sitzungen);

  return (
    <>
      {/* Feiertage, Ferien */}
      <Title level={2}>{t('zeit.zeitplanungenTable.tabs.kalendersicht.selectCalendar.modal.heading')}</Title>

      <Title style={{ marginTop: 40, marginBottom: 8 }} level={3}>
        {t('zeit.zeitplanungenTable.tabs.kalendersicht.selectCalendar.modal.selects.feiertage.label')}
      </Title>
      <SelectCalendarCheckbox
        checkedList={props.alleBundesFeiertage}
        setCheckedList={props.setAlleBundesFeiertage}
        form={props.form}
        name="alle_bundes_feiertage"
        type={KalenderType.Feiertage}
        title={t('zeit.zeitplanungenTable.tabs.kalendersicht.selectCalendar.modal.selects.feiertage.bundesEntry')}
        initialCalenders={props.initialCalenders}
        externalCalendars={props.externalCalendars}
        onlyCheckbox
      />

      <SelectCalendarCheckbox
        checkedList={props.alleFeiertage}
        setCheckedList={props.setAlleFeiertage}
        form={props.form}
        name="alle_feiertage"
        type={KalenderType.Feiertage}
        title={t('zeit.zeitplanungenTable.tabs.kalendersicht.selectCalendar.modal.selects.feiertage.allEntries')}
        initialCalenders={props.initialCalenders}
        externalCalendars={props.externalCalendars}
      />

      <SelectCalendarCheckbox
        checkedList={props.alleSchulferien}
        setCheckedList={props.setAlleSchulferien}
        form={props.form}
        name="alle_schulferien"
        type={KalenderType.Ferien}
        title={t('zeit.zeitplanungenTable.tabs.kalendersicht.selectCalendar.modal.selects.ferien.allEntries')}
        initialCalenders={props.initialCalenders}
        externalCalendars={props.externalCalendars}
      />
      <Title style={{ marginTop: 24, marginBottom: 8 }} level={3}>
        {t(
          'zeit.zeitplanungenTable.tabs.kalendersicht.selectCalendar.modal.selects.selectedLabels.feiertageSchulferien',
        )}
      </Title>

      <div className="ext-cal-selected-items-div">
        {pickedItemsFerienFeiertage.length > 0
          ? pickedItemsFerienFeiertage.map((item) => {
              return (
                <div className="ext-cal-selected-item" key={item.id}>
                  {item.titel}
                  <CloseOutlined
                    aria-label={`Auswahl ${item.titel} entfernen`}
                    tabIndex={0}
                    onClick={() => onClickRemoveCalendarItem(item.id)}
                    onKeyDown={(e) => {
                      if (e.code === 'Enter') {
                        onClickRemoveCalendarItem(item.id);
                      }
                    }}
                    role="button"
                  />
                </div>
              );
            })
          : t(
              'zeit.zeitplanungenTable.tabs.kalendersicht.selectCalendar.modal.selects.selectedLabels.noFeiertageSchulferien',
            )}
      </div>
      {/* Sitzungen */}
      <Title style={{ marginTop: 40, marginBottom: 8 }} level={3}>
        {t('zeit.zeitplanungenTable.tabs.kalendersicht.selectCalendar.modal.selects.sitzungen.label')}
      </Title>
      <SelectCalendarCheckbox
        checkedList={props.alleSitzungen}
        setCheckedList={props.setAlleSitzungen}
        form={props.form}
        name="alle_sitzungen"
        type={KalenderType.Sitzungen}
        title={t('zeit.zeitplanungenTable.tabs.kalendersicht.selectCalendar.modal.selects.sitzungen.allEntries')}
        initialCalenders={props.initialCalenders}
        externalCalendars={props.externalCalendars}
      />
      <Title style={{ marginTop: 24, marginBottom: 8 }} level={3}>
        {t('zeit.zeitplanungenTable.tabs.kalendersicht.selectCalendar.modal.selects.selectedLabels.sitzungen')}
      </Title>

      <div className="ext-cal-selected-items-div">
        {pickedItemsSitzungen.length > 0
          ? pickedItemsSitzungen.map((item) => {
              return (
                <div className="ext-cal-selected-item" key={item.id}>
                  {getInitiantTitle(item)}
                  <CloseOutlined
                    aria-label={`Auswahl ${getInitiantTitle(item)} entfernen`}
                    tabIndex={0}
                    onClick={() => onClickRemoveCalendarItem(item.id)}
                    onKeyDown={(e) => {
                      if (e.code === 'Enter') {
                        onClickRemoveCalendarItem(item.id);
                      }
                    }}
                    role="button"
                  />
                </div>
              );
            })
          : t('zeit.zeitplanungenTable.tabs.kalendersicht.selectCalendar.modal.selects.selectedLabels.noSitzungen')}
      </div>
    </>
  );
}
