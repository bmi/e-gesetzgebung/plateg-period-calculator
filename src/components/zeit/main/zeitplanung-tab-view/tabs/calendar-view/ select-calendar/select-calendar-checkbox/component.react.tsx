// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './select-calendar-checkbox.less';

import { Button, Checkbox, Form, FormInstance, GetProp } from 'antd';
import React, { useState } from 'react';

import { KalenderType, SitzungenDTO } from '@plateg/rest-api';
import { CheckboxWithInfo, DownOutlined, RightOutlined } from '@plateg/theme';
import {
  CheckboxGroupWithInfo,
  CheckboxItem,
} from '@plateg/theme/src/components/checkbox-group-with-info/component.react';

import { KalenderUnionDTO } from '../../component.react';

export interface FormValuesExternCal {
  alle_schulferien?: CheckboxValueType[];
  alle_feiertage?: CheckboxValueType[];
  alle_bundes_feiertage?: CheckboxValueType[];
  alle_sitzungen: CheckboxValueType[];
}

interface Props {
  initialCalenders: KalenderUnionDTO[];
  externalCalendars: KalenderUnionDTO[];
  title: string;
  type: KalenderType;
  name: string;
  form: FormInstance<FormValuesExternCal>;
  checkedList: CheckboxValueType[];
  setCheckedList: (val: CheckboxValueType[]) => void;
  onlyCheckbox?: boolean;
}

type CheckboxValueType = GetProp<typeof Checkbox.Group, 'value'>[number];

export const getInitiantTitle = (item: KalenderUnionDTO) => {
  const sitzungItem = item as SitzungenDTO;
  const initiant = [
    sitzungItem.initiantType?.charAt(0).toUpperCase(),
    sitzungItem.initiantType?.slice(1).toLowerCase(),
  ].join('');
  return `${item.titel} ${initiant}`;
};

export function SelectCalendarCheckbox(props: Props): React.ReactElement {
  const [groupCheckboxVisible, setGroupCheckboxVisible] = useState(false);

  const checkboxItems: CheckboxItem[] = props.initialCalenders
    .filter((item) => {
      if (props.name === 'alle_feiertage') {
        return item.titel !== 'Bundesweite Feiertage' && item.typ === props.type;
      } else {
        return item.typ === props.type;
      }
    })
    .map((item) => {
      return {
        label: <>{item.typ === KalenderType.Sitzungen ? getInitiantTitle(item) : item.titel}</>,
        value: item.id,
        title: item.titel,
        style: { paddingBottom: 4, paddingTop: 4 },
      };
    });

  const checkAll = checkboxItems.length === props.checkedList?.length;

  const interminate = props.checkedList?.length > 0 && props.checkedList?.length < checkboxItems.length;

  const onChangeGroup = (list: CheckboxValueType[]) => {
    props.form.setFieldsValue({ [props.name]: list });
    props.setCheckedList(list);
  };
  const [isRotated, setIsRotated] = useState(false);

  const handleHideShowCheckboxGroup = () => {
    const element = document.getElementById(`extern-calendar-checkbox-group-div-${props.name}`);
    if (element) {
      if (groupCheckboxVisible) {
        element.style.height = '0';
        element.style.opacity = '0';
      } else {
        element.style.height = `${34 * checkboxItems.length + 4}px`;
        element.style.opacity = '1';
      }
    }

    setIsRotated(!isRotated);
    setTimeout(() => {
      setGroupCheckboxVisible(!groupCheckboxVisible);
    }, 200);
  };

  return (
    <>
      {props.onlyCheckbox ? (
        <Form.Item style={{ marginBottom: 0, marginLeft: 32 }} name={props.name}>
          <CheckboxWithInfo
            style={{ marginBottom: 2, marginTop: 2 }}
            checked={props.checkedList.length > 0 ? true : false}
            onChange={(e) => {
              props.form.setFieldsValue({
                [props.name]: e.target.checked
                  ? checkboxItems.filter((item) => item.title === props.title).map((item) => item.value)
                  : [],
              });
              props.setCheckedList(
                e.target.checked
                  ? checkboxItems.filter((item) => item.title === props.title).map((item) => item.value)
                  : [],
              );
            }}
            title={props.title}
            wrapperId={`calendar-checkbox-${props.name}`}
          >
            {props.title}
          </CheckboxWithInfo>
        </Form.Item>
      ) : (
        <div className="ext-cal-checkbox-group-holder-div">
          <div className="ext-cal-arrow-holder-div" style={{ marginTop: isRotated ? 1 : 2 }}>
            <Button
              onClick={() => {
                handleHideShowCheckboxGroup();
              }}
              aria-label={`Auswahlgruppe ${props.title} ${isRotated ? 'zuklappen' : 'aufklappen'}`}
              id={`ext-cal-btn-expand-${props.name}`}
              type="text"
            >
              {isRotated ? <DownOutlined /> : <RightOutlined />}
            </Button>
          </div>
          <Form.Item style={{ marginBottom: 0 }} name={props.name}>
            <div>
              <CheckboxWithInfo
                style={{ marginBottom: 2, marginTop: 2 }}
                title={props.title}
                wrapperId={`ext-cal-checkbox-${props.name}`}
                onChange={(e) => {
                  props.form.setFieldsValue({
                    [props.name]: e.target.checked ? checkboxItems.map((item) => item.value) : [],
                  });
                  props.setCheckedList(e.target.checked ? checkboxItems.map((item) => item.value) : []);
                }}
                checked={checkAll}
                indeterminate={interminate}
              >
                {props.title}
              </CheckboxWithInfo>

              <div
                id={`extern-calendar-checkbox-group-div-${props.name}`}
                className={`extern-calendar-checkboxgroup-list-div`}
              >
                {groupCheckboxVisible && (
                  <CheckboxGroupWithInfo
                    className="extern-calendar-checkboxgroup"
                    value={props.checkedList}
                    onChange={onChangeGroup}
                    items={checkboxItems}
                    id={`ext-cal-checkboxgroup-${props.name}`}
                  />
                )}
              </div>
            </div>
          </Form.Item>
        </div>
      )}
    </>
  );
}
