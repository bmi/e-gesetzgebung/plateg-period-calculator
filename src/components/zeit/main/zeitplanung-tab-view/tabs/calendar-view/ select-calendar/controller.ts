// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import i18n from 'i18next';
import { DataNode } from 'rc-tree/lib/interface';
import { Observable } from 'rxjs';

import {
  FeiertageDTO,
  FerienDTO,
  KalenderDTO,
  KalenderType,
  SitzungenDTO,
  ZeitplanungControllerApi,
  ZeitplanungLightControllerApi,
} from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme';

import { KalenderUnionDTO } from '../component.react';
export class SelectExternalCalendarController {
  private readonly zeitplanungController = GlobalDI.get<ZeitplanungControllerApi>('zeitplanungRestController');
  private readonly zeitplanungLightControllerApi = GlobalDI.getOrRegister(
    'zeitplanungLightControllerApi',
    () => new ZeitplanungLightControllerApi(),
  );
  public getCalendarCall(): Observable<KalenderUnionDTO[]> {
    return this.zeitplanungController.getKalender();
  }

  public getCalendarLightCall(): Observable<KalenderUnionDTO[]> {
    return this.zeitplanungLightControllerApi.getKalenderLight();
  }

  private createDataNode = (cal: KalenderDTO) => {
    return {
      title: cal.titel,
      key: cal.id,
      value: cal.id,
    };
  };

  private createSitzungeNode = (cal: SitzungenDTO) => {
    return {
      title: `${cal.titel} ${(cal.initiantType as string)[0]}${(cal.initiantType as string).toLowerCase().slice(1)}`,
      key: cal.id,
      value: cal.id,
    };
  };

  public getTreeStructure = (type: KalenderType, cals: KalenderUnionDTO[]): DataNode[] => {
    const structure = [];
    switch (type) {
      case KalenderType.Feiertage: {
        const feiertageAllKey = `feiertage all`;
        structure?.push(
          cals
            ?.filter((cal) => {
              return cal.typ === KalenderType.Feiertage && !(cal as FeiertageDTO).bundeslandType;
            })
            .map((cal: FeiertageDTO) => {
              return this.createDataNode(cal);
            })?.[0],
        );
        structure?.push({
          title: i18n.t(`zeit.zeitplanungenTable.tabs.kalendersicht.selectCalendar.modal.selects.feiertage.allEntries`),
          key: feiertageAllKey,
          value: feiertageAllKey,
          children: cals
            ?.filter((cal) => {
              return cal.typ === KalenderType.Feiertage && (cal as FeiertageDTO).bundeslandType;
            })
            .filter(
              (v: FeiertageDTO, i, a: FeiertageDTO[]) =>
                a.findIndex((t) => t.bundeslandType === v.bundeslandType) === i,
            )
            .map((cal: FeiertageDTO) => {
              return this.createDataNode(cal);
            }),
        });
        break;
      }
      case KalenderType.Ferien: {
        const ferienAllKey = `ferien all`;
        structure?.push({
          title: i18n.t(`zeit.zeitplanungenTable.tabs.kalendersicht.selectCalendar.modal.selects.ferien.allEntries`),
          key: ferienAllKey,
          value: ferienAllKey,
          children: cals
            ?.filter((cal) => {
              return cal.typ === KalenderType.Ferien;
            })
            .filter(
              (v: FeiertageDTO, i, a: FeiertageDTO[]) =>
                a.findIndex((t) => t.bundeslandType === v.bundeslandType) === i,
            )
            .map((cal: FerienDTO) => {
              return this.createDataNode(cal);
            }),
        });
        break;
      }
      case KalenderType.Sitzungen: {
        const sitzungenAllKey = `sitzungen all`;
        structure?.push({
          title: i18n.t(`zeit.zeitplanungenTable.tabs.kalendersicht.selectCalendar.modal.selects.sitzungen.allEntries`),
          key: sitzungenAllKey,
          value: sitzungenAllKey,
          children: cals
            ?.filter((cal) => {
              return cal.typ === KalenderType.Sitzungen;
            })
            .filter((v: SitzungenDTO, i, a: SitzungenDTO[]) => {
              return a.findIndex((t) => t.initiantType === v.initiantType) === i;
            })
            .map((cal: SitzungenDTO) => {
              return this.createSitzungeNode(cal);
            }),
        });
        break;
      }
    }
    return structure;
  };
}
