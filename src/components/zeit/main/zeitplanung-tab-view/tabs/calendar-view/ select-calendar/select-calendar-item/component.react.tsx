// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './select-calendar-item.less';

import { Form, Tag, TreeSelect } from 'antd';
import { CustomTagProps } from 'rc-select/lib/BaseSelect';
import { DataNode } from 'rc-tree/lib/interface';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { KalenderType } from '@plateg/rest-api';
import { CloseOutlined, GlobalDI } from '@plateg/theme';
import { SelectDown } from '@plateg/theme/src/components/icons/SelectDown';

import { KalenderUnionDTO } from '../../component.react';
import { SelectExternalCalendarController } from '../controller';

interface SelectCalendarItemProps {
  title: string;
  name: string;
  calendars: KalenderUnionDTO[];
  types: KalenderType[];
}

export function SelectCalendarItem(props: SelectCalendarItemProps): React.ReactElement {
  const { t } = useTranslation();
  const selectExternalCalendarController = GlobalDI.getOrRegister(
    'selectExternalCalendarController',
    () => new SelectExternalCalendarController(),
  );
  const [treeData, setTreeData] = useState<DataNode[]>();
  const [isTreeSelectOpen, setIsTreeSelectOpen] = useState(false);

  useEffect(() => {
    let data: DataNode[] = [];
    props.types.forEach((type) => {
      data = [...data, ...selectExternalCalendarController.getTreeStructure(type, props.calendars)];
    });
    setTreeData(data);
  }, [props.calendars]);

  return (
    <Form.Item name={props.name} label={props.title}>
      <TreeSelect
        popupClassName="calendar-select-dropdown"
        placeholder={t('zeit.zeitplanungenTable.tabs.kalendersicht.selectCalendar.modal.selects.placeholder')}
        treeData={treeData}
        treeCheckable={true}
        treeNodeFilterProp="title"
        virtual={false}
        suffixIcon={<SelectDown />}
        onDropdownVisibleChange={(open) => {
          setIsTreeSelectOpen(open);
        }}
        onKeyDown={(e) => {
          if (e.code == 'Escape' && isTreeSelectOpen) {
            e.stopPropagation();
          }
        }}
        tagRender={(tagProps: CustomTagProps): React.ReactElement => {
          const { label, closable, onClose } = tagProps;
          return (
            <Tag
              color="blue"
              closable={closable}
              onClose={onClose}
              style={{ marginRight: 3 }}
              closeIcon={<CloseOutlined />}
            >
              {label}
            </Tag>
          );
        }}
      />
    </Form.Item>
  );
}
