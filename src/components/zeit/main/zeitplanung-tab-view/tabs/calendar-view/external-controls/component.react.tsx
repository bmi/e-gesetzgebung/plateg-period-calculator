// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './external-controls.less';

import { addMonths } from 'date-fns';
import React, { useEffect, useState } from 'react';

import { VisibleTime } from '../component.react';
import { DaySelectComponent } from '../day-select/component.react';
import { ScaleSelectComponent } from '../scale-select/component.react';
export interface ExternalControlsComponentProps {
  visibleTime?: VisibleTime;
  setVisibleTime: (visibleTime: VisibleTime) => void;
  defaultScale: number;
  selectScale: (val: number) => void;
}

export function ExternalControlsComponent(props: ExternalControlsComponentProps): React.ReactElement {
  const [selectedScale, setSelectedScale] = useState(props.defaultScale);
  const [customStartDay, setCustomStartDay] = useState<Date>();

  useEffect(() => {
    if (props.visibleTime && selectedScale) {
      props.setVisibleTime({
        start: props.visibleTime?.start,
        end: addMonths(props.visibleTime.start, selectedScale),
      });
      props.selectScale(selectedScale);
    }
  }, [selectedScale]);

  useEffect(() => {
    if (customStartDay && selectedScale) {
      props.setVisibleTime({
        start: customStartDay,
        end: addMonths(customStartDay, selectedScale),
      });
    }
  }, [customStartDay]);

  if (!props.visibleTime) {
    return <></>;
  }

  return (
    <div className="external-calendar-controls">
      <DaySelectComponent
        visibleTime={props.visibleTime}
        selectedScale={selectedScale}
        setCustomStartDay={setCustomStartDay}
      />
      <ScaleSelectComponent defaultScale={selectedScale} setSelectedScale={setSelectedScale} />
    </div>
  );
}
