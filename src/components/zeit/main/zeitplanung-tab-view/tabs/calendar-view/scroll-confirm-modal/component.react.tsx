// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { AjaxError } from 'rxjs/ajax';

import { LoadingStatusController, ModalWrapper } from '@plateg/theme';
import { GlobalDI } from '@plateg/theme/src/shares';

import { ZeitLightLocalController } from '../../../../../../zeit-light/controller-local';
import { ZeitplanungController } from '../../../../home-tabs-zeit/controller';

interface ScrollConfirmModalComponentProps {
  isActiveTab: boolean;
  isLight?: boolean;
}

export function ScrollConfirmModalComponent(props: ScrollConfirmModalComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const zeitplanungController = GlobalDI.getOrRegister('zeitplanungController', () => new ZeitplanungController());
  const zeitLightLocalController = GlobalDI.getOrRegister(
    'zeitLightLocalController',
    () => new ZeitLightLocalController(),
  );
  const [isVisible, setIsVisible] = useState(false);
  const [shouldAsk, setShouldAsk] = useState<boolean>();

  const errorCallback = (error: AjaxError) => {
    console.error(error, error);
  };

  useEffect(() => {
    const apiCall = props.isLight
      ? zeitLightLocalController.getScrollCalendarStatusObservable()
      : zeitplanungController.getIsShowZeitplanungScrollDialog();
    apiCall.subscribe({
      next: (val: boolean) => {
        setShouldAsk(val);
      },
      error: errorCallback,
    });
  }, []);

  useEffect(() => {
    if (props.isActiveTab && shouldAsk) {
      setIsVisible(true);
    }
  }, [props.isActiveTab, shouldAsk]);

  const leaveHandler = (shouldAskAgain: boolean) => {
    setIsVisible(!isVisible);
    setShouldAsk(shouldAskAgain);
    loadingStatusController.setLoadingStatus(false);
  };

  const leaveAndNotAskedAgainListener = () => {
    loadingStatusController.setLoadingStatus(true);
    const apiCall = props.isLight
      ? zeitLightLocalController.setScrollCalendarStatusObservable(false)
      : zeitplanungController.setShowZeitplanungScrollDialog(false);
    apiCall.subscribe({
      next: () => leaveHandler(false),
      error: errorCallback,
    });
  };

  return (
    <>
      {shouldAsk === true && (
        <ModalWrapper
          width="635px"
          key="scroll-confirm-modal"
          open={isVisible}
          closable={false}
          title={<h3>{t('zeit.zeitplanungenTable.tabs.kalendersicht.scrollConfirmModal.title')}</h3>}
          cancelText={t('zeit.zeitplanungenTable.tabs.kalendersicht.scrollConfirmModal.confirm')}
          okText={t('zeit.zeitplanungenTable.tabs.kalendersicht.scrollConfirmModal.confirmDontShowAgain')}
          cancelButtonProps={{ onClick: () => leaveHandler(shouldAsk), type: 'primary' }}
          okButtonProps={{ onClick: leaveAndNotAskedAgainListener, type: 'default' }}
        >
          <p
            dangerouslySetInnerHTML={{
              __html: t('zeit.zeitplanungenTable.tabs.kalendersicht.scrollConfirmModal.content', {
                interpolation: { escapeValue: false },
              }),
            }}
          />
        </ModalWrapper>
      )}
    </>
  );
}
