// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

// make sure you include the timeline stylesheet or the timeline will not be styled
import 'react-calendar-timeline/lib/Timeline.css';
import './zeitplanung-calendar-view.less';

import addMonths from 'date-fns/addMonths';
import React, { useEffect, useState } from 'react';
import Timeline, {
  DateHeader,
  SidebarHeader,
  TimelineHeaders,
  TimelineMarkers,
  TodayMarker,
} from 'react-calendar-timeline';

import { FeiertageDTO, FerienDTO, KalenderDTO, RolleLokalType, SitzungenDTO } from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme';

import { routes } from '../../../../../../shares/routes';
import { NewElementButton } from '../../../general-zeitelement/ new-element-button/component.react';
import {
  DeleteConfirmModalComponent,
  DeleteConfirmModalPropsInterface,
  ZeitplanungDeleteInterface,
} from '../../../general-zeitelement/delete-confirm-modal/component.react';
import { ZeitplanungsElement } from '../../../general-zeitelement/new-time-element/new-element/component.react';
import { ZeitplanungTableViewComponentProps } from '../table-view/component.react';
import { SelectCalendar } from './ select-calendar/component.react';
import { CalendarController, CalendarItem } from './controller.react';
import { ExternalControlsComponent } from './external-controls/component.react';
import { ScrollConfirmModalComponent } from './scroll-confirm-modal/component.react';

export type KalenderUnionDTO = KalenderDTO | FeiertageDTO | FerienDTO | SitzungenDTO;

export interface VisibleTime {
  start: Date;
  end: Date;
}

interface ZeitplanungCalendarViewComponentProps extends ZeitplanungTableViewComponentProps {
  isActiveTab: boolean;
  initialCalenders: KalenderUnionDTO[];
  isLight?: boolean;
}

export interface CalGroup {
  id: string;
  title: React.ReactElement;
  isExternalGroup?: boolean;
  isWichtig?: boolean;
}
export function ZeitplanungCalendarViewComponent(props: ZeitplanungCalendarViewComponentProps): React.ReactElement {
  const [groups, setGroups] = useState<CalGroup[]>([]);
  const [dirtyFlag, setDirtyFlag] = useState<number>(0);
  const [items, setItems] = useState<CalendarItem[]>([]);
  const calendarCtrl = GlobalDI.getOrRegister('ezeitCalendarCtrl', () => new CalendarController());
  calendarCtrl.initalizeIsDirtyFlag(setDirtyFlag);
  const [isReadOnly, setIsReadOnly] = useState<boolean>(false);
  const [deleteConfirmModalProps, setDeleteConfirmModalProps] = useState<DeleteConfirmModalPropsInterface>({
    isVisible: false,
    target: {} as ZeitplanungDeleteInterface,
  });
  const [externalCalendars, setExternalCalendars] = useState<KalenderUnionDTO[]>([]);
  const [visibleTime, setVisibleTime] = useState<VisibleTime>();
  const [headerOffset, setHeaderOffset] = useState(0);
  const defaultScale = 3;
  const calendarControlsHeight = 56;
  const [selectedScale, setSelectedScale] = useState(defaultScale);

  useEffect(() => {
    const setOffset = () => setHeaderOffset(calculateHeaderOffset());
    setOffset();
    window.addEventListener('resize', setOffset);
    return () => {
      window.removeEventListener('resize', setOffset);
    };
  }, []);

  useEffect(() => {
    if (props.zeitplanung) {
      const externalCals = calendarCtrl.createExternalItemsAndGroups(externalCalendars, setExternalCalendars);
      const tmpGroups = calendarCtrl.createGroups(props.zeitplanung?.dto?.elemente, 0);
      const tmpItems = calendarCtrl.createItems(props.zeitplanung?.dto?.elemente || []);
      setGroups([...externalCals.groups, ...tmpGroups]);
      setItems([...tmpItems, ...externalCals.items]);
      setIsReadOnly(props.zeitplanung.dto.readOnlyAccess);
    }
  }, [props.zeitplanung, dirtyFlag, externalCalendars]);

  useEffect(() => {
    if (props.zeitplanung) {
      const tmpItems = calendarCtrl.createItems(props.zeitplanung?.dto?.elemente || []);
      const start = calendarCtrl.getZeitplanungStart(props.zeitplanung?.dto?.elemente || []);
      // Clear Timeline items and visible time - trick for reinit Timeline(bug in Firefox with wrong render)
      setItems([]);
      setVisibleTime(undefined);
      // Use timeout to avoid wrong render calendar in Firefox after calendar item deletion
      setTimeout(() => {
        setVisibleTime({ start: start, end: addMonths(start, defaultScale) });
        setItems(tmpItems);
      }, 10);
    }
  }, [props.zeitplanung]);

  const calculateHeaderOffset = () => {
    const zeitHeaderRect = document.getElementById('zeit-header')?.getBoundingClientRect();
    return window.outerWidth <= 768 ? 0 : (zeitHeaderRect?.height || 0) + (zeitHeaderRect?.top || 0);
  };

  const getWeekMonthYearHeaderRow = (selectedScale: number, component: string) => {
    switch (component) {
      case 'week':
        return (
          <>
            <DateHeader unit="week" labelFormat="ww" className={`secondaryHeader weekSmall scale-${selectedScale}`} />
            <DateHeader unit="week" labelFormat="K\W ww" className={`secondaryHeader weekBig scale-${selectedScale}`} />
          </>
        );
      case 'month':
        return (
          <>
            <DateHeader unit="month" labelFormat="MMM YY" className={`primaryHeader month scale-${selectedScale}`} />
            <DateHeader unit="month" labelFormat="MMM" className={`primaryHeader monthSmall scale-${selectedScale}`} />
          </>
        );
      case 'year':
        return <DateHeader unit="year" labelFormat="YYYY" className={`primaryHeader year scale-${selectedScale}`} />;
      default:
        return <></>;
    }
  };
  return (
    <>
      <ScrollConfirmModalComponent isActiveTab={props.isActiveTab} isLight={props.isLight} />
      <DeleteConfirmModalComponent
        isVisible={deleteConfirmModalProps?.isVisible}
        target={deleteConfirmModalProps?.target}
        vorhabenart={props.zeitplanung?.dto.regelungsvorhaben?.dto.vorhabenart}
        setIsVisible={(isVisible: boolean) => {
          setDeleteConfirmModalProps({ isVisible: isVisible, target: deleteConfirmModalProps?.target });
        }}
        reloadData={props.reloadData}
        isZeitplanung={true}
        isLight={props.isLight}
      />

      {visibleTime && (
        <div className="external-calendar-holder">
          <div className="calendar-header" style={{ top: headerOffset, height: calendarControlsHeight }}>
            <div className="calendar-header-left">
              <ExternalControlsComponent
                visibleTime={visibleTime}
                setVisibleTime={setVisibleTime}
                defaultScale={defaultScale}
                selectScale={(val) => setSelectedScale(val)}
              />
            </div>
            <div className="calendar-header-right">
              <SelectCalendar
                externalCalendars={externalCalendars}
                setExternalCalendars={setExternalCalendars}
                initialCalenders={props.initialCalenders}
              />
              {props.zeitplanung && (
                <NewElementButton
                  disabled={props.zeitplanung.dto.readOnlyAccess}
                  path={`/zeit/${routes.MEINE_ZEITPLANUNGEN}/${props.zeitplanung?.base.id}/${routes.KALENDERSICHT}/${routes.NEUES_ELEMENT}`}
                />
              )}
            </div>
          </div>
          <Timeline
            traditionalZoom={false}
            groups={groups}
            items={items}
            lineHeight={48}
            canChangeGroup={false}
            sidebarWidth={400}
            canResize={false}
            canMove={false}
            minZoom={visibleTime.end.getTime() - visibleTime.start.getTime()}
            maxZoom={visibleTime.end.getTime() - visibleTime.start.getTime()}
            itemRenderer={(itemProps) =>
              calendarCtrl.renderCustomItem(
                itemProps,
                isReadOnly ||
                  ((itemProps.item.itemData.dto as ZeitplanungsElement)?.gesperrt &&
                    props.zeitplanung?.dto.rolleTyp !== RolleLokalType.Federfuehrer),
                setDeleteConfirmModalProps,
                props.isLight || false,
              )
            }
            visibleTimeStart={visibleTime?.start.valueOf()}
            visibleTimeEnd={visibleTime?.end.valueOf()}
            onTimeChange={(visibleTimeStart: number, visibleTimeEnd: number) => {
              if (!(isNaN(visibleTimeStart) || isNaN(visibleTimeEnd))) {
                setVisibleTime({ start: new Date(visibleTimeStart), end: new Date(visibleTimeEnd) });
              }
            }}
            horizontalLineClassNamesForGroup={(group: CalGroup) => {
              const externalGroups = groups.filter((entry) => {
                return entry.isExternalGroup === true;
              });
              const classes = [];
              if (group.isExternalGroup) {
                classes.push('external-group-row');
                if (
                  externalGroups.findIndex((entry) => {
                    return entry.id === group.id;
                  }) === 0
                ) {
                  classes.push('first-external-group-row');
                }
                if (
                  externalGroups.findIndex((entry) => {
                    return entry.id === group.id;
                  }) +
                    1 ===
                  externalGroups.length
                ) {
                  classes.push('last-external-group-row');
                }
              }
              if (group.isWichtig) {
                classes.push('wichtig-row');
              }
              return classes;
            }}
          >
            <TimelineMarkers>
              <TodayMarker date={Date.now()}>
                {({ styles }) => {
                  const customStyles = {
                    ...styles,
                    backgroundColor: '#D60B0B',
                    width: '3px',
                  };
                  return <div style={customStyles} />;
                }}
              </TodayMarker>
            </TimelineMarkers>
            <TimelineHeaders className="calendar-view-header" style={{ top: headerOffset + calendarControlsHeight }}>
              <SidebarHeader>
                {({ getRootProps }) => {
                  return <div {...getRootProps()} className="sidebar-header" />;
                }}
              </SidebarHeader>
              {getWeekMonthYearHeaderRow(selectedScale, 'year')}
              {getWeekMonthYearHeaderRow(selectedScale, 'month')}
              {getWeekMonthYearHeaderRow(selectedScale, 'week')}
            </TimelineHeaders>
          </Timeline>
        </div>
      )}
    </>
  );
}
