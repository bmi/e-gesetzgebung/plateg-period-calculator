// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './zeitplanung-table-view.less';

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { ZeitplanungEntityResponseDTO } from '@plateg/rest-api';
import { TableComponent, TableComponentProps } from '@plateg/theme';
import { GlobalDI } from '@plateg/theme/src/shares';

import { routes } from '../../../../../../shares/routes';
import { NewElementButton } from '../../../general-zeitelement/ new-element-button/component.react';
import {
  DeleteConfirmModalComponent,
  DeleteConfirmModalPropsInterface,
  ZeitplanungDeleteInterface,
} from '../../../general-zeitelement/delete-confirm-modal/component.react';
import { ExternKalender } from '../../../general-zeitelement/new-time-element/new-element/sections/start-date/extern-kalender/component.react';
import { ZeitplanungRow, ZeitplanungTableViewController } from './controller.react';

export interface ZeitplanungTableViewComponentProps {
  zeitplanung?: ZeitplanungEntityResponseDTO;
  reloadData: () => void;
  isLight?: boolean;
}

export function ZeitplanungTableViewComponent(props: ZeitplanungTableViewComponentProps): React.ReactElement {
  const zeitplanungTableViewController = GlobalDI.getOrRegister(
    'zeitplanungTableViewController',
    () => new ZeitplanungTableViewController(),
  );
  const { t } = useTranslation();
  const [zeitplanung, setZeitplanung] = useState(props.zeitplanung);
  const [phasenTableVals, setPhasenTableVals] = useState<Omit<TableComponentProps<ZeitplanungRow>, 'id'>>({
    columns: [],
    content: [],
    expandable: false,
    sorterOptions: [],
    customDefaultSortIndex: 0,
    expandableCondition: undefined,
    className: '',
  });

  const [deleteConfirmModalProps, setDeleteConfirmModalProps] = useState<DeleteConfirmModalPropsInterface>({
    isVisible: false,
    target: {} as ZeitplanungDeleteInterface,
  });

  useEffect(() => {
    setZeitplanung(props.zeitplanung);
  }, [props.zeitplanung]);

  useEffect(() => {
    if (zeitplanung) {
      setPhasenTableVals(
        zeitplanungTableViewController.getZeitplanungTableVals(
          zeitplanung,
          zeitplanung.dto.readOnlyAccess,
          setDeleteConfirmModalProps,
          setZeitplanung,
          props.isLight,
        ),
      );
    }
  }, [zeitplanung]);

  return (
    <div className="zeitplanung-table">
      <>
        <DeleteConfirmModalComponent
          isVisible={deleteConfirmModalProps?.isVisible}
          target={deleteConfirmModalProps?.target}
          vorhabenart={zeitplanung?.dto.regelungsvorhaben?.dto.vorhabenart}
          setIsVisible={(isVisible: boolean) => {
            setDeleteConfirmModalProps({ isVisible: isVisible, target: deleteConfirmModalProps?.target });
          }}
          reloadData={props.reloadData}
          isZeitplanung={true}
          isLight={props.isLight}
        />
        <TableComponent
          id="period-zeitplanung-view-table"
          columns={phasenTableVals.columns}
          content={phasenTableVals.content}
          filteredColumns={phasenTableVals.filteredColumns}
          sorterOptions={phasenTableVals.sorterOptions}
          customDefaultSortIndex={phasenTableVals.customDefaultSortIndex}
          className={phasenTableVals.className}
          expandable={phasenTableVals.expandable}
          expandableCondition={phasenTableVals.expandableCondition}
          expandedRowRender={phasenTableVals.expandedRowRender}
          expandAllRowsButtonIsVisible={true}
          expandAllRowsButtonCustomTexts={{
            open: t('zeit.newElementGeneral.expandAllRows'),
            close: t('zeit.newElementGeneral.closeAllRows'),
          }}
          rowClassName={(record: ZeitplanungRow) => {
            return record.wichtig ? 'important-row' : '';
          }}
          hiddenRowCount={true}
          customButtonAriaLables={{
            opened: t('zeit.zeitplanungenTable.tabs.tabellensicht.ariaLabels.buttonOpened'),
            closed: t('zeit.zeitplanungenTable.tabs.tabellensicht.ariaLabels.buttonClosed'),
          }}
          additionalRightControl={
            zeitplanung && (
              <>
                <NewElementButton
                  disabled={zeitplanung.dto.readOnlyAccess}
                  path={`/zeit/${routes.MEINE_ZEITPLANUNGEN}/${zeitplanung?.base.id}/${routes.TABELLENSICHT}/${routes.NEUES_ELEMENT}`}
                />
                <ExternKalender isLight={props.isLight} />
              </>
            )
          }
          isLight={props.isLight}
        />
      </>
    </div>
  );
}
