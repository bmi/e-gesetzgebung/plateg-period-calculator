// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { ColumnsType } from 'antd/lib/table';
import Text from 'antd/lib/typography/Text';
import { createHashHistory } from 'history';
import i18n from 'i18next';
import React, { ReactElement } from 'react';

import {
  PhaseEntityDTO,
  PhaseEntityResponseDTO,
  RolleLokalType,
  TerminEntityDTO,
  TerminEntityResponseDTO,
  ZeitplanungEntityResponseDTO,
  ZeitplanungselementEntityDTO,
  ZeitplanungselementEntityResponseDTOZeitplanungselementEntityDTO,
  ZeitplanungselementHinweisType,
} from '@plateg/rest-api';
import { CommonRow, displayMessage, GlobalDI, LoadingStatusController, TableComponentProps } from '@plateg/theme';
import {
  DropdownMenu,
  DropdownMenuItem,
} from '@plateg/theme/src/components/table-component/table-sub-components/dropdown-button-component/component.react';
import { Filters } from '@plateg/theme/src/shares/filters';

import { routes } from '../../../../../../shares/routes';
import { DeleteConfirmModalPropsInterface } from '../../../general-zeitelement/delete-confirm-modal/component.react';
import { NewZeitElementController } from '../../../general-zeitelement/new-time-element/new-element/controller';
import { TabellensichtController } from '../../../general-zeitelement/tabellensicht/controller.react';

export interface ZeitplanungRow extends CommonRow {
  titel: string;
  isTermin: boolean;
  wichtig: boolean;
  gesperrt: boolean;
  geplanterZeitraum?: string;
  frist?: string;
  infoKey?: string;
  children?: ZeitplanungRow[];
  hinweise: ZeitplanungselementHinweisType[];
  parentId?: string;
  isLight?: boolean;
  level?: number;
}

type SearchResult = {
  item: ZeitplanungselementEntityResponseDTOZeitplanungselementEntityDTO | null;
};

export class ZeitplanungTableViewController {
  private readonly zeitraumRenderer = (record: ZeitplanungRow): ReactElement => {
    return record.isTermin ? <></> : <Text>{record.geplanterZeitraum}</Text>;
  };

  private readonly fristRenderer = (record: ZeitplanungRow): ReactElement => {
    return record.isTermin ? <Text>{record.frist}</Text> : <></>;
  };

  private readonly newZeitElementController = GlobalDI.getOrRegister(
    'newZeitElementController',
    () => new NewZeitElementController(),
  );

  private readonly loadingstatusController = GlobalDI.getOrRegister(
    'loadingStatusController',
    () => new LoadingStatusController(),
  );

  private readonly dropdownRenderer = (
    record: ZeitplanungRow,
    readOnlyAccess: boolean,
    setDeleteConfirmModalProps: (openConfirmModalProps: DeleteConfirmModalPropsInterface) => void,
    content: ZeitplanungEntityResponseDTO,
    setZeitplanung: (content: ZeitplanungEntityResponseDTO) => void,
    isLight?: boolean,
  ): ReactElement => {
    const history = createHashHistory();
    const items: DropdownMenuItem[] = [];

    const bearbeitenAction = {
      element: i18n.t('zeit.zeitplanungenTable.tabs.tabellensicht.actions.bearbeiten'),
      onClick: () => {
        history.push(`${routes.TABELLENSICHT}/${routes.ELEMENT_BEARBEITEN}/${record.id}/`);
      },
      disabled: () => readOnlyAccess,
    };
    items.push(bearbeitenAction);

    const finishCallbackLockAction = () => {
      const successMsg = i18n.t(
        `zeit.zeitplanungsvorlagenTable.tabs.tabellensicht.actions.msgs.${!record.gesperrt ? 'locked' : 'unlocked'}`,
        { name: record.titel, type: record.isTermin ? 'Der Termin' : 'Die Phase' },
      );
      const elementById = this.getElementById(record.id, content.dto.elemente);
      if (elementById) {
        elementById.dto.gesperrt = !elementById.dto.gesperrt;
        if (!elementById.dto.termin) {
          const phasenElement = elementById.dto as PhaseEntityDTO;
          updateUntergeordneteEl(phasenElement.untergeordneteElemente, elementById.dto.gesperrt);
        }
      }
      setZeitplanung({ ...content });
      displayMessage(successMsg, 'success');
      this.loadingstatusController.setLoadingStatus(false);
    };

    const updateUntergeordneteEl = (
      elements: ZeitplanungselementEntityResponseDTOZeitplanungselementEntityDTO[] | undefined,
      gesperrtVal: boolean,
    ) => {
      if (elements && elements.length > 0) {
        elements.forEach((el) => {
          el.dto.gesperrt = gesperrtVal;

          if (!el.dto.termin) {
            const phaseElement = el.dto as PhaseEntityDTO;
            updateUntergeordneteEl(phaseElement.untergeordneteElemente, gesperrtVal);
          }
        });
      }
    };

    const lockActionHandler = () => {
      const foundElement = this.getElementById(record.id, content.dto.elemente);
      const element = { ...(foundElement?.dto as ZeitplanungselementEntityDTO), gesperrt: !foundElement?.dto.gesperrt };
      this.newZeitElementController.saveElement(
        finishCallbackLockAction,
        true,
        record.id,
        element,
        isLight,
        record.parentId,
      );
    };

    if (!record.isLight && content.dto.rolleTyp === RolleLokalType.Federfuehrer && record.level == 0) {
      const elementName = record.gesperrt
        ? i18n.t('zeit.zeitplanungenTable.tabs.tabellensicht.actions.gesperrt')
        : i18n.t('zeit.zeitplanungenTable.tabs.tabellensicht.actions.nichtGesperrt');
      const lockAction = {
        element: elementName,
        onClick: lockActionHandler,
        disabled: () => readOnlyAccess,
      };
      items.push(lockAction);
    }

    const finishCallbackImportantAction = () => {
      const successMsg = i18n.t(
        `zeit.zeitplanungsvorlagenTable.tabs.tabellensicht.actions.msgs.${
          !record.wichtig ? 'important' : 'unimportant'
        }`,
        { name: record.titel },
      );
      const terminById = this.getElementById(record.id, content.dto.elemente);
      if (terminById) {
        terminById.dto.wichtig = !terminById.dto.wichtig;
      }
      setZeitplanung({ ...content });
      displayMessage(successMsg, 'success');
      this.loadingstatusController.setLoadingStatus(false);
    };

    const importantActionHandler = () => {
      const foundTermin = this.getElementById(record.id, content.dto.elemente);
      const element = { ...(foundTermin?.dto as TerminEntityDTO), wichtig: !foundTermin?.dto.wichtig };
      this.newZeitElementController.saveElement(
        finishCallbackImportantAction,
        true,
        record.id,
        element,
        isLight,
        record.parentId,
      );
    };

    if (record.isTermin) {
      const elementName = record.wichtig
        ? i18n.t('zeit.zeitplanungenTable.tabs.tabellensicht.actions.notImportant')
        : i18n.t('zeit.zeitplanungenTable.tabs.tabellensicht.actions.important');
      const importantAction = {
        element: elementName,
        disabled: () => readOnlyAccess,
        onClick: importantActionHandler,
      };
      items.push(importantAction);
    }

    const deleteAction = {
      element: i18n.t('zeit.zeitplanungenTable.tabs.tabellensicht.actions.delete'),
      onClick: () => {
        setDeleteConfirmModalProps({
          isVisible: true,
          target: {
            id: record.id,
            titel: record.titel,
            isTermin: record.isTermin,
            infoKey: record.infoKey,
            parentId: record.parentId as string,
          },
        });
      },
      disabled: () => readOnlyAccess,
    };
    items.push(deleteAction);

    return (
      <>
        <DropdownMenu
          placement="bottomLeft"
          openLink={`${routes.TABELLENSICHT}/${routes.UEBERSICHT}/${record.id}`}
          items={items}
          elementId={record.id}
        />
      </>
    );
  };

  private readonly createZeitplanungRows = (
    content: ZeitplanungselementEntityResponseDTOZeitplanungselementEntityDTO[],
    parentId: string,
    isLight?: boolean,
    level = 0,
  ): ZeitplanungRow[] => {
    return content.map((element) => {
      const isTermin = element.dto.termin;
      return {
        id: element.base.id,
        isTermin: isTermin,
        titel: element.dto.titel,
        wichtig: isTermin ? (element as TerminEntityResponseDTO).dto.wichtig : false,
        gesperrt: element.dto.gesperrt,
        geplanterZeitraum: isTermin
          ? undefined
          : `${Filters.dateFromString((element as PhaseEntityResponseDTO).dto.beginn)} – ${Filters.dateFromString(
              (element as PhaseEntityResponseDTO).dto.ende,
            )}`,
        frist: isTermin ? `${Filters.dateFromString((element as TerminEntityResponseDTO).dto.datum)}` : undefined,
        children:
          isTermin || !(element as PhaseEntityResponseDTO).dto.untergeordneteElemente?.length
            ? undefined
            : this.createZeitplanungRows(
                (element as PhaseEntityResponseDTO).dto
                  .untergeordneteElemente as ZeitplanungselementEntityResponseDTOZeitplanungselementEntityDTO[],
                parentId,
                isLight,
                level + 1,
              ),
        level: level,
        infoKey: element.dto.bauplan,
        hinweise: element.dto.hinweise,
        parentId: parentId,
        isLight: isLight,
      };
    });
  };

  public getZeitplanungTableVals(
    content: ZeitplanungEntityResponseDTO,
    readOnlyAccess: boolean,
    setDeleteConfirmModalProps: (openConfirmModalProps: DeleteConfirmModalPropsInterface) => void,
    setZeitplanung: (content: ZeitplanungEntityResponseDTO) => void,
    isLight?: boolean,
  ): Omit<TableComponentProps<ZeitplanungRow>, 'id'> {
    const ctrl = GlobalDI.getOrRegister('tabellensichtController', () => new TabellensichtController());
    const rows = this.createZeitplanungRows(content.dto.elemente, content.base.id, isLight);
    const columns: ColumnsType<ZeitplanungRow> = [
      {
        title: i18n.t('zeit.zeitplanungenTable.tabs.tabellensicht.tableHeads.phasenUndTermine').toString(),
        key: 'c1',
        fixed: 'left',
        className: 'title-column',
        render: ctrl.titelRenderer,
      },
      {
        title: i18n.t('zeit.zeitplanungenTable.tabs.tabellensicht.tableHeads.geplanterZeitraum').toString(),
        key: 'c2',
        render: this.zeitraumRenderer,
      },
      {
        title: i18n.t('zeit.zeitplanungenTable.tabs.tabellensicht.tableHeads.frist').toString(),
        key: 'c3',
        render: this.fristRenderer,
      },
      {
        title: i18n.t('zeit.zeitplanungenTable.tabs.tabellensicht.tableHeads.eigenschaften').toString(),
        key: 'c4',
        render: ctrl.eigenschaftenRenderer,
      },
      {
        title: i18n.t('zeit.zeitplanungenTable.tabs.tabellensicht.tableHeads.aktionen').toString(),
        key: 'c5',
        render: (record: ZeitplanungRow): ReactElement => {
          return this.dropdownRenderer(
            record,
            readOnlyAccess || (record.gesperrt && content.dto.rolleTyp !== RolleLokalType.Federfuehrer),
            setDeleteConfirmModalProps,
            content,
            setZeitplanung,
            isLight,
          );
        },
      },
    ];

    return {
      expandable: true,
      expandableCondition: (record: ZeitplanungRow) => {
        if (record.children) {
          return true;
        }
        return false;
      },
      columns,
      content: rows,
      className: 'hidden-expanded-rows',
    };
  }

  private findItemById(
    targetId: string,
    items: ZeitplanungselementEntityResponseDTOZeitplanungselementEntityDTO[],
  ): SearchResult {
    let result: SearchResult = { item: null };

    items.forEach((currentItem) => {
      if (currentItem.base.id === targetId) {
        result = { item: currentItem };
        return;
      }

      if ('untergeordneteElemente' in currentItem.dto) {
        const nestedResult = this.findItemById(
          targetId,
          currentItem.dto.untergeordneteElemente as ZeitplanungselementEntityResponseDTOZeitplanungselementEntityDTO[],
        );
        if (nestedResult.item !== null) {
          result = nestedResult;
          return;
        }
      }
    });

    return result;
  }

  private getElementById(
    targetId: string,
    items: ZeitplanungselementEntityResponseDTOZeitplanungselementEntityDTO[],
  ): TerminEntityResponseDTO | null {
    const searchResult = this.findItemById(targetId, items);
    return searchResult.item !== null ? (searchResult.item as TerminEntityResponseDTO) : null;
  }
}
