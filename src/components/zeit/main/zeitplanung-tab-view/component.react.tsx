// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../zeitplanungsvorlage-tab-view/zeitplanungsvoralage-tab-view.less';

import { Button, Col, Row, TabsProps } from 'antd';
import Title from 'antd/lib/typography/Title';
import React, { Fragment, useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Route, Switch, useHistory, useRouteMatch } from 'react-router-dom';
import { forkJoin } from 'rxjs';
import { AjaxError } from 'rxjs/ajax';

import { AktionType, ZeitplanungEntityResponseDTO } from '@plateg/rest-api';
import {
  BreadcrumbComponent,
  BtnBackComponent,
  DropdownMenu,
  ErrorController,
  HeaderController,
  LoadingStatusController,
  TabsWrapper,
  TitleWrapperComponent,
} from '@plateg/theme';
import { GlobalDI } from '@plateg/theme/src/shares';

import { routes } from '../../../../shares/routes';
import { ZeitLightCollectionsKeys, ZeitLightLocalController } from '../../../zeit-light/controller-local';
import { ZeitHelpLink } from '../../component.react';
import { NewZeitplanungsvorlageElement } from '../general-zeitelement/new-time-element/component.react';
import { SaveInfoComponent } from '../general-zeitelement/save-info/component.react';
import { ZeitplanungController } from '../home-tabs-zeit/controller';
import { ZeitFreigabeModalRBAC } from '../home-tabs-zeit/freigabe-rbac-modal/component.react';
import { SpeichernAlsZeitplanungsvorlageComponent } from '../new-zeitplanungsvorlage-modal/speichern-als-zeitplanungvorlage/component.react';
import { TabText } from '../zeitplanungsvorlage-tab-view/header/tab-text/component.react';
import { CommentsContainerComponent } from './comments/component.react';
import { BilligungStatus } from './header/billigung-status-element/component.react';
import { ParentLink } from './header/parent-link/component.react';
import { ZeitplanungsLink } from './header/zeitplanung-link/component.react';
import { NoParticipantErrorPageComponent } from './no-participant-error-page/component.react';
import { SelectExternalCalendarController } from './tabs/calendar-view/ select-calendar/controller';
import { KalenderUnionDTO, ZeitplanungCalendarViewComponent } from './tabs/calendar-view/component.react';
import { ZeitplanungTableViewComponent } from './tabs/table-view/component.react';

export function ZeitplanungenTabViewComponent(props: Readonly<{ isLight?: boolean }>): React.ReactElement {
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const headerController = GlobalDI.getOrRegister('ezeitHeaderController', () => new HeaderController());
  const selectExternalCalendarController = GlobalDI.getOrRegister(
    'selectExternalCalendarController',
    () => new SelectExternalCalendarController(),
  );
  const zeitplanungController = GlobalDI.getOrRegister('zeitplanungController', () => new ZeitplanungController());
  const zeitLightLocalController = GlobalDI.getOrRegister(
    'zeitLightLocalController',
    () => new ZeitLightLocalController(),
  );
  const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  const { t } = useTranslation();
  const history = useHistory();
  const routeMatcher = useRouteMatch<{ archiv?: string; id: string; tabKey: string; action?: string }>(
    `/zeit/:archiv?/${routes.MEINE_ZEITPLANUNGEN}/:id/:tabKey/:action?`,
  );
  const id = routeMatcher?.params.id as string;
  const isKalendersicht = routeMatcher?.params?.tabKey === 'kalendersicht';
  const [activeTab, setActiveTab] = useState<string>(routeMatcher?.params?.tabKey ?? routes.TABELLENSICHT);
  const [zeitplanung, setZeitplanung] = useState<ZeitplanungEntityResponseDTO>();
  const [calendars, setCalendars] = useState<KalenderUnionDTO[]>();
  const [isSpeichernAlsVorlageModalVisible, setIsSpeichernAlsVorlageModalVisible] = useState<boolean>(false);
  const [isTeilnehmer, setIsTeilnehmer] = useState<boolean>(true);
  const speichernAlsVorlageButtonRef = useRef<HTMLButtonElement>(null);

  useEffect(() => {
    loadData();
  }, [id]);

  useEffect(() => {
    const tabKey: string = routeMatcher?.params?.tabKey as string;
    if (tabKey !== activeTab) {
      setActiveTab(tabKey);
    }
  }, [routeMatcher?.params?.tabKey]);

  useEffect(() => {
    if (zeitplanung) setBreadcrumb(zeitplanung);
  }, [activeTab, zeitplanung]);
  useEffect(() => {
    if (!routeMatcher?.params?.action && zeitplanung) {
      setBreadcrumb(zeitplanung);
    }
  }, [routeMatcher?.params?.action, zeitplanung]);

  useEffect(() => {
    speichernAlsVorlageButtonRef.current?.focus();
  }, [isSpeichernAlsVorlageModalVisible]);

  const errorHandler = (error: AjaxError) => {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
    if (error?.response?.status === 1006) {
      setIsTeilnehmer(false);
    } else {
      errorCtrl.displayErrorMsg(error, 'zeit.generalErrorMsg');
    }
    console.log('Error sub', error);
    loadingStatusController.setLoadingStatus(false);
  };

  const loadData = (redirectURL?: string) => {
    loadingStatusController.setLoadingStatus(true);
    const apiCall = props.isLight
      ? zeitLightLocalController.getItemByIDObservable(id, ZeitLightCollectionsKeys.Zeitplanung)
      : zeitplanungController.getZeitplanungCall(id);
    const calendarCall = props.isLight
      ? selectExternalCalendarController.getCalendarLightCall()
      : selectExternalCalendarController.getCalendarCall();
    const subscription = forkJoin([apiCall, calendarCall]).subscribe({
      next: (data) => {
        setZeitplanung(data[0] as ZeitplanungEntityResponseDTO);
        setIsTeilnehmer(data[0] !== undefined);
        setCalendars(data[1]);
        loadingStatusController.setLoadingStatus(false);
        if (redirectURL) {
          history.push(redirectURL);
        }
      },
      error: errorHandler,
    });
    return function cleanup() {
      subscription.unsubscribe();
    };
  };

  const setBreadcrumb = (zPlanung: ZeitplanungEntityResponseDTO) => {
    const usersList = (
      <ZeitFreigabeModalRBAC
        disabled={!zPlanung.dto.aktionen?.includes(AktionType.BerechtigungenAendern)}
        objectTitle={zPlanung.dto.regelungsvorhaben?.dto.kurzbezeichnung}
        objectId={zPlanung.dto.regelungsvorhaben?.base.id as string}
        key="usersList-component"
      />
    );

    const isArchive = routeMatcher?.params.archiv;
    const parentTabLinkUrl = `/zeit/${isArchive ? routes.ARCHIV : routes.MEINE_ZEITPLANUNGEN}`;
    const parentTab = (
      <ParentLink keyTab={isArchive ? routes.ARCHIV : routes.MEINE_ZEITPLANUNGEN} url={parentTabLinkUrl} />
    );
    const zeitplanungsLink = (
      <ZeitplanungsLink key="zeit-tabellensicht">
        <>
          {zeitplanung?.dto.titel} - <TabText key="zeit-tab" activeTab={activeTab} originalTable="zeitplanungenTable" />
        </>
      </ZeitplanungsLink>
    );

    headerController.setHeaderProps({
      headerLeft: [<BreadcrumbComponent key="breadcrumb" items={[parentTab, zeitplanungsLink]} />],
      headerLast: [props.isLight ? <></> : usersList, <ZeitHelpLink key="zeit-help-link" />],
      headerCenter: [],
      headerRight: [
        <SaveInfoComponent key="edit-information" zeitplanungBase={zeitplanung} />,
        !isKalendersicht ? (
          <Button
            id="ezeit-zeitplanungenTableHeaderExport-btn"
            key="ezeit-zeitplanungenTableHeaderExport-btn"
            onClick={() => {
              zeitplanungController.exportZeitplanung(zeitplanung?.dto, false);
            }}
          >
            {t('zeit.header.exportButton')}
          </Button>
        ) : (
          <Fragment key="no-ezeit-zeitplanungenTableHeaderExport-btn"></Fragment>
        ),
        /* Hide button for light version*/
        props.isLight ? (
          <Fragment key="no-light-ezeit-zeitplanungenTableHeaderSpeichern-btn"></Fragment>
        ) : (
          <Button
            id="ezeit-zeitplanungenTableHeaderSpeichern-btn"
            key="speichern-button"
            onClick={() => setIsSpeichernAlsVorlageModalVisible(true)}
            ref={speichernAlsVorlageButtonRef}
          >
            {t('zeit.zeitplanungenTable.header.btnSpeichern')}
          </Button>
        ),
        <DropdownMenu
          key="header-right-dropdown"
          items={
            !isKalendersicht
              ? [
                  {
                    element: t('zeit.header.exportButton'),
                    onClick: () => {
                      zeitplanungController.exportZeitplanung(zeitplanung?.dto, false);
                    },
                  },
                  {
                    element: t('zeit.zeitplanungenTable.header.btnSpeichern'),
                    onClick: () => setIsSpeichernAlsVorlageModalVisible(true),
                  },
                ]
              : [
                  {
                    element: t('zeit.zeitplanungenTable.header.btnSpeichern'),
                    onClick: () => setIsSpeichernAlsVorlageModalVisible(true),
                  },
                ]
          }
          elementId={'headerRightAlternative'}
          overlayClass={'headerRightAlternative-overlay'}
        />,
      ],
    });
  };

  if (!isTeilnehmer) {
    return <NoParticipantErrorPageComponent />;
  }
  const tabItems: TabsProps['items'] = [
    {
      key: routes.KALENDERSICHT,
      label: t('zeit.zeitplanungenTable.tabs.kalendersicht.tabNav'),
      className: 'no-border-tabpane',
      children: (
        <>
          {zeitplanung && calendars && (
            <ZeitplanungCalendarViewComponent
              zeitplanung={zeitplanung}
              reloadData={loadData}
              isActiveTab={activeTab === routes.KALENDERSICHT}
              initialCalenders={calendars}
              isLight={props.isLight}
            />
          )}
        </>
      ),
    },
    {
      key: routes.TABELLENSICHT,
      label: t('zeit.zeitplanungenTable.tabs.tabellensicht.tabNav'),
      children: (
        <ZeitplanungTableViewComponent zeitplanung={zeitplanung} reloadData={loadData} isLight={props.isLight} />
      ),
    },
  ];
  if (!props.isLight && tabItems.length === 2) {
    tabItems.push({
      key: routes.KOMMENTARE,
      label: t('zeit.zeitplanungenTable.tabs.kommentare.tabNav'),
      children: zeitplanung ? (
        <CommentsContainerComponent zeitItemId={zeitplanung.base.id} isReadOnly={zeitplanung.dto.readOnlyAccess} />
      ) : undefined,
    });
  }
  return (
    <Switch>
      <Route
        path={[
          `/zeit/${routes.MEINE_ZEITPLANUNGEN}/:id/(${routes.TABELLENSICHT}|${routes.KALENDERSICHT})/${routes.NEUES_ELEMENT}`,
          `/zeit/${routes.MEINE_ZEITPLANUNGEN}/:id/(${routes.TABELLENSICHT}|${routes.KALENDERSICHT})/${routes.ELEMENT_BEARBEITEN}`,
          `/zeit/${routes.MEINE_ZEITPLANUNGEN}/:id/${routes.TABELLENSICHT}/${routes.UEBERSICHT}`,
          `/zeit/${routes.ARCHIV}/${routes.MEINE_ZEITPLANUNGEN}/:id/${routes.TABELLENSICHT}/${routes.UEBERSICHT}`,
        ]}
      >
        <NewZeitplanungsvorlageElement
          currentZeitParent={zeitplanung}
          reloadZeitParent={loadData}
          isLight={props.isLight}
        />
      </Route>
      <Route
        path={[
          `*/${routes.MEINE_ZEITPLANUNGEN}/:id/${routes.TABELLENSICHT}`,
          `*/${routes.MEINE_ZEITPLANUNGEN}/:id/${routes.KALENDERSICHT}`,
          `*/${routes.MEINE_ZEITPLANUNGEN}/:id/${routes.KOMMENTARE}`,
        ]}
      >
        {zeitplanung && (
          <SpeichernAlsZeitplanungsvorlageComponent
            isVisible={isSpeichernAlsVorlageModalVisible}
            setIsVisible={setIsSpeichernAlsVorlageModalVisible}
            resetTab={() => false}
            selectedZeitplanung={zeitplanung?.dto}
            selectedId={zeitplanung.base.id}
          />
        )}

        <div className="zeitplanungsvorlage-tabs">
          <TitleWrapperComponent>
            <Row>
              <Col xs={{ span: 22, offset: 1 }}>
                <div className="heading-holder">
                  <Title className="main-title" level={1}>
                    {zeitplanung?.dto.titel}
                  </Title>
                  {zeitplanung?.dto.billigung && <BilligungStatus billigung={zeitplanung.dto.billigung} />}
                </div>
              </Col>
            </Row>
          </TitleWrapperComponent>
          <Row>
            <Col xs={{ span: 22, offset: 1 }}>
              <TabsWrapper
                activeKey={activeTab}
                className="my-votes-tabs standard-tabs"
                onChange={(key: string) => {
                  const archivePath = routeMatcher?.params.archiv ? `${routes.ARCHIV}/` : '';
                  history.push(`/zeit/${archivePath}${routes.MEINE_ZEITPLANUNGEN}/${id}/${key}`);
                }}
                moduleName="Zeitplanungsvorlage"
                items={tabItems}
              />
              <BtnBackComponent id="ezeit-zeitplanung-back-btn" url={`/zeit/${routes.MEINE_ZEITPLANUNGEN}`} />
            </Col>
          </Row>
        </div>
      </Route>
    </Switch>
  );
}
