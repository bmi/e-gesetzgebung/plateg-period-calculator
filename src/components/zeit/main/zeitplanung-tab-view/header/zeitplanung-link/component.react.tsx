// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';
import { Link } from 'react-router-dom';
export interface ZeitplanungsLinkProps {
  children: React.ReactElement;
  url?: string;
}

export function ZeitplanungsLink(props: ZeitplanungsLinkProps): React.ReactElement {
  let element;
  if (props.url) {
    element = (
      <Link id="ezeit-zeitplanungsTab-zeitplanungsTitel-link" to={props.url} key="zeit-planung-tabellensicht">
        {props.children}
      </Link>
    );
  } else {
    element = <span key="zeit-vorlagen-tab">{props.children}</span>;
  }
  return element;
}
