// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

import { routes } from '../../../../../../shares/routes';
interface ParentLinkInterface {
  url: string;
  keyTab?: string;
}
export function ParentLink(props: ParentLinkInterface): React.ReactElement {
  const { t } = useTranslation();
  const tableKey = props.keyTab || routes.MEINE_ZEITPLANUNGEN;
  return (
    <Link id="ezeit-parent-link" key="parent-link" to={`${props.url}`}>
      {t('zeit.header.breadcrumbs.projectName')} - {t(`zeit.header.breadcrumbs.${tableKey}`)}
    </Link>
  );
}
