// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';
import { Link } from 'react-router-dom';
export interface ElementLinkProps {
  text: string;
  url?: string;
}

export function ElementLink(props: ElementLinkProps): React.ReactElement {
  let element;
  if (props.url) {
    element = (
      <Link id="ezeit-zeitplanung-element-link" to={props.url} key="ezeit-zeitplanung-element-link">
        {props.text}
      </Link>
    );
  } else {
    element = <span key="ezeit-zeitplanung-element-text">{props.text}</span>;
  }

  return element;
}
