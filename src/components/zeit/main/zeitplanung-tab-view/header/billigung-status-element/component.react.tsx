// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './billigung-status.less';

import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { BilligungEntityResponseDTO, BilligungStatusType } from '@plateg/rest-api';
import { Filters } from '@plateg/theme/src/shares/filters';

export interface BilligungStatusProps {
  billigung: BilligungEntityResponseDTO;
}

export function BilligungStatus(props: Readonly<BilligungStatusProps>): React.ReactElement {
  const { t } = useTranslation();
  const istOffen = props.billigung.dto.status === BilligungStatusType.NichtBearbeitet;
  const istZurueckgezogen = props.billigung.dto.status === BilligungStatusType.Zurueckgezogen;

  const getBadgeColor = (status: BilligungStatusType) => {
    if (status === BilligungStatusType.NichtBearbeitet) {
      return 'blue';
    } else if (status === BilligungStatusType.KeineBilligung || status === BilligungStatusType.Zurueckgezogen) {
      return 'red';
    } else {
      return 'green';
    }
  };

  return (
    <>
      <Title className="billigung-status-title" level={2}>
        {t('zeit.zeitplanungenTable.billigungsanfragenStatus.title')}
      </Title>
      <div className="status-container">
        <div
          className={`status-badge ${getBadgeColor(props.billigung.dto.status)}`}
          dangerouslySetInnerHTML={{
            __html: t(`zeit.zeitplanungenTable.billigungsanfragenStatus.status.${props.billigung.dto.status}`, {
              interpolation: { escapeValue: false },
            }),
          }}
        />
        {!istOffen && !istZurueckgezogen && (
          <span
            dangerouslySetInnerHTML={{
              __html: t(`zeit.zeitplanungenTable.billigungsanfragenStatus.person`, {
                interpolation: { escapeValue: false },
                person: props.billigung.dto.gestelltAn.dto.name,
              }),
            }}
          />
        )}
        <span
          dangerouslySetInnerHTML={{
            __html: t(`zeit.zeitplanungenTable.billigungsanfragenStatus.${istOffen ? 'datumSeit' : 'datumAm'}`, {
              interpolation: { escapeValue: false },
              datum: Filters.date(new Date(props.billigung.dto.geantwortetAm ?? props.billigung.base.erstelltAm)),
            }),
          }}
        />
      </div>
    </>
  );
}
