// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../comments.less';

import { Button, Col, Form, Row } from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import React, { FC, memo, useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { UserEntityWithStellvertreterResponseDTO } from '@plateg/rest-api';

import { CommentFormType, getFachreferatAndAbteilung } from '../all-comments/react.component';
import CommentUserInfoComponent from '../comment-user-info/component.react';
import { CommentsController } from '../controller';

interface Props {
  zeitplanungId: string;
  getAllComments: () => void;
  userData: UserEntityWithStellvertreterResponseDTO;
  commentId?: string;
  setActiveForm?: (val: boolean) => void;
  activeForm?: boolean;
  commentText?: string;
  formType?: CommentFormType;
  setUpdatedComment?: (val: { id: string; body: string }) => void;
  isZeitplanungsVorlage?: boolean;
}

interface FormValues {
  comment_body: string;
}

const NewCommentComponent: FC<Props> = ({
  zeitplanungId,
  getAllComments,
  userData,
  commentId,
  commentText,
  activeForm,
  setActiveForm,
  formType,
  setUpdatedComment,
  isZeitplanungsVorlage,
}) => {
  const commentsCtrl = new CommentsController();

  const { t } = useTranslation();
  const [activeComment, setActiveComment] = useState(false);
  const [form] = Form.useForm();
  const inputRef = useRef<HTMLTextAreaElement>(null);
  const [inputVal, setInputVal] = useState(commentText || '');
  const [activeRequest, setActiveRequest] = useState(false);

  useEffect(() => {
    if (activeForm && setActiveForm) {
      inputRef.current?.focus();
    }
    if (formType === CommentFormType.edit) {
      form.setFieldValue('comment_body', commentText);
    }
  }, [activeForm, formType]);

  const onFinish = (values: FormValues) => {
    if (inputVal.replace(/ /g, '').length > 0) {
      setActiveRequest(true);
      if (formType === CommentFormType.new || formType === CommentFormType.answer) {
        commentsCtrl.createComment(
          zeitplanungId,
          values.comment_body,
          getAllComments,
          resetForm,
          setActiveRequest,
          commentId,
          isZeitplanungsVorlage,
        );
      } else if (formType === CommentFormType.edit && commentId && setUpdatedComment) {
        commentsCtrl.editComment(
          commentId,
          values.comment_body,
          resetForm,
          setActiveRequest,
          setUpdatedComment,
          isZeitplanungsVorlage,
        );
      }
    }
  };

  const onInputFocus = () => {
    setActiveComment(true);
  };

  const resetForm = () => {
    setActiveComment(false);
    form.resetFields();
    if (activeForm && setActiveForm) {
      setActiveForm(false);
    }
  };

  const handleReset = () => {
    resetForm();
  };

  const fachreferatAndAbteilung = getFachreferatAndAbteilung(userData);

  const getInputLabel = (formType: CommentFormType | undefined) => {
    switch (formType) {
      case CommentFormType.answer:
        return t('zeit.zeitplanungenTable.tabs.kommentare.newComment.writeAnswer');
      case CommentFormType.edit:
        return t('zeit.zeitplanungenTable.tabs.kommentare.newComment.editComment');
      case CommentFormType.new:
        return t('zeit.zeitplanungenTable.tabs.kommentare.newComment.writeComment');
      default:
        return;
    }
  };

  return (
    <div
      className={`new-comment-container ${
        activeForm && formType === CommentFormType.answer ? 'new-comment-divider  comment-left-offset' : ''
      }`}
    >
      <div>
        {userData.dto.name && (
          <CommentUserInfoComponent
            ressortBezeichnung={userData.dto.ressort?.kurzbezeichnung}
            userName={userData.dto.name}
            fachreferatAndAbteilung={fachreferatAndAbteilung}
          />
        )}
        <div className="comment-form-container ">
          <Form
            form={form}
            labelCol={{ span: 6 }}
            onFinish={onFinish}
            onFinishFailed={(val) => console.error('Error in Fields: ', val.errorFields)}
          >
            <label className="comment-label" htmlFor="comment-input-id">
              {getInputLabel(formType)}
            </label>
            <Form.Item name="comment_body">
              <TextArea
                rows={activeComment ? 3 : 1}
                aria-label={getInputLabel(formType)}
                value={inputVal}
                onChange={(val) => setInputVal(val.target.value)}
                ref={inputRef}
                onFocus={onInputFocus}
                className="comment-input"
                id="comment-input-id"
                autoSize={{ minRows: activeComment ? 3 : 1, maxRows: 6 }}
              />
            </Form.Item>
            {activeComment && (
              <Row justify={'space-between'}>
                <Col>
                  <Button onClick={handleReset}>
                    {t('zeit.zeitplanungenTable.tabs.kommentare.newComment.cancelBtn')}
                  </Button>
                </Col>
                <Col>
                  <Button
                    disabled={inputVal.replace(/ /g, '').length < 1 || commentText === inputVal || activeRequest}
                    type="primary"
                    htmlType="submit"
                  >
                    {formType === CommentFormType.edit
                      ? t('zeit.zeitplanungenTable.tabs.kommentare.newComment.saveBtn')
                      : t('zeit.zeitplanungenTable.tabs.kommentare.newComment.addBtn')}
                  </Button>
                </Col>
              </Row>
            )}
          </Form>
        </div>
      </div>
    </div>
  );
};

export default memo(NewCommentComponent);
