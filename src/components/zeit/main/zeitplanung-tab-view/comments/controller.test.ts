// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import chai from 'chai';
import chaiString from 'chai-string';
import { Observable, of } from 'rxjs';
import sinon from 'sinon';

import {
  Base,
  GetZpKommentareRequest,
  ZeitplanungControllerApi,
  ZeitplanungKommentarEntityDTO,
  ZeitplanungKommentarEntityResponseDTO,
} from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme';

import { displayErrorMsgStub } from '../../../../../general.test';
import { CommentsController } from './controller';

chai.use(chaiString);

const zeitplanungRestController = GlobalDI.getOrRegister(
  'zeitplanungRestController',
  () => new ZeitplanungControllerApi(),
);

const commentsCtrl = new CommentsController();
const getKommentareRequest: GetZpKommentareRequest = {
  zeitplanungId: 'd96e8b91-6487-4ef7-ab82-7cc9d639c895',
};

const commentParamTest = { id: 'commentId-1', body: 'mein Kommentar editiert' };

describe('get all comments', () => {
  let setAllComments: sinon.SinonStub;

  const testKommentResp: ZeitplanungKommentarEntityResponseDTO[] = [
    { base: {} as Base, dto: {} as ZeitplanungKommentarEntityDTO },
  ];

  const getAllCommentsCallStub: sinon.SinonStub = sinon.stub(zeitplanungRestController, 'getZpKommentare');
  before(() => {
    setAllComments = sinon.stub();
  });

  after(() => {
    getAllCommentsCallStub.restore();
  });

  describe('get all comments - success', () => {
    before(() => {
      getAllCommentsCallStub.returns(of({ dtos: testKommentResp }));
    });
    beforeEach(() => {
      getAllCommentsCallStub.resetHistory();
      setAllComments.resetHistory();
    });
    after(() => {
      getAllCommentsCallStub.reset();
      setAllComments.reset();
    });

    it('get all comments - success test', (done) => {
      commentsCtrl.getAllComments(getKommentareRequest.zeitplanungId, setAllComments);
      setTimeout(() => {
        sinon.assert.calledOnceWithMatch(getAllCommentsCallStub, getKommentareRequest);
        sinon.assert.calledOnceWithExactly(setAllComments, testKommentResp);

        sinon.assert.callOrder(getAllCommentsCallStub, setAllComments);
        done();
      }, 20);
    });
  });

  describe('get all comments - error', () => {
    after(() => {
      getAllCommentsCallStub.reset();
    });
    afterEach(() => {
      displayErrorMsgStub.resetHistory();
    });

    it('get all comments - error test', () => {
      getAllCommentsCallStub.callsFake(() => {
        return new Observable<void>((observer) => observer.error());
      });
      commentsCtrl.getAllComments('not-real-id', setAllComments);

      sinon.assert.notCalled(setAllComments);
      sinon.assert.calledOnce(displayErrorMsgStub);
    });
  });
});

describe('get user', () => {
  let setUserData: sinon.SinonStub;
  before(() => {
    setUserData = sinon.stub();
  });
  afterEach(() => {
    setUserData.resetHistory();
  });

  describe('create comment', () => {
    let createCommentApiCallStub: sinon.SinonStub;

    const getAllComments = sinon.stub();
    const resetForm = sinon.stub();
    const setActiveRequest = sinon.stub();

    before(() => {
      createCommentApiCallStub = sinon.stub(zeitplanungRestController, 'createZpKommentar');
    });

    beforeEach(() => {
      createCommentApiCallStub.resetHistory();
      getAllComments.resetHistory();
      resetForm.resetHistory();
      setActiveRequest.resetHistory();
      displayErrorMsgStub.resetHistory();
    });
    after(() => {
      createCommentApiCallStub.restore();
    });

    it('create comment - success', (done) => {
      createCommentApiCallStub.callsFake(() => {
        return new Observable<void>((observer) => observer.complete());
      });
      commentsCtrl.createComment(
        getKommentareRequest.zeitplanungId,
        'mein Kommentar',
        getAllComments,
        resetForm,
        setActiveRequest,
      );
      setTimeout(() => {
        sinon.assert.calledOnce(createCommentApiCallStub);
        sinon.assert.calledOnce(resetForm);
        sinon.assert.calledOnce(getAllComments);
        sinon.assert.calledWithExactly(setActiveRequest, false);
        sinon.assert.notCalled(displayErrorMsgStub);
        done();
      }, 20);
    });

    it('create comment - error', (done) => {
      createCommentApiCallStub.callsFake(() => {
        return new Observable<void>((observer) => observer.error());
      });
      commentsCtrl.createComment(
        getKommentareRequest.zeitplanungId,
        'mein Kommentar',
        getAllComments,
        resetForm,
        setActiveRequest,
      );
      setTimeout(() => {
        sinon.assert.calledOnce(createCommentApiCallStub);
        sinon.assert.notCalled(resetForm);
        sinon.assert.notCalled(getAllComments);
        sinon.assert.calledWithExactly(setActiveRequest, false);
        sinon.assert.calledOnce(displayErrorMsgStub);
        done();
      }, 20);
    });
  });

  describe('edit comment', () => {
    let editCommentApiCallStub: sinon.SinonStub;
    const setActiveRequest = sinon.stub();

    let setUpdatedComment: sinon.SinonStub;
    const resetForm = sinon.stub();

    before(() => {
      editCommentApiCallStub = sinon.stub(zeitplanungRestController, 'modifyZpKommentar');
      setUpdatedComment = sinon.stub();
    });

    beforeEach(() => {
      editCommentApiCallStub.resetHistory();
      setUpdatedComment.resetHistory();
      resetForm.resetHistory();
      setActiveRequest.resetHistory();
      displayErrorMsgStub.resetHistory();
    });

    after(() => {
      editCommentApiCallStub.restore();
    });

    it('edit comment - success', (done) => {
      editCommentApiCallStub.callsFake(() => {
        return new Observable<void>((observer) => observer.complete());
      });

      commentsCtrl.editComment(
        commentParamTest.id,
        commentParamTest.body,
        resetForm,
        setActiveRequest,
        setUpdatedComment,
      );

      setTimeout(() => {
        sinon.assert.calledOnceWithMatch(editCommentApiCallStub);
        sinon.assert.calledOnce(resetForm);
        sinon.assert.calledOnceWithMatch(setUpdatedComment, commentParamTest);
        sinon.assert.calledOnce(setUpdatedComment);
        sinon.assert.notCalled(displayErrorMsgStub);
        sinon.assert.calledWithExactly(setActiveRequest, false);
        done();
      }, 20);
    });

    it('edit comment - error', (done) => {
      editCommentApiCallStub.callsFake(() => {
        return new Observable<void>((observer) => observer.error());
      });

      commentsCtrl.editComment(
        commentParamTest.id,
        commentParamTest.body,
        resetForm,
        setActiveRequest,
        setUpdatedComment,
      );

      setTimeout(() => {
        sinon.assert.calledOnce(editCommentApiCallStub);
        sinon.assert.notCalled(resetForm);
        sinon.assert.notCalled(setUpdatedComment);
        sinon.assert.calledOnce(displayErrorMsgStub);
        sinon.assert.calledWithExactly(setActiveRequest, false);
        done();
      }, 20);
    });
  });

  describe('delete comment', () => {
    let deleteCommentApiCallStub: sinon.SinonStub;
    const removeComment = sinon.stub();

    before(() => {
      deleteCommentApiCallStub = sinon.stub(zeitplanungRestController, 'deleteZpKommentar');
    });

    beforeEach(() => {
      deleteCommentApiCallStub.resetHistory();
      displayErrorMsgStub.resetHistory();
      removeComment.resetHistory();
    });

    after(() => {
      deleteCommentApiCallStub.restore();
    });

    it('delete comment - success', (done) => {
      deleteCommentApiCallStub.callsFake(() => {
        return new Observable<void>((observer) => observer.complete());
      });
      commentsCtrl.deleteComment(commentParamTest.id, removeComment);

      setTimeout(() => {
        sinon.assert.calledOnceWithExactly(deleteCommentApiCallStub, { kommentarId: commentParamTest.id });
        sinon.assert.notCalled(displayErrorMsgStub);
        sinon.assert.calledOnce(removeComment);
        done();
      }, 20);
    });

    it('delete comment - error', (done) => {
      deleteCommentApiCallStub.callsFake(() => {
        return new Observable<void>((observer) => observer.error());
      });
      commentsCtrl.deleteComment(commentParamTest.id, removeComment);

      setTimeout(() => {
        sinon.assert.calledOnceWithExactly(deleteCommentApiCallStub, { kommentarId: commentParamTest.id });
        sinon.assert.calledOnce(displayErrorMsgStub);
        sinon.assert.notCalled(removeComment);
        done();
      }, 20);
    });
  });
});
