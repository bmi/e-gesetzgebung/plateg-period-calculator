// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../comments.less';

import { format } from 'date-fns';
import React, { FC, memo } from 'react';
import { useTranslation } from 'react-i18next';

interface Props {
  userName: string;
  ressortBezeichnung?: string;
  fachreferatAndAbteilung?: string;
  commentDate?: string;
}

const CommentUserInfoComponent: FC<Props> = ({
  userName,
  ressortBezeichnung,
  fachreferatAndAbteilung,
  commentDate,
}) => {
  const { t } = useTranslation();
  const allNames = userName?.split(' ');
  const firstNameLetters = allNames
    // get only very first and last name for max. 2 names
    .filter((_, index) => index == 0 || index == allNames.length - 1)
    .map((name) => name[0]);

  return (
    <>
      <div style={{ display: 'flex' }}>
        <div className="comment-user-icon">
          <span>{firstNameLetters}</span>
        </div>

        <div className="comment-user-name-div">
          {`${userName} (${ressortBezeichnung || ''}${fachreferatAndAbteilung || ''}) `}
          {commentDate ? (
            <span className="comment-date-div">
              {format(new Date(commentDate), 'dd.MM.yyyy · HH:mm')}{' '}
              {t('zeit.zeitplanungenTable.tabs.kommentare.allComments.timeLabel')}
            </span>
          ) : (
            <></>
          )}
        </div>
      </div>
    </>
  );
};

export default memo(CommentUserInfoComponent);
