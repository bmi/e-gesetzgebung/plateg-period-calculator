// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { AjaxError } from 'rxjs/ajax';

import {
  ModifyZpKommentarRequest,
  ZeitplanungControllerApi,
  ZeitplanungKommentarCreateRequestDTO,
  ZeitplanungKommentarEntityResponseDTO,
  ZeitplanungsvorlageControllerApi,
  ZeitplanungsvorlageKommentarEntityResponseDTO,
} from '@plateg/rest-api';
import { ErrorController, GlobalDI, LoadingStatusController } from '@plateg/theme';

export class CommentsController {
  // private vars
  private readonly zeitplanungController = GlobalDI.get<ZeitplanungControllerApi>('zeitplanungRestController');

  private readonly zeitplanungsvorlageController = GlobalDI.get<ZeitplanungsvorlageControllerApi>(
    'zeitplanunsvorlageRestController',
  );
  public readonly loadingStatusController = GlobalDI.getOrRegister(
    'loadingStatusController',
    () => new LoadingStatusController(),
  );
  private readonly errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  private readonly errorCallback = (error: AjaxError, setActiveRequest?: Function): void => {
    if (setActiveRequest) {
      setActiveRequest(false);
    }
    this.loadingStatusController.setLoadingStatus(false);
    console.error(error, error);
    this.errorCtrl.displayErrorMsg(error, 'zeit.generalErrorMsg');
  };

  // public vars
  public getAllComments = (
    zeitItemId: string,
    setAllComments: (
      comments: ZeitplanungKommentarEntityResponseDTO[] | ZeitplanungsvorlageKommentarEntityResponseDTO[],
    ) => void,
    isZeitplanungsVorlage?: boolean,
  ) => {
    const zeitplanungId = zeitItemId;
    const zeitplanungsvorlageId = zeitItemId;

    const handlerGetComments = isZeitplanungsVorlage
      ? this.zeitplanungsvorlageController.getZpvKommentare({ zeitplanungsvorlageId })
      : this.zeitplanungController.getZpKommentare({ zeitplanungId });

    handlerGetComments.subscribe({
      next: (data) => {
        setAllComments(data.dtos);
      },
      error: this.errorCallback,
    });
  };

  public createComment = (
    zeitItemId: string,
    commentBody: string,
    getAllComments: Function,
    resetForm: Function,
    setActiveRequest: Function,
    geantwortetAufKommentarId?: string,
    isZeitplanungsVorlage?: boolean,
  ) => {
    const zeitplanungId = zeitItemId;
    const zeitplanungsvorlageId = zeitItemId;
    const newComment: ZeitplanungKommentarCreateRequestDTO = {
      kommentar: commentBody,
      geantwortetAufKommentarId,
    };

    const handlerCreateComment = isZeitplanungsVorlage
      ? this.zeitplanungsvorlageController.createZpvKommentar({
          zeitplanungsvorlageId,
          zeitplanungKommentarCreateRequestDTO: newComment,
        })
      : this.zeitplanungController.createZpKommentar({
          zeitplanungId,
          zeitplanungKommentarCreateRequestDTO: newComment,
        });

    handlerCreateComment.subscribe({
      complete: () => {
        setActiveRequest(false);
        resetForm();
        getAllComments();
      },
      error: (error: AjaxError) => {
        setActiveRequest(false);
        this.errorCallback(error);
      },
    });
  };

  public editComment = (
    commentId: string,
    commentText: string,
    resetForm: Function,
    setActiveRequest: Function,
    setUpdatedComment: (val: { id: string; body: string }) => void,
    isZeitplanungsVorlage?: boolean,
  ) => {
    const editCommentObj: ModifyZpKommentarRequest = {
      zeitplanungKommentarModifyRequestDTO: { kommentar: commentText },
      kommentarId: commentId,
    };

    const handlerEditComment = isZeitplanungsVorlage
      ? this.zeitplanungsvorlageController.modifyZpvKommentar(editCommentObj)
      : this.zeitplanungController.modifyZpKommentar(editCommentObj);
    handlerEditComment.subscribe({
      complete: () => {
        setActiveRequest(false);
        resetForm();
        setUpdatedComment({ id: commentId, body: commentText });
      },
      error: (error: AjaxError) => {
        setActiveRequest(false);
        this.errorCallback(error);
      },
    });
  };

  public deleteComment = (
    kommentarId: string,
    removeComment: (commentId: string) => void,
    isZeitplanungsVorlage?: boolean,
  ) => {
    const handlerDeleteComment = isZeitplanungsVorlage
      ? this.zeitplanungsvorlageController.deleteZpvKommentar({ kommentarId })
      : this.zeitplanungController.deleteZpKommentar({ kommentarId });

    handlerDeleteComment.subscribe({
      complete: () => {
        removeComment(kommentarId);
      },
      error: this.errorCallback,
    });
  };
}
