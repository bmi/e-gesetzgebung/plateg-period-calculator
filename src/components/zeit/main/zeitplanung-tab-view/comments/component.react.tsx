// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './comments.less';

import { Col, Row } from 'antd';
import React, { FC, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import {
  UserEntityWithStellvertreterResponseDTO,
  ZeitplanungKommentarEntityResponseDTO,
  ZeitplanungsvorlageKommentarEntityResponseDTO,
} from '@plateg/rest-api';
import { useAppSelector } from '@plateg/theme/src/components/store';

import { AllCommentsComponent, CommentFormType } from './all-comments/react.component';
import { CommentsController } from './controller';
import NewCommentComponent from './new-comment/component.react';

interface Props {
  zeitItemId: string;
  isReadOnly: boolean;
  isZeitplanungsVorlage?: boolean;
}

export const CommentsContainerComponent: FC<Props> = ({ zeitItemId, isReadOnly, isZeitplanungsVorlage }) => {
  const { t } = useTranslation();
  const commentsCtrl = new CommentsController();
  const [allComments, setAllComments] = useState<
    ZeitplanungKommentarEntityResponseDTO[] | ZeitplanungsvorlageKommentarEntityResponseDTO[]
  >();
  const [userData, setUserData] = useState<UserEntityWithStellvertreterResponseDTO>();
  const [updatedComment, setUpdatedComment] = useState<{ id: string; body: string }>();
  const appStore = useAppSelector((state) => state.user);

  const getAllComments = () => {
    commentsCtrl.getAllComments(zeitItemId, setAllComments, isZeitplanungsVorlage);
  };

  useEffect(() => {
    if (allComments) {
      let allCommentsCopy = [...allComments];
      allCommentsCopy = allCommentsCopy.map((comment) => {
        if (comment.base.id === updatedComment?.id) {
          comment.dto.kommentar = updatedComment.body;
          return comment;
        }
        return comment;
      });

      setAllComments(allCommentsCopy);
    }
  }, [updatedComment]);

  const removeComment = (commentId: string) => {
    if (allComments) {
      let allCommentsCopy = [...allComments];
      allCommentsCopy = allCommentsCopy.filter((comment) => comment.base.id !== commentId);
      setAllComments(allCommentsCopy);
    }
    getAllComments();
  };

  useEffect(() => {
    getAllComments();
    if (appStore.user) {
      setUserData(appStore.user);
    }
  }, [appStore.user, updatedComment]);

  useEffect(() => {
    if (!allComments) {
      commentsCtrl.loadingStatusController.setLoadingStatus(true);
    } else {
      commentsCtrl.loadingStatusController.setLoadingStatus(false);
    }
  }, [allComments]);

  const commentsCounter =
    allComments &&
    (allComments.length === 1
      ? `${allComments.length} ${t('zeit.zeitplanungenTable.tabs.kommentare.allComments.commentSgl')}`
      : `${allComments.length} ${t('zeit.zeitplanungenTable.tabs.kommentare.allComments.commentPl')}`);

  return (
    <div className="comment-main-styles">
      <div aria-label={commentsCounter} className="comments-container">
        {commentsCounter}
      </div>
      {/*comments and form */}
      <Row>
        <Col xs={24} sm={24} md={24} lg={15} xl={15}>
          {allComments && zeitItemId && userData && (
            <AllCommentsComponent
              userId={userData.base.id}
              removeComment={removeComment}
              allComments={allComments}
              isReadOnly={isReadOnly}
              isZeitplanungsVorlage={isZeitplanungsVorlage}
            >
              {
                <NewCommentComponent
                  setUpdatedComment={setUpdatedComment}
                  userData={userData}
                  getAllComments={getAllComments}
                  zeitplanungId={zeitItemId}
                  isZeitplanungsVorlage={isZeitplanungsVorlage}
                />
              }
            </AllCommentsComponent>
          )}
          {userData && !isReadOnly && (
            <NewCommentComponent
              formType={CommentFormType.new}
              userData={userData}
              getAllComments={getAllComments}
              zeitplanungId={zeitItemId}
              isZeitplanungsVorlage={isZeitplanungsVorlage}
            />
          )}
        </Col>
      </Row>
    </div>
  );
};
