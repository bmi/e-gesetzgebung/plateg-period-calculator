// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { FC, useState } from 'react';

import {
  UserEntityResponseDTO,
  UserEntityWithStellvertreterResponseDTO,
  ZeitplanungKommentarEntityResponseDTO,
} from '@plateg/rest-api';

import { CommentActionButtons } from '../comment-action-buttons/component.react';
import CommentUserInfoComponent from '../comment-user-info/component.react';

export enum CommentFormType {
  edit = 'EDIT',
  answer = 'ANSWER',
  new = 'NEW',
}

interface Props {
  allComments: ZeitplanungKommentarEntityResponseDTO[];
  removeComment: (commentId: string) => void;
  children?: React.ReactElement;
  userId: string;
  isReadOnly: boolean;
  isZeitplanungsVorlage?: boolean;
}

export const AllCommentsComponent: FC<Props> = ({
  children,
  allComments,
  removeComment,
  userId,
  isReadOnly,
  isZeitplanungsVorlage,
}) => {
  const [commentId, setCommentId] = useState<string>();
  const [formType, setFormType] = useState<CommentFormType>();
  const [activeForm, setActiveForm] = useState<boolean>();

  const displayComments = [...allComments]
    .sort((a, b) => new Date(a.base.erstelltAm).valueOf() - new Date(b.base.erstelltAm).valueOf())
    .map((comment) => {
      const commentWithAnswers = comment.dto.antworten.length > 0;

      return (
        <div key={comment.base.id} className={`single-comment-div`}>
          {commentId === comment.base.id && activeForm && formType === CommentFormType.edit ? (
            children &&
            React.cloneElement(children, {
              formType,
              activeForm,
              commentId,
              setActiveForm,
              commentText: comment.dto.kommentar,
            })
          ) : (
            <>
              {comment.dto.ersteller.dto.name && (
                <CommentUserInfoComponent
                  userName={comment.dto.ersteller.dto.name}
                  ressortBezeichnung={
                    comment.dto.ersteller.dto.ressort?.kurzbezeichnung || comment.dto.ersteller.dto.ressort?.bezeichnung
                  }
                  fachreferatAndAbteilung={getFachreferatAndAbteilung(comment.dto.ersteller)}
                  commentDate={comment.base.erstelltAm}
                />
              )}
              <div className="comment-body">
                {comment.dto.kommentar}
                {!commentWithAnswers && !activeForm && !isReadOnly && (
                  <CommentActionButtons
                    isOwner={userId === comment.dto.ersteller.base.id}
                    editable
                    removeComment={removeComment}
                    setActiveForm={setActiveForm}
                    setFormType={setFormType}
                    setCommentId={setCommentId}
                    kommentarId={comment.base.id}
                    isZeitplanungsVorlage={isZeitplanungsVorlage}
                  />
                )}
              </div>
              {/* antworten */}
              {[...comment.dto.antworten]
                .sort((a, b) => new Date(a.base.erstelltAm).valueOf() - new Date(b.base.erstelltAm).valueOf())
                .map((antwort, index) => {
                  const lastIndex = comment.dto.antworten.length - 1;
                  return (
                    <div key={antwort.base.id}>
                      {index === 0 && <div className="comment-divider" />}
                      <div
                        className={`comment-left-offset comment-answer ${
                          index === lastIndex ? 'comment-no-border' : ''
                        }`}
                      >
                        {antwort.dto.ersteller.dto.name && (
                          <CommentUserInfoComponent
                            userName={antwort.dto.ersteller.dto.name}
                            ressortBezeichnung={
                              antwort.dto.ersteller.dto.ressort?.kurzbezeichnung ||
                              antwort.dto.ersteller.dto.ressort?.bezeichnung
                            }
                            fachreferatAndAbteilung={getFachreferatAndAbteilung(antwort.dto.ersteller)}
                            commentDate={antwort.base.erstelltAm}
                          />
                        )}
                        <div
                          className={`comment-body ${index === lastIndex && !activeForm ? 'comment-less-padding' : ''}`}
                        >
                          {antwort.dto.kommentar}
                        </div>
                        {!isReadOnly && (
                          <CommentActionButtons
                            isOwner={userId === antwort.dto.ersteller.base.id}
                            editable
                            removeComment={removeComment}
                            setActiveForm={setActiveForm}
                            setFormType={setFormType}
                            setCommentId={setCommentId}
                            kommentarId={antwort.base.id}
                            isZeitplanungsVorlage={isZeitplanungsVorlage}
                            isLastIndex={index === lastIndex}
                            isAntwort={!!antwort.dto.kommentar}
                          />
                        )}{' '}
                        {commentId === antwort.base.id &&
                          activeForm &&
                          formType === CommentFormType.edit &&
                          children &&
                          React.cloneElement(children, {
                            formType,
                            activeForm,
                            commentId,
                            setActiveForm,
                            commentText: antwort.dto.kommentar,
                          })}
                      </div>
                    </div>
                  );
                })}

              {!activeForm && commentWithAnswers && !isReadOnly && (
                <CommentActionButtons
                  setActiveForm={setActiveForm}
                  setFormType={setFormType}
                  setCommentId={setCommentId}
                  kommentarId={comment.base.id}
                />
              )}
            </>
          )}

          {commentId === comment.base.id &&
            activeForm &&
            formType === CommentFormType.answer &&
            children &&
            React.cloneElement(children, { formType, activeForm, commentId, setActiveForm })}
        </div>
      );
    });
  return <>{displayComments}</>;
};

export const getFachreferatAndAbteilung = (
  userData: UserEntityWithStellvertreterResponseDTO | UserEntityResponseDTO,
) => {
  const abteilungString = userData?.dto.abteilung ? `${userData?.dto.abteilung}` : '';
  const fachreferatString =
    userData?.dto.abteilung && userData?.dto.fachreferat ? `, ${userData?.dto.fachreferat}` : '';
  return abteilungString ? `, ${abteilungString}${fachreferatString}` : '';
};
