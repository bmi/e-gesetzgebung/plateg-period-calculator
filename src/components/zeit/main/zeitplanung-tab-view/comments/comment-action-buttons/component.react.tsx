// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button, Popover } from 'antd';
import React, { FC, RefObject, useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { ExclamationCircleFilled } from '@plateg/theme';

import { CommentFormType } from '../all-comments/react.component';
import { CommentsController } from '../controller';

interface Props {
  kommentarId: string;
  setActiveForm: (val: boolean) => void;
  setCommentId: (val: string) => void;
  setFormType: (val: CommentFormType) => void;
  editable?: boolean;
  removeComment?: (commentId: string) => void;
  isOwner?: boolean;
  isZeitplanungsVorlage?: boolean;
  isAntwort?: boolean;
  isLastIndex?: boolean;
}

export const CommentActionButtons: FC<Props> = ({
  editable,
  setCommentId,
  setFormType,
  kommentarId,
  setActiveForm,
  removeComment,
  isOwner,
  isZeitplanungsVorlage,
  isAntwort,
  isLastIndex,
}) => {
  const commentsCtrl = new CommentsController();
  const { t } = useTranslation();
  const [opened, setOpened] = useState(false);
  const deleteMainBtnRef = useRef<HTMLAnchorElement>(null);
  const cancelBtnRef = useRef<HTMLButtonElement>(null);
  const deleteBtnRef = useRef<HTMLButtonElement>(null);

  useEffect(() => {
    if (opened) {
      setTimeout(() => {
        deleteBtnRef.current?.focus();
      }, 100);
    }
  }, [opened]);

  const handleAnswer = () => {
    setFormType(CommentFormType.answer);
    setCommentId(kommentarId);
    setActiveForm(true);
  };

  const handleEdit = () => {
    setFormType(CommentFormType.edit);
    setActiveForm(true);
    setCommentId(kommentarId);
  };

  const handleDelete = () => {
    if (removeComment) {
      commentsCtrl.deleteComment(kommentarId, removeComment, isZeitplanungsVorlage);
    }
  };

  const onKeyDownHandler = (e: React.KeyboardEvent<HTMLElement>, btnRef: RefObject<HTMLButtonElement>) => {
    if (e.code === 'Tab' && !e.shiftKey) {
      e.preventDefault();
      btnRef.current?.focus();
    }
  };

  const popoverContent = (
    <div className="comment-popover-div">
      <div className="ant-popover-buttons">
        <Button
          id="evor-handleDelete-btn"
          onClick={handleDelete}
          ref={deleteBtnRef}
          onKeyDown={(e) => onKeyDownHandler(e, cancelBtnRef)}
          type="primary"
        >
          {t('zeit.zeitplanungenTable.tabs.kommentare.actionBtns.deleteBtn')}
        </Button>
        <Button
          id="evor-abortDelete-btn"
          ref={cancelBtnRef}
          onKeyDown={(e) => onKeyDownHandler(e, deleteBtnRef)}
          onClick={() => {
            setOpened(false);
            deleteMainBtnRef.current?.focus();
          }}
          type="default"
        >
          {t('zeit.zeitplanungenTable.tabs.kommentare.actionBtns.cancelBtn')}
        </Button>
      </div>
    </div>
  );

  const popoverTitle = (
    <>
      <span role="img" aria-label="Achtung-Icon" className="anticon anticon-exclamation-circle">
        <ExclamationCircleFilled />
      </span>
      <span className="comment-popover-title">
        {t('zeit.zeitplanungenTable.tabs.kommentare.actionBtns.deleteConfirm')}
      </span>
    </>
  );

  return (
    <>
      <div className={`comment-action-btn-div ${!editable ? 'comment-with-answer' : ''}`}>
        {!isAntwort && (
          <Button onClick={handleAnswer} className="comment-answer-btn" type="link">
            <span className="comment-answer-btn-label ">
              {t('zeit.zeitplanungenTable.tabs.kommentare.actionBtns.answerBtn')}
            </span>
          </Button>
        )}
        {editable && isOwner && !isAntwort && (
          <>
            <Button onClick={handleEdit} className="comment-answer-btn" type="link">
              <span className="comment-answer-btn-label ">
                {t('zeit.zeitplanungenTable.tabs.kommentare.actionBtns.editBtn')}
              </span>
            </Button>
            <Popover open={opened} content={popoverContent} placement="bottom" title={popoverTitle}>
              <Button onClick={() => setOpened(true)} ref={deleteMainBtnRef} className="comment-answer-btn" type="text">
                <span className="comment-answer-btn-label ">
                  {t('zeit.zeitplanungenTable.tabs.kommentare.actionBtns.deleteBtn')}
                </span>
              </Button>
            </Popover>
          </>
        )}
        {editable && isOwner && isAntwort && isLastIndex && (
          <>
            <Button onClick={handleEdit} className="comment-answer-btn" type="link">
              <span className="comment-answer-btn-label ">
                {t('zeit.zeitplanungenTable.tabs.kommentare.actionBtns.editBtn')}
              </span>
            </Button>
            <Popover open={opened} content={popoverContent} placement="bottom" title={popoverTitle}>
              <Button onClick={() => setOpened(true)} ref={deleteMainBtnRef} className="comment-answer-btn" type="text">
                <span className="comment-answer-btn-label ">
                  {t('zeit.zeitplanungenTable.tabs.kommentare.actionBtns.deleteBtn')}
                </span>
              </Button>
            </Popover>
          </>
        )}
      </div>
    </>
  );
};
