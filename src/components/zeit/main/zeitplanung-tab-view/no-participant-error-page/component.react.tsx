// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';
import { useTranslation } from 'react-i18next';

import { ErrorPage } from '@plateg/theme';

import { routes } from '../../../../../shares/routes';
export function NoParticipantErrorPageComponent(): React.ReactElement {
  const { t } = useTranslation();
  return (
    <ErrorPage
      title={t(`zeit.zeitplanungenTable.noParticipantError.title`)}
      text={t(`zeit.zeitplanungenTable.noParticipantError.text`)}
      link={`/zeit/${routes.MEINE_ZEITPLANUNGEN}`}
      linkText={t(`zeit.zeitplanungenTable.noParticipantError.buttonText`)}
    />
  );
}
