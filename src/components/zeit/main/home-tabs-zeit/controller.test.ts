// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import chai, { assert, expect } from 'chai';
import chaiString from 'chai-string';
import { Observable, of } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';
import sinon from 'sinon';

import {
  Configuration,
  ErstellerType,
  ExportControllerApi,
  RegelungsvorhabenTypType,
  TerminEntityResponseDTO,
  UserControllerApi,
  UserEntityResponseDTO,
  VorhabenStatusType,
  ZeitplanungArchivTableDTO,
  ZeitplanungArchivTableDTOOriginalTabEnum,
  ZeitplanungControllerApi,
  ZeitplanungEntityDTO,
  ZeitplanungsvorlageControllerApi,
  ZeitplanungsvorlageEntityDTO,
  ZeitplanungsvorlagenBauplan,
  ZeitplanungsvorlageTableDTO,
  ZeitplanungTableDTO,
  ZeitvorlagevorlagenBauplanDTO,
} from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme/src/shares';

import { displayErrorMsgStub, setLoadingStatusStub } from '../../../../general.test';
import i18n from '../../../../shares/i18n';
import { ZeitplanungController } from './controller';

chai.use(chaiString);

const zeitplanungController = GlobalDI.getOrRegister('zeitplanungController', () => new ZeitplanungController());
const zeitplanungRestController = GlobalDI.getOrRegister(
  'zeitplanungRestController',
  () => new ZeitplanungControllerApi(new Configuration()),
);
const zeitplanungsvorlageController = GlobalDI.getOrRegister(
  'zeitplanunsvorlageRestController',
  () => new ZeitplanungsvorlageControllerApi(new Configuration()),
);
const userController = GlobalDI.getOrRegister('userController', () => new UserControllerApi(new Configuration()));
const exportController = GlobalDI.getOrRegister(
  'exportControllerApi',
  () => new ExportControllerApi(new Configuration()),
);

const isShowZeitplanungSystemvorschlagOpenDialogStub = sinon.stub(
  userController,
  'isShowZeitplanungSystemvorschlagOpenDialog',
);
const setShowZeitplanungSystemvorschlagOpenDialogStub = sinon.stub(
  userController,
  'setShowZeitplanungSystemvorschlagOpenDialog',
);

const isShowZeitplanungScrollDialogStub = sinon.stub(userController, 'isShowZeitplanungScrollDialog');
const setShowZeitplanungScrollDialogStub = sinon.stub(userController, 'setShowZeitplanungScrollDialog');

const date1 = '2021-04-21T16:05:30.958156';
const date2 = '2028-04-16T14:02:38.958156';
const invalidDate = '2021-04-2148';
const termin: TerminEntityResponseDTO = {
  base: { id: 'termin1', bearbeitetAm: date1, erstelltAm: date1 },
  dto: { titel: 'Termin1', datum: date1, termin: false, wichtig: false, vorherigeElementeIds: [] },
};
const user: UserEntityResponseDTO = {
  base: { id: 'user1', bearbeitetAm: date1, erstelltAm: date1 },
  dto: { email: 'example1@example1xample2.example', name: 'Tester' },
};
const zeitplanungContent: ZeitplanungTableDTO[] = [
  {
    id: 'abcd123456',
    bearbeitetAm: date1,
    bearbeitetVon: user,
    erstePhaseBeginntAm: date1,
    letztePhaseEndetAm: date1,
    naechsterTermin: termin,
    naechsterTerminFaelligIn: 1,
    titel: 'Beispiel1',
    erstelltAm: date1,
    erstelltVon: user,
    beschreibung: 'besch1',
    erstellerType: ErstellerType.Selbst,
    regelungsvorhaben: {
      base: { id: 'b1', bearbeitetAm: date1, erstelltAm: date1 },
      dto: { abkuerzung: 'dto1short', status: VorhabenStatusType.InBearbeitung, kurzbezeichnung: 'dto1shortdesc' },
    },
  },
  {
    id: 'abcd123457',
    bearbeitetAm: date1,
    bearbeitetVon: user,
    erstePhaseBeginntAm: date1,
    letztePhaseEndetAm: date1,
    naechsterTermin: termin,
    naechsterTerminFaelligIn: 1,
    titel: 'Beispiel2',
    erstelltAm: date1,
    erstelltVon: user,
    beschreibung: 'besch2',
    erstellerType: ErstellerType.Selbst,
    regelungsvorhaben: {
      base: { id: 'b2', bearbeitetAm: date1, erstelltAm: date1 },
      dto: { abkuerzung: 'dto2short', status: VorhabenStatusType.InBearbeitung, kurzbezeichnung: 'dto2shortdesc' },
    },
  },
  {
    id: 'abcd123458',
    bearbeitetAm: date1,
    bearbeitetVon: user,
    erstePhaseBeginntAm: date1,
    letztePhaseEndetAm: date1,
    naechsterTermin: termin,
    naechsterTerminFaelligIn: 1,
    titel: 'Beispiel3',
    erstelltAm: date1,
    erstelltVon: user,
    beschreibung: 'besch3',
    erstellerType: ErstellerType.Selbst,
    regelungsvorhaben: {
      base: { id: 'b3', bearbeitetAm: date1, erstelltAm: date1 },
      dto: { abkuerzung: 'dto3short', status: VorhabenStatusType.InBearbeitung, kurzbezeichnung: 'dto3shortdesc' },
    },
  },
  {
    id: 'abcd123459',
    bearbeitetAm: date1,
    bearbeitetVon: user,
    erstePhaseBeginntAm: date1,
    letztePhaseEndetAm: date1,
    naechsterTermin: termin,
    naechsterTerminFaelligIn: 1,
    titel: 'Beispiel4',
    erstelltAm: date1,
    erstelltVon: user,
    beschreibung: 'besch4',
    erstellerType: ErstellerType.Selbst,
    regelungsvorhaben: {
      base: { id: 'b4', bearbeitetAm: date1, erstelltAm: date1 },
      dto: { abkuerzung: 'dto4short', status: VorhabenStatusType.InBearbeitung, kurzbezeichnung: 'dto4shortdesc' },
    },
  },
];

const zeitplanungsVorlagenContent: ZeitplanungsvorlageTableDTO[] = [
  {
    id: 'abcd123456',
    bearbeitetAm: date1,
    bearbeitetVon: user,
    titel: 'Beispiel1',
    erstelltAm: date1,
    erstelltVon: user,
    beschreibung: 'besch1',
    erstellerType: ErstellerType.Selbst,
    dauer: 1,
    vorhabenart: RegelungsvorhabenTypType.Gesetz,
  },
  {
    id: 'abcd123457',
    bearbeitetAm: date1,
    bearbeitetVon: user,
    titel: 'Beispiel2',
    erstelltAm: date1,
    erstelltVon: user,
    beschreibung: 'besch2',
    erstellerType: ErstellerType.Selbst,
    dauer: 2,
    vorhabenart: RegelungsvorhabenTypType.Rechtsverordnung,
  },
  {
    id: 'abcd123458',
    bearbeitetAm: date1,
    bearbeitetVon: user,
    titel: 'Beispiel3',
    erstelltAm: date1,
    erstelltVon: user,
    beschreibung: 'besch3',
    erstellerType: ErstellerType.Selbst,
    dauer: 3,
    vorhabenart: RegelungsvorhabenTypType.Verwaltungsvorschrift,
  },
];
const archiveContent: ZeitplanungArchivTableDTO[] = [
  {
    archiviertAm: date1,
    bearbeitetAm: date1,
    bearbeitetVon: user,
    erstellerType: ErstellerType.Selbst,
    erstelltAm: date1,
    erstelltVon: user,
    id: 'abcd12345678',
    originalTab: ZeitplanungArchivTableDTOOriginalTabEnum.Billigungen,
    regelungsvorhaben: {
      base: { id: 'b4', bearbeitetAm: date1, erstelltAm: date1 },
      dto: { abkuerzung: 'dto4short', status: VorhabenStatusType.InBearbeitung, kurzbezeichnung: 'dto4shortdesc' },
    },
    titel: 'Planung 1 - Mindestdauer',
    vorhabenart: RegelungsvorhabenTypType.Gesetz,
  },
  {
    archiviertAm: date1,
    bearbeitetAm: date1,
    bearbeitetVon: user,
    erstellerType: ErstellerType.Selbst,
    erstelltAm: date1,
    erstelltVon: user,
    id: 'abcd123456',
    originalTab: ZeitplanungArchivTableDTOOriginalTabEnum.MeineZeitplanungen,
    regelungsvorhaben: {
      base: { id: 'b4', bearbeitetAm: date1, erstelltAm: date1 },
      dto: { abkuerzung: 'dto4short', status: VorhabenStatusType.InBearbeitung, kurzbezeichnung: 'dto4shortdesc' },
    },
    titel: 'Planung 1 - Mindestdauer',
    vorhabenart: RegelungsvorhabenTypType.Rechtsverordnung,
  },
  {
    archiviertAm: date1,
    bearbeitetAm: date1,
    bearbeitetVon: user,
    erstellerType: ErstellerType.Selbst,
    erstelltAm: date1,
    erstelltVon: user,
    id: 'abcd123456',
    originalTab: ZeitplanungArchivTableDTOOriginalTabEnum.Zeitplanungsvorlagen,
    regelungsvorhaben: {
      base: { id: 'b4', bearbeitetAm: date1, erstelltAm: date1 },
      dto: { abkuerzung: 'dto4short', status: VorhabenStatusType.InBearbeitung, kurzbezeichnung: 'dto4shortdesc' },
    },
    titel: 'Planung 1 - Mindestdauer',
    vorhabenart: RegelungsvorhabenTypType.Verwaltungsvorschrift,
  },
];

const zeitvorlagevorlageContent: ZeitvorlagevorlagenBauplanDTO[] = [
  {
    termin: false,
    bauplan: ZeitplanungsvorlagenBauplan.GVorphase,
    titel: 'Vorphase',
    dauerMin: {
      weeks: 8,
    },
    dauerAvg: {
      weeks: 12,
    },
    dauerMax: {
      weeks: 20,
    },
  },
  {
    termin: false,
    bauplan: ZeitplanungsvorlagenBauplan.GKonzeptionelleUeberlegungen,
    titel: 'Konzeptionelle Überlegungen',
    dauerMin: {
      weeks: 2,
    },
    dauerAvg: {
      weeks: 3,
    },
    dauerMax: {
      weeks: 4,
    },
  },
  {
    termin: false,
    bauplan: ZeitplanungsvorlagenBauplan.GPruefungDerPolitischenInhaltlichenUndZeitlichenRahmenbedingungen,
    titel: 'Prüfung der politischen, inhaltlichen und zeitlichen Rahmenbedingungen',
    dauerMin: {
      weeks: 1,
    },
    dauerAvg: {
      weeks: 1,
    },
    dauerMax: {
      weeks: 2,
    },
  },
  {
    termin: false,
    bauplan: ZeitplanungsvorlagenBauplan.GPruefungVonRegelungsalternativen,
    titel: 'Prüfung von Regelungsalternativen',
    dauerMin: {
      weeks: 1,
    },
    dauerAvg: {
      weeks: 1,
    },
    dauerMax: {
      weeks: 1,
    },
  },
];

describe(`Test: getVorlagenListByArt`, () => {
  let getZeitvorlagenvorlagenForVorhabenartStub: sinon.SinonStub;
  const vorlagenListResponse: ZeitvorlagevorlagenBauplanDTO[] = zeitvorlagevorlageContent;
  const returnedObs = of({ response: vorlagenListResponse } as AjaxResponse<ZeitvorlagevorlagenBauplanDTO[]>);

  before(() => {
    getZeitvorlagenvorlagenForVorhabenartStub = sinon
      .stub(zeitplanungsvorlageController, 'getBauplaeneForVorhabenart')
      .returns(returnedObs);
  });
  after(() => {
    getZeitvorlagenvorlagenForVorhabenartStub.restore();
  });

  it('check if REST API is called', () => {
    const resp = zeitplanungController.getVorlagenListByArt(RegelungsvorhabenTypType.Gesetz);
    const resp2 = zeitplanungController.getVorlagenListByArt(RegelungsvorhabenTypType.Gesetz);

    resp.subscribe((data: AjaxResponse<ZeitvorlagevorlagenBauplanDTO[]>) => {
      sinon.assert.match(data.response, vorlagenListResponse);
    });

    resp2.subscribe((data: AjaxResponse<ZeitvorlagevorlagenBauplanDTO[]>) => {
      sinon.assert.match(data.response, vorlagenListResponse);
    });
    sinon.assert.calledOnce(getZeitvorlagenvorlagenForVorhabenartStub);
  });
});

describe('TEST: getMeineZeitplanungenCall', () => {
  let getMeineZeitplanungenStub: sinon.SinonStub;

  before(() => {
    getMeineZeitplanungenStub = sinon.stub(zeitplanungRestController, 'getMeineZeitplanungen');
  });
  after(() => {
    getMeineZeitplanungenStub.restore();
  });

  it('check if REST API is called', () => {
    zeitplanungController.getMeineZeitplanungenCall();
    sinon.assert.calledOnce(getMeineZeitplanungenStub);
  });
});

describe('TEST: getZeitplanungsvorlagenCall', () => {
  let getZeitplanungsvorlagenStub: sinon.SinonStub;

  before(() => {
    getZeitplanungsvorlagenStub = sinon.stub(zeitplanungsvorlageController, 'getZeitplanungsvorlagenForTableView');
  });
  after(() => {
    getZeitplanungsvorlagenStub.restore();
  });
  it('check if REST API is called', () => {
    zeitplanungController.getZeitplanungsvorlagenCall();
    sinon.assert.calledOnce(getZeitplanungsvorlagenStub);
  });
});

describe('TEST: getSystemZeitplanungsvorlagenCall', () => {
  let getSystemZeitplanungsvorlagenStub: sinon.SinonStub;

  before(() => {
    getSystemZeitplanungsvorlagenStub = sinon.stub(
      zeitplanungsvorlageController,
      'getDefaultZeitplanungsvorlagenForTableView',
    );
  });
  after(() => {
    getSystemZeitplanungsvorlagenStub.restore();
  });
  it('check if REST API is called', () => {
    zeitplanungController.getSystemZeitplanungsvorlagenCall();
    sinon.assert.calledOnce(getSystemZeitplanungsvorlagenStub);
  });
});

describe('TEST: getZeitplanungsvorlagenListCall', () => {
  let getZeitplanungsvorlagenListStub: sinon.SinonStub;

  before(() => {
    getZeitplanungsvorlagenListStub = sinon.stub(zeitplanungsvorlageController, 'getZeitplanungsvorlagen');
  });
  after(() => {
    getZeitplanungsvorlagenListStub.restore();
  });
  it('check if REST API is called', () => {
    zeitplanungController.getZeitplanungsvorlagenListCall();
    sinon.assert.calledOnce(getZeitplanungsvorlagenListStub);
  });
});

describe('TEST: getArchiveCall', () => {
  let getArchiveCallStub: sinon.SinonStub;

  before(() => {
    getArchiveCallStub = sinon.stub(zeitplanungRestController, 'getZeitplanungenArchiv');
  });
  after(() => {
    getArchiveCallStub.restore();
  });
  it('check if REST API is called', () => {
    zeitplanungController.getArchiveCall();
    sinon.assert.calledOnce(getArchiveCallStub);
  });
});

describe('TEST: getZeitplanungsvorlageCall', () => {
  let getZeitplanungsvorlageStub: sinon.SinonStub;
  let getArchiveStub: sinon.SinonStub;

  before(() => {
    getZeitplanungsvorlageStub = sinon.stub(zeitplanungsvorlageController, 'getZeitplanungsvorlage');
    getArchiveStub = sinon.stub(zeitplanungRestController, 'getZeitplanungenArchiv');
  });
  after(() => {
    getZeitplanungsvorlageStub.restore();
    getArchiveStub.restore();
  });
  it('check if REST API is called', () => {
    zeitplanungController.getZeitplanungsvorlageCall('cnejcnew39d3');
    sinon.assert.calledOnce(getZeitplanungsvorlageStub);
  });
});

describe('TEST: getSystemVorlageCall', () => {
  let getZeitplanungsvorlageStub: sinon.SinonStub;
  let getArchiveStub: sinon.SinonStub;

  before(() => {
    getZeitplanungsvorlageStub = sinon.stub(zeitplanungsvorlageController, 'getDefaultZeitplanungsvorlageById');
    getArchiveStub = sinon.stub(zeitplanungRestController, 'getZeitplanungenArchiv');
  });
  after(() => {
    getZeitplanungsvorlageStub.restore();
    getArchiveStub.restore();
  });
  it('check if REST API is called', () => {
    zeitplanungController.getSystemVorlageCall('cnejcnew39d3');
    sinon.assert.calledOnce(getZeitplanungsvorlageStub);
  });
});

describe('TEST: getBilligungsanfragenCall', () => {
  let getZeitplanungenBilligungsanfragenStub: sinon.SinonStub;

  before(() => {
    getZeitplanungenBilligungsanfragenStub = sinon.stub(zeitplanungRestController, 'getZeitplanungenBilligungen');
  });
  after(() => {
    getZeitplanungenBilligungsanfragenStub.restore();
  });
  it('check if REST API is called', () => {
    zeitplanungController.getBilligungsanfragenCall();
    sinon.assert.calledOnce(getZeitplanungenBilligungsanfragenStub);
  });
});

describe('TEST: getIsShowZeitplanungSystemvorschlagOpenDialog', () => {
  it('check if REST API is called', () => {
    zeitplanungController.getIsShowZeitplanungSystemvorschlagOpenDialog();
    sinon.assert.calledOnce(isShowZeitplanungSystemvorschlagOpenDialogStub);
    isShowZeitplanungSystemvorschlagOpenDialogStub.restore();
  });
});

describe('TEST: setShowZeitplanungSystemvorschlagOpenDialog', () => {
  it('check if REST API is called', () => {
    zeitplanungController.setShowZeitplanungSystemvorschlagOpenDialog(false);
    sinon.assert.calledOnce(setShowZeitplanungSystemvorschlagOpenDialogStub);
    setShowZeitplanungSystemvorschlagOpenDialogStub.restore();
  });
});

describe('TEST: getIsShowZeitplanungScrollDialog', () => {
  it('check if REST API is called', () => {
    zeitplanungController.getIsShowZeitplanungScrollDialog();
    sinon.assert.calledOnce(isShowZeitplanungScrollDialogStub);
    isShowZeitplanungScrollDialogStub.restore();
  });
});

describe('TEST: setShowZeitplanungScrollDialog', () => {
  it('check if REST API is called', () => {
    zeitplanungController.setShowZeitplanungScrollDialog(false);
    sinon.assert.calledOnce(setShowZeitplanungScrollDialogStub);
    setShowZeitplanungScrollDialogStub.restore();
  });
});

describe('TEST: archiveZeitplanungsvorlageCall', () => {
  let archiveZeitplanungsvorlageStub: sinon.SinonStub;
  before(() => {
    archiveZeitplanungsvorlageStub = sinon.stub(zeitplanungsvorlageController, 'archiveZeitplanungsvorlage');
  });
  after(() => {
    archiveZeitplanungsvorlageStub.restore();
  });
  it('check if REST API is called', () => {
    zeitplanungController.archiveZeitplanungsvorlageCall('abc');
    sinon.assert.calledOnce(archiveZeitplanungsvorlageStub);
  });
});

describe(`Test: exportZeitplanung `, () => {
  let exportPlanungStub: sinon.SinonStub;
  let exportVorlageStub: sinon.SinonStub;
  before(() => {
    exportPlanungStub = sinon.stub(exportController, 'createZeitplanungPdfSummary');
    exportVorlageStub = sinon.stub(exportController, 'createZeitplanungsvorlagePdfSummary');
    window.URL.createObjectURL = sinon.stub();
  });
  afterEach(() => {
    setLoadingStatusStub.resetHistory();
    exportPlanungStub.resetHistory();
    exportVorlageStub.resetHistory();
  });
  after(() => {
    exportPlanungStub.reset();
    exportVorlageStub.reset();
  });
  it(`no zeitplanung, nothing called`, () => {
    zeitplanungController.exportZeitplanung();
    sinon.assert.notCalled(setLoadingStatusStub);
    sinon.assert.notCalled(exportPlanungStub);
    sinon.assert.notCalled(exportVorlageStub);
  });
  it(`correct endpoint for zeitplanung and setLoadingStatus called`, (done) => {
    exportPlanungStub.callsFake(() => {
      return new Observable<AjaxResponse<Blob>>((observer) => {
        observer.next({
          text: () =>
            new Promise((resolve) => {
              resolve('Test');
            }),
          type: 'pdf',
        });
      });
    });
    zeitplanungController.exportZeitplanung({} as ZeitplanungEntityDTO, false);
    setTimeout(() => {
      sinon.assert.calledTwice(setLoadingStatusStub);
      sinon.assert.calledOnce(exportPlanungStub);
      sinon.assert.notCalled(exportVorlageStub);
      done();
    }, 20);
  });
  it(`correct endpoint for zeitplanungsvorlage and setLoadingStatus called`, (done) => {
    exportVorlageStub.callsFake(() => {
      return new Observable<AjaxResponse<Blob>>((observer) => {
        observer.next({
          text: () =>
            new Promise((resolve) => {
              resolve('Test');
            }),
          type: 'pdf',
        });
      });
    });
    zeitplanungController.exportZeitplanung({} as ZeitplanungsvorlageEntityDTO, true);
    setTimeout(() => {
      sinon.assert.calledTwice(setLoadingStatusStub);
      sinon.assert.calledOnce(exportVorlageStub);
      sinon.assert.notCalled(exportPlanungStub);
      done();
    }, 20);
  });
});

describe('TEST: archiveZeitplanungsvorlage', () => {
  let archiveZeitplanungsvorlageStub: sinon.SinonStub;
  const resetTabSpy = sinon.stub();
  before(() => {
    archiveZeitplanungsvorlageStub = sinon.stub(zeitplanungsvorlageController, 'archiveZeitplanungsvorlage');
  });

  afterEach(() => {
    resetTabSpy.resetHistory();
    archiveZeitplanungsvorlageStub.resetHistory();
    setLoadingStatusStub.resetHistory();
    displayErrorMsgStub.resetHistory();
  });

  after(() => {
    archiveZeitplanungsvorlageStub.restore();
  });

  it('unsuccessful call, correct functions should be called', () => {
    archiveZeitplanungsvorlageStub.callsFake(() => {
      return new Observable<void>((observer) => observer.error());
    });
    zeitplanungController.archiveZeitplanungsvorlage('abc', 'titel', resetTabSpy);
    sinon.assert.calledTwice(setLoadingStatusStub);
    sinon.assert.calledOnce(displayErrorMsgStub);
    expect(resetTabSpy.called).to.be.false;
  });

  it('successful call, correct functions should be called', () => {
    archiveZeitplanungsvorlageStub.returns(of(undefined));
    zeitplanungController.archiveZeitplanungsvorlage('abc', 'titel', resetTabSpy);
    sinon.assert.calledTwice(setLoadingStatusStub);
    sinon.assert.calledOnce(archiveZeitplanungsvorlageStub);
    sinon.assert.calledOnce(resetTabSpy);
    archiveZeitplanungsvorlageStub.restore();
  });
});

describe('TEST: archiveZeitplanung', () => {
  let archiveZeitplanungStub: sinon.SinonStub;
  const resetTabSpy = sinon.stub();
  before(() => {
    archiveZeitplanungStub = sinon.stub(zeitplanungRestController, 'archiveZeitplanung');
  });

  afterEach(() => {
    resetTabSpy.resetHistory();
    archiveZeitplanungStub.resetHistory();
    setLoadingStatusStub.resetHistory();
    displayErrorMsgStub.resetHistory();
  });

  after(() => {
    archiveZeitplanungStub.restore();
  });

  it('unsuccessful call, correct functions should be called', () => {
    archiveZeitplanungStub.callsFake(() => {
      return new Observable<void>((observer) => observer.error());
    });
    zeitplanungController.archiveZeitplanung('abc', 'titel', resetTabSpy);
    sinon.assert.calledTwice(setLoadingStatusStub);
    sinon.assert.calledOnce(displayErrorMsgStub);
    sinon.assert.notCalled(resetTabSpy);
  });

  it('successful call, correct functions should be called', () => {
    archiveZeitplanungStub.returns(of(undefined));
    zeitplanungController.archiveZeitplanung('abc', 'titel', resetTabSpy);
    sinon.assert.calledTwice(setLoadingStatusStub);
    sinon.assert.calledOnce(archiveZeitplanungStub);
    sinon.assert.calledOnce(resetTabSpy);
    archiveZeitplanungStub.restore();
  });
});

describe('TEST: archiveZeichnung', () => {
  let archiveZeichnungStub: sinon.SinonStub;
  const resetTabSpy = sinon.stub();
  before(() => {
    archiveZeichnungStub = sinon.stub(zeitplanungRestController, 'archiveBilligungsanfrage');
  });

  afterEach(() => {
    resetTabSpy.resetHistory();
    archiveZeichnungStub.resetHistory();
    setLoadingStatusStub.resetHistory();
    displayErrorMsgStub.resetHistory();
  });

  after(() => {
    archiveZeichnungStub.restore();
  });

  it('unsuccessful call, correct functions should be called', () => {
    archiveZeichnungStub.callsFake(() => {
      return new Observable<void>((observer) => observer.error());
    });
    zeitplanungController.archiveZeichnung('abc', 'titel', resetTabSpy);
    sinon.assert.calledTwice(setLoadingStatusStub);
    sinon.assert.calledOnce(displayErrorMsgStub);
    sinon.assert.notCalled(resetTabSpy);
  });

  it('successful call, correct functions should be called', () => {
    archiveZeichnungStub.returns(of(undefined));
    zeitplanungController.archiveZeichnung('abc', 'titel', resetTabSpy);
    sinon.assert.calledTwice(setLoadingStatusStub);
    sinon.assert.calledOnce(archiveZeichnungStub);
    sinon.assert.calledOnce(resetTabSpy);
    archiveZeichnungStub.restore();
  });
});

describe('TEST: deleteZeitvorlageFromZeitplanungsvorlageCall', () => {
  let deleteZeitvorlageFromZeitplanungsvorlageCallStub: sinon.SinonStub;

  before(() => {
    deleteZeitvorlageFromZeitplanungsvorlageCallStub = sinon.stub(
      zeitplanungsvorlageController,
      'deleteZeitplanungsvorlagenelementFromZeitplanungsvorlage',
    );
  });
  after(() => {
    deleteZeitvorlageFromZeitplanungsvorlageCallStub.restore();
  });
  it('check if REST API is called', () => {
    zeitplanungController.deleteZeitvorlageFromZeitplanungsvorlageCall('123');
    sinon.assert.calledOnce(deleteZeitvorlageFromZeitplanungsvorlageCallStub);
  });
});

describe('TEST: getZeitplanungCall', () => {
  let getZeitplanungCallStub: sinon.SinonStub;

  before(() => {
    getZeitplanungCallStub = sinon.stub(zeitplanungRestController, 'getZeitplanung');
  });
  after(() => {
    getZeitplanungCallStub.restore();
  });
  it('check if REST API is called', () => {
    zeitplanungController.getZeitplanungCall('123');
    sinon.assert.calledOnce(getZeitplanungCallStub);
  });
});

describe('TEST: getDurationString - create string containing date and duration in days in correct format', () => {
  it('can create duration string', () => {
    expect(zeitplanungController.getDurationString(date1, '5')).to.equal('21.04.2021 (in 5 Tagen)');
    expect(zeitplanungController.getDurationString(date2, '6')).to.equal('16.04.2028 (in 6 Tagen)');
    expect(zeitplanungController.getDurationString(date1, '73948')).to.equal('21.04.2021 (in 73948 Tagen)');
    expect(zeitplanungController.getDurationString(date2, '89383')).to.equal('16.04.2028 (in 89383 Tagen)');
  });
  it('cant create duration string, date is invalid', () => {
    assert.throws(() => zeitplanungController.getDurationString(invalidDate, '5'), RangeError, 'Invalid time value');
  });
});

describe('TEST: getFilteredRows - filter rows by value for specified column', () => {
  it('filter rows by value for specified column - check for correct length', () => {
    expect(
      zeitplanungController.getFilteredRows(zeitplanungContent, 'dto1short', {
        name: 'regelungsvorhaben',
        columnIndex: 2,
      }).length,
    ).to.equal(1);
    expect(
      zeitplanungController.getFilteredRows(zeitplanungContent, 'dto2short', {
        name: 'regelungsvorhaben',
        columnIndex: 2,
      }).length,
    ).to.equal(1);
    expect(
      zeitplanungController.getFilteredRows(zeitplanungContent, 'dto3short', {
        name: 'regelungsvorhaben',
        columnIndex: 2,
      }).length,
    ).to.equal(1);

    expect(
      zeitplanungController.getFilteredRows(
        zeitplanungsVorlagenContent,
        i18n.t(`zeit.myZeitHome.tabs.zeitplanungsvorlagen.table.vorhabenart.${RegelungsvorhabenTypType.Gesetz}`),
        {
          name: 'vorhabenart',
          columnIndex: 16,
        },
      ).length,
    ).to.equal(1);
    expect(
      zeitplanungController.getFilteredRows(
        zeitplanungsVorlagenContent,
        i18n.t(
          `zeit.myZeitHome.tabs.zeitplanungsvorlagen.table.vorhabenart.${RegelungsvorhabenTypType.Rechtsverordnung}`,
        ),
        {
          name: 'vorhabenart',
          columnIndex: 16,
        },
      ).length,
    ).to.equal(1);
    expect(
      zeitplanungController.getFilteredRows(
        zeitplanungsVorlagenContent,
        i18n.t(
          `zeit.myZeitHome.tabs.zeitplanungsvorlagen.table.vorhabenart.${RegelungsvorhabenTypType.Verwaltungsvorschrift}`,
        ),
        {
          name: 'vorhabenart',
          columnIndex: 16,
        },
      ).length,
    ).to.equal(1);
  });
  it('filter rows by value for specified column - check correct content is returned', () => {
    expect(
      zeitplanungController.getFilteredRows(zeitplanungContent, 'dto1short', {
        name: 'regelungsvorhaben',
        columnIndex: 2,
      })[0],
    ).to.equal(zeitplanungContent[0]);

    expect(
      zeitplanungController.getFilteredRows(zeitplanungContent, 'dto2short', {
        name: 'regelungsvorhaben',
        columnIndex: 2,
      })[0],
    ).to.equal(zeitplanungContent[1]);

    expect(
      zeitplanungController.getFilteredRows(zeitplanungContent, 'dto3short', {
        name: 'regelungsvorhaben',
        columnIndex: 2,
      })[0],
    ).to.equal(zeitplanungContent[2]);

    expect(
      zeitplanungController.getFilteredRows(
        zeitplanungsVorlagenContent,
        i18n.t(`zeit.myZeitHome.tabs.zeitplanungsvorlagen.table.vorhabenart.${RegelungsvorhabenTypType.Gesetz}`),
        {
          name: 'vorhabenart',
          columnIndex: 16,
        },
      )[0],
    ).to.equal(zeitplanungsVorlagenContent[0]);
    expect(
      zeitplanungController.getFilteredRows(
        zeitplanungsVorlagenContent,
        i18n.t(
          `zeit.myZeitHome.tabs.zeitplanungsvorlagen.table.vorhabenart.${RegelungsvorhabenTypType.Rechtsverordnung}`,
        ),
        {
          name: 'vorhabenart',
          columnIndex: 16,
        },
      )[0],
    ).to.equal(zeitplanungsVorlagenContent[1]);
    expect(
      zeitplanungController.getFilteredRows(
        zeitplanungsVorlagenContent,
        i18n.t(
          `zeit.myZeitHome.tabs.zeitplanungsvorlagen.table.vorhabenart.${RegelungsvorhabenTypType.Verwaltungsvorschrift}`,
        ),
        {
          name: 'vorhabenart',
          columnIndex: 16,
        },
      )[0],
    ).to.equal(zeitplanungsVorlagenContent[2]);

    expect(
      zeitplanungController.getFilteredRows(
        archiveContent,
        i18n.t(`zeit.myZeitHome.tabs.archiv.table.tabs.${ZeitplanungArchivTableDTOOriginalTabEnum.MeineZeitplanungen}`),
        {
          name: 'originalTab',
          columnIndex: 16,
        },
      )[0],
    ).to.equal(archiveContent[1]);
    expect(
      zeitplanungController.getFilteredRows(
        archiveContent,
        i18n.t(`zeit.myZeitHome.tabs.archiv.table.tabs.${ZeitplanungArchivTableDTOOriginalTabEnum.Billigungen}`),
        {
          name: 'originalTab',
          columnIndex: 16,
        },
      )[0],
    ).to.equal(archiveContent[0]);
    expect(
      zeitplanungController.getFilteredRows(
        archiveContent,
        i18n.t(
          `zeit.myZeitHome.tabs.archiv.table.tabs.${ZeitplanungArchivTableDTOOriginalTabEnum.Zeitplanungsvorlagen}`,
        ),
        {
          name: 'originalTab',
          columnIndex: 16,
        },
      )[0],
    ).to.equal(archiveContent[2]);
    expect(
      zeitplanungController.getFilteredRows(zeitplanungContent, user.dto.name, {
        name: 'erstelltVon',
        columnIndex: 4,
      })[0],
    ).to.equal(zeitplanungContent[0]);
  });

  it('filter rows by value for unknown column - check if empty array is returned', () => {
    expect(
      zeitplanungController.getFilteredRows(zeitplanungContent, 'dto1short', { name: 'xyz', columnIndex: 2 }).length,
    ).to.equal(0);
  });
});

describe('TEST: getFilterButton - get correct filter button properties', () => {
  it('get filter button props for regelungsvorhaben', () => {
    const filterButton = zeitplanungController.getFilterButton(zeitplanungContent, {
      name: 'regelungsvorhaben',
      columnIndex: 2,
    });
    expect(filterButton.displayName).to.equal(i18n.t('zeit.myZeitHome.tabs.filter.displayNames.regelungsvorhaben'));
    expect(filterButton.labelContent).to.equal(i18n.t(`zeit.myZeitHome.tabs.filter.labelContent`));
    expect(Array.from(filterButton.options)[0]).to.equal('dto1short');
    expect(Array.from(filterButton.options)[1]).to.equal('dto2short');
    expect(Array.from(filterButton.options)[2]).to.equal('dto3short');
  });

  it('get filter button props for typ', () => {
    const filterButton = zeitplanungController.getFilterButton(zeitplanungContent, { name: 'typ', columnIndex: 3 });
    expect(filterButton.displayName).to.equal(i18n.t('zeit.myZeitHome.tabs.filter.displayNames.typ'));
    expect(filterButton.labelContent).to.equal(i18n.t(`zeit.myZeitHome.tabs.filter.labelContent`));
  });

  it('get filter button props for vorhabenart', () => {
    const filterButton = zeitplanungController.getFilterButton(zeitplanungsVorlagenContent, {
      name: 'vorhabenart',
      columnIndex: 4,
    });
    expect(filterButton.displayName).to.equal(i18n.t('zeit.myZeitHome.tabs.filter.displayNames.vorhabenart'));
    expect(filterButton.labelContent).to.equal(i18n.t(`zeit.myZeitHome.tabs.filter.labelContent`));
    expect(Array.from(filterButton.options)[0]).to.equal(
      i18n.t(`zeit.myZeitHome.tabs.zeitplanungsvorlagen.table.vorhabenart.${RegelungsvorhabenTypType.Gesetz}`),
    );
    expect(Array.from(filterButton.options)[1]).to.equal(
      i18n.t(
        `zeit.myZeitHome.tabs.zeitplanungsvorlagen.table.vorhabenart.${RegelungsvorhabenTypType.Rechtsverordnung}`,
      ),
    );
    expect(Array.from(filterButton.options)[2]).to.equal(
      i18n.t(
        `zeit.myZeitHome.tabs.zeitplanungsvorlagen.table.vorhabenart.${RegelungsvorhabenTypType.Verwaltungsvorschrift}`,
      ),
    );
  });

  it('get filter button props for originalTab', () => {
    const filterButton = zeitplanungController.getFilterButton(archiveContent, {
      name: 'originalTab',
      columnIndex: 4,
    });
    expect(filterButton.displayName).to.equal(i18n.t('zeit.myZeitHome.tabs.filter.displayNames.originalTab'));
    expect(filterButton.labelContent).to.equal(i18n.t(`zeit.myZeitHome.tabs.filter.labelContent`));
    expect(Array.from(filterButton.options)[0]).to.equal(
      i18n.t(`zeit.myZeitHome.tabs.archiv.table.tabs.${ZeitplanungArchivTableDTOOriginalTabEnum.Billigungen}`),
    );
    expect(Array.from(filterButton.options)[1]).to.equal(
      i18n.t(`zeit.myZeitHome.tabs.archiv.table.tabs.${ZeitplanungArchivTableDTOOriginalTabEnum.MeineZeitplanungen}`),
    );
    expect(Array.from(filterButton.options)[2]).to.equal(
      i18n.t(`zeit.myZeitHome.tabs.archiv.table.tabs.${ZeitplanungArchivTableDTOOriginalTabEnum.Zeitplanungsvorlagen}`),
    );
  });

  it('get filter button props for ersteller', () => {
    const filterButton = zeitplanungController.getFilterButton(zeitplanungContent, {
      name: 'erstelltVon',
      columnIndex: 4,
    });
    expect(filterButton.displayName).to.equal(i18n.t('zeit.myZeitHome.tabs.filter.displayNames.erstelltVon'));
    expect(filterButton.labelContent).to.equal(i18n.t(`zeit.myZeitHome.tabs.filter.labelContent`));
    expect(Array.from(filterButton.options)[0]).to.equal(user.dto.name);
  });
});
