// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import { saveAs } from 'file-saver';
/* eslint-disable @typescript-eslint/restrict-template-expressions */
import i18n from 'i18next';
import { Observable, of } from 'rxjs';
import { AjaxError } from 'rxjs/ajax';
import { shareReplay } from 'rxjs/operators';

import {
  ExportControllerApi,
  PaginierungDTO,
  UserControllerApi,
  ZeitplanungArchivTableDTOs,
  ZeitplanungBilligungTableDTOs,
  ZeitplanungControllerApi,
  ZeitplanungsvorlageControllerApi,
  ZeitplanungsvorlageTableDTOs,
  ZeitplanungTableDTOs,
} from '@plateg/rest-api';
import {
  RegelungsvorhabenEntityResponseShortDTO,
  RegelungsvorhabenTypType,
  VorhabenStatusType,
  ZeitplanungEntityDTO,
  ZeitplanungEntityResponseDTO,
  ZeitplanungsvorlageEntityDTO,
  ZeitplanungsvorlageEntityListResponseShortDTO,
  ZeitplanungsvorlageEntityResponseDTO,
  ZeitvorlagevorlagenBauplanDTO,
} from '@plateg/rest-api/models';
import { CommonRow, displayMessage, ErrorController, LoadingStatusController } from '@plateg/theme';
import { GlobalDI } from '@plateg/theme/src/shares';
import { Filters } from '@plateg/theme/src/shares/filters';

export class ZeitplanungController {
  private readonly vorlagenList = new Map<string, Array<ZeitvorlagevorlagenBauplanDTO>>();
  private readonly loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  private readonly errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  private readonly userController = GlobalDI.getOrRegister('userController', () => new UserControllerApi());
  private readonly zeitplanungsvorlageController = GlobalDI.get<ZeitplanungsvorlageControllerApi>(
    'zeitplanunsvorlageRestController',
  );
  private readonly zeitplanungController = GlobalDI.get<ZeitplanungControllerApi>('zeitplanungRestController');

  public getVorlagenListByArt(vorhabenart: RegelungsvorhabenTypType): Observable<Array<ZeitvorlagevorlagenBauplanDTO>> {
    let obs: Observable<Array<ZeitvorlagevorlagenBauplanDTO>>;
    if (!this.vorlagenList.get(vorhabenart)) {
      obs = this.zeitplanungsvorlageController.getBauplaeneForVorhabenart({ vorhabenart }).pipe(shareReplay(1));
      obs.subscribe((data) => {
        this.vorlagenList.set(vorhabenart, data);
      });
    } else {
      obs = of(this.vorlagenList.get(vorhabenart)) as Observable<Array<ZeitvorlagevorlagenBauplanDTO>>;
    }
    return obs;
  }

  public getMeineZeitplanungenCall = (
    isFilterNeeded: boolean,
    paginierungDTO: PaginierungDTO,
  ): Observable<ZeitplanungTableDTOs> => {
    return this.zeitplanungController.getMeineZeitplanungen({ isFilterNeeded, paginierungDTO });
  };

  public getZeitplanungsvorlagenCall = (
    isFilterNeeded: boolean,
    paginierungDTO: PaginierungDTO,
  ): Observable<ZeitplanungsvorlageTableDTOs> => {
    return this.zeitplanungsvorlageController.getZeitplanungsvorlagenForTableView({ isFilterNeeded, paginierungDTO });
  };

  public getSystemZeitplanungsvorlagenCall = (
    isFilterNeeded: boolean,
    paginierungDTO: PaginierungDTO,
  ): Observable<ZeitplanungsvorlageTableDTOs> => {
    return this.zeitplanungsvorlageController.getDefaultZeitplanungsvorlagenForTableView({
      paginierungDTO,
    });
  };

  public getZeitplanungsvorlagenListCall = (): Observable<ZeitplanungsvorlageEntityListResponseShortDTO> => {
    return this.zeitplanungsvorlageController.getZeitplanungsvorlagen();
  };

  public getArchiveCall = (
    isFilterNeeded: boolean,
    paginierungDTO: PaginierungDTO,
  ): Observable<ZeitplanungArchivTableDTOs> => {
    return this.zeitplanungController.getZeitplanungenArchiv({ isFilterNeeded, paginierungDTO });
  };

  public getZeitplanungsvorlageCall = (id: string): Observable<ZeitplanungsvorlageEntityResponseDTO> => {
    return this.zeitplanungsvorlageController.getZeitplanungsvorlage({ id });
  };
  public getSystemVorlageCall = (id: string): Observable<ZeitplanungsvorlageEntityResponseDTO> => {
    return this.zeitplanungsvorlageController.getDefaultZeitplanungsvorlageById({ id });
  };

  public getBilligungsanfragenCall = (
    isFilterNeeded: boolean,
    paginierungDTO: PaginierungDTO,
  ): Observable<ZeitplanungBilligungTableDTOs> => {
    return this.zeitplanungController.getZeitplanungenBilligungen({ isFilterNeeded, paginierungDTO });
  };

  public getIsShowZeitplanungScrollDialog = (): Observable<boolean> => {
    return this.userController.isShowZeitplanungScrollDialog();
  };

  public setShowZeitplanungScrollDialog = (value: boolean): Observable<void> => {
    return this.userController.setShowZeitplanungScrollDialog({ value });
  };

  public getIsShowZeitplanungSystemvorschlagOpenDialog = (): Observable<boolean> => {
    return this.userController.isShowZeitplanungSystemvorschlagOpenDialog();
  };

  public setShowZeitplanungSystemvorschlagOpenDialog = (value: boolean): Observable<void> => {
    return this.userController.setShowZeitplanungSystemvorschlagOpenDialog({ value });
  };

  public archiveZeitplanungsvorlageCall = (id: string): Observable<void> => {
    return this.zeitplanungsvorlageController.archiveZeitplanungsvorlage({ id });
  };

  public archiveZeitplanungCall = (id: string): Observable<void> => {
    return this.zeitplanungController.archiveZeitplanung({ id });
  };

  private archiveZeichnungCall = (billigungId: string): Observable<void> => {
    return this.zeitplanungController.archiveBilligungsanfrage({ billigungId });
  };

  private cancelBilligungsanfrageCall = (billigungId: string): Observable<void> => {
    return this.zeitplanungController.cancelBilligungsanfrage({ billigungId });
  };

  public exportZeitplanung = (
    zeitplanung?: ZeitplanungEntityDTO | ZeitplanungsvorlageEntityDTO,
    isVorlage?: boolean,
  ) => {
    if (zeitplanung && isVorlage !== undefined) {
      this.loadingStatusController.setLoadingStatus(true);
      const exportController = GlobalDI.get<ExportControllerApi>('exportControllerApi');
      const obs = isVorlage
        ? exportController.createZeitplanungsvorlagePdfSummary({
            zeitplanungsvorlageEntityDTO: zeitplanung as ZeitplanungsvorlageEntityDTO,
          })
        : exportController.createZeitplanungPdfSummary({ zeitplanungEntityDTO: zeitplanung as ZeitplanungEntityDTO });
      obs.subscribe({
        next: (data) => {
          saveAs(data, `${zeitplanung.titel || 'zeit'}.pdf`);
          this.loadingStatusController.setLoadingStatus(false);
        },
        error: (error: AjaxError) => this.errorCallback(error),
      });
    }
  };

  public archiveZeitplanungsvorlage = (id: string, titel: string, onSucces: () => void): void => {
    this.loadingStatusController.setLoadingStatus(true);
    this.archiveZeitplanungsvorlageCall(id).subscribe({
      next: () => {
        displayMessage(
          i18n.t('zeit.myZeitHome.tabs.zeitplanungsvorlagen.table.actions.msgs.archiveSuccess', {
            titel: titel,
          }),
          'success',
        );
        this.loadingStatusController.setLoadingStatus(false);
        onSucces();
      },
      error: (error: AjaxError) => this.errorCallback(error),
    });
  };

  private errorCallback = (error: AjaxError): void => {
    this.errorCtrl.displayErrorMsg(error, 'zeit.generalErrorMsg');
    console.log(error);
    this.loadingStatusController.setLoadingStatus(false);
  };

  public archiveZeitplanung = (id: string, titel: string, onSucces: () => void): void => {
    this.loadingStatusController.setLoadingStatus(true);
    this.archiveZeitplanungCall(id).subscribe({
      next: () => {
        displayMessage(
          i18n.t('zeit.myZeitHome.tabs.meineZeitplanungen.archiveConfirmModal.archiveSuccess', {
            titel: titel,
          }),
          'success',
        );
        this.loadingStatusController.setLoadingStatus(false);
        onSucces();
      },
      error: (error: AjaxError) => this.errorCallback(error),
    });
  };

  public archiveZeichnung = (id: string, titel: string, onSucces: () => void): void => {
    this.loadingStatusController.setLoadingStatus(true);
    this.archiveZeichnungCall(id).subscribe({
      next: () => {
        displayMessage(
          i18n.t('zeit.myZeitHome.tabs.billigungsanfragen.archiveConfirmModal.archiveSuccess', {
            titel: titel,
          }),
          'success',
        );
        this.loadingStatusController.setLoadingStatus(false);
        onSucces();
      },
      error: (error: AjaxError) => this.errorCallback(error),
    });
  };

  public cancelBilligungsanfrage = (id: string, titel: string, onSucces: () => void): void => {
    this.loadingStatusController.setLoadingStatus(true);
    this.cancelBilligungsanfrageCall(id).subscribe({
      next: () => {
        displayMessage(
          i18n.t('zeit.myZeitHome.tabs.billigungsanfragen.cancelConfirmModal.cancelSuccess', {
            titel: titel,
          }),
          'success',
        );
        this.loadingStatusController.setLoadingStatus(false);
        onSucces();
      },
      error: (error: AjaxError) => this.errorCallback(error),
    });
  };

  public deleteZeitvorlageFromZeitplanungsvorlageCall = (zeitvorlageId: string): Observable<void> => {
    return this.zeitplanungsvorlageController.deleteZeitplanungsvorlagenelementFromZeitplanungsvorlage({
      elementId: zeitvorlageId,
    });
  };

  public deleteZeitplanungselementFromZeitplanungCall = (
    elementId: string,
    zeitplanungAnpassen: boolean,
  ): Observable<void> => {
    return this.zeitplanungController.deleteZeitplanungselementFromZeitplanung({ elementId, zeitplanungAnpassen });
  };

  public getZeitplanungCall = (id: string): Observable<ZeitplanungEntityResponseDTO> => {
    return this.zeitplanungController.getZeitplanung({ id });
  };

  public getRvString = (regelungsvorhaben: RegelungsvorhabenEntityResponseShortDTO): string => {
    if (!regelungsvorhaben) {
      return '';
    }
    return regelungsvorhaben.dto.status === VorhabenStatusType.Abgebrochen ||
      regelungsvorhaben.dto.status === VorhabenStatusType.Abgeschlossen
      ? `${i18n.t('zeit.myZeitHome.archivedLabel')}: ${regelungsvorhaben.dto.abkuerzung}`
      : regelungsvorhaben.dto.abkuerzung;
  };

  public getRvLink = (regelungsvorhaben: RegelungsvorhabenEntityResponseShortDTO): string => {
    return `#/regelungsvorhaben/meineRegelungsvorhaben/${regelungsvorhaben?.base.id}/datenblatt`;
  };

  public getDurationString = (date: string, days: string): string => {
    let daysStr = 'Tagen';
    if (days === '1') {
      daysStr = 'Tag';
    }
    return `${Filters.dateFromString(date)} (in ${days} ${daysStr})`;
  };

  public getDurationDatesString = (date: string, dateEnd: string, days: string): string => {
    let daysStr = 'Tagen';
    if (days === '1') {
      daysStr = 'Tag';
    }
    return `${Filters.dateFromString(date)} -${Filters.dateFromString(dateEnd)} (in ${days} ${daysStr})`;
  };

  public getFilteredRows = <T extends CommonRow>(
    actualContent: T[],
    filteredBy: string,
    column: { name: string; columnIndex: number },
  ): T[] => {
    let newRows: Set<T> = new Set();
    if (column.name === 'regelungsvorhaben') {
      newRows = new Set(
        actualContent.filter((row) => {
          return row.regelungsvorhaben?.dto?.abkuerzung === filteredBy;
        }),
      );
    }

    if (column.name === 'vorhabenart') {
      newRows = new Set(
        actualContent.filter((row) => {
          return (
            i18n.t(`zeit.myZeitHome.tabs.zeitplanungsvorlagen.table.vorhabenart.${row.vorhabenart}`) === filteredBy
          );
        }),
      );
    }
    if (column.name === 'originalTab') {
      newRows = new Set(
        actualContent.filter((row) => {
          return i18n.t(`zeit.myZeitHome.tabs.archiv.table.tabs.${row.originalTab}`) === filteredBy;
        }),
      );
    }
    if (column.name === 'erstelltVon') {
      newRows = new Set(
        actualContent.filter((row) => {
          return row.erstelltVon.dto.name === filteredBy;
        }),
      );
    }
    if (column.name === 'rueckmeldung') {
      newRows = new Set(
        actualContent.filter((row) => {
          return (
            i18n.t(`zeit.myZeitHome.tabs.billigungsanfragen.table.status.${row.billigung.dto.status}`) === filteredBy
          );
        }),
      );
    }
    return Array.from(newRows);
  };

  public getFilterButton = <T extends CommonRow>(
    initialContent: T[],
    column: { name: string; columnIndex: number },
  ): { displayName: string; labelContent: string; options: Set<string> } => {
    const displayName = i18n.t(`zeit.myZeitHome.tabs.filter.displayNames.${column.name}`);
    let options: Set<string> = new Set();
    const labelContent = i18n.t(`zeit.myZeitHome.tabs.filter.labelContent`);
    if (column.name === 'regelungsvorhaben') {
      options = new Set(
        initialContent
          .map((row) => {
            return row.regelungsvorhaben?.dto?.abkuerzung as string;
          })
          .filter(function (x) {
            return x !== undefined;
          }),
      );
    }

    if (column.name === 'vorhabenart') {
      options = new Set(
        initialContent.map((row) => {
          return i18n.t(`zeit.myZeitHome.tabs.zeitplanungsvorlagen.table.vorhabenart.${row.vorhabenart}`);
        }),
      );
    }
    if (column.name === 'originalTab') {
      options = new Set(
        initialContent.map((row) => {
          return i18n.t(`zeit.myZeitHome.tabs.archiv.table.tabs.${row.originalTab}`);
        }),
      );
    }
    if (column.name === 'erstelltVon') {
      options = new Set(
        initialContent.map((row) => {
          return row.erstelltVon.dto.name as string;
        }),
      );
    }
    if (column.name === 'rueckmeldung') {
      options = new Set(
        initialContent.map((row) => {
          return i18n.t(`zeit.myZeitHome.tabs.billigungsanfragen.table.status.${row.billigung.dto.status}`);
        }),
      );
    }
    return { displayName, labelContent, options };
  };
}
