// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '@plateg/theme/src/components/table-component/table-sub-components/archiv-confirm/archive-confirm.less';

import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { ZeitplanungBilligungTableDTO } from '@plateg/rest-api';
import { GlobalDI, ModalWrapper } from '@plateg/theme';

import { routes } from '../../../../../shares/routes';
import { ZeitplanungController } from '../controller';

interface AnfrageZurueckziehenModalProps {
  visible: boolean;
  setVisible: (visible: boolean) => void;
  zeichnung: ZeitplanungBilligungTableDTO;
  resetTab: (key: string) => void;
}

export function AnfrageZurueckziehenModal(props: Readonly<AnfrageZurueckziehenModalProps>): React.ReactElement {
  const { t } = useTranslation();
  const zeitplanungController = GlobalDI.getOrRegister('zeitplanungController', () => new ZeitplanungController());
  const onFinish = () => {
    zeitplanungController.cancelBilligungsanfrage(props.zeichnung.billigung.base.id, props.zeichnung.titel, () => {
      props.setVisible(false);
      props.resetTab(routes.BILLIGUNGSANFRAGEN);
    });
  };
  return (
    <ModalWrapper
      className="archiv-confirm-modal"
      open={props.visible}
      onOk={onFinish}
      onCancel={() => props.setVisible(!props.visible)}
      okText={t('zeit.myZeitHome.tabs.billigungsanfragen.cancelConfirmModal.confirm')}
      cancelText={t('zeit.myZeitHome.tabs.billigungsanfragen.cancelConfirmModal.cancel')}
      title={<></>}
    >
      <Title level={3} className="archiv-confirm-title">
        {t('zeit.myZeitHome.tabs.billigungsanfragen.cancelConfirmModal.title')}
      </Title>
      <p className="archiv-confirm-content">
        {t('zeit.myZeitHome.tabs.billigungsanfragen.cancelConfirmModal.content')}
      </p>
    </ModalWrapper>
  );
}
