// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import i18n from 'i18next';
import { AjaxError } from 'rxjs/ajax';

import {
  BerechtigungControllerApi,
  BerechtigungListResponseDTO,
  BerechtigungResponseDTO,
  RessourceType,
  UpdatePermissionsDTO,
} from '@plateg/rest-api';
import { displayMessage, ErrorController, LoadingStatusController } from '@plateg/theme';
import { GlobalDI } from '@plateg/theme/src/shares';

export class FreigebenModalRBACController {
  private readonly loadingStatusController = GlobalDI.getOrRegister(
    'loadingStatusController',
    () => new LoadingStatusController(),
  );
  private readonly errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());

  private readonly berechtigungController = GlobalDI.getOrRegister<BerechtigungControllerApi>(
    'berechtigungControllerApi',
    () => new BerechtigungControllerApi(),
  );

  public getFreigaben(
    rvId: string,
    setFreigaben: (value: BerechtigungResponseDTO[]) => void,
    isZeitplanungvorlage = false,
  ) {
    this.berechtigungController
      .getBerechtigungen({
        id: rvId,
        type: isZeitplanungvorlage ? RessourceType.Zeitplanungsvorlage : RessourceType.Regelungsvorhaben,
      })
      .subscribe({
        next: (data: BerechtigungListResponseDTO) => {
          setFreigaben(data.dtos);
        },
        error: (error: AjaxError) => {
          setFreigaben([]);
          this.errorCallback(error);
        },
      });
  }

  public vorlageFreigeben(
    objectTitle: string | undefined,
    objectId: string,
    freigebenValues: UpdatePermissionsDTO,
    setIsVisible: (visible: boolean) => void,
    reloadFreigeben?: () => void,
    isZeitplanungvorlage = false,
  ): void {
    setIsVisible(false);
    this.loadingStatusController.setLoadingStatus(true);
    const freigabeRequest = this.berechtigungController.setBerechtigungen({
      type: isZeitplanungvorlage ? RessourceType.Zeitplanungsvorlage : RessourceType.Regelungsvorhaben,
      id: objectId,
      updatePermissionsDTO: freigebenValues,
    });

    freigabeRequest.subscribe({
      next: () => {
        this.handleSave(objectTitle || '', isZeitplanungvorlage);
        if (reloadFreigeben) {
          reloadFreigeben();
        }
      },
      error: (error: AjaxError) => this.errorCallback(error),
    });
  }

  private handleSave(title: string, isZeitplanungvorlage = false): void {
    this.loadingStatusController.setLoadingStatus(false);
    displayMessage(this.getSuccessMsg(title, isZeitplanungvorlage), 'success');
  }

  private errorCallback(error: AjaxError): void {
    this.loadingStatusController.setLoadingStatus(false);
    console.error(error, error);
    this.errorCtrl.displayErrorMsg(error, 'zeit.generalErrorMsg');
  }

  private getSuccessMsg(title: string, isZeitplanungvorlage = false): string {
    return i18n.t(`zeit.myZeitHome.freigabe.${isZeitplanungvorlage ? 'success' : 'successRv'}`, {
      title: title,
    });
  }
}
