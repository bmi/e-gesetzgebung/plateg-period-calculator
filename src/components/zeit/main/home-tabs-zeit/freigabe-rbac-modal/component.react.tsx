// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */

import './freigabe-modal.less';

import React, { useEffect, useState } from 'react';

import {
  BerechtigungAnBenutzerDTO,
  BerechtigungResponseDTO,
  FreigabeEntityResponseDTO,
  RolleLokalType,
  UserEntityDTO,
  UserEntityResponseDTO,
} from '@plateg/rest-api';
import { FreigabeModal } from '@plateg/theme';
import { useAppSelector } from '@plateg/theme/src/components/store';
import { GlobalDI } from '@plateg/theme/src/shares';
import { Filters } from '@plateg/theme/src/shares/filters';

import { FreigebenModalRBACController } from './controller';

interface ZeitFreigabeModalRBACProps {
  isVisible?: boolean;
  setIsVisible?: (isVisible: boolean) => void;
  objectId: string;
  objectTitle: string | undefined;
  leserechtOnly?: boolean;
  disabled?: boolean;
  isZeitplanungvorlage?: boolean;
  loadContent?: () => void;
  allowChanges?: boolean;
  revokeObserverRight?: boolean;
}

export function ZeitFreigabeModalRBAC(props: Readonly<ZeitFreigabeModalRBACProps>): React.ReactElement {
  const freigebenModalRBACController = GlobalDI.getOrRegister(
    'ezeitFreigebenModalRBACController',
    () => new FreigebenModalRBACController(),
  );
  const appStore = useAppSelector((state) => state.user);
  const [federfuehrung, setFederfuehrung] = useState<UserEntityDTO | undefined>();
  const [freigaben, setFreigaben] = useState<FreigabeEntityResponseDTO[]>([]);
  const [checkOwner, setCheckOwner] = useState(true);
  const [documentEditor, setDocumentEditor] = useState<UserEntityResponseDTO | undefined>();
  const reloadFreigaben = () => {
    if (props.objectId) {
      freigebenModalRBACController.getFreigaben(
        props.objectId,
        (value: BerechtigungResponseDTO[]) => {
          const foundEditorFreigabe = value.find(
            (item) =>
              item.berechtigter.base.id === appStore.user?.base.id && item.rolleType === RolleLokalType.Mitarbeiter,
          );

          if (foundEditorFreigabe) {
            setCheckOwner(false);
            setDocumentEditor(foundEditorFreigabe.berechtigter);
            value = value.filter((item) => item.berechtigter.base.id !== foundEditorFreigabe.berechtigter.base.id);
          } else {
            setCheckOwner(true);
            setDocumentEditor(undefined);
          }

          const federfuehrung = value.filter(
            (item) => item.rolleType === RolleLokalType.Federfuehrer || item.rolleType === RolleLokalType.ZpvBesitzer,
          );
          if (federfuehrung.length > 0) {
            setFederfuehrung(federfuehrung[0].berechtigter.dto);
          }

          setFreigaben(
            value
              .filter(
                (item) =>
                  item.rolleType !== RolleLokalType.Federfuehrer && item.rolleType !== RolleLokalType.ZpvBesitzer,
              )
              .map((item) => {
                return {
                  base: item.berechtigter.base,
                  dto: {
                    readOnlyAccess:
                      item.rolleType === RolleLokalType.Gast || item.rolleType === RolleLokalType.ZpvBeobachter,
                    user: item.berechtigter,
                  },
                };
              }),
          );
        },
        props.isZeitplanungvorlage,
      );
    }
  };
  useEffect(() => {
    if (props.isVisible !== false) {
      reloadFreigaben();
    }
  }, [props.objectId, props.isVisible]);

  const texts = {
    title: `${props.isZeitplanungvorlage ? 'Zeitplanungsvorlage' : 'Regelungsvorhaben'} „${props.objectTitle ?? ''}“`,
  };

  const freigeben = (vals: BerechtigungAnBenutzerDTO[], resetModal: (isVisible: boolean) => void) => {
    const oldUsers: BerechtigungAnBenutzerDTO[] = freigaben.map((item) => {
      return {
        email: item.dto.user.dto.email,
        rolleType: item.dto.readOnlyAccess ? RolleLokalType.ZpvBeobachter : RolleLokalType.Mitarbeiter,
      };
    });
    const resultOfComparing = Filters.prepareBerechtigungenRequestBody(vals, oldUsers);

    if (props.objectId) {
      freigebenModalRBACController.vorlageFreigeben(
        props.objectTitle,
        props.objectId,
        resultOfComparing,
        resetModal,
        props.isZeitplanungvorlage ? props.loadContent : reloadFreigaben,
        props.isZeitplanungvorlage,
      );
    }
  };

  return (
    <FreigabeModal
      {...props}
      ersteller={federfuehrung}
      disabled={props.disabled}
      checkOwner={checkOwner}
      ressortCompare={true}
      freigaben={freigaben}
      className="zeit-freigabe-modal"
      texts={texts}
      freigeben={freigeben}
      documentEditor={documentEditor}
      isRBACActivated={true}
      isChangeRightsOnlyOnRV={!props.isZeitplanungvorlage}
      allowChanges={props.allowChanges}
      revokeObserverRight={props.revokeObserverRight}
    />
  );
}
