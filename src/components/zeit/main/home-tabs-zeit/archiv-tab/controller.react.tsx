// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { ColumnsType } from 'antd/lib/table';
import Text from 'antd/lib/typography/Text';
import i18n from 'i18next';
import React, { ReactElement } from 'react';

import { ZeitplanungArchivTableDTO, ZeitplanungArchivTableDTOOriginalTabEnum } from '@plateg/rest-api';
import { compareDates, DropdownMenu, getDateTimeString, GlobalDI, TableComponentProps } from '@plateg/theme';

import { routes } from '../../../../../shares/routes';
import { ZeitplanungController } from '../controller';

export interface ExtendedArchivTableDTO extends ZeitplanungArchivTableDTO {
  zeitId: string;
}

export function getArchiveTableVals(
  initialContent: ZeitplanungArchivTableDTO[],
): TableComponentProps<ExtendedArchivTableDTO> {
  const zeitplanungController = GlobalDI.getOrRegister('zeitplanungController', () => new ZeitplanungController());
  const columns: ColumnsType<ExtendedArchivTableDTO> = [
    {
      title: i18n.t('zeit.myZeitHome.tabs.archiv.table.menuItemRegelungsvorhaben').toString(),
      key: 'menuItemRegelungsvorhaben',
      render: (record: ExtendedArchivTableDTO): ReactElement => {
        return record.regelungsvorhaben ? (
          <a
            id={`ezeit-zeitplanungsArchiveTab-${record.id}-RvLink`}
            type="link"
            href={zeitplanungController.getRvLink(record.regelungsvorhaben)}
          >
            {zeitplanungController.getRvString(record.regelungsvorhaben)}
          </a>
        ) : (
          <></>
        );
      },
    },
    {
      title: i18n.t('zeit.myZeitHome.tabs.archiv.table.menuItemName').toString(),
      key: 'menuItemName',
      render: (record: ExtendedArchivTableDTO): ReactElement => {
        const originalTabName =
          record.originalTab === ZeitplanungArchivTableDTOOriginalTabEnum.Zeitplanungsvorlagen
            ? routes.ZEITPLANUNGSVORLAGEN
            : routes.MEINE_ZEITPLANUNGEN;
        return (
          <a
            id={`theme-generic-tableComponentFirtsRow-link-${record.id}`}
            type="link"
            style={{ fontWeight: 'normal' }}
            href={`#/zeit/${routes.ARCHIV}/${originalTabName}/${record.zeitId}/${routes.TABELLENSICHT}`}
          >
            {record.titel}
          </a>
        );
      },
    },
    {
      title: i18n.t('zeit.myZeitHome.tabs.archiv.table.menuItemVorhabentyp').toString(),
      key: 'menuItemVorhabentyp',
      render: (record: ExtendedArchivTableDTO): ReactElement => {
        return (
          <Text strong>{i18n.t(`zeit.myZeitHome.tabs.archiv.table.vorhabenart.${record.vorhabenart}`).toString()}</Text>
        );
      },
    },
    {
      title: i18n.t('zeit.myZeitHome.tabs.archiv.table.menuItemUrspruenglicherTab').toString(),
      key: 'menuItemUrspruenglicherTab',
      render: (record: ExtendedArchivTableDTO): ReactElement => {
        return <Text strong>{i18n.t(`zeit.myZeitHome.tabs.archiv.table.tabs.${record.originalTab}`).toString()}</Text>;
      },
    },
    {
      title: i18n.t('zeit.myZeitHome.tabs.archiv.table.menuItemArchiviertAm').toString(),
      key: 'archiviertAm',
      sorter: (a, b) => compareDates(a.archiviertAm, b.archiviertAm),
      render: (record: ExtendedArchivTableDTO): ReactElement => {
        return <Text>{getDateTimeString(record.archiviertAm)}</Text>;
      },
    },
    {
      title: i18n.t('zeit.myZeitHome.tabs.zeitplanungsvorlagen.table.menuItemAktionen').toString(),
      key: 'menuItemAktionen',
      render: (record: ExtendedArchivTableDTO): ReactElement => {
        return (
          <DropdownMenu
            openLink={`/zeit/${routes.ARCHIV}/${
              record.originalTab === ZeitplanungArchivTableDTOOriginalTabEnum.Zeitplanungsvorlagen
                ? routes.ZEITPLANUNGSVORLAGEN
                : routes.MEINE_ZEITPLANUNGEN
            }/${record.zeitId}/${routes.TABELLENSICHT}`}
            items={[]}
            elementId={record.id}
          />
        );
      },
    },
  ];
  const content: ExtendedArchivTableDTO[] = initialContent.map((item, index) => {
    return {
      ...item,
      zeitId: item.id,
      id: `${item.id}-${index}`,
    };
  });

  return {
    id: 'period-archiv-table',
    expandable: false,
    columns,
    content,
    filteredColumns: [
      { name: 'regelungsvorhaben', columnIndex: 0 },
      { name: 'vorhabenart', columnIndex: 1 },
      { name: 'originalTab', columnIndex: 2 },
    ],
    sorterOptions: [
      {
        columnKey: 'archiviertAm',
        titleAsc: i18n.t('zeit.myZeitHome.tabs.archiv.table.sorter.archivedAsc'),
        titleDesc: i18n.t('zeit.myZeitHome.tabs.archiv.table.sorter.archivedDesc'),
      },
    ],
    customDefaultSortIndex: 2,
  };
}
