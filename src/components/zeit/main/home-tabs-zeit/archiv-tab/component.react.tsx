// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { ZeitplanungArchivTableDTO } from '@plateg/rest-api';
import { EmptyContentComponent, ImageModule, TableComponent, TableComponentProps } from '@plateg/theme';
import { useAppSelector } from '@plateg/theme/src/components/store';
import { GlobalDI } from '@plateg/theme/src/shares';

import { ZeitplanungController } from '../controller';
import { ExtendedArchivTableDTO, getArchiveTableVals } from './controller.react';

interface ArchivTabComponentInterface {
  archivData: ZeitplanungArchivTableDTO[];
  tabKey: string;
}
export function ArchivTabComponent(props: ArchivTabComponentInterface): React.ReactElement {
  const { t } = useTranslation();
  const zeitplanungController = GlobalDI.getOrRegister('zeitplanungController', () => new ZeitplanungController());
  const pagDataResult = useAppSelector((state) => state.tablePagination.tabs[props.tabKey]).result;

  const [archivData, setArchivData] = useState(props.archivData);
  const [archivTableVals, setArchivTableVals] = useState<TableComponentProps<ExtendedArchivTableDTO>>();

  useEffect(() => {
    setArchivData(props.archivData);
  }, [props.archivData]);

  useEffect(() => {
    setArchivTableVals(getArchiveTableVals(archivData));
  }, [archivData]);

  return (
    <>
      {archivTableVals && !pagDataResult.allContentEmpty && (
        <TableComponent
          bePagination
          tabKey={props.tabKey}
          id={archivTableVals.id}
          columns={archivTableVals.columns}
          content={archivTableVals.content}
          filteredColumns={archivTableVals.filteredColumns}
          filterRowsMethod={zeitplanungController.getFilteredRows}
          prepareFilterButtonMethod={zeitplanungController.getFilterButton}
          expandable={archivTableVals.expandable}
          expandedRowRender={archivTableVals.expandedRowRender}
          sorterOptions={archivTableVals.sorterOptions}
          customDefaultSortIndex={archivTableVals.customDefaultSortIndex}
          className={archivTableVals.className}
          expandableCondition={archivTableVals.expandableCondition}
        ></TableComponent>
      )}
      <EmptyContentComponent
        images={[
          {
            label: t('zeit.myZeitHome.tabs.archiv.imgText1'),
            imgSrc: require(`../../../../../media/empty-content-images/archiv-tab/img1-archiv.svg`) as ImageModule,
            height: 147,
          },
          {
            label: t('zeit.myZeitHome.tabs.archiv.imgText2'),
            imgSrc: require(`../../../../../media/empty-content-images/archiv-tab/img2-archiv.svg`) as ImageModule,
            height: 114,
          },
        ]}
      >
        <p
          className="info-text"
          dangerouslySetInnerHTML={{
            __html: t('zeit.myZeitHome.tabs.archiv.text', {
              interpolation: { escapeValue: false },
            }),
          }}
        ></p>
      </EmptyContentComponent>
    </>
  );
}
