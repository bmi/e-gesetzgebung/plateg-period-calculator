// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import i18n from 'i18next';
import { AjaxError } from 'rxjs/ajax';

import {
  ZeitplanungBilligungAntwortDTO,
  ZeitplanungBilligungTableDTO,
  ZeitplanungControllerApi,
} from '@plateg/rest-api';
import { displayMessage, ErrorController, GlobalDI, LoadingStatusController } from '@plateg/theme';

import { routes } from '../../../../../shares/routes';
export class AnfrageBeantworteModalController {
  private readonly loadingStatusController = GlobalDI.getOrRegister(
    'loadingStatusController',
    () => new LoadingStatusController(),
  );
  private readonly zeitplanungController = GlobalDI.get<ZeitplanungControllerApi>('zeitplanungRestController');
  private readonly errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  public anfrageBeantworten(
    zeichnung: ZeitplanungBilligungTableDTO,
    anfrageBeantwortenValues: ZeitplanungBilligungAntwortDTO,
    resetModal: (isVisible: boolean) => void,
    resetTab: (key: string) => void,
  ): void {
    this.loadingStatusController.setLoadingStatus(true);
    this.zeitplanungController
      .replyToBilligungsanfrage({
        billigungId: zeichnung.billigung.base.id,
        zeitplanungBilligungAntwortDTO: anfrageBeantwortenValues,
      })
      .subscribe({
        next: () => {
          this.loadingStatusController.setLoadingStatus(false);
          displayMessage(
            i18n.t(`zeit.myZeitHome.anfrageBeantwortenDialog.successMsg.${anfrageBeantwortenValues.status}`, {
              title: zeichnung.titel,
            }),
            'success',
          );
          resetTab(routes.BILLIGUNGSANFRAGEN);
          resetModal(false);
        },
        error: (error: AjaxError) => {
          this.loadingStatusController.setLoadingStatus(false);
          console.error(error, error);
          resetModal(false);
          this.errorCtrl.displayErrorMsg(error, 'zeit.generalErrorMsg');
        },
      });
  }
}
