// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, Radio } from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { BilligungStatusType, ZeitplanungBilligungAnfrageDTO, ZeitplanungBilligungAntwortDTO } from '@plateg/rest-api';
import { HiddenInfoComponent } from '@plateg/theme';

import { ZeitFormComponent } from '../../../../general-zeitelement/zeit-form-component/component.react';

export interface Page1AntwortComponentProps {
  activePageIndex: number;
  name: string;
  setActivePageIndex: (pageIndex: number) => void;
  values: ZeitplanungBilligungAntwortDTO;
  setValues: Function;
  titel: string;
  setNextPageBtnActive?: (val: boolean) => void;
}
export function Page1AntwortComponent(props: Page1AntwortComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const [form] = Form.useForm();

  return (
    <>
      <ZeitFormComponent
        onChange={() => {
          if (form.getFieldValue('status') === BilligungStatusType.NichtBearbeitet) {
            props.setNextPageBtnActive?.(false);
          } else {
            props.setNextPageBtnActive?.(true);
          }
        }}
        form={form}
        title={t(`zeit.myZeitHome.anfrageBeantwortenDialog.page1.subtitle`, {
          title: props.titel,
        })}
        name={props.name}
        values={props.values}
        activePageIndex={props.activePageIndex}
        setActivePageIndex={props.setActivePageIndex}
        setValues={(formValues: ZeitplanungBilligungAnfrageDTO) => {
          props.setValues(formValues);
        }}
      >
        <>
          <fieldset className="fieldset-form-items">
            <legend className="seo">{t(`zeit.myZeitHome.anfrageBeantwortenDialog.page1.answer.title`)}</legend>
            <Form.Item
              name="status"
              label={<span>{t('zeit.myZeitHome.anfrageBeantwortenDialog.page1.answer.title')}</span>}
              rules={[
                {
                  required: true,
                  message: t('zeit.myZeitHome.anfrageBeantwortenDialog.page1.answer.error'),
                },
              ]}
            >
              <Radio.Group name={'status'} className="horizontal-radios">
                {[
                  BilligungStatusType.Billigung,
                  BilligungStatusType.BilligungUnterVorbehalt,
                  BilligungStatusType.KeineBilligung,
                ].map((type) => {
                  return (
                    <Radio id="ezeit-anfrageAntwortBilligung-radio" value={type}>
                      {t(`zeit.myZeitHome.anfrageBeantwortenDialog.page1.answer.options.${type}.title`)}
                      <HiddenInfoComponent
                        title={t(`zeit.myZeitHome.anfrageBeantwortenDialog.page1.answer.options.${type}.title`)}
                        text={
                          <p
                            dangerouslySetInnerHTML={{
                              __html: t(`zeit.myZeitHome.anfrageBeantwortenDialog.page1.answer.options.${type}.info`, {
                                interpolation: { escapeValue: false },
                              }),
                            }}
                          />
                        }
                      />
                    </Radio>
                  );
                })}
              </Radio.Group>
            </Form.Item>
          </fieldset>
          <Form.Item
            name="anmerkungen"
            label={<span>{t('zeit.myZeitHome.billigungsanfragenDialog.page1.notes')}</span>}
          >
            <TextArea rows={4} />
          </Form.Item>
        </>
      </ZeitFormComponent>
    </>
  );
}
