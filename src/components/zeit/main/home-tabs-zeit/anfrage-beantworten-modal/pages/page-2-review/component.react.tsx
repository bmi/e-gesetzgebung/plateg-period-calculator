// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { UserEntityResponseDTO, ZeitplanungBilligungAntwortDTO } from '@plateg/rest-api';

interface Page2ReviewComponentProps {
  titel: string;
  activePageIndex: number;
  setActivePageIndex: (pageIndex: number) => void;
  values: ZeitplanungBilligungAntwortDTO;
  adressat: UserEntityResponseDTO;
}

export function Page2ReviewComponent(props: Page2ReviewComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const noAnswer = <em>{t(`zeit.newZeitplanungsvorlage.modal.page4.noAnswer`)}</em>;
  const formatUsers = (user: UserEntityResponseDTO): string => {
    const name = user.dto.name || user.dto.email;
    const ressort = user.dto.ressort?.kurzbezeichnung ?? null;
    const abteilungString = user.dto.abteilung ? `, ${user.dto.abteilung}` : '';
    const fachreferatString = user.dto.abteilung && user.dto.fachreferat ? `, ${user.dto.fachreferat}` : '';
    const ressortString = ressort ? ` (${ressort}${abteilungString}${fachreferatString})` : '';
    return `${name}${ressortString}`;
  };

  return (
    <div className="review-section">
      <Title level={2}>{t('zeit.myZeitHome.anfrageBeantwortenDialog.page2.subtitle')}</Title>
      <dl>
        <div className="review-row">
          <dt>{t(`zeit.myZeitHome.anfrageBeantwortenDialog.page2.nameOfZeitplanung`)}</dt>
          <dd>{props.titel ?? noAnswer}</dd>
        </div>
        <div className="review-row">
          <dt>{t('zeit.myZeitHome.anfrageBeantwortenDialog.page2.type')}</dt>
          <dd>
            {props.values?.status
              ? t(`zeit.myZeitHome.anfrageBeantwortenDialog.page1.answer.options.${props.values?.status}.title`)
              : noAnswer}
          </dd>
        </div>
        <div className="review-row">
          <dt>{t(`zeit.myZeitHome.anfrageBeantwortenDialog.page2.adressat`)}</dt>
          <dd>{props.adressat ? formatUsers(props.adressat) : noAnswer}</dd>
        </div>
        <div className="review-row">
          <dt>{t('zeit.myZeitHome.anfrageBeantwortenDialog.page2.notes')}</dt>
          <dd>{props.values?.anmerkungen?.length ? props.values?.anmerkungen : noAnswer}</dd>
        </div>
      </dl>
    </div>
  );
}
