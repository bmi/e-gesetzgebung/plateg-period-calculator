// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { of, throwError } from 'rxjs';
import sinon from 'sinon';

import { BilligungStatusType, ZeitplanungBilligungTableDTO, ZeitplanungControllerApi } from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme';

import { displayErrorMsgStub, setLoadingStatusStub } from '../../../../../general.test';
import { AnfrageBeantworteModalController } from './controller';
describe('AnfrageBeantworteModalController testen', () => {
  let ctrl: AnfrageBeantworteModalController;
  let zeitCtrl: ZeitplanungControllerApi;
  let httpStub: sinon.SinonStub;
  let resetModal: sinon.SinonStub;
  let resetTab: sinon.SinonStub;
  const billigungen = { titel: 'Test', billigung: { base: { id: '1' } } } as ZeitplanungBilligungTableDTO;
  const antwortValues = { anmerkungen: 'Hello World', status: BilligungStatusType.Billigung };
  before(() => {
    ctrl = new AnfrageBeantworteModalController();

    zeitCtrl = GlobalDI.getOrRegister('zeitplanungRestController', () => new ZeitplanungControllerApi());
    resetModal = sinon.stub();
    resetTab = sinon.stub();
    httpStub = sinon.stub(zeitCtrl, 'replyToBilligungsanfrage');
  });
  after(() => {
    httpStub.restore();
  });
  describe('TEST: anfragenBeantworten - Success', () => {
    before(() => {
      httpStub.returns(of(void 0));
    });
    afterEach(() => {
      setLoadingStatusStub.resetHistory();
      resetTab.resetHistory();
      displayErrorMsgStub.resetHistory();
    });
    after(() => {
      httpStub.resetBehavior();
    });
    it('check if REST API is called', (done) => {
      ctrl.anfrageBeantworten(billigungen, antwortValues, resetModal, resetTab);
      setTimeout(() => {
        sinon.assert.calledTwice(setLoadingStatusStub);
        sinon.assert.notCalled(displayErrorMsgStub);
        sinon.assert.calledOnce(resetTab);
        done();
      }, 20);
    });
  });

  describe('TEST: anfragenBeantworten - Failure', () => {
    before(() => {
      httpStub.returns(throwError({ status: 404 }));
    });
    afterEach(() => {
      setLoadingStatusStub.resetHistory();
      displayErrorMsgStub.resetHistory();
    });
    after(() => {
      httpStub.resetBehavior();
    });
    it('check if REST API is called - single adressat', (done) => {
      ctrl.anfrageBeantworten(billigungen, antwortValues, resetModal, resetTab);
      setTimeout(() => {
        sinon.assert.calledTwice(setLoadingStatusStub);
        sinon.assert.calledOnce(displayErrorMsgStub);
        sinon.assert.notCalled(resetTab);
        done();
      }, 20);
    });
  });
});
