// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { BilligungStatusType, ZeitplanungBilligungAntwortDTO, ZeitplanungBilligungTableDTO } from '@plateg/rest-api';
import { MultiPageModalComponent } from '@plateg/theme';
import { GlobalDI } from '@plateg/theme/src/shares';

import { AnfrageBeantworteModalController } from './controller';
import { Page1AntwortComponent } from './pages/page-1-antwort/component.react';
import { Page2ReviewComponent } from './pages/page-2-review/component.react';

export interface AnfrageBeantwortenProps {
  isVisible: boolean;
  setIsVisible: (isVisible: boolean) => void;
  zeichnung: ZeitplanungBilligungTableDTO;
  resetTab: (key: string) => void;
}

export function AnfrageBeantwortenModal(props: AnfrageBeantwortenProps): React.ReactElement {
  const { t } = useTranslation();
  const anfrageBeantworteModalController = GlobalDI.getOrRegister(
    'anfrageBeantworteModalController',
    () => new AnfrageBeantworteModalController(),
  );
  const [activePageIndex, setActivePageIndex] = useState<number>(0);
  const defaultObject: ZeitplanungBilligungAntwortDTO = {
    anmerkungen: '',
    status: BilligungStatusType.NichtBearbeitet,
  };
  const [page1NextBtnActive, setPage1NextBtnActive] = useState<boolean>(false);
  const [anfrageBeantwortenValues, setAnfrageBeantwortenValues] =
    useState<ZeitplanungBilligungAntwortDTO>(defaultObject);

  const resetModal = (isVisible: boolean) => {
    if (!isVisible) {
      setAnfrageBeantwortenValues(defaultObject);
    }
    setActivePageIndex(0);
    props.setIsVisible(isVisible);
  };
  useEffect(() => {
    setPage1NextBtnActive(false);
  }, [props.isVisible]);

  return (
    <>
      <MultiPageModalComponent
        key={'anfrage-beantworten-dialog' + props.isVisible.toString()}
        isVisible={props.isVisible}
        setIsVisible={resetModal}
        activePageIndex={activePageIndex}
        setActivePageIndex={setActivePageIndex}
        title={t(`zeit.myZeitHome.anfrageBeantwortenDialog.title`)}
        cancelBtnText={t('zeit.myZeitHome.anfrageBeantwortenDialog.cancelBtnText')}
        nextBtnText={t('zeit.myZeitHome.anfrageBeantwortenDialog.nextBtnText')}
        prevBtnText={t('zeit.myZeitHome.anfrageBeantwortenDialog.prevBtnText')}
        componentReferenceContext="anfrageBeantwortenDialog"
        pages={[
          {
            formName: 'page1',
            content: (
              <Page1AntwortComponent
                setNextPageBtnActive={setPage1NextBtnActive}
                name="page1"
                titel={props.zeichnung.titel}
                activePageIndex={activePageIndex}
                setActivePageIndex={setActivePageIndex}
                values={anfrageBeantwortenValues}
                setValues={setAnfrageBeantwortenValues}
              />
            ),
            primaryInsteadNextBtn: {
              buttonText: t('zeit.myZeitHome.anfrageBeantwortenDialog.page1.nextBtnText'),
              shouldNavToNextPage: true,
              disabled: !page1NextBtnActive,
            },
          },
          {
            content: (
              <Page2ReviewComponent
                titel={props.zeichnung.titel}
                adressat={props.zeichnung.billigung.dto.erstelltVon}
                activePageIndex={activePageIndex}
                setActivePageIndex={setActivePageIndex}
                values={anfrageBeantwortenValues}
              />
            ),
            primaryInsteadNextBtn: {
              buttonText: t(`zeit.myZeitHome.anfrageBeantwortenDialog.page2.submit`),
            },
            nextOnClick: () => {
              anfrageBeantworteModalController.anfrageBeantworten(
                props.zeichnung,
                anfrageBeantwortenValues,
                resetModal,
                props.resetTab,
              );
            },
          },
        ]}
      />
    </>
  );
}
