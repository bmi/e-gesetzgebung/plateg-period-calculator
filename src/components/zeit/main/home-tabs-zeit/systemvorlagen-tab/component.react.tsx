// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { UserEntityResponseDTO, ZeitplanungsvorlageTableDTO } from '@plateg/rest-api';
import { EmptyContentComponent, ImageModule, TableComponent, TableComponentProps } from '@plateg/theme';
import { useAppSelector } from '@plateg/theme/src/components/store';
import { GlobalDI } from '@plateg/theme/src/shares';

import { CopyZeitplanungsvorlageComponent } from '../../new-zeitplanungsvorlage-modal/copy-zeitplanungsvorlage/component.react';
import { ZeitplanungController } from '../controller';
import { getSystemZeitplanungsvorlagenTableVals } from './controller.react';

interface ZeitplanungsvorlagenTabComponentInterface {
  systemvorlagenData: ZeitplanungsvorlageTableDTO[];
  resetTab: (key: string) => void;
  currentUser: UserEntityResponseDTO;
  tabKey: string;
  loadContent: () => void;
}

export function SystemvorlagenTabComponent(props: ZeitplanungsvorlagenTabComponentInterface): React.ReactElement {
  const { t } = useTranslation();
  const zeitplanungController = GlobalDI.getOrRegister('zeitplanungController', () => new ZeitplanungController());
  const [systemvorlagenData, setSystemvorlagenData] = useState(props.systemvorlagenData);
  const [copyZeitplanungsvorlageModalIsVisible, setCopyZeitplanungsvorlageModalIsVisible] = useState(false);
  const [selectedZeitplanungsvorlage, setSelectedZeitplanungsvorlage] = useState<ZeitplanungsvorlageTableDTO>(
    {} as ZeitplanungsvorlageTableDTO,
  );
  const [zeitplanungsvorlagenTableVals, setZeitplanungsvorlagenTableVals] =
    useState<TableComponentProps<ZeitplanungsvorlageTableDTO>>();

  const pagDataResult = useAppSelector((state) => state.tablePagination.tabs[props.tabKey]).result;

  useEffect(() => {
    setSystemvorlagenData(props.systemvorlagenData);
  }, [props.systemvorlagenData]);

  useEffect(() => {
    setZeitplanungsvorlagenTableVals(
      getSystemZeitplanungsvorlagenTableVals(
        systemvorlagenData,
        setSelectedZeitplanungsvorlage,
        setCopyZeitplanungsvorlageModalIsVisible,
      ),
    );
  }, [systemvorlagenData]);

  return (
    <>
      {zeitplanungsvorlagenTableVals && !pagDataResult.allContentEmpty && (
        <>
          <CopyZeitplanungsvorlageComponent
            isVisible={copyZeitplanungsvorlageModalIsVisible}
            setIsVisible={setCopyZeitplanungsvorlageModalIsVisible}
            resetTab={props.resetTab}
            selectedZeitplanungsvorlage={selectedZeitplanungsvorlage}
            selectedId={selectedZeitplanungsvorlage?.id}
          />
          <TableComponent
            bePagination
            tabKey={props.tabKey}
            id={zeitplanungsvorlagenTableVals.id}
            columns={zeitplanungsvorlagenTableVals.columns}
            content={zeitplanungsvorlagenTableVals.content}
            filteredColumns={zeitplanungsvorlagenTableVals.filteredColumns}
            filterRowsMethod={zeitplanungController.getFilteredRows}
            prepareFilterButtonMethod={zeitplanungController.getFilterButton}
            expandable={zeitplanungsvorlagenTableVals.expandable}
            sorterOptions={zeitplanungsvorlagenTableVals.sorterOptions}
            customDefaultSortIndex={zeitplanungsvorlagenTableVals.customDefaultSortIndex}
            className={zeitplanungsvorlagenTableVals.className}
            expandableCondition={zeitplanungsvorlagenTableVals.expandableCondition}
          />
        </>
      )}
      <EmptyContentComponent
        noIndex={true}
        images={[
          {
            label: t('zeit.myZeitHome.tabs.systemvorlagen.imgText1'),
            imgSrc: require(
              `../../../../../media/empty-content-images/zeitplanungsvorlagen-tab/img1-zeitplanungsvorlagen.svg`,
            ) as ImageModule,
            height: 110,
          },
        ]}
      >
        <p className="info-text">
          {t('zeit.myZeitHome.tabs.systemvorlagen.text', {
            interpolation: { escapeValue: false },
          })}
        </p>
      </EmptyContentComponent>
    </>
  );
}
