// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';

import { UserEntityResponseDTO, ZeitplanungBilligungAnfrageDTO } from '@plateg/rest-api';
import { MultiPageModalComponent } from '@plateg/theme';
import { GlobalDI } from '@plateg/theme/src/shares';

import { BilligungsanfragenModalController } from './controller';
import { Page1AdressatenComponent } from './pages/page-1-adressaten/component.react';
import { Page2ReviewComponent } from './pages/page-2-review/component.react';
export interface BilligungsanfragenDialogProps {
  isVisible: boolean;
  setIsVisible: (isVisible: boolean) => void;
  objectId: string | undefined;
  objectTitle: string | undefined;
  resetTab: (key: string) => void;
  currentUser: UserEntityResponseDTO;
}

export function BilligungsanfragenDialog(props: BilligungsanfragenDialogProps): React.ReactElement {
  const { t } = useTranslation();
  const billigungsanfragenModalController = GlobalDI.getOrRegister(
    'billigungsanfragenModalController',
    () => new BilligungsanfragenModalController(),
  );
  const [activePageIndex, setActivePageIndex] = useState<number>(0);
  const defaultObject: ZeitplanungBilligungAnfrageDTO = {
    anmerkungen: '',
    teilnehmer: [],
  };
  const [anfragenValues, setAnfragenValues] = useState<ZeitplanungBilligungAnfrageDTO>(defaultObject);

  const resetModal = (isVisible: boolean) => {
    if (!isVisible) {
      setAnfragenValues(defaultObject);
    }
    setActivePageIndex(0);
    props.setIsVisible(isVisible);
  };

  return (
    <>
      <MultiPageModalComponent
        key={'billigungsanfragen-dialog' + props.isVisible.toString()}
        isVisible={props.isVisible}
        setIsVisible={resetModal}
        activePageIndex={activePageIndex}
        setActivePageIndex={setActivePageIndex}
        title={t(`zeit.myZeitHome.billigungsanfragenDialog.title`)}
        cancelBtnText={t('zeit.myZeitHome.billigungsanfragenDialog.cancelBtnText')}
        nextBtnText={t('zeit.myZeitHome.billigungsanfragenDialog.nextBtnText')}
        prevBtnText={t('zeit.myZeitHome.billigungsanfragenDialog.prevBtnText')}
        componentReferenceContext="billigungsanfragenDialog"
        pages={[
          {
            formName: 'page1',
            content: (
              <Page1AdressatenComponent
                name="page1"
                titel={props.objectTitle || ''}
                activePageIndex={activePageIndex}
                setActivePageIndex={setActivePageIndex}
                values={anfragenValues}
                setValues={setAnfragenValues}
                currentUser={props.currentUser}
              />
            ),
            primaryInsteadNextBtn: {
              buttonText: t('zeit.myZeitHome.billigungsanfragenDialog.page1.nextBtnText'),
              shouldNavToNextPage: true,
            },
          },
          {
            content: (
              <Page2ReviewComponent
                titel={props.objectTitle || ''}
                activePageIndex={activePageIndex}
                setActivePageIndex={setActivePageIndex}
                values={anfragenValues}
              />
            ),
            primaryInsteadNextBtn: {
              buttonText: t(`zeit.myZeitHome.billigungsanfragenDialog.page2.submit`),
            },
            nextOnClick: () => {
              billigungsanfragenModalController.anfragenSenden(
                props.objectTitle,
                props.objectId || '',
                anfragenValues,
                resetModal,
                props.resetTab,
              );
            },
          },
        ]}
      />
    </>
  );
}
