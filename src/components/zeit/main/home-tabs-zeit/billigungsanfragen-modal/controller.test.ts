// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { of, throwError } from 'rxjs';
import sinon from 'sinon';

import { Configuration, ZeitplanungControllerApi } from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme/src/shares';

import { displayErrorMsgStub, setLoadingStatusStub } from '../../../../../general.test';
import { BilligungsanfragenModalController } from './controller';

describe('BilligungsanfragenModalController testen', () => {
  let ctrl: BilligungsanfragenModalController;
  let httpStub: sinon.SinonStub;
  let setIsVisibleStub: sinon.SinonStub;
  let resetTab: sinon.SinonStub;
  before(() => {
    ctrl = new BilligungsanfragenModalController();
    const zeitplanungController = GlobalDI.getOrRegister(
      'zeitplanungRestController',
      () => new ZeitplanungControllerApi(new Configuration()),
    );
    setIsVisibleStub = sinon.stub();
    resetTab = sinon.stub();
    httpStub = sinon.stub(zeitplanungController, 'requestBilligung');
  });
  after(() => {
    httpStub.restore();
  });
  describe('TEST: anfragenSenden - Success', () => {
    before(() => {
      httpStub.returns(of(void 0));
    });
    afterEach(() => {
      setLoadingStatusStub.resetHistory();
      resetTab.resetHistory();
      displayErrorMsgStub.resetHistory();
    });
    after(() => {
      httpStub.resetBehavior();
    });
    it('check if REST API is called - single adressat', (done) => {
      const vorlageTitle = 'abc';
      const vorlageId = 'xyz';
      const anfragenValues = { anmerkungen: 'Hello World', teilnehmer: ['a b'] };
      ctrl.anfragenSenden(vorlageTitle, vorlageId, anfragenValues, setIsVisibleStub, resetTab);
      setTimeout(() => {
        sinon.assert.calledTwice(setLoadingStatusStub);
        sinon.assert.notCalled(displayErrorMsgStub);
        sinon.assert.calledTwice(resetTab);
        done();
      }, 20);
    });
  });
  describe('TEST: anfragenSenden - Failure', () => {
    before(() => {
      httpStub.returns(throwError({ status: 404 }));
    });
    afterEach(() => {
      setLoadingStatusStub.resetHistory();
      displayErrorMsgStub.resetHistory();
    });
    after(() => {
      httpStub.resetBehavior();
    });
    it('check if REST API is called - single adressat', (done) => {
      const vorlageTitle = 'abc';
      const vorlageId = 'xyz';
      const anfragenValues = { anmerkungen: 'Hello World', teilnehmer: ['a b'] };
      ctrl.anfragenSenden(vorlageTitle, vorlageId, anfragenValues, setIsVisibleStub, resetTab);
      setTimeout(() => {
        sinon.assert.calledTwice(setLoadingStatusStub);
        sinon.assert.calledOnce(displayErrorMsgStub);
        sinon.assert.notCalled(resetTab);
        done();
      }, 20);
    });
  });
});
