// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import i18n from 'i18next';
import { AjaxError } from 'rxjs/ajax';

import { ZeitplanungBilligungAnfrageDTO, ZeitplanungControllerApi } from '@plateg/rest-api';
import { displayMessage, ErrorController, GlobalDI, LoadingStatusController } from '@plateg/theme';

import { routes } from '../../../../../shares/routes';

export class BilligungsanfragenModalController {
  private readonly loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  private readonly zeitplanungController = GlobalDI.get<ZeitplanungControllerApi>('zeitplanungRestController');
  private readonly errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  public anfragenSenden(
    zeitplanungTitle: string | undefined,
    zeitplanungId: string,
    anfragenValues: ZeitplanungBilligungAnfrageDTO,
    setIsVisible: (visible: boolean) => void,
    resetTab: (key: string) => void,
  ): void {
    setIsVisible(false);
    this.loadingStatusController.setLoadingStatus(true);
    this.zeitplanungController
      .requestBilligung({ zeitplanungId, zeitplanungBilligungAnfrageDTO: anfragenValues })
      .subscribe({
        next: () => {
          this.loadingStatusController.setLoadingStatus(false);
          displayMessage(
            i18n.t(`zeit.myZeitHome.billigungsanfragenDialog.successMsg`, {
              title: zeitplanungTitle,
            }),
            'success',
          );
          resetTab(routes.BILLIGUNGSANFRAGEN);
          resetTab(routes.MEINE_ZEITPLANUNGEN);
        },
        error: (error: AjaxError) => {
          this.loadingStatusController.setLoadingStatus(false);
          console.error(error, error);
          this.errorCtrl.displayErrorMsg(error, 'zeit.generalErrorMsg');
        },
      });
  }
}
