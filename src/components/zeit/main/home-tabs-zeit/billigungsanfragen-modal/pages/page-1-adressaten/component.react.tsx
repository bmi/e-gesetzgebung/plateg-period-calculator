// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form } from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { UserEntityResponseDTO, ZeitplanungBilligungAnfrageDTO } from '@plateg/rest-api';
import { Constants, EmailsSearchComponent, HiddenInfoComponent } from '@plateg/theme';

import { ZeitFormComponent } from '../../../../general-zeitelement/zeit-form-component/component.react';

export interface Page1AdressatenComponentProps {
  activePageIndex: number;
  name: string;
  setActivePageIndex: (pageIndex: number) => void;
  values: ZeitplanungBilligungAnfrageDTO;
  setValues: Function;
  titel: string;
  currentUser: UserEntityResponseDTO;
}
export function Page1AdressatenComponent(props: Page1AdressatenComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const [form] = Form.useForm();

  const mailValidator = (values: any, msg: string) => {
    return props.currentUser.dto.email && (values as string[]).includes(props.currentUser.dto.email)
      ? Promise.reject(msg)
      : Promise.resolve();
  };

  const updateFormField = (fieldName: string, value: string[]) => {
    form.setFieldsValue({
      [fieldName]: value,
    });
    void form.validateFields(['teilnehmer']);
  };

  return (
    <>
      <ZeitFormComponent
        form={form}
        title={t(`zeit.myZeitHome.billigungsanfragenDialog.page1.subtitle`, {
          title: props.titel,
        })}
        name={props.name}
        values={props.values}
        activePageIndex={props.activePageIndex}
        setActivePageIndex={props.setActivePageIndex}
        setValues={(formValues: ZeitplanungBilligungAnfrageDTO) => {
          props.setValues(formValues);
        }}
      >
        <>
          <p>{t(`zeit.myZeitHome.billigungsanfragenDialog.page1.roleNote`)}</p>
          <EmailsSearchComponent
            name="teilnehmer"
            form={form}
            currentUserEmail={props.currentUser.dto.email || ''}
            texts={{
              searchLabel: t('zeit.myZeitHome.billigungsanfragenDialog.page1.searchLabel'),
              searchDrawer: (
                <HiddenInfoComponent
                  title={t('zeit.myZeitHome.billigungsanfragenDialog.page1.searchDrawerTitle')}
                  text={
                    <p
                      dangerouslySetInnerHTML={{
                        __html: t('zeit.myZeitHome.billigungsanfragenDialog.page1.searchDrawerText', {
                          interpolation: { escapeValue: false },
                        }),
                      }}
                    />
                  }
                />
              ),
              searchHint: t('zeit.myZeitHome.billigungsanfragenDialog.page1.searchHint'),
              addressLabel: t('zeit.myZeitHome.billigungsanfragenDialog.page1.addressLabel'),
              addressDrawer: (
                <HiddenInfoComponent
                  title={t('zeit.myZeitHome.billigungsanfragenDialog.page1.addressDrawerTitle')}
                  text={
                    <p
                      dangerouslySetInnerHTML={{
                        __html: t('zeit.myZeitHome.billigungsanfragenDialog.page1.addressDrawerText', {
                          interpolation: { escapeValue: false },
                        }),
                      }}
                    />
                  }
                />
              ),
            }}
            rules={[
              { required: true, message: t('zeit.myZeitHome.billigungsanfragenDialog.page1.errorAdressaten') },
              {
                pattern: Constants.EMAIL_REGEXP_PATTERN,
                message: t('zeit.myZeitHome.billigungsanfragenDialog.page1.errorMailadress'),
              },
              {
                validator: (_rule, values) => {
                  return mailValidator(values, t('zeit.myZeitHome.billigungsanfragenDialog.page1.errorParticipant'));
                },
              },
            ]}
            updateFormField={updateFormField}
            isOnlyOneUser={true}
          />
          <Form.Item
            name="anmerkungen"
            label={<span>{t('zeit.myZeitHome.billigungsanfragenDialog.page1.notes')}</span>}
          >
            <TextArea rows={4} />
          </Form.Item>
        </>
      </ZeitFormComponent>
    </>
  );
}
