// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import Title from 'antd/lib/typography/Title';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { ZeitplanungBilligungAnfrageDTO } from '@plateg/rest-api';
import { UserFormattingController } from '@plateg/theme';
import { GlobalDI } from '@plateg/theme/src/shares';

interface Page2ReviewComponentProps {
  titel: string;
  activePageIndex: number;
  setActivePageIndex: (pageIndex: number) => void;
  values: ZeitplanungBilligungAnfrageDTO;
}

export function Page2ReviewComponent(props: Page2ReviewComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const noAnswer = <em>{t(`zeit.newZeitplanungsvorlage.modal.page4.noAnswer`)}</em>;
  const [teilnehmerOutput, setTeilnehmerOutput] = useState<string>('');
  const userFormattingCtrl = GlobalDI.getOrRegister('userFormattingController', () => new UserFormattingController());

  useEffect(() => {
    userFormattingCtrl.getUsersByEmailCall(props.values?.teilnehmer || []).subscribe({
      next: (value) => {
        setTeilnehmerOutput(value);
      },
    });
  }, [props.values?.teilnehmer]);

  return (
    <div className="review-section">
      <Title level={2}>{t('zeit.myZeitHome.billigungsanfragenDialog.page2.subtitle')}</Title>
      <dl>
        <div className="review-row">
          <dt>{t(`zeit.myZeitHome.billigungsanfragenDialog.page2.nameOfZeitplanung`)}</dt>
          <dd>{props.titel ?? noAnswer}</dd>
        </div>
        <div className="review-row">
          <dt>{t('zeit.myZeitHome.billigungsanfragenDialog.page2.adressaten')}</dt>
          <dd>{props.values?.teilnehmer.length !== 0 ? teilnehmerOutput : noAnswer}</dd>
        </div>
        <div className="review-row">
          <dt>{t('zeit.myZeitHome.billigungsanfragenDialog.page2.notes')}</dt>
          <dd>{props.values?.anmerkungen?.length ? props.values?.anmerkungen : noAnswer}</dd>
        </div>
      </dl>
    </div>
  );
}
