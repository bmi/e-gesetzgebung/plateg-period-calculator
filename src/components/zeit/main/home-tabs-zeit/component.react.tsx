// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './my-zeit.less';

import { Button, Col, Row, TabsProps, Typography } from 'antd';
import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory, useRouteMatch } from 'react-router';
import { Observable } from 'rxjs';
import { AjaxError } from 'rxjs/ajax';

import {
  PaginierungDTO,
  PaginierungDTOSortByEnum,
  UserEntityResponseDTO,
  ZeitplanungArchivTableDTO,
  ZeitplanungArchivTableDTOs,
  ZeitplanungBilligungTableDTO,
  ZeitplanungBilligungTableDTOs,
  ZeitplanungsvorlageTableDTO,
  ZeitplanungsvorlageTableDTOs,
  ZeitplanungTableDTO,
  ZeitplanungTableDTOs,
} from '@plateg/rest-api';
import {
  BreadcrumbComponent,
  ErrorController,
  HeaderController,
  LoadingStatusController,
  OpenEntriesIndicator,
  TabsWrapper,
  TitleWrapperComponent,
} from '@plateg/theme';
import { useAppDispatch, useAppSelector } from '@plateg/theme/src/components/store';
import {
  setPaginationFilterInfo,
  setPaginationInitState,
  setPaginationResult,
} from '@plateg/theme/src/components/store/slices/tablePaginationSlice';
import { GlobalDI } from '@plateg/theme/src/shares';

import { routes } from '../../../../shares/routes';
import { ZeitHelpLink } from '../../component.react';
import { NewZeitplanungModal } from '../new-zeitplanung-modal/component.react';
import { NewZeitplanungsvorlageModal } from '../new-zeitplanungsvorlage-modal/component.react';
import { ArchivTabComponent } from './archiv-tab/component.react';
import { BilligungsanfragenTabComponent } from './billigungsanfragen-tab/component.react';
import { ZeitplanungController } from './controller';
import { MeineZeitplanungenTabComponent } from './meine-zeitplanungen-tab/component.react';
import { SystemvorlagenTabComponent } from './systemvorlagen-tab/component.react';
import { ZeitplanungsvorlagenTabComponent } from './zeitplanungsvorlagen-tab/component.react';

type ObservablesTypeUnion =
  | ZeitplanungTableDTOs
  | ZeitplanungsvorlageTableDTOs
  | ZeitplanungArchivTableDTOs
  | ZeitplanungBilligungTableDTOs;
interface MapTabsContentItemInterface {
  loadMethod: (isFilterNeeded: boolean, paginierungDTO: PaginierungDTO) => Observable<ObservablesTypeUnion>;
  setMethod: Function;
  getTableMethod?: Function;
}
interface MapTabsContentInterface {
  [key: string]: MapTabsContentItemInterface;
}

export function HomeTabsZeit(): React.ReactElement {
  const { t } = useTranslation();
  const history = useHistory();
  const { Title } = Typography;
  const headerController = GlobalDI.getOrRegister('ezeitHeaderController', () => new HeaderController());
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  const routeMachterNeueZeitplanung = useRouteMatch<{ neueZeitplanung?: string; rvId: string; rv: string }>(
    '/zeit/:neueZeitplanung/:rv/:rvId',
  );
  const neueZeitplanung = routeMachterNeueZeitplanung?.params.neueZeitplanung;
  const rvId = routeMachterNeueZeitplanung?.params.rvId;
  const rv = routeMachterNeueZeitplanung?.params.rv;
  const routeMachter = useRouteMatch<{ tabKey?: string }>('/zeit/:tabKey?');
  const initialRoute =
    routeMachter?.params.tabKey != null && neueZeitplanung !== routes.NEUEZEITPLANUNG
      ? routeMachter?.params.tabKey
      : routes.MEINE_ZEITPLANUNGEN;
  const [activeTab, setActiveTab] = useState<string>(initialRoute);
  const [loadedTabs, setLoadedTabs] = useState<string[]>([]);
  const [currentUser, setCurrentUser] = useState<UserEntityResponseDTO>();
  const zeitplanungController = GlobalDI.getOrRegister('zeitplanungController', () => new ZeitplanungController());

  const [zeitplanungenData, setZeitplanungenData] = useState<ZeitplanungTableDTO[]>([]);
  const [zeitplanungsvorlagenData, setZeitplanungsvorlagenData] = useState<ZeitplanungsvorlageTableDTO[]>([]);
  const [systemvorlagenData, setSystemvorlagenData] = useState<ZeitplanungsvorlageTableDTO[]>([]);
  const [archivData, setArchivData] = useState<ZeitplanungArchivTableDTO[]>([]);
  const [badgeNumberBilligung, setBadgeNumberBilligung] = useState<number>();
  const [billigungsanfragenData, setBilligungsanfragenData] = useState<ZeitplanungBilligungTableDTO[]>([]);

  const [isNewZeitplanungsvorlageModalVisible, setIsNewZeitplanungsvorlageModalVisible] = useState(false);

  const [isNewZeitplanungModalVisible, setIsNewZeitplanungModalVisible] = useState<boolean>(
    neueZeitplanung === routes.NEUEZEITPLANUNG && rv === routes.REGELUNGSVORHABEN && !!rvId,
  );
  const newZeitplanungModalButtonRef = useRef<HTMLButtonElement>(null);
  const newZeitplanungsvorlageModalButtonRef = useRef<HTMLButtonElement>(null);
  const appStoreUser = useAppSelector((state) => state.user);
  const pagDataRequest = useAppSelector((state) => state.tablePagination.tabs[initialRoute]).request;
  const dispatch = useAppDispatch();

  const mapTabsContent: MapTabsContentInterface = {
    meineZeitplanungen: {
      loadMethod: zeitplanungController.getMeineZeitplanungenCall,
      setMethod: setZeitplanungenData,
    },
    zeitplanungsvorlagen: {
      loadMethod: zeitplanungController.getZeitplanungsvorlagenCall,
      setMethod: setZeitplanungsvorlagenData,
    },
    systemvorlagen: {
      loadMethod: zeitplanungController.getSystemZeitplanungsvorlagenCall,
      setMethod: setSystemvorlagenData,
    },
    archiv: {
      loadMethod: zeitplanungController.getArchiveCall,
      setMethod: setArchivData,
    },
    billigungsanfragen: {
      loadMethod: zeitplanungController.getBilligungsanfragenCall,
      setMethod: setBilligungsanfragenData,
    },
  };

  const isFirstRender = useRef(true);

  useEffect(() => {
    if (appStoreUser.user) {
      setCurrentUser(appStoreUser.user);
    }
  }, [appStoreUser.user]);

  useEffect(() => {
    setBreadcrumb(activeTab);
  }, [activeTab]);

  useEffect(() => {
    if (initialRoute !== activeTab) {
      setActiveTab(initialRoute);
      if (!loadedTabs.includes(initialRoute)) {
        loadContent(initialRoute, false, true);
      }
    } else if (!isFirstRender.current) {
      loadContent(activeTab);
    } else {
      if (initialRoute !== routes.BILLIGUNGSANFRAGEN) {
        loadContent(routes.BILLIGUNGSANFRAGEN, true, true);
      }
      loadContent(initialRoute, false, true);
      isFirstRender.current = false;
    }
  }, [
    pagDataRequest.currentPage,
    pagDataRequest.sortOrder,
    pagDataRequest.columnKey,
    pagDataRequest.filters,
    initialRoute,
  ]);

  useEffect(() => {
    newZeitplanungModalButtonRef.current?.focus();
  }, [isNewZeitplanungModalVisible]);

  useEffect(() => {
    newZeitplanungsvorlageModalButtonRef.current?.focus();
  }, [isNewZeitplanungsvorlageModalVisible]);

  const loadContent = (key: string, inBearbeitung = false, isFilterNeeded = false) => {
    if (!inBearbeitung && isFilterNeeded) {
      loadingStatusController.setLoadingStatus(true);
    }

    if (isFirstRender.current) {
      dispatch(setPaginationInitState({ tabKey: key }));
    }
    const pagReqObj: PaginierungDTO = {
      pageNumber: pagDataRequest.currentPage || 0,
      pageSize: 20,
      filters: pagDataRequest.filters,
      ...(pagDataRequest.columnKey && { sortBy: pagDataRequest.columnKey }),
      ...(pagDataRequest.sortOrder && { sortDirection: pagDataRequest.sortOrder }),
    };

    if (mapTabsContent[key]) {
      if (isFirstRender.current && routes.BILLIGUNGSANFRAGEN) {
        pagReqObj.sortBy = PaginierungDTOSortByEnum.ZuletztBearbeitet;
      }

      const subLoadMethod = mapTabsContent[key].loadMethod(isFilterNeeded, pagReqObj).subscribe({
        next: (data) => {
          const { allContentEmpty, dtos, filterNames } = data;
          const { totalElements, number, content } = dtos;
          mapTabsContent[key].setMethod(content);

          setLoadedTabs((loadT) => [...loadT, key]);

          dispatch(
            setPaginationResult({ tabKey: key, allContentEmpty, currentPage: number, totalItems: totalElements }),
          );

          if (isFilterNeeded) {
            if (key === routes.MEINE_ZEITPLANUNGEN) {
              dispatch(
                setPaginationFilterInfo({
                  tabKey: key,
                  filterNames,
                  erstellerFilter: (data as ZeitplanungTableDTOs).erstellerFilter,
                  regelungsvorhabenFilter: (data as ZeitplanungTableDTOs).regelungsvorhabenFilter,
                }),
              );
            } else if (key === routes.BILLIGUNGSANFRAGEN) {
              setBadgeNumberBilligung((data as ZeitplanungBilligungTableDTOs).badgeNumber);
              dispatch(
                setPaginationFilterInfo({
                  tabKey: key,
                  filterNames,
                  billigungstatusFilter: (data as ZeitplanungBilligungTableDTOs).billigungstatusFilter,
                  billigungregelungsvorhabenFilter: (data as ZeitplanungBilligungTableDTOs)
                    .billigungregelungsvorhabenFilter,
                }),
              );
            } else if (key === routes.ZEITPLANUNGSVORLAGEN) {
              dispatch(
                setPaginationFilterInfo({
                  tabKey: key,
                  filterNames,
                  erstellerFilter: (data as ZeitplanungsvorlageTableDTOs).erstellerFilter,
                  zeitplanungvorhabenartFilter: (data as ZeitplanungsvorlageTableDTOs).zeitplanungvorhabenartFilter,
                }),
              );
            } else if (key === routes.SYSTEMVORLAGEN) {
              dispatch(
                setPaginationFilterInfo({
                  tabKey: key,
                  filterNames,
                  erstellerFilter: (data as ZeitplanungsvorlageTableDTOs).erstellerFilter,
                  zeitplanungvorhabenartFilter: (data as ZeitplanungsvorlageTableDTOs).zeitplanungvorhabenartFilter,
                }),
              );
            } else if (key === routes.ARCHIV) {
              dispatch(
                setPaginationFilterInfo({
                  tabKey: key,
                  filterNames,
                  regelungsvorhabenFilter: (data as ZeitplanungArchivTableDTOs).regelungsvorhabenFilter,
                  zeitplanungvorhabenartFilter: (data as ZeitplanungArchivTableDTOs).zeitplanungvorhabenartFilter,
                  ursprungFilter: (data as ZeitplanungArchivTableDTOs).ursprungFilter,
                }),
              );
            }
          }

          if (!inBearbeitung) {
            loadingStatusController.setLoadingStatus(false);
          }
          subLoadMethod.unsubscribe();
        },
        error: (error: AjaxError) => {
          subLoadMethod.unsubscribe();

          loadingStatusController.setLoadingStatus(false);
          errorCtrl.displayErrorMsg(error, 'zeit.generalErrorMsg');
        },
      });
    }
  };

  const resetTab = (key: string) => {
    setLoadedTabs(
      loadedTabs.filter((tab: string) => {
        return tab !== key;
      }),
    );
    if (key === routes.BILLIGUNGSANFRAGEN) {
      loadContent(key, false, true);
    } else {
      loadContent(key, false, false);
    }
  };

  const setBreadcrumb = (tabName: string) => {
    const tabText = (
      <span>
        {t(`zeit.header.breadcrumbs.projectName`)} - {t(`zeit.header.breadcrumbs.${tabName}`)}
      </span>
    );
    headerController.setHeaderProps({
      headerLeft: [<BreadcrumbComponent key="breadcrumb" items={[tabText]} />],
      headerCenter: [],
      headerRight: [],
      headerLast: [<ZeitHelpLink key="zeit-header-help" />],
    });
  };

  if (!currentUser) {
    return <></>;
  }
  const tabItems: TabsProps['items'] = [
    {
      key: routes.MEINE_ZEITPLANUNGEN,
      label: t('zeit.myZeitHome.tabs.meineZeitplanungen.tabNav'),
      children: (
        <MeineZeitplanungenTabComponent
          tabKey={routes.MEINE_ZEITPLANUNGEN}
          zeitplanungenData={zeitplanungenData}
          resetTab={resetTab}
          currentUser={currentUser}
          loadContent={() => loadContent(routes.MEINE_ZEITPLANUNGEN, false, true)}
        />
      ),
    },
    {
      key: routes.BILLIGUNGSANFRAGEN,
      label: (
        <div>
          {t('zeit.myZeitHome.tabs.billigungsanfragen.tabNav')}
          {badgeNumberBilligung ? <OpenEntriesIndicator numberOfOpenEntries={badgeNumberBilligung} /> : ''}
        </div>
      ),
      children: (
        <BilligungsanfragenTabComponent
          tabKey={routes.BILLIGUNGSANFRAGEN}
          billigungsanfragenData={billigungsanfragenData}
          currentUserID={currentUser?.base.id}
          resetTab={resetTab}
          loadContent={() => loadContent(routes.BILLIGUNGSANFRAGEN, false, true)}
        />
      ),
    },
    {
      key: routes.ZEITPLANUNGSVORLAGEN,
      label: t('zeit.myZeitHome.tabs.zeitplanungsvorlagen.tabNav'),
      children: (
        <ZeitplanungsvorlagenTabComponent
          tabKey={routes.ZEITPLANUNGSVORLAGEN}
          resetTab={resetTab}
          zeitplanungsvorlagenData={zeitplanungsvorlagenData}
          currentUser={currentUser}
          loadContent={() => loadContent(routes.ZEITPLANUNGSVORLAGEN, false, true)}
        />
      ),
    },
    {
      key: routes.SYSTEMVORLAGEN,
      label: t('zeit.myZeitHome.tabs.systemvorlagen.tabNav'),
      children: (
        <SystemvorlagenTabComponent
          tabKey={routes.SYSTEMVORLAGEN}
          resetTab={resetTab}
          systemvorlagenData={systemvorlagenData}
          currentUser={currentUser}
          loadContent={() => loadContent(routes.SYSTEMVORLAGEN, false, true)}
        />
      ),
    },
    {
      key: routes.ARCHIV,
      label: t('zeit.myZeitHome.tabs.archiv.tabNav'),
      children: <ArchivTabComponent tabKey={routes.ARCHIV} archivData={archivData} />,
    },
  ];
  return (
    <div className="zeit-home-page">
      <TitleWrapperComponent>
        <Row>
          <Col xs={{ span: 22, offset: 1 }}>
            <div className="heading-holder">
              <Title level={1}>{t('zeit.myZeitHome.title')}</Title>
              <div className="btn-container">
                <Button
                  id="ezeit-newZeitplanungsVorlage-btn"
                  type="default"
                  size={'large'}
                  onClick={() => setIsNewZeitplanungsvorlageModalVisible(true)}
                  ref={newZeitplanungsvorlageModalButtonRef}
                >
                  {t('zeit.header.btnNewZeitPlanungsVorlage')}
                </Button>
                <Button
                  id="ezeit-newZeitplanung-btn"
                  type="primary"
                  size={'large'}
                  onClick={() => setIsNewZeitplanungModalVisible(true)}
                  ref={newZeitplanungModalButtonRef}
                >
                  {t('zeit.header.btnNewZeitPlanung')}
                </Button>
              </div>
            </div>
          </Col>
        </Row>
      </TitleWrapperComponent>

      <Row>
        <Col xs={{ span: 22, offset: 1 }}>
          <NewZeitplanungsvorlageModal
            isVisible={isNewZeitplanungsvorlageModalVisible}
            setIsVisible={setIsNewZeitplanungsvorlageModalVisible}
            resetTab={resetTab}
          />
          <NewZeitplanungModal
            isVisible={isNewZeitplanungModalVisible}
            setIsVisible={setIsNewZeitplanungModalVisible}
            resetTab={resetTab}
            rvId={neueZeitplanung === routes.NEUEZEITPLANUNG && rv === routes.REGELUNGSVORHABEN && rvId ? rvId : ''}
          />
          <TabsWrapper
            activeKey={activeTab}
            className="my-periods-tabs standard-tabs"
            onChange={(key: string) => history.push(`/zeit/${key}`)}
            moduleName={t('zeit.myZeitHome.title')}
            items={tabItems}
          />
        </Col>
      </Row>
    </div>
  );
}
