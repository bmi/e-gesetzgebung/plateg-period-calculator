// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button } from 'antd';
import { ColumnsType } from 'antd/lib/table';
import Text from 'antd/lib/typography/Text';
import i18n from 'i18next';
import React, { ReactElement } from 'react';

import { AktionType, BilligungStatusType, ZeitplanungBilligungTableDTO } from '@plateg/rest-api';
import {
  compareDates,
  ContactPerson,
  getDateTimeString,
  GlobalDI,
  InfoComponent,
  TableComponentProps,
  TwoLineText,
} from '@plateg/theme';
import {
  DropdownMenu,
  DropdownMenuItem,
} from '@plateg/theme/src/components/table-component/table-sub-components/dropdown-button-component/component.react';

import { routes } from '../../../../../shares/routes';
import { ZeitplanungController } from '../controller';

export interface ExtendedZeitplanungBilligungsanfragenTableDTO extends ZeitplanungBilligungTableDTO {
  zeitplanungId: string;
}
export const getAnmerkungenRenderer = (record: ZeitplanungBilligungTableDTO): ReactElement => {
  return (
    <>
      <label htmlFor={'expandable-anmerkung-' + record.billigung.base.id}>
        {i18n.t(`zeit.myZeitHome.tabs.billigungsanfragen.table.anmerkungenLabel`).toString()}
      </label>
      <Text id={'expandable-anmerkung-' + record.billigung.base.id} className="expandable-row-beschreibung">
        {record.billigung.dto.status === BilligungStatusType.NichtBearbeitet
          ? record.billigung.dto.kommentar
          : record.billigung.dto.geantwortetKommentar}
      </Text>
    </>
  );
};

export function getBilligungsanfragenTableVals(
  content: ZeitplanungBilligungTableDTO[],
  currentUserID: string,
  setSelectedZeichnung: (selectedZeichnung: ZeitplanungBilligungTableDTO) => void,
  setAnfrageBeantwortenVisible: (isAnfrageBeantwortenVisible: boolean) => void,
  setIsArchiveModalVisible: (isArchiveModalVisible: boolean) => void,
  setIsAnfrageZurueckziehenVisible: (isAnfrageZurueckziehenVisible: boolean) => void,
): TableComponentProps<ExtendedZeitplanungBilligungsanfragenTableDTO> {
  const zeitplanungController = GlobalDI.getOrRegister('zeitplanungController', () => new ZeitplanungController());
  const columns: ColumnsType<ExtendedZeitplanungBilligungsanfragenTableDTO> = [
    {
      title: i18n.t('zeit.myZeitHome.tabs.billigungsanfragen.table.menuItemRegelungsvorhaben').toString(),
      key: 'c1',
      render: (record: ZeitplanungBilligungTableDTO): ReactElement => {
        return (
          <div className="breakable-text">
            {record.regelungsvorhaben && (
              <Text strong>{zeitplanungController.getRvString(record.regelungsvorhaben)}</Text>
            )}
          </div>
        );
      },
    },
    {
      title: i18n.t('zeit.myZeitHome.tabs.billigungsanfragen.table.menuItemZeitplanung').toString(),
      key: 'c2',
      render: (record: ExtendedZeitplanungBilligungsanfragenTableDTO): ReactElement => {
        return (
          <a
            id={`theme-generic-tableComponentFirtsRow-link-${record.id}`}
            type="link"
            style={{ fontWeight: 'normal' }}
            href={`#/zeit/${routes.MEINE_ZEITPLANUNGEN}/${record.zeitplanungId}/${routes.TABELLENSICHT}`}
          >
            {record.titel}
          </a>
        );
      },
    },
    {
      title: i18n.t('zeit.myZeitHome.tabs.billigungsanfragen.table.menuItemGesendet').toString(),
      // key matches be sort key
      key: 'bearbeitetam',
      sorter: (a, b) => compareDates(a.billigung.base.bearbeitetAm, b.billigung.base.bearbeitetAm),
      render: (record: ZeitplanungBilligungTableDTO): ReactElement => {
        return <Text>{getDateTimeString(record.billigung.base.bearbeitetAm)}</Text>;
      },
    },
    {
      title: i18n.t('zeit.myZeitHome.tabs.billigungsanfragen.table.menuItemAnfrage').toString(),
      key: 'c4',
      render: (record: ZeitplanungBilligungTableDTO): ReactElement => {
        const isMyAbstimmung = record.billigung.dto.erstelltVon.base.id === currentUserID;
        return (
          <TwoLineText
            firstRow={i18n.t(`zeit.myZeitHome.tabs.billigungsanfragen.table.status.${record.billigung.dto.status}`)}
            firstRowBold={true}
            secondRow={
              <>
                {isMyAbstimmung ? 'An ' : 'Von '}
                <InfoComponent
                  id={`contact-${record.billigung.base.id}`}
                  isContactPerson={true}
                  title={i18n.t('enorm.myZeitHome.contactPersonTitle')}
                  buttonText={
                    isMyAbstimmung
                      ? (record.billigung.dto.gestelltAn.dto.name ?? record.billigung.dto.gestelltAn.dto.email)
                      : record.billigung.dto.erstelltVon.dto.name
                  }
                >
                  <ContactPerson
                    user={{
                      ...(isMyAbstimmung ? record.billigung.dto.gestelltAn.dto : record.billigung.dto.erstelltVon.dto),
                      additionalInfo: '',
                    }}
                  />
                </InfoComponent>
              </>
            }
            secondRowLight={true}
            secondRowBold={false}
            elementId={record.id}
          />
        );
      },
    },
    {
      title: i18n.t('zeit.myZeitHome.tabs.billigungsanfragen.table.menuItemAktionen').toString(),
      key: 'c5',
      render: (record: ExtendedZeitplanungBilligungsanfragenTableDTO): ReactElement => {
        const antwortButton =
          record.billigung.dto.gestelltAn.base.id === currentUserID ? (
            <Button
              id={`zeit-anfrageBeantworten-btn-${record.id}`}
              disabled={record.billigung.dto.status !== BilligungStatusType.NichtBearbeitet}
              onClick={() => {
                setSelectedZeichnung(record);
                setAnfrageBeantwortenVisible(true);
              }}
              className="ant-btn-secondary"
            >
              {i18n.t('zeit.myZeitHome.tabs.billigungsanfragen.table.actions.antworten').toString()}
            </Button>
          ) : (
            <></>
          );
        const items: DropdownMenuItem[] = [
          {
            element: i18n.t('zeit.myZeitHome.tabs.billigungsanfragen.table.actions.archivieren'),
            disabled: () => !record.aktionen?.includes(AktionType.Archivieren),
            onClick: () => {
              setSelectedZeichnung(record);
              setIsArchiveModalVisible(true);
            },
          },
        ];
        if (record.billigung.dto.erstelltVon.base.id === currentUserID) {
          items.push({
            element: i18n.t('zeit.myZeitHome.tabs.billigungsanfragen.table.actions.zurueckziehen'),
            disabled: () => record.billigung.dto.status !== BilligungStatusType.NichtBearbeitet,
            onClick: () => {
              setSelectedZeichnung(record);
              setIsAnfrageZurueckziehenVisible(true);
            },
          });
        }
        return (
          <div className="actions-holder">
            {antwortButton}
            <DropdownMenu
              openLink={`/zeit/${routes.MEINE_ZEITPLANUNGEN}/${record.zeitplanungId}/${routes.TABELLENSICHT}`}
              items={items}
              elementId={record.id}
            />
          </div>
        );
      },
    },
  ];

  const actualContent = content.map((item) => {
    return {
      ...item,
      zeitplanungId: item.id,
      id: item.billigung.base.id,
    };
  });

  return {
    id: 'period-billigungsanfragen-table',
    expandable: true,
    expandedRowRender: (record) => getAnmerkungenRenderer(record),
    expandableCondition: (record: ZeitplanungBilligungTableDTO) => {
      const billigung = record.billigung.dto;
      if (
        (billigung.status === BilligungStatusType.NichtBearbeitet && billigung.kommentar) ||
        (billigung.status !== BilligungStatusType.NichtBearbeitet && billigung.geantwortetKommentar)
      ) {
        return true;
      }
      return false;
    },
    columns,
    content: actualContent,
    filteredColumns: [
      { name: 'regelungsvorhaben', columnIndex: 0 },
      { name: 'rueckmeldung', columnIndex: 3 },
    ],
    sorterOptions: [
      {
        columnKey: 'bearbeitetam',
        titleAsc: i18n.t('zeit.myZeitHome.tabs.sorters.gesendetAsc'),
        titleDesc: i18n.t('zeit.myZeitHome.tabs.sorters.gesendetDesc'),
      },
    ],
    customDefaultSortIndex: 2,
  };
}
