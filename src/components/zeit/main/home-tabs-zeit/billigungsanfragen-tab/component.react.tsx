// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { ZeitplanungBilligungTableDTO } from '@plateg/rest-api';
import {
  ArchivConfirmComponent,
  EmptyContentComponent,
  GlobalDI,
  ImageModule,
  TableComponent,
  TableComponentProps,
} from '@plateg/theme';
import { useAppSelector } from '@plateg/theme/src/components/store';

import { routes } from '../../../../../shares/routes';
import { AnfrageBeantwortenModal } from '../anfrage-beantworten-modal/component.react';
import { AnfrageZurueckziehenModal } from '../anfrage-zurueckziehen-modal/component.react';
import { ZeitplanungController } from '../controller';
import { ExtendedZeitplanungBilligungsanfragenTableDTO, getBilligungsanfragenTableVals } from './controller.react';

interface ZeitplanungsvorlagenTabComponentInterface {
  billigungsanfragenData: ZeitplanungBilligungTableDTO[];
  currentUserID: string;
  resetTab: (key: string) => void;
  tabKey: string;
  loadContent: () => void;
}
export function BilligungsanfragenTabComponent(
  props: Readonly<ZeitplanungsvorlagenTabComponentInterface>,
): React.ReactElement {
  const { t } = useTranslation();
  const zeitplanungController = GlobalDI.getOrRegister('zeitplanungController', () => new ZeitplanungController());
  const [isAnfrageBeantwortenVisible, setIsAnfrageBeantwortenVisible] = useState<boolean>(false);
  const [isAnfrageZurueckziehenVisible, setIsAnfrageZurueckziehenVisible] = useState<boolean>(false);
  const [isArchiveModalVisible, setIsArchiveModalVisible] = useState<boolean>(false);
  const [selectedZeichnung, setSelectedZeichnung] = useState<ZeitplanungBilligungTableDTO>();
  const [billigungsanfragenData, setBilligungsanfragenData] = useState(props.billigungsanfragenData);
  const [billigungsanfragenTableVals, setBilligungsanfragenTableVals] =
    useState<TableComponentProps<ExtendedZeitplanungBilligungsanfragenTableDTO>>();

  const pagDataResult = useAppSelector((state) => state.tablePagination.tabs[props.tabKey]).result;

  useEffect(() => {
    setBilligungsanfragenData(props.billigungsanfragenData);
  }, [props.billigungsanfragenData]);

  useEffect(() => {
    setBilligungsanfragenTableVals(
      getBilligungsanfragenTableVals(
        billigungsanfragenData,
        props.currentUserID,
        setSelectedZeichnung,
        setIsAnfrageBeantwortenVisible,
        setIsArchiveModalVisible,
        setIsAnfrageZurueckziehenVisible,
      ),
    );
  }, [billigungsanfragenData]);

  return (
    <>
      {selectedZeichnung && (
        <>
          <AnfrageBeantwortenModal
            isVisible={isAnfrageBeantwortenVisible}
            setIsVisible={setIsAnfrageBeantwortenVisible}
            zeichnung={selectedZeichnung}
            resetTab={props.resetTab}
          />
          <ArchivConfirmComponent
            actionTitle={t('zeit.myZeitHome.tabs.billigungsanfragen.archiveConfirmModal.content')}
            visible={isArchiveModalVisible}
            setVisible={setIsArchiveModalVisible}
            setArchived={() => {
              zeitplanungController.archiveZeichnung(
                selectedZeichnung.billigung.base.id,
                selectedZeichnung.titel,
                () => {
                  props.resetTab(routes.ARCHIV);
                  setBilligungsanfragenData(
                    billigungsanfragenData.filter((data) => {
                      return data.billigung.base.id !== selectedZeichnung.billigung.base.id;
                    }),
                  );
                  setIsArchiveModalVisible(false);
                  props.loadContent();
                },
              );
            }}
            okText={t('zeit.myZeitHome.tabs.billigungsanfragen.archiveConfirmModal.cancel')}
            cancelText={t('zeit.myZeitHome.tabs.billigungsanfragen.archiveConfirmModal.confirm')}
          />
          <AnfrageZurueckziehenModal
            visible={isAnfrageZurueckziehenVisible}
            setVisible={setIsAnfrageZurueckziehenVisible}
            zeichnung={selectedZeichnung}
            resetTab={props.resetTab}
          />
        </>
      )}
      {billigungsanfragenTableVals && !pagDataResult.allContentEmpty && (
        <TableComponent
          bePagination
          tabKey={props.tabKey}
          id={billigungsanfragenTableVals.id}
          columns={billigungsanfragenTableVals.columns}
          content={billigungsanfragenTableVals.content}
          filteredColumns={billigungsanfragenTableVals.filteredColumns}
          filterRowsMethod={zeitplanungController.getFilteredRows}
          prepareFilterButtonMethod={zeitplanungController.getFilterButton}
          expandable={billigungsanfragenTableVals.expandable}
          expandedRowRender={billigungsanfragenTableVals.expandedRowRender}
          sorterOptions={billigungsanfragenTableVals.sorterOptions}
          customDefaultSortIndex={billigungsanfragenTableVals.customDefaultSortIndex}
          className={billigungsanfragenTableVals.className}
          expandableCondition={billigungsanfragenTableVals.expandableCondition}
        />
      )}
      <EmptyContentComponent
        noIndex={true}
        images={[
          {
            label: t('zeit.myZeitHome.tabs.billigungsanfragen.imgText1'),
            imgSrc: require(
              `../../../../../media/empty-content-images/billigungsanfragen-tab/img1-billigungsanfragen.svg`,
            ) as ImageModule,
            height: 109,
          },
          {
            label: t('zeit.myZeitHome.tabs.billigungsanfragen.imgText2'),
            imgSrc: require(
              `../../../../../media/empty-content-images/billigungsanfragen-tab/img2-billigungsanfragen.svg`,
            ) as ImageModule,
            height: 99,
          },
        ]}
      >
        <p className="info-text">
          {t('zeit.myZeitHome.tabs.billigungsanfragen.text', {
            interpolation: { escapeValue: false },
          })}
        </p>
      </EmptyContentComponent>
    </>
  );
}
