// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { ColumnsType } from 'antd/lib/table';
import Text from 'antd/lib/typography/Text';
import i18n from 'i18next';
import React, { ReactElement } from 'react';

import {
  AktionType,
  BilligungStatusType,
  RolleLokalType,
  ZeitplanungsvorlageTableDTO,
  ZeitplanungTableDTO,
} from '@plateg/rest-api';
import {
  compareDates,
  getDateTimeString,
  InfoComponent,
  TableComponentProps,
  TextAndContact,
  TwoLineText,
} from '@plateg/theme';
import {
  DropdownMenu,
  DropdownMenuItem,
} from '@plateg/theme/src/components/table-component/table-sub-components/dropdown-button-component/component.react';
import { GlobalDI } from '@plateg/theme/src/shares';
import { Filters } from '@plateg/theme/src/shares/filters';

import { routes } from '../../../../../shares/routes';
import { ZeitplanungController } from '../controller';

export const getBeschreibungRenderer = (
  record: ZeitplanungTableDTO | ZeitplanungsvorlageTableDTO,
  tab: string,
): ReactElement => {
  return (
    <>
      <label htmlFor={'expandable-beschreibung-' + record.id.toString()}>
        {i18n.t(`zeit.myZeitHome.tabs.${tab}.table.beschreibungLabel`).toString()}
      </label>
      <Text id={'expandable-beschreibung-' + record.id.toString()} className="expandable-row-beschreibung">
        {record.beschreibung}
      </Text>
    </>
  );
};

export function getMeineZeitplanungenTableVals(
  content: ZeitplanungTableDTO[],
  setSelectedZeitplanung: (selectedZeitplanung: ZeitplanungTableDTO) => void,
  setIsFreigebenModalVisible: (value: boolean) => void,
  setIsArchiveModalVisible: (value: boolean) => void,
  setIsSpeichernAlsVorlageModalVisible: (value: boolean) => void,
  setIsZeichnungAnfragenModalVisible: (value: boolean) => void,
): TableComponentProps<ZeitplanungTableDTO> {
  const zeitplanungController = GlobalDI.getOrRegister('zeitplanungController', () => new ZeitplanungController());

  const columns: ColumnsType<ZeitplanungTableDTO> = [
    {
      title: i18n.t('zeit.myZeitHome.tabs.meineZeitplanungen.table.menuItemRegelungsvorhaben').toString(),
      key: 'c1',
      render: (record: ZeitplanungTableDTO): ReactElement => {
        return (
          <div className="breakable-text">
            {record.regelungsvorhaben && (
              <TwoLineText
                firstRow={zeitplanungController.getRvString(record.regelungsvorhaben)}
                firstRowBold={true}
                secondRow={record.regelungsvorhaben.dto.kurzbezeichnung || ''}
                secondRowLight={true}
                secondRowBold={false}
                elementId={`${record.id}-rv`}
              />
            )}
          </div>
        );
      },
    },
    {
      title: i18n.t('zeit.myZeitHome.tabs.meineZeitplanungen.table.menuItemZeitplanung').toString(),
      key: 'c2',
      render: (record: ZeitplanungTableDTO): ReactElement => {
        return (
          <a
            id={`theme-generic-tableComponentFirtsRow-link-${record.id}`}
            type="link"
            style={{ fontWeight: 'normal' }}
            href={`#/zeit/${routes.MEINE_ZEITPLANUNGEN}/${record.id}/${routes.TABELLENSICHT}`}
          >
            {record.titel}
          </a>
        );
      },
    },
    {
      title: i18n.t('zeit.myZeitHome.tabs.meineZeitplanungen.table.menuItemNaechstesElement').toString(),
      key: 'c3',
      // not yet possible to sort this column because no field for it in be db given
      // sorter: (a, b) => compareDates(a.naechstesElement?.beginn, b.naechstesElement?.beginn),
      render: (record: ZeitplanungTableDTO): ReactElement => {
        if (!record.naechstesElement) {
          return <></>;
        } else {
          return (
            <>
              <Text strong={true} className="limited-width">
                {record.naechstesElement.titel}
              </Text>
              <br />
              <Text>
                {record.naechstesElement.termin === true
                  ? zeitplanungController.getDurationString(
                      record.naechstesElement.beginn,
                      record.naechstesElement.faelligIn.toString(),
                    )
                  : zeitplanungController.getDurationDatesString(
                      record.naechstesElement.beginn,
                      record.naechstesElement.ende,
                      record.naechstesElement.faelligIn.toString(),
                    )}
              </Text>
            </>
          );
        }
      },
    },
    {
      title: i18n.t('zeit.myZeitHome.tabs.meineZeitplanungen.table.menuItemBeplanterZeitraum').toString(),
      key: 'c4',
      render: (record: ZeitplanungTableDTO): ReactElement => {
        return (
          <Text>{`${Filters.dateFromString(record.erstePhaseBeginntAm)} – ${Filters.dateFromString(
            record.letztePhaseEndetAm,
          )}`}</Text>
        );
      },
    },
    {
      title: i18n.t('zeit.myZeitHome.tabs.meineZeitplanungen.table.menuItemErsteller').toString(),
      key: 'c5',
      render: (record: ZeitplanungTableDTO): ReactElement => {
        return (
          <TextAndContact
            drawerId={`erstellt-drawer-${record.id}`}
            drawerTitle={i18n.t('zeit.myZeitHome.contactPersonTitle')}
            firstRow={getDateTimeString(record.erstelltAm)}
            user={record.erstelltVon.dto}
          />
        );
      },
    },
    {
      title: i18n.t('zeit.myZeitHome.tabs.meineZeitplanungen.table.menuItemZuletztBearbeitet').toString(),
      key: 'bearbeitetam',
      sorter: (a, b) => compareDates(a.bearbeitetAm || a.erstelltAm, b.bearbeitetAm || b.erstelltAm),
      render: (record: ZeitplanungTableDTO): ReactElement => {
        const user = record.bearbeitetVonStellvertreter || record.bearbeitetVon || record.erstelltVon;
        return (
          <TextAndContact
            drawerId={`bearbeitet-am-drawer-${record.id}`}
            drawerTitle={i18n.t('zeit.myZeitHome.contactPersonTitle')}
            firstRow={getDateTimeString(record.bearbeitetAm || record.erstelltAm)}
            user={user.dto}
            isStellvertretung={!!record.bearbeitetVonStellvertreter}
          />
        );
      },
    },
    {
      title: (
        <>
          {i18n.t('zeit.myZeitHome.tabs.meineZeitplanungen.table.zugriffsrecht')}
          <InfoComponent title={i18n.t(`zeit.myZeitHome.tabs.meineZeitplanungen.table.zugriffsrechtInfo.title`)}>
            <div
              dangerouslySetInnerHTML={{
                __html: i18n.t(`zeit.myZeitHome.tabs.meineZeitplanungen.table.zugriffsrechtInfo.drawerText`),
              }}
            ></div>
          </InfoComponent>
        </>
      ),
      key: 'c7',
      render: (record: ZeitplanungTableDTO): ReactElement => {
        return <Text>{i18n.t(`zeit.myZeitHome.tabs.zugriffsrechte.${record.rolleTyp}`).toString()}</Text>;
      },
    },
    {
      title: i18n.t('zeit.myZeitHome.tabs.meineZeitplanungen.table.menuItemAktionen').toString(),
      key: 'c8',
      render: (record: ZeitplanungTableDTO): ReactElement => {
        const items: DropdownMenuItem[] = [
          {
            element: i18n.t('zeit.myZeitHome.tabs.meineZeitplanungen.table.actions.freigeben'),
            onClick: () => {
              setIsFreigebenModalVisible(true);
              setSelectedZeitplanung(record);
            },
          },
          {
            element: i18n.t('zeit.myZeitHome.tabs.meineZeitplanungen.table.actions.zeichnungAnfragen'),
            disabled: () =>
              record.rolleTyp !== RolleLokalType.Federfuehrer ||
              record.billigungen?.some((item) => item.status === BilligungStatusType.NichtBearbeitet),
            onClick: () => {
              setIsZeichnungAnfragenModalVisible(true);
              setSelectedZeitplanung(record);
            },
          },
          {
            element: i18n.t('zeit.myZeitHome.tabs.meineZeitplanungen.table.actions.vorlageSpeichern'),
            onClick: () => {
              setIsSpeichernAlsVorlageModalVisible(true);
              setSelectedZeitplanung(record);
            },
          },
          {
            element: i18n.t('zeit.myZeitHome.tabs.meineZeitplanungen.table.actions.archivieren'),
            disabled: () => !record.aktionen?.includes(AktionType.Archivieren),
            onClick: () => {
              setIsArchiveModalVisible(true);
              setSelectedZeitplanung(record);
            },
          },
        ];
        return (
          <DropdownMenu
            openLink={`/zeit/${routes.MEINE_ZEITPLANUNGEN}/${record.id}/${routes.TABELLENSICHT}`}
            items={items}
            elementId={record.id}
          />
        );
      },
    },
  ];

  return {
    id: 'period-zeitplanungen-table',
    expandable: true,
    expandedRowRender: (record) => getBeschreibungRenderer(record, routes.MEINE_ZEITPLANUNGEN),
    expandableCondition: (record: ZeitplanungTableDTO) => {
      if (record.beschreibung) {
        return true;
      }
      return false;
    },
    columns,
    content,
    filteredColumns: [
      { name: 'regelungsvorhaben', columnIndex: 0 },
      { name: 'erstelltVon', columnIndex: 4 },
    ],
    sorterOptions: [
      {
        columnKey: 'bearbeitetam',
        titleAsc: i18n.t('zeit.myZeitHome.tabs.sorters.bearbeitetAsc'),
        titleDesc: i18n.t('zeit.myZeitHome.tabs.sorters.bearbeitetDesc'),
      },
    ],
    customDefaultSortIndex: 2,
  };
}
