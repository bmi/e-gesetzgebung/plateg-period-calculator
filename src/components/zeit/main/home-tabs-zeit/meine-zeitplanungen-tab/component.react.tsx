// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { AktionType, UserEntityResponseDTO, ZeitplanungTableDTO } from '@plateg/rest-api';
import {
  ArchivConfirmComponent,
  EmptyContentComponent,
  ImageModule,
  TableComponent,
  TableComponentProps,
} from '@plateg/theme';
import { useAppSelector } from '@plateg/theme/src/components/store';
import { GlobalDI } from '@plateg/theme/src/shares';

import { routes } from '../../../../../shares/routes';
import { SpeichernAlsZeitplanungsvorlageComponent } from '../../new-zeitplanungsvorlage-modal/speichern-als-zeitplanungvorlage/component.react';
import { BilligungsanfragenDialog } from '../billigungsanfragen-modal/component.react';
import { ZeitplanungController } from '../controller';
import { ZeitFreigabeModalRBAC } from '../freigabe-rbac-modal/component.react';
import { getMeineZeitplanungenTableVals } from './controller.react';

interface MeineZeitplanungenTabComponentInterface {
  zeitplanungenData: ZeitplanungTableDTO[];
  resetTab: (key: string) => void;
  currentUser: UserEntityResponseDTO;
  tabKey: string;
  loadContent: () => void;
}

export function MeineZeitplanungenTabComponent(
  props: Readonly<MeineZeitplanungenTabComponentInterface>,
): React.ReactElement {
  const { t } = useTranslation();
  const zeitplanungController = GlobalDI.getOrRegister('zeitplanungController', () => new ZeitplanungController());
  const [zeitplanungenData, setZeitplanungenData] = useState(props.zeitplanungenData);
  const [zeitplanungenTableVals, setZeitplanungenTableVals] = useState<TableComponentProps<ZeitplanungTableDTO>>();
  const [isFreigebenModalVisible, setIsFreigebenModalVisible] = useState<boolean>(false);
  const [isArchiveModalVisible, setIsArchiveModalVisible] = useState<boolean>(false);
  const [selectedZeitplanung, setSelectedZeitplanung] = useState<ZeitplanungTableDTO>({} as ZeitplanungTableDTO);
  const [isSpeichernAlsVorlageModalVisible, setIsSpeichernAlsVorlageModalVisible] = useState<boolean>(false);
  const [isZeichnungAnfragenModalVisible, setIsZeichnungAnfragenModalVisible] = useState<boolean>(false);

  const pagDataResult = useAppSelector((state) => state.tablePagination.tabs[props.tabKey]).result;

  useEffect(() => {
    setZeitplanungenData(props.zeitplanungenData);
  }, [props.zeitplanungenData]);

  useEffect(() => {
    setZeitplanungenTableVals(
      getMeineZeitplanungenTableVals(
        zeitplanungenData,
        setSelectedZeitplanung,
        setIsFreigebenModalVisible,
        setIsArchiveModalVisible,
        setIsSpeichernAlsVorlageModalVisible,
        setIsZeichnungAnfragenModalVisible,
      ),
    );
  }, [zeitplanungenData]);

  return (
    <>
      <ArchivConfirmComponent
        actionTitle={t('zeit.myZeitHome.tabs.meineZeitplanungen.archiveConfirmModal.content')}
        visible={isArchiveModalVisible}
        setVisible={setIsArchiveModalVisible}
        setArchived={() => {
          zeitplanungController.archiveZeitplanung(selectedZeitplanung.id, selectedZeitplanung.titel, () => {
            props.resetTab(routes.ARCHIV);
            props.resetTab(routes.BILLIGUNGSANFRAGEN);
            setZeitplanungenData(
              zeitplanungenData.filter((data) => {
                return data.id !== selectedZeitplanung.id;
              }),
            );
            setIsArchiveModalVisible(false);
            props.loadContent();
          });
        }}
        okText={t('zeit.myZeitHome.tabs.meineZeitplanungen.archiveConfirmModal.cancel')}
        cancelText={t('zeit.myZeitHome.tabs.meineZeitplanungen.archiveConfirmModal.confirm')}
      />
      <ZeitFreigabeModalRBAC
        disabled={!selectedZeitplanung?.aktionen?.includes(AktionType.BerechtigungenAendern)}
        isVisible={isFreigebenModalVisible}
        setIsVisible={setIsFreigebenModalVisible}
        objectTitle={selectedZeitplanung.regelungsvorhaben?.dto.kurzbezeichnung}
        objectId={selectedZeitplanung.regelungsvorhaben?.base.id as string}
      />
      <BilligungsanfragenDialog
        isVisible={isZeichnungAnfragenModalVisible}
        setIsVisible={setIsZeichnungAnfragenModalVisible}
        objectTitle={selectedZeitplanung.titel}
        objectId={selectedZeitplanung.id}
        resetTab={props.resetTab}
        currentUser={props.currentUser}
      />
      {zeitplanungenTableVals && !pagDataResult.allContentEmpty && (
        <>
          <SpeichernAlsZeitplanungsvorlageComponent
            isVisible={isSpeichernAlsVorlageModalVisible}
            setIsVisible={setIsSpeichernAlsVorlageModalVisible}
            resetTab={props.resetTab}
            selectedZeitplanung={selectedZeitplanung}
            selectedId={selectedZeitplanung.id}
          />
          <TableComponent
            bePagination
            tabKey={props.tabKey}
            id={zeitplanungenTableVals.id}
            columns={zeitplanungenTableVals.columns}
            content={zeitplanungenTableVals.content}
            filteredColumns={zeitplanungenTableVals.filteredColumns}
            filterRowsMethod={zeitplanungController.getFilteredRows}
            prepareFilterButtonMethod={zeitplanungController.getFilterButton}
            expandable={zeitplanungenTableVals.expandable}
            expandedRowRender={zeitplanungenTableVals.expandedRowRender}
            sorterOptions={zeitplanungenTableVals.sorterOptions}
            customDefaultSortIndex={zeitplanungenTableVals.customDefaultSortIndex}
            className={zeitplanungenTableVals.className}
            expandableCondition={zeitplanungenTableVals.expandableCondition}
          ></TableComponent>
        </>
      )}
      <EmptyContentComponent
        images={[
          {
            label: t('zeit.myZeitHome.tabs.meineZeitplanungen.imgText1'),
            imgSrc: require(
              `../../../../../media/empty-content-images/meine-zeitplanung-tab/img1-myZeitplanungen.svg`,
            ) as ImageModule,
            height: 100,
          },
          {
            label: t('zeit.myZeitHome.tabs.meineZeitplanungen.imgText2'),
            imgSrc: require(
              `../../../../../media/empty-content-images/meine-zeitplanung-tab/img2-myZeitplanungen.svg`,
            ) as ImageModule,
            height: 114,
          },
          {
            label: t('zeit.myZeitHome.tabs.meineZeitplanungen.imgText3'),
            imgSrc: require(
              `../../../../../media/empty-content-images/meine-zeitplanung-tab/img3-myZeitplanungen.svg`,
            ) as ImageModule,
            height: 119,
          },
        ]}
      >
        <p className="info-text">
          {t('zeit.myZeitHome.tabs.meineZeitplanungen.text', {
            interpolation: { escapeValue: false },
          })}
        </p>
      </EmptyContentComponent>
    </>
  );
}
