// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import Text from 'antd/lib/typography/Text';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router';
import { AjaxError } from 'rxjs/ajax';

import { ErrorController, LoadingStatusController } from '@plateg/theme';
import { ModalWrapper } from '@plateg/theme/src/components/modal-wrapper/component.react';
import { GlobalDI } from '@plateg/theme/src/shares';

import { ZeitplanungController } from '../../controller';

export interface OpenConfirmModalComponentProps {
  isVisible: boolean;
  setIsVisible: (isVisible: boolean) => void;
  target: string;
}

export function OpenConfirmModalComponent(props: OpenConfirmModalComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const history = useHistory();
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const zeitplanungController = GlobalDI.getOrRegister('zeitplanungController', () => new ZeitplanungController());
  const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  const [shouldAsk, setShouldAsk] = useState(false);

  const errorCallback = (error: AjaxError) => {
    loadingStatusController.setLoadingStatus(false);
    console.error(error, error);
    errorCtrl.displayErrorMsg(error, 'zeit.generalErrorMsg');
  };

  useEffect(() => {
    if (props.isVisible) {
      loadingStatusController.setLoadingStatus(true);
      zeitplanungController.getIsShowZeitplanungSystemvorschlagOpenDialog().subscribe({
        next: (val: boolean) => {
          loadingStatusController.setLoadingStatus(false);
          if (val) {
            setShouldAsk(val);
          } else {
            history.push(props.target);
          }
        },
        error: errorCallback,
      });
    }
  }, [props.isVisible]);

  const leaveHandler = () => {
    props.setIsVisible(!props.isVisible);
    loadingStatusController.setLoadingStatus(false);
    history.push(props.target);
  };

  const leaveAndNotAskedAgainListener = () => {
    loadingStatusController.setLoadingStatus(true);
    zeitplanungController.setShowZeitplanungSystemvorschlagOpenDialog(false).subscribe({
      next: leaveHandler,
      error: errorCallback,
    });
  };

  return (
    <>
      {shouldAsk === true && (
        <ModalWrapper
          width="590px"
          key="open-confirm-modal"
          open={props.isVisible}
          closable={false}
          title={<h3>{t('zeit.myZeitHome.tabs.zeitplanungsvorlagen.openConfirmModal.title')}</h3>}
          cancelText={t('zeit.myZeitHome.tabs.zeitplanungsvorlagen.openConfirmModal.confirm')}
          okText={t('zeit.myZeitHome.tabs.zeitplanungsvorlagen.openConfirmModal.confirmDontShowAgain')}
          cancelButtonProps={{ onClick: leaveHandler, type: 'primary' }}
          okButtonProps={{ onClick: leaveAndNotAskedAgainListener, type: 'default' }}
        >
          <Text>{t('zeit.myZeitHome.tabs.zeitplanungsvorlagen.openConfirmModal.content')}</Text>
        </ModalWrapper>
      )}
    </>
  );
}
