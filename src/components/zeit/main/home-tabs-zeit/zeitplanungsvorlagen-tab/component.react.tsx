// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { AktionType, UserEntityResponseDTO, ZeitplanungsvorlageTableDTO } from '@plateg/rest-api';
import {
  ArchivConfirmComponent,
  EmptyContentComponent,
  ImageModule,
  TableComponent,
  TableComponentProps,
} from '@plateg/theme';
import { useAppSelector } from '@plateg/theme/src/components/store';
import { GlobalDI } from '@plateg/theme/src/shares';

import { routes } from '../../../../../shares/routes';
import { CopyZeitplanungsvorlageComponent } from '../../new-zeitplanungsvorlage-modal/copy-zeitplanungsvorlage/component.react';
import { ZeitplanungController } from '../controller';
import { ZeitFreigabeModalRBAC } from '../freigabe-rbac-modal/component.react';
import { getZeitplanungsvorlagenTableVals } from './controller.react';
import { OpenConfirmModalComponent } from './open-confirm-modal/component.react';

interface ZeitplanungsvorlagenTabComponentInterface {
  zeitplanungsvorlagenData: ZeitplanungsvorlageTableDTO[];
  resetTab: (key: string) => void;
  currentUser: UserEntityResponseDTO;
  tabKey: string;
  loadContent: () => void;
}

export function ZeitplanungsvorlagenTabComponent(
  props: Readonly<ZeitplanungsvorlagenTabComponentInterface>,
): React.ReactElement {
  const { t } = useTranslation();
  const zeitplanungController = GlobalDI.getOrRegister('zeitplanungController', () => new ZeitplanungController());
  const [openConfirmModalProps, setOpenConfirmModalProps] = useState<{ isVisible: boolean; target: string }>({
    isVisible: false,
    target: '',
  });
  const [isFreigebenModalVisible, setIsFreigebenModalVisible] = useState<boolean>(false);
  const [isArchiveModalVisible, setIsArchiveModalVisible] = useState<boolean>(false);
  const [zeitplanungsvorlagenData, setZeitplanungsvorlagenData] = useState(props.zeitplanungsvorlagenData);
  const [copyZeitplanungsvorlageModalIsVisible, setCopyZeitplanungsvorlageModalIsVisible] = useState(false);
  const [selectedZeitplanungsvorlage, setSelectedZeitplanungsvorlage] = useState<ZeitplanungsvorlageTableDTO>(
    {} as ZeitplanungsvorlageTableDTO,
  );
  const [zeitplanungsvorlagenTableVals, setZeitplanungsvorlagenTableVals] =
    useState<TableComponentProps<ZeitplanungsvorlageTableDTO>>();

  const pagDataResult = useAppSelector((state) => state.tablePagination.tabs[props.tabKey]).result;

  useEffect(() => {
    setZeitplanungsvorlagenData(props.zeitplanungsvorlagenData);
  }, [props.zeitplanungsvorlagenData]);

  useEffect(() => {
    setZeitplanungsvorlagenTableVals(
      getZeitplanungsvorlagenTableVals(
        zeitplanungsvorlagenData,
        setSelectedZeitplanungsvorlage,
        setCopyZeitplanungsvorlageModalIsVisible,
        setOpenConfirmModalProps,
        setIsFreigebenModalVisible,
        setIsArchiveModalVisible,
      ),
    );
  }, [zeitplanungsvorlagenData]);

  return (
    <>
      <ArchivConfirmComponent
        actionTitle={t('zeit.myZeitHome.archivierenDialog.text')}
        visible={isArchiveModalVisible}
        setVisible={setIsArchiveModalVisible}
        setArchived={() => {
          zeitplanungController.archiveZeitplanungsvorlage(
            selectedZeitplanungsvorlage.id,
            selectedZeitplanungsvorlage.titel,
            () => {
              props.resetTab(routes.ARCHIV);
              setZeitplanungsvorlagenData(
                zeitplanungsvorlagenData.filter((data) => {
                  return data.id !== selectedZeitplanungsvorlage.id;
                }),
              );
              setIsArchiveModalVisible(false);
              props.loadContent();
            },
          );
        }}
        okText={t('zeit.myZeitHome.archivierenDialog.okButtonText')}
        cancelText={t('zeit.myZeitHome.archivierenDialog.cancelButtonText')}
      />
      <ZeitFreigabeModalRBAC
        disabled={!selectedZeitplanungsvorlage?.aktionen?.includes(AktionType.BerechtigungenAendern)}
        isVisible={isFreigebenModalVisible}
        setIsVisible={setIsFreigebenModalVisible}
        objectTitle={selectedZeitplanungsvorlage.titel}
        objectId={selectedZeitplanungsvorlage.id}
        isZeitplanungvorlage={true}
        leserechtOnly
        loadContent={props.loadContent}
        allowChanges={[AktionType.BerechtigungenAendern, AktionType.EigeneBerechtigungenLoeschen].some((aktion) =>
          selectedZeitplanungsvorlage?.aktionen?.includes(aktion),
        )}
        revokeObserverRight={selectedZeitplanungsvorlage?.aktionen?.includes(AktionType.EigeneBerechtigungenLoeschen)}
      />
      {zeitplanungsvorlagenTableVals && !pagDataResult.allContentEmpty && (
        <>
          <CopyZeitplanungsvorlageComponent
            isVisible={copyZeitplanungsvorlageModalIsVisible}
            setIsVisible={setCopyZeitplanungsvorlageModalIsVisible}
            resetTab={props.resetTab}
            selectedZeitplanungsvorlage={selectedZeitplanungsvorlage}
            selectedId={selectedZeitplanungsvorlage?.id}
          />
          <OpenConfirmModalComponent
            isVisible={openConfirmModalProps?.isVisible}
            target={openConfirmModalProps?.target}
            setIsVisible={(isVisible: boolean) => {
              setOpenConfirmModalProps({ isVisible: isVisible, target: openConfirmModalProps?.target });
            }}
          />
          <TableComponent
            bePagination
            tabKey={props.tabKey}
            id={zeitplanungsvorlagenTableVals.id}
            columns={zeitplanungsvorlagenTableVals.columns}
            content={zeitplanungsvorlagenTableVals.content}
            filteredColumns={zeitplanungsvorlagenTableVals.filteredColumns}
            filterRowsMethod={zeitplanungController.getFilteredRows}
            prepareFilterButtonMethod={zeitplanungController.getFilterButton}
            expandable={zeitplanungsvorlagenTableVals.expandable}
            expandedRowRender={zeitplanungsvorlagenTableVals.expandedRowRender}
            sorterOptions={zeitplanungsvorlagenTableVals.sorterOptions}
            customDefaultSortIndex={zeitplanungsvorlagenTableVals.customDefaultSortIndex}
            className={zeitplanungsvorlagenTableVals.className}
            expandableCondition={zeitplanungsvorlagenTableVals.expandableCondition}
          />
        </>
      )}
      <EmptyContentComponent
        noIndex={true}
        images={[
          {
            label: t('zeit.myZeitHome.tabs.zeitplanungsvorlagen.imgText1'),
            imgSrc: require(
              `../../../../../media/empty-content-images/zeitplanungsvorlagen-tab/img1-zeitplanungsvorlagen.svg`,
            ) as ImageModule,
            height: 110,
          },
        ]}
      >
        <p className="info-text">
          {t('zeit.myZeitHome.tabs.zeitplanungsvorlagen.text', {
            interpolation: { escapeValue: false },
          })}
        </p>
      </EmptyContentComponent>
    </>
  );
}
