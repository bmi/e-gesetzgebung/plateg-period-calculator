// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { ColumnsType } from 'antd/lib/table';
import Text from 'antd/lib/typography/Text';
import i18n from 'i18next';
import React, { MouseEvent, ReactElement } from 'react';
import { Link } from 'react-router-dom';

import { AktionType, ErstellerType, ZeitplanungsvorlageTableDTO } from '@plateg/rest-api';
import { compareDates, getDateTimeString, InfoComponent, TableComponentProps, TextAndContact } from '@plateg/theme';
import {
  DropdownMenu,
  DropdownMenuItem,
} from '@plateg/theme/src/components/table-component/table-sub-components/dropdown-button-component/component.react';

import { routes } from '../../../../../shares/routes';
import { getBeschreibungRenderer } from '../meine-zeitplanungen-tab/controller.react';

export function getZeitplanungsvorlagenTableVals(
  content: ZeitplanungsvorlageTableDTO[],
  setSelectedZeitplanungsvorlage: (selectedZeitplanungsvorlage: ZeitplanungsvorlageTableDTO) => void,
  setCopyZeitplanungsvorlageModalIsVisible: (copyZeitplanungsvorlageModalIsVisible: boolean) => void,
  setOpenConfirmModalProps: (openConfirmModalProps: { isVisible: boolean; target: string }) => void,
  setIsFreigebenModalVisible: (value: boolean) => void,
  setIsArchiveModalVisible: (value: boolean) => void,
): TableComponentProps<ZeitplanungsvorlageTableDTO> {
  const vorlagenTranslationsKey = `zeit.myZeitHome.tabs.zeitplanungsvorlagen.table`;
  const columns: ColumnsType<ZeitplanungsvorlageTableDTO> = [
    {
      title: i18n.t(`${vorlagenTranslationsKey}.menuItemVorhabentyp`).toString(),
      key: 'c1',
      render: (record: ZeitplanungsvorlageTableDTO): ReactElement => {
        return <Text strong>{i18n.t(`${vorlagenTranslationsKey}.vorhabenart.${record.vorhabenart}`).toString()}</Text>;
      },
    },
    {
      title: i18n.t(`${vorlagenTranslationsKey}.menuItemZeitplanungsvorlage`).toString(),
      key: 'c2',
      render: (record: ZeitplanungsvorlageTableDTO): ReactElement => {
        const target = `/zeit/${routes.ZEITPLANUNGSVORLAGEN}/${record.id}/${routes.TABELLENSICHT}`;
        return (
          <Link
            id={`ezeit-zeitplanungsVorlageTab-${record.id}-link`}
            onClick={(e: MouseEvent<HTMLAnchorElement>) => {
              if (record.erstellerType === ErstellerType.Admin) {
                e.preventDefault();
                setOpenConfirmModalProps({
                  isVisible: true,
                  target,
                });
              }
            }}
            to={target}
          >
            {record.titel}
          </Link>
        );
      },
    },
    {
      title: i18n.t(`${vorlagenTranslationsKey}.menuItemDauer`).toString(),
      key: 'vorlageDauer',
      sorter: (a, b) => a.dauer - b.dauer,
      render: (record: ZeitplanungsvorlageTableDTO): ReactElement => {
        return <Text>{`${record?.dauer?.toString()} ${i18n.t(`${vorlagenTranslationsKey}.monate`)}`}</Text>;
      },
    },
    {
      title: i18n.t(`${vorlagenTranslationsKey}.menuItemErsteller`).toString(),
      key: 'erstellt',
      sorter: (a, b) => compareDates(a.erstelltAm, b.erstelltAm),
      render: (record: ZeitplanungsvorlageTableDTO): ReactElement => {
        return (
          <TextAndContact
            drawerId={`erstellt-drawer-${record.id}`}
            drawerTitle={i18n.t('zeit.myZeitHome.contactPersonTitle')}
            firstRow={getDateTimeString(record.erstelltAm)}
            user={record.erstelltVon.dto}
          />
        );
      },
    },
    {
      title: i18n.t(`${vorlagenTranslationsKey}.menuItemZuletztBearbeitet`).toString(),
      key: 'bearbeitetam',
      sorter: (a, b) => compareDates(a.bearbeitetAm || a.erstelltAm, b.bearbeitetAm || b.erstelltAm),
      render: (record: ZeitplanungsvorlageTableDTO): ReactElement => {
        const user = record.bearbeitetVonStellvertreter || record.bearbeitetVon || record.erstelltVon;
        return (
          <TextAndContact
            drawerId={`bearbeitet-am-drawer-${record.id}`}
            drawerTitle={i18n.t('zeit.myZeitHome.contactPersonTitle')}
            firstRow={getDateTimeString(record.bearbeitetAm || record.erstelltAm)}
            user={user.dto}
            isStellvertretung={!!record.bearbeitetVonStellvertreter}
          />
        );
      },
    },
    {
      title: (
        <>
          {i18n.t(`${vorlagenTranslationsKey}.zugriffsrecht`)}
          <InfoComponent title={i18n.t(`${vorlagenTranslationsKey}.zugriffsrechtInfo.title`)}>
            <div
              dangerouslySetInnerHTML={{
                __html: i18n.t(`${vorlagenTranslationsKey}.zugriffsrechtInfo.drawerText`),
              }}
            ></div>
          </InfoComponent>
        </>
      ),
      key: 'c6',
      render: (record: ZeitplanungsvorlageTableDTO): ReactElement => {
        return <Text>{i18n.t(`zeit.myZeitHome.tabs.zugriffsrechte.${record.rolleTyp}`).toString()}</Text>;
      },
    },
    {
      title: i18n.t(`${vorlagenTranslationsKey}.menuItemAktionen`).toString(),
      key: 'c7',
      render: (record: ZeitplanungsvorlageTableDTO): ReactElement => {
        const items: DropdownMenuItem[] = [
          {
            element: i18n.t(`${vorlagenTranslationsKey}.actions.freigeben`),
            onClick: () => {
              setIsFreigebenModalVisible(true);
              setSelectedZeitplanungsvorlage(record);
            },
            disabled: () => !record.aktionen?.includes(AktionType.BerechtigungenLesen),
          },
          {
            element: i18n.t(`${vorlagenTranslationsKey}.actions.vorlageSpeichern`),
            disabled: () => !record.aktionen?.includes(AktionType.NeueVersionErstellen),
            onClick: () => {
              setCopyZeitplanungsvorlageModalIsVisible(true);
              setSelectedZeitplanungsvorlage(record);
            },
          },
          {
            element: i18n.t(`${vorlagenTranslationsKey}.actions.archivieren`),
            disabled: () => !record.aktionen?.includes(AktionType.Archivieren),
            onClick: () => {
              setIsArchiveModalVisible(true);
              setSelectedZeitplanungsvorlage(record);
            },
          },
        ];
        return (
          <DropdownMenu
            openLink={`/zeit/${routes.ZEITPLANUNGSVORLAGEN}/${record.id}/${routes.TABELLENSICHT}`}
            items={items}
            elementId={record.id}
          />
        );
      },
    },
  ];

  return {
    id: 'period-zeitplanungsvorlagen-table',
    expandable: true,
    expandedRowRender: (record) => getBeschreibungRenderer(record, routes.ZEITPLANUNGSVORLAGEN),
    expandableCondition: (record: ZeitplanungsvorlageTableDTO) => {
      if (record.beschreibung) {
        return true;
      }
      return false;
    },
    columns,
    content,
    filteredColumns: [
      { name: 'vorhabenart', columnIndex: 0 },
      { name: 'erstelltVon', columnIndex: 4 },
    ],
    className: 'zeitplanungsvorlagen-table',
    customDefaultSortIndex: 4,
    sorterOptions: [
      {
        columnKey: 'vorlageDauer',
        titleAsc: i18n.t(`${vorlagenTranslationsKey}.sorters.dauerAsc`),
        titleDesc: i18n.t(`${vorlagenTranslationsKey}.sorters.dauerDesc`),
      },
      {
        columnKey: 'bearbeitetam',
        titleAsc: i18n.t(`${vorlagenTranslationsKey}.sorters.bearbeitetAsc`),
        titleDesc: i18n.t(`${vorlagenTranslationsKey}.sorters.bearbeitetDesc`),
      },
      {
        columnKey: 'erstellt',
        titleAsc: i18n.t(`${vorlagenTranslationsKey}.sorters.erstelltAsc`),
        titleDesc: i18n.t(`${vorlagenTranslationsKey}.sorters.erstelltDesc`),
      },
    ],
  };
}
