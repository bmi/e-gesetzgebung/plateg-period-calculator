// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { setHours } from 'date-fns';
import { createHashHistory } from 'history';
import { Observable } from 'rxjs';
import { AjaxError } from 'rxjs/ajax';

import { ZeitplanungControllerApi } from '@plateg/rest-api';
import { ZeitplanungEntityResponseShortDTO, ZeitplanungEntityShortDTO } from '@plateg/rest-api/models';
import { ErrorController, LoadingStatusController } from '@plateg/theme';
import { GlobalDI } from '@plateg/theme/src/shares';

import { routes } from '../../../../shares/routes';
import { ZeitplanungFormVal } from './component.react';

export class ZeitplanungModalController {
  private readonly loadingStatusController = GlobalDI.getOrRegister(
    'loadingStatusController',
    () => new LoadingStatusController(),
  );
  private readonly errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  private readonly history = createHashHistory();
  private createZeitplanungCall(zeitplanung: ZeitplanungEntityShortDTO): Observable<ZeitplanungEntityResponseShortDTO> {
    const zeitplanungController = GlobalDI.get<ZeitplanungControllerApi>('zeitplanungRestController');
    return zeitplanungController.createZeitplanung({ zeitplanungEntityShortDTO: zeitplanung });
  }

  private readonly errorCallback = (error: AjaxError): void => {
    this.loadingStatusController.setLoadingStatus(false);
    console.error(error, error);
    this.errorCtrl.displayErrorMsg(error, 'zeit.generalErrorMsg');
  };

  private readonly handleSave = (val: ZeitplanungEntityResponseShortDTO, resetTab?: (key: string) => void): void => {
    resetTab?.(routes.MEINE_ZEITPLANUNGEN);
    if (!location.pathname.endsWith('tabellensicht')) {
      this.history.push(`/zeit/${routes.MEINE_ZEITPLANUNGEN}/${val.base.id}/tabellensicht`);
    }
  };

  public saveZeitplanung = (
    setIsVisible: (isVisible: boolean) => void,
    resetTab?: (key: string) => void,
    newZeitplanung?: ZeitplanungFormVal,
  ): void => {
    if (newZeitplanung) {
      newZeitplanung.startdatum = setHours(newZeitplanung.startdatumDate, 12) as unknown as string;
      setIsVisible(false);
      this.loadingStatusController.setLoadingStatus(true);
      this.createZeitplanungCall(newZeitplanung).subscribe({
        next: (val) => this.handleSave(val, resetTab),
        error: this.errorCallback,
      });
    }
  };
}
