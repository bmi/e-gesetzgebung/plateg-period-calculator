// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../../../general-zeitelement/review-component.less';

import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';

import {
  RegelungsvorhabenBundesregierungTableDTO,
  RegelungsvorhabenTypType,
  ZeitplanungsvorlageEntityListResponseShortDTO,
} from '@plateg/rest-api';
import { HinweisComponent } from '@plateg/theme';
import { Filters } from '@plateg/theme/src/shares/filters';

import { ZeitplanungFormVal } from '../../component.react';

export interface Page3ReviewComponentProps {
  zeitplanung?: ZeitplanungFormVal;
  regelungsvorhaben?: RegelungsvorhabenBundesregierungTableDTO[];
  zeitplanungsvorlagen?: ZeitplanungsvorlageEntityListResponseShortDTO;
  isLight?: boolean;
}

export function Page3ReviewComponent(props: Page3ReviewComponentProps): React.ReactElement {
  const { t } = useTranslation();

  const zeitplanungsvorlage = props.zeitplanungsvorlagen?.dtos.find((vorlage) => {
    return vorlage.base.id === props.zeitplanung?.sourceId;
  });

  return (
    <div className="review-section" tabIndex={0}>
      <Title level={2}>{t('zeit.newZeitplanung.modal.page3.title')}</Title>
      <dl>
        {!props.isLight && (
          <div className="review-row">
            <dt>{t('zeit.newZeitplanung.modal.page3.definitionTitles.regelungsvorhaben')}</dt>
            <dd>
              {
                props.regelungsvorhaben?.find((rv) => {
                  return rv.id === props.zeitplanung?.regelungsvorhabenId;
                })?.kurzbezeichnung
              }
            </dd>
          </div>
        )}
        <div className="review-row">
          <dt>{t('zeit.newZeitplanung.modal.page3.definitionTitles.verwendeteZeitplanungsvorlage')}</dt>
          <dd>{zeitplanungsvorlage?.dto.titel}</dd>
        </div>
        <div className="review-row">
          <dt>{t('zeit.newZeitplanung.modal.page3.definitionTitles.titelDerZeitplanung')}</dt>
          <dd>{props.zeitplanung?.titel}</dd>
        </div>
        <div className="review-row">
          <dt>{t('zeit.newZeitplanung.modal.page3.definitionTitles.beschreibung')}</dt>
          <dd>{props.zeitplanung?.beschreibung}</dd>
        </div>
        <div className="review-row">
          <dt>{t('zeit.newZeitplanung.modal.page3.definitionTitles.datum')}</dt>
          <dd>{Filters.dateFromString(props.zeitplanung?.startdatumDate.toString())}</dd>
        </div>
      </dl>
      {zeitplanungsvorlage && (
        <HinweisComponent
          title={t('zeit.newZeitplanung.modal.page3.hinweis.title')}
          content={
            <p>
              {t(
                `zeit.newZeitplanung.modal.page3.hinweis.${
                  zeitplanungsvorlage.dto.vorhabenart === RegelungsvorhabenTypType.Gesetz
                    ? 'contentGesetze'
                    : 'contentRvVv'
                }`,
              )}
            </p>
          }
        />
      )}
    </div>
  );
}
