// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../../../general-zeitelement/new-zeitplanungsvorlage.less';

import { Form } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import {
  BASE_PATH,
  RegelungsvorhabenBundesregierungTableDTO,
  ZeitplanungsvorlageEntityListResponseShortDTO,
} from '@plateg/rest-api';
import { CloseOutlined, FormItemWithInfo, HiddenInfoComponent, SelectWrapper } from '@plateg/theme';

import { ZeitFormComponent } from '../../../general-zeitelement/zeit-form-component/component.react';
import { zplVorlagenOptionsOrder } from '../../../new-zeitplanungsvorlage-modal/pages/page-1-vorlage/component.react';

export interface Page1RvVorlageComponentProps {
  name: string;
  selectedZeitplanungsVorlageId?: string;
  setSelectedZeitplanungsVorlageId: (selectedZeitplanungsVorlageId: string) => void;
  activePageIndex: number;
  setActivePageIndex: (activePageIndex: number) => void;
  zeitplanungsvorlagen?: ZeitplanungsvorlageEntityListResponseShortDTO;
  regelungsvorhaben?: RegelungsvorhabenBundesregierungTableDTO[];
  selectedRegelungsvorhabenId?: string;
  setSelectedRegelungsvorhabenId: (selectedRegelungsvorhabenId: string | undefined) => void;
  isLight?: boolean;
}

export function Page1RvVorlageComponent(props: Page1RvVorlageComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const [zeitplanungsvorlagen, setZeitplanungsvorlagen] = useState<
    ZeitplanungsvorlageEntityListResponseShortDTO | undefined
  >();
  const [form] = Form.useForm();

  useEffect(() => {
    setZeitplanungsvorlagenList(
      props.regelungsvorhaben?.find((rv) => {
        return rv.id === props.selectedRegelungsvorhabenId;
      })?.id,
    );
    if (props.selectedRegelungsvorhabenId) {
      form.setFieldValue('regelungsvorhabenId', props.selectedRegelungsvorhabenId);
    }
  }, [null, props.zeitplanungsvorlagen]);
  const setZeitplanungsvorlagenList = (id?: string) => {
    setZeitplanungsvorlagen({
      dtos: props.isLight
        ? props.zeitplanungsvorlagen?.dtos || []
        : props.zeitplanungsvorlagen?.dtos.filter((zeitplanungsvorlage) => {
            return (
              zeitplanungsvorlage.dto.vorhabenart ===
              props.regelungsvorhaben?.find((rv) => {
                return rv.id === id;
              })?.vorhabenart
            );
          }) || [],
    });
  };

  return (
    <ZeitFormComponent
      form={form}
      name={props.name}
      title={t('zeit.newZeitplanung.modal.page1.title')}
      activePageIndex={props.activePageIndex}
      setActivePageIndex={props.setActivePageIndex}
      values={{
        sourceId: props.selectedZeitplanungsVorlageId,
        regelungsvorhabenId: props.selectedRegelungsvorhabenId,
      }}
      setValues={(value: { sourceId: string; regelungsvorhabenId: string }) => {
        props.setSelectedZeitplanungsVorlageId(value.sourceId);
        props.setSelectedRegelungsvorhabenId(value.regelungsvorhabenId);
      }}
    >
      <>
        {!props.isLight && (
          <FormItemWithInfo
            customName={`${props.name}_regelungsvorhabenId`}
            label={
              <span>
                {t('zeit.newZeitplanung.modal.page1.item1.label')}
                <HiddenInfoComponent
                  title={t('zeit.newZeitplanung.modal.page1.item1.info.title')}
                  text={t('zeit.newZeitplanung.modal.page1.item1.info.text')}
                />
              </span>
            }
            rules={[
              {
                required: true,
                message: t('zeit.newZeitplanung.modal.page1.item1.requiredMsg'),
              },
            ]}
            name="regelungsvorhabenId"
          >
            <SelectWrapper
              id={`${props.name}_regelungsvorhabenId`}
              allowClear={{ clearIcon: <CloseOutlined /> }}
              placeholder={t('zeit.newZeitplanung.modal.page1.item1.placeholder')}
              onChange={(value) => setZeitplanungsvorlagenList(value as string)}
              options={props.regelungsvorhaben?.map((element, index) => {
                const label = `${element.abkuerzung || ''} - ${element?.kurzbezeichnung || ''}`;
                return {
                  label: (
                    <span key={`${label}-${index}`} aria-label={element?.kurzbezeichnung}>
                      {label}
                    </span>
                  ),
                  value: element.id,
                  title: label,
                };
              })}
            />
          </FormItemWithInfo>
        )}
        <FormItemWithInfo
          customName={`${props.name}_sourceId`}
          label={
            <>
              {t('zeit.newZeitplanung.modal.page1.item2.label')}
              <HiddenInfoComponent
                title={t('zeit.newZeitplanung.modal.page1.item2.info.title')}
                text={
                  <span
                    dangerouslySetInnerHTML={{
                      __html: t('zeit.newZeitplanung.modal.page1.item2.info.text', {
                        linkGGO: `${BASE_PATH}/arbeitshilfen/download/34`,
                      }),
                    }}
                  />
                }
              />
            </>
          }
          rules={
            props.isLight
              ? [
                  {
                    required: true,
                    message: t('zeit.newZeitplanung.modal.page1.item2.requiredMsg'),
                  },
                ]
              : []
          }
          name="sourceId"
        >
          <SelectWrapper
            id={`${props.name}_sourceId`}
            allowClear={{ clearIcon: <CloseOutlined /> }}
            placeholder={t('zeit.newZeitplanung.modal.page1.item2.placeholder')}
            options={zeitplanungsvorlagen?.dtos
              .sort(
                (a, b) => zplVorlagenOptionsOrder.indexOf(a.dto.titel) - zplVorlagenOptionsOrder.indexOf(b.dto.titel),
              )
              .map((element) => {
                const title = element.dto.titel;
                return {
                  label: (
                    <span aria-label={title} key={`zeitplanungsvorlagen-option-${title}-${element.base.id}`}>
                      {element.dto.titel}
                    </span>
                  ),
                  value: element.base.id,
                  title,
                };
              })}
          />
        </FormItemWithInfo>
      </>
    </ZeitFormComponent>
  );
}
