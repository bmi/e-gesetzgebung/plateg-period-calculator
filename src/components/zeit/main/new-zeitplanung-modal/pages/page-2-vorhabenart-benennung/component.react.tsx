// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../../../general-zeitelement/new-zeitplanungsvorlage.less';

import { FormInstance, Input } from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { Constants, CustomDatepicker, FormItemWithInfo, GlobalDI, HiddenInfoComponent } from '@plateg/theme';
import { Filters } from '@plateg/theme/src/shares/filters';

import { PeriodController } from '../../../general-zeitelement/new-time-element/new-element/sections/period/controller';
import {
  BasicFormPageComponent,
  BasicGeneralFormPageComponentProps,
} from '../../../general-zeitelement/pages/basic-form-page/component.react';

export function Page2BenennungComponent(props: BasicGeneralFormPageComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const periodController = GlobalDI.getOrRegister('periodController', () => new PeriodController());

  const [formInstance, setFormInstance] = useState<FormInstance>();
  useEffect(() => {
    if (props.data?.titel) {
      formInstance?.setFieldValue('titel', props.data?.titel);
    }
  }, [props.data?.titel]);
  const errorTextDateFormat = t(`zeit.newElementGeneral.errorDateFormat`);
  return (
    <BasicFormPageComponent formProps={props} setFormInstance={setFormInstance}>
      <>
        <FormItemWithInfo
          customName={`${props.name}_titel`}
          label={
            <span>
              {t('zeit.newZeitplanung.modal.page2.item1.label')}
              <span style={{ position: 'absolute', marginLeft: '10px' }}>
                <HiddenInfoComponent
                  title={t('zeit.newZeitplanung.modal.page2.item1.info.title')}
                  text={t('zeit.newZeitplanung.modal.page2.item1.info.text')}
                />
              </span>
            </span>
          }
          name="titel"
          rules={[
            {
              required: true,
              message: t('zeit.newZeitplanung.modal.page2.item1.requiredMsg'),
            },
            {
              max: Constants.TEXT_BOX_LENGTH,
              message: t('zeit.newZeitplanung.modal.page2.item1.errorLength', {
                maxChars: Constants.TEXT_BOX_LENGTH,
              }),
            },
          ]}
        >
          <Input type="text" />
        </FormItemWithInfo>
        <FormItemWithInfo
          customName={`${props.name}_beschreibung`}
          label={
            <span>
              {t('zeit.newZeitplanung.modal.page2.item2.label')}
              <span style={{ position: 'absolute', marginLeft: '10px' }}>
                <HiddenInfoComponent
                  title={t('zeit.newZeitplanung.modal.page2.item2.info.title')}
                  text={t('zeit.newZeitplanung.modal.page2.item2.info.text')}
                />
              </span>
            </span>
          }
          name="beschreibung"
          rules={[
            {
              max: Constants.TEXT_AREA_LENGTH,
              message: t('zeit.newZeitplanung.modal.page2.item2.errorLength', {
                maxChars: Filters.numberToDecimalString(Constants.TEXT_AREA_LENGTH),
              }),
            },
          ]}
        >
          <TextArea autoSize={{ minRows: 4, maxRows: 4 }} />
        </FormItemWithInfo>

        {formInstance && (
          <CustomDatepicker
            disabledDate={periodController.disabledDate}
            setDate={(date) => {
              props.setData({ ...props.data, startdatumDate: date });
              formInstance?.setFields([
                { name: `startdatumDate`, value: date },
                { name: 'startdatumDate-date-placeholder', value: '' },
              ]);
            }}
            aliasDate={'startdatumDate'}
            aliasTime={''}
            form={formInstance}
            required={true}
            timePlaceholderName={'timePlaceholderName'}
            additionalValidators={[]}
            errorRequired={t(`zeit.newZeitplanung.modal.page2.item3.requiredMsg`)}
            errorDateTime={''}
            errorInvalidFormat={errorTextDateFormat}
            errorDisabledDate={''}
            customLabel={
              <span>
                {t('zeit.newZeitplanung.modal.page2.item3.label')}
                <span style={{ position: 'absolute', marginLeft: '10px' }}>
                  <HiddenInfoComponent
                    title={t('zeit.newZeitplanung.modal.page2.item3.info.title')}
                    text={t('zeit.newZeitplanung.modal.page2.item3.info.text')}
                  />
                </span>
              </span>
            }
          />
        )}
      </>
    </BasicFormPageComponent>
  );
}
