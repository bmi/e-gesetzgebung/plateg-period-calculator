// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import chai from 'chai';
import chaiString from 'chai-string';
import { Observable } from 'rxjs';
import sinon from 'sinon';

import { Configuration, ZeitplanungControllerApi, ZeitplanungEntityResponseShortDTO } from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme/src/shares';

import { displayErrorMsgStub, setLoadingStatusStub } from '../../../../general.test';
import { ZeitplanungFormVal } from './component.react';
import { ZeitplanungModalController } from './controller';

chai.use(chaiString);

const zeitplanungsController = GlobalDI.getOrRegister('zeitplanungsController', () => new ZeitplanungModalController());
const zeitplanungRestController = GlobalDI.getOrRegister(
  'zeitplanungRestController',
  () => new ZeitplanungControllerApi(new Configuration()),
);

const zeitplanung: ZeitplanungFormVal = {
  titel: 'Zeitplanungsvorlage XYZ',
  beschreibung: 'Beschreibung XYZ',
  regelungsvorhabenId: '123',
  startdatum: '2021-04-21T16:05:30.958156',
  startdatumDate: new Date(),
};

const responseDto: ZeitplanungEntityResponseShortDTO = {
  base: { id: '1', erstelltAm: '', bearbeitetAm: '' },
  dto: zeitplanung,
};

describe('TEST: saveZeitplanung', () => {
  let createZeitplanungCallStub: sinon.SinonStub;
  const setIsVisible = sinon.stub();
  const resetTab = sinon.stub();
  before(() => {
    createZeitplanungCallStub = sinon.stub(zeitplanungRestController, 'createZeitplanung').returns(
      new Observable(function (observer) {
        observer.next(responseDto);
        observer.complete();
      }),
    );
  });
  after(() => {
    createZeitplanungCallStub.restore();
  });

  afterEach(() => {
    createZeitplanungCallStub.resetHistory();
    setLoadingStatusStub.resetHistory();
    displayErrorMsgStub.resetHistory();
    setIsVisible.resetHistory();
    resetTab.resetHistory();
  });

  it('check if functions are called', () => {
    zeitplanungsController.saveZeitplanung(setIsVisible, resetTab, zeitplanung);
    sinon.assert.calledOnce(createZeitplanungCallStub);
    sinon.assert.calledOnce(setIsVisible);
    sinon.assert.calledOnce(setLoadingStatusStub);
    sinon.assert.calledOnce(resetTab);
  });

  it('check if nothing is called, if newZeitplanungsVorlage is undefined', () => {
    zeitplanungsController.saveZeitplanung(setIsVisible, resetTab, undefined);
    sinon.assert.notCalled(createZeitplanungCallStub);
    sinon.assert.notCalled(setIsVisible);
    sinon.assert.notCalled(setLoadingStatusStub);
    sinon.assert.notCalled(resetTab);
  });
});
