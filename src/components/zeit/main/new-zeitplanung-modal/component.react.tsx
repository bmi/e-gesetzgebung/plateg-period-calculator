// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../general-zeitelement/new-zeitplanungsvorlage.less';

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { forkJoin, map, of } from 'rxjs';

import {
  RegelungsvorhabenControllerApi,
  RegelungsvorhabenEntityListResponseShortDTO,
  RegelungsvorhabenEntityResponseShortDTO,
  RegelungsvorhabenTableDTO,
  RegelungsvorhabenTypType,
  ZeitplanungEntityShortDTO,
  ZeitplanungsvorlageEntityListResponseShortDTO,
} from '@plateg/rest-api';
import { LoadingStatusController, MultiPageModalComponent } from '@plateg/theme';
import { GlobalDI } from '@plateg/theme/src/shares';

import { ZeitLightCollectionsKeys, ZeitLightLocalController } from '../../../zeit-light/controller-local';
import { ZeitplanungController } from '../home-tabs-zeit/controller';
import { NewZeitplanungsvorlageModalProps } from '../new-zeitplanungsvorlage-modal/component.react';
import { ZeitplanungsvorlagenModalController } from '../new-zeitplanungsvorlage-modal/controller';
import { ZeitplanungModalController } from './controller';
import { Page1RvVorlageComponent } from './pages/page-1-rv-vorlage/component.react';
import { Page2BenennungComponent } from './pages/page-2-vorhabenart-benennung/component.react';
import { Page3ReviewComponent } from './pages/page-3-review/component.react';

export interface ZeitplanungFormVal extends ZeitplanungEntityShortDTO {
  startdatumDate: Date;
  vorhabenart?: RegelungsvorhabenTypType;
}
export function NewZeitplanungModal(props: NewZeitplanungsvorlageModalProps): React.ReactElement {
  const { t } = useTranslation();
  const zeitplanungsvorlagenController = GlobalDI.getOrRegister(
    'zeitplanungsvorlagenController',
    () => new ZeitplanungsvorlagenModalController(),
  );
  const zeitLightLocalController = GlobalDI.getOrRegister(
    'zeitLightLocalController',
    () => new ZeitLightLocalController(),
  );
  const zeitplanungsController = GlobalDI.getOrRegister(
    'zeitplanungsController',
    () => new ZeitplanungModalController(),
  );
  const emptyZeitplanung: ZeitplanungFormVal = {
    regelungsvorhabenId: '',
    titel: '',
    startdatum: '',
    startdatumDate: new Date(),
  };
  const zeitplanungController = GlobalDI.getOrRegister('zeitplanungController', () => new ZeitplanungController());
  const regelungsvorhabenController = GlobalDI.get<RegelungsvorhabenControllerApi>('regelungsvorhabenController');
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const [activePageIndex, setActivePageIndex] = useState<number>(0);
  const [zeitplanungsvorlagen, setZeitplanungsvorlagen] = useState<ZeitplanungsvorlageEntityListResponseShortDTO>();
  const [regelungsvorhaben, setRegelungsvorhaben] = useState<RegelungsvorhabenTableDTO[]>();
  const [selectedRegelungsvorhabenId, setSelectedRegelungsvorhabenId] = useState<string | undefined>();
  const [selectedZeitplanungsVorlageId, setSelectedZeitplanungsVorlageId] = useState<string>();
  const [newZeitplanung, setNewZeitplanung] = useState<ZeitplanungFormVal>(emptyZeitplanung);

  useEffect(() => {
    if (props.isVisible) {
      loadContent();
    } else {
      setActivePageIndex(0);
      setSelectedZeitplanungsVorlageId(undefined);
      setNewZeitplanung(emptyZeitplanung);
      setSelectedRegelungsvorhabenId(undefined);
    }
  }, [props.isVisible]);

  useEffect(() => {
    const selectedVorlage = zeitplanungsvorlagen?.dtos.find((zeitplanungsvorlage) => {
      return zeitplanungsvorlage.base.id === selectedZeitplanungsVorlageId;
    });

    if (selectedRegelungsvorhabenId || props.isLight) {
      setNewZeitplanung({
        ...newZeitplanung,
        ...selectedVorlage?.dto,
        regelungsvorhabenId: selectedRegelungsvorhabenId || '',
        sourceId: selectedZeitplanungsVorlageId || undefined,
      });
    }
  }, [selectedZeitplanungsVorlageId, selectedRegelungsvorhabenId]);

  const loadContent = () => {
    loadingStatusController.setLoadingStatus(true);
    const sub = forkJoin({
      regelungsvorhaben: props.isLight
        ? of({ dtos: [] } as RegelungsvorhabenEntityListResponseShortDTO)
        : regelungsvorhabenController.getRegelungsvorhabenShortList(),
      zeitplanungsvorlagen: props.isLight
        ? forkJoin([
            zeitLightLocalController.getDraftListObservable(ZeitLightCollectionsKeys.Systemvorlagen),
            zeitLightLocalController.getDraftListObservable(ZeitLightCollectionsKeys.Zeitplanungsvorlagen),
          ]).pipe(
            map((data) => {
              return {
                dtos: [...data[0].dtos, ...data[1].dtos],
              };
            }),
          )
        : zeitplanungController.getZeitplanungsvorlagenListCall(),
    }).subscribe({
      next: (data) => {
        const sortedRegelungsvorhaben = [...data.regelungsvorhaben.dtos].sort(
          (o1, o2) => new Date(o2.dto.bearbeitetAm) - new Date(o1.dto.bearbeitetAm),
        );
        setRegelungsvorhaben(
          sortedRegelungsvorhaben.map((item: RegelungsvorhabenEntityResponseShortDTO) => {
            return { ...item.dto, ...item.base };
          }) as RegelungsvorhabenTableDTO[],
        );
        if (props.rvId) {
          const foundRv = sortedRegelungsvorhaben.find((item) => item.base.id === props.rvId);
          if (foundRv) {
            setSelectedRegelungsvorhabenId(props.rvId);
          }
        }
        setZeitplanungsvorlagen(data.zeitplanungsvorlagen);
        loadingStatusController.setLoadingStatus(false);
      },
      error: zeitplanungsvorlagenController.errorCallback,
    });
    return () => sub.unsubscribe();
  };

  return (
    <MultiPageModalComponent
      key={`new-zeitplanung-${props.isVisible.toString()}`}
      className="new-zeitplanung"
      isVisible={props.isVisible}
      setIsVisible={props.setIsVisible}
      activePageIndex={activePageIndex}
      setActivePageIndex={setActivePageIndex}
      title={t('zeit.newZeitplanung.modal.title')}
      cancelBtnText={t('zeit.newZeitplanungsvorlage.modal.cancelBtnText')}
      nextBtnText={t('zeit.newZeitplanungsvorlage.modal.nextBtnText')}
      prevBtnText={t('zeit.newZeitplanungsvorlage.modal.prevBtnText')}
      componentReferenceContext="newZeitplanungsvorlage"
      pages={[
        {
          formName: 'page1',
          content: (
            <Page1RvVorlageComponent
              name="page1"
              activePageIndex={activePageIndex}
              setActivePageIndex={setActivePageIndex}
              setSelectedZeitplanungsVorlageId={setSelectedZeitplanungsVorlageId}
              selectedZeitplanungsVorlageId={selectedZeitplanungsVorlageId}
              zeitplanungsvorlagen={zeitplanungsvorlagen}
              regelungsvorhaben={regelungsvorhaben}
              selectedRegelungsvorhabenId={selectedRegelungsvorhabenId}
              setSelectedRegelungsvorhabenId={setSelectedRegelungsvorhabenId}
              isLight={props.isLight}
            />
          ),
        },
        {
          formName: 'page2',
          content: (
            <Page2BenennungComponent
              name="page2"
              activePageIndex={activePageIndex}
              setActivePageIndex={setActivePageIndex}
              setData={setNewZeitplanung}
              data={newZeitplanung}
              zeitType="newZeitplanung"
            />
          ),
          primaryInsteadNextBtn: {
            buttonText: t('zeit.newZeitplanung.modal.page2.nextBtn'),
          },
        },
        {
          content: (
            <Page3ReviewComponent
              zeitplanung={newZeitplanung}
              regelungsvorhaben={regelungsvorhaben}
              zeitplanungsvorlagen={zeitplanungsvorlagen}
              isLight={props.isLight}
            />
          ),
          primaryInsteadNextBtn: {
            buttonText: t('zeit.newZeitplanung.modal.page3.nextBtn'),
          },
          nextOnClick: () => {
            const saveMethod = props.isLight
              ? zeitLightLocalController.createNewZeitplanungLight
              : zeitplanungsController.saveZeitplanung;
            saveMethod(props.setIsVisible, props.resetTab, newZeitplanung);
          },
        },
      ]}
    />
  );
}
