// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { createHashHistory } from 'history';
import i18n from 'i18next';
import { Observable } from 'rxjs';
import { AjaxError } from 'rxjs/ajax';

import { ZeitplanungControllerApi, ZeitplanungsvorlageControllerApi } from '@plateg/rest-api';
import { ZeitplanungsvorlageEntityResponseShortDTO, ZeitplanungsvorlageEntityShortDTO } from '@plateg/rest-api/models';
import { ErrorController, LoadingStatusController } from '@plateg/theme';
import { GlobalDI } from '@plateg/theme/src/shares';

import { routes } from '../../../../shares/routes';

export class ZeitplanungsvorlagenModalController {
  private readonly loadingStatusController = GlobalDI.getOrRegister(
    'loadingStatusController',
    () => new LoadingStatusController(),
  );
  private readonly errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  private readonly zeitplanungRestController = GlobalDI.get<ZeitplanungControllerApi>('zeitplanungRestController');
  private readonly history = createHashHistory();
  public getSuccessMsg(zeitplanungsvorlageResponse: ZeitplanungsvorlageEntityResponseShortDTO, copy = false): string {
    return copy
      ? `Sie haben die Kopie als neue Zeitplanungsvorlage „${zeitplanungsvorlageResponse.dto.titel}“ angelegt.`
      : `Sie haben die neue Zeitplanungsvorlage „${zeitplanungsvorlageResponse.dto.titel}“ angelegt.`;
  }

  public createZeitplanungsvorlageCall(
    zeitplanungsvorlage: ZeitplanungsvorlageEntityShortDTO,
  ): Observable<ZeitplanungsvorlageEntityResponseShortDTO> {
    const zeitplanunsvorlageRestController = GlobalDI.get<ZeitplanungsvorlageControllerApi>(
      'zeitplanunsvorlageRestController',
    );
    return zeitplanunsvorlageRestController.createZeitplanungsvorlage({
      zeitplanungsvorlageEntityShortDTO: zeitplanungsvorlage,
    });
  }

  public saveAlsZeitplanungsvorlageCall(
    zeitplanungsvorlage: ZeitplanungsvorlageEntityShortDTO,
  ): Observable<ZeitplanungsvorlageEntityResponseShortDTO> {
    return this.zeitplanungRestController.createZeitplanungsvorlage1({
      zeitplanungsvorlageEntityShortDTO: zeitplanungsvorlage,
    });
  }

  public errorCallback = (error: AjaxError): void => {
    this.loadingStatusController.setLoadingStatus(false);
    console.error(error, error);
    this.errorCtrl.displayErrorMsg(error, 'zeit.generalErrorMsg');
  };

  public handleSave = (val: ZeitplanungsvorlageEntityResponseShortDTO, copy?: boolean): void => {
    this.history.push(`/zeit/${routes.ZEITPLANUNGSVORLAGEN}/${val.base.id}/${routes.TABELLENSICHT}`);
  };

  public saveZeitplanungsvorlage = (
    setIsVisible: (isVisible: boolean) => void,
    copy: boolean,
    newZeitplanungsVorlage?: ZeitplanungsvorlageEntityShortDTO,
  ): void => {
    if (newZeitplanungsVorlage) {
      setIsVisible(false);
      this.loadingStatusController.setLoadingStatus(true);
      this.createZeitplanungsvorlageCall(newZeitplanungsVorlage).subscribe({
        next: (val) => this.handleSave(val, copy),
        error: this.errorCallback,
      });
    }
  };

  public saveAlsZeitplanungsvorlage = (
    setIsVisible: (isVisible: boolean) => void,
    newZeitplanungsVorlage?: ZeitplanungsvorlageEntityShortDTO,
  ): void => {
    if (newZeitplanungsVorlage) {
      setIsVisible(false);
      this.loadingStatusController.setLoadingStatus(true);
      this.saveAlsZeitplanungsvorlageCall(newZeitplanungsVorlage).subscribe({
        next: (val) => this.handleSave(val),
        error: this.errorCallback,
      });
    }
  };
}
