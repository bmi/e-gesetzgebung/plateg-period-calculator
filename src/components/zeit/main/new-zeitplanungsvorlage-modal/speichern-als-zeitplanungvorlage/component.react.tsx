// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../../general-zeitelement/new-zeitplanungsvorlage.less';

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { ZeitplanungEntityDTO, ZeitplanungsvorlageEntityShortDTO, ZeitplanungTableDTO } from '@plateg/rest-api';
import { MultiPageModalComponent } from '@plateg/theme';
import { GlobalDI } from '@plateg/theme/src/shares';

import { NewZeitplanungsvorlageModalProps } from '../component.react';
import { ZeitplanungsvorlagenModalController } from '../controller';
import { Page2VorhabenartComponent } from '../pages/page-2-vorhabenart/component.react';
import { Page3BenennungComponent } from '../pages/page-3-vorhabenart-benennung/component.react';
import { Page4ReviewComponent } from '../pages/page-4-review/component.react';

export interface SpeichernAlsZeitplanungsvorlageComponentProps extends NewZeitplanungsvorlageModalProps {
  selectedZeitplanung?: ZeitplanungTableDTO | ZeitplanungEntityDTO;
  selectedId?: string;
}

export function SpeichernAlsZeitplanungsvorlageComponent(
  props: SpeichernAlsZeitplanungsvorlageComponentProps,
): React.ReactElement {
  const { t } = useTranslation();
  const zeitplanungsvorlagenController = GlobalDI.getOrRegister(
    'zeitplanungsvorlagenController',
    () => new ZeitplanungsvorlagenModalController(),
  );
  const [activePageIndex, setActivePageIndex] = useState<number>(0);
  const [newZeitplanungsVorlage, setNewZeitplanungsVorlage] = useState<ZeitplanungsvorlageEntityShortDTO>();

  useEffect(() => {
    setActivePageIndex(0);
    setNewZeitplanungsVorlage(undefined);
  }, [props.isVisible]);

  const mergeZeitplanungsVorlagen = (vorlage: ZeitplanungsvorlageEntityShortDTO) => {
    setNewZeitplanungsVorlage({ ...props.selectedZeitplanung, ...vorlage, sourceId: props.selectedId });
  };

  return (
    <>
      {props.selectedZeitplanung && (
        <MultiPageModalComponent
          key={`speichern-zeitplanungsvorlage-${props.isVisible.toString()}`}
          isVisible={props.isVisible}
          setIsVisible={props.setIsVisible}
          activePageIndex={activePageIndex}
          setActivePageIndex={setActivePageIndex}
          title={t('zeit.newZeitplanungsvorlage.modal.speichernAlsZeitplanungsvorlage.title')}
          cancelBtnText={t('zeit.newZeitplanungsvorlage.modal.cancelBtnText')}
          nextBtnText={t('zeit.newZeitplanungsvorlage.modal.nextBtnText')}
          prevBtnText={t('zeit.newZeitplanungsvorlage.modal.prevBtnText')}
          componentReferenceContext="speichernZeitplanVorlage"
          pages={[
            {
              formName: 'speichernAlsZeitplanungsvorlage.page1',
              content: (
                <Page2VorhabenartComponent
                  name="speichernAlsZeitplanungsvorlage.page1"
                  activePageIndex={activePageIndex}
                  setActivePageIndex={setActivePageIndex}
                  setData={setNewZeitplanungsVorlage}
                  data={newZeitplanungsVorlage}
                  zeitType="newZeitplanungsvorlage"
                  vorhabenart={props.selectedZeitplanung.regelungsvorhaben?.dto.vorhabenart}
                />
              ),
            },
            {
              formName: 'speichernAlsZeitplanungsvorlage.page2',
              content: (
                <Page3BenennungComponent
                  name="speichernAlsZeitplanungsvorlage.page2"
                  activePageIndex={activePageIndex}
                  setActivePageIndex={setActivePageIndex}
                  setData={mergeZeitplanungsVorlagen}
                  data={newZeitplanungsVorlage}
                  zeitType="newZeitplanungsvorlage"
                />
              ),
              primaryInsteadNextBtn: {
                buttonText: t('zeit.newZeitplanungsvorlage.modal.speichernAlsZeitplanungsvorlage.page2.nextBtn'),
              },
            },
            {
              content: (
                <Page4ReviewComponent zeitplanungsVorlage={newZeitplanungsVorlage} isZeitplanungsVorlage={true} />
              ),
              primaryInsteadNextBtn: {
                buttonText: t('zeit.newZeitplanungsvorlage.modal.speichernAlsZeitplanungsvorlage.page3.nextBtn'),
              },
              nextOnClick: () =>
                zeitplanungsvorlagenController.saveAlsZeitplanungsvorlage(props.setIsVisible, newZeitplanungsVorlage),
            },
          ]}
        />
      )}
    </>
  );
}
