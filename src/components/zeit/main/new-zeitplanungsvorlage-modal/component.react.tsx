// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../general-zeitelement/new-zeitplanungsvorlage.less';

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { forkJoin, map } from 'rxjs';

import { ZeitplanungsvorlageEntityListResponseShortDTO, ZeitplanungsvorlageEntityShortDTO } from '@plateg/rest-api';
import { LoadingStatusController, MultiPageModalComponent } from '@plateg/theme';
import { GlobalDI } from '@plateg/theme/src/shares';

import { ZeitLightCollectionsKeys, ZeitLightLocalController } from '../../../zeit-light/controller-local';
import { ZeitplanungController } from '../home-tabs-zeit/controller';
import { ZeitplanungsvorlagenModalController } from './controller';
import { Page1VorlageComponent } from './pages/page-1-vorlage/component.react';
import { Page2VorhabenartComponent } from './pages/page-2-vorhabenart/component.react';
import { Page3BenennungComponent } from './pages/page-3-vorhabenart-benennung/component.react';
import { Page4ReviewComponent } from './pages/page-4-review/component.react';

export interface NewZeitplanungsvorlageModalProps {
  isVisible: boolean;
  setIsVisible: (isVisible: boolean) => void;
  resetTab?: (key: string) => void;
  isLight?: boolean;
  rvId?: string;
}

export function NewZeitplanungsvorlageModal(props: NewZeitplanungsvorlageModalProps): React.ReactElement {
  const { t } = useTranslation();
  const zeitplanungsvorlagenController = GlobalDI.getOrRegister(
    'zeitplanungsvorlagenController',
    () => new ZeitplanungsvorlagenModalController(),
  );
  const zeitplanungController = GlobalDI.getOrRegister('zeitplanungController', () => new ZeitplanungController());
  const zeitLightLocalController = GlobalDI.getOrRegister(
    'zeitLightLocalController',
    () => new ZeitLightLocalController(),
  );
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const [activePageIndex, setActivePageIndex] = useState<number>(0);
  const [zeitplanungsvorlagen, setZeitplanungsvorlagen] = useState<ZeitplanungsvorlageEntityListResponseShortDTO>();
  const [selectedZeitplanungsVorlageIndex, setSelectedZeitplanungsVorlageIndex] = useState<number>();
  const [newZeitplanungsVorlage, setNewZeitplanungsVorlage] = useState<ZeitplanungsvorlageEntityShortDTO>();

  useEffect((): void | (() => void) => {
    if (props.isVisible) {
      loadingStatusController.setLoadingStatus(true);
      const callApi = props.isLight
        ? forkJoin([
            zeitLightLocalController.getDraftListObservable(ZeitLightCollectionsKeys.Systemvorlagen),
            zeitLightLocalController.getDraftListObservable(ZeitLightCollectionsKeys.Zeitplanungsvorlagen),
          ]).pipe(
            map((data) => {
              return {
                dtos: [...data[0].dtos, ...data[1].dtos],
              };
            }),
          )
        : zeitplanungController.getZeitplanungsvorlagenListCall();
      const sub = callApi.subscribe({
        next: (values: ZeitplanungsvorlageEntityListResponseShortDTO) => {
          setZeitplanungsvorlagen(values);
          loadingStatusController.setLoadingStatus(false);
        },
        error: zeitplanungsvorlagenController.errorCallback,
      });
      return () => sub.unsubscribe();
    } else {
      setActivePageIndex(0);
      setSelectedZeitplanungsVorlageIndex(undefined);
      setNewZeitplanungsVorlage(undefined);
    }
  }, [props.isVisible]);

  useEffect(() => {
    if (selectedZeitplanungsVorlageIndex !== undefined) {
      const selectedVorlage = zeitplanungsvorlagen?.dtos[selectedZeitplanungsVorlageIndex];
      if (selectedVorlage) {
        setNewZeitplanungsVorlage({ ...selectedVorlage.dto, sourceId: selectedVorlage.base.id });
      }
    } else {
      setNewZeitplanungsVorlage(undefined);
    }
  }, [selectedZeitplanungsVorlageIndex]);

  return (
    <>
      <MultiPageModalComponent
        key={`new-zeitplanungsvorlage-${props.isVisible.toString()}`}
        isVisible={props.isVisible}
        setIsVisible={props.setIsVisible}
        activePageIndex={activePageIndex}
        setActivePageIndex={setActivePageIndex}
        title={t('zeit.newZeitplanungsvorlage.modal.title')}
        cancelBtnText={t('zeit.newZeitplanungsvorlage.modal.cancelBtnText')}
        nextBtnText={t('zeit.newZeitplanungsvorlage.modal.nextBtnText')}
        prevBtnText={t('zeit.newZeitplanungsvorlage.modal.prevBtnText')}
        componentReferenceContext={`newZeitplanungsvorlage-${activePageIndex}`}
        pages={[
          {
            formName: 'page1',
            content: (
              <Page1VorlageComponent
                name="page1"
                activePageIndex={activePageIndex}
                setActivePageIndex={setActivePageIndex}
                setSelectedZeitplanungsVorlageIndex={setSelectedZeitplanungsVorlageIndex}
                selectedZeitplanungsVorlageIndex={selectedZeitplanungsVorlageIndex}
                zeitplanungsvorlagen={zeitplanungsvorlagen}
              />
            ),
          },
          {
            formName: 'page2',
            content: (
              <Page2VorhabenartComponent
                name="page2"
                activePageIndex={activePageIndex}
                setActivePageIndex={setActivePageIndex}
                setData={setNewZeitplanungsVorlage}
                data={newZeitplanungsVorlage}
                zeitType="newZeitplanungsvorlage"
              />
            ),
          },
          {
            formName: 'page3',
            content: (
              <Page3BenennungComponent
                name="page3"
                activePageIndex={activePageIndex}
                setActivePageIndex={setActivePageIndex}
                setData={setNewZeitplanungsVorlage}
                data={newZeitplanungsVorlage}
                zeitType="newZeitplanungsvorlage"
              />
            ),
            primaryInsteadNextBtn: {
              buttonText: t('zeit.newZeitplanungsvorlage.modal.page3.nextBtn'),
            },
          },
          {
            content: (
              <Page4ReviewComponent
                zeitplanungsVorlage={newZeitplanungsVorlage}
                selectedZeitplanungsVorlage={
                  selectedZeitplanungsVorlageIndex !== undefined
                    ? zeitplanungsvorlagen?.dtos[selectedZeitplanungsVorlageIndex].dto
                    : undefined
                }
              />
            ),
            primaryInsteadNextBtn: {
              buttonText: t('zeit.newZeitplanungsvorlage.modal.page4.nextBtn'),
            },
            nextOnClick: () => {
              const saveMethod = props.isLight
                ? zeitLightLocalController.createNewVorlageLight
                : zeitplanungsvorlagenController.saveZeitplanungsvorlage;
              saveMethod(props.setIsVisible, false, newZeitplanungsVorlage);
            },
          },
        ]}
      />
    </>
  );
}
