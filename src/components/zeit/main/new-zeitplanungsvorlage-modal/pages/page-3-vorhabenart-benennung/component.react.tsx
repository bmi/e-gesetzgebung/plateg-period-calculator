// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../../../general-zeitelement/new-zeitplanungsvorlage.less';

import { Input } from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { Constants, FormItemWithInfo, HiddenInfoComponent } from '@plateg/theme';
import { Filters } from '@plateg/theme/src/shares/filters';

import {
  BasicFormPageComponent,
  BasicGeneralFormPageComponentProps,
} from '../../../general-zeitelement/pages/basic-form-page/component.react';

export function Page3BenennungComponent(props: BasicGeneralFormPageComponentProps): React.ReactElement {
  const { t } = useTranslation();

  return (
    <>
      <BasicFormPageComponent formProps={props}>
        <>
          <FormItemWithInfo
            customName={`${props.name}_titel`}
            label={
              <>
                {t('zeit.newZeitplanungsvorlage.modal.page3.item1.label')}
                <span style={{ position: 'absolute', marginLeft: '10px' }}>
                  <HiddenInfoComponent
                    title={t('zeit.newZeitplanungsvorlage.modal.page3.item1.info.title')}
                    text={t('zeit.newZeitplanungsvorlage.modal.page3.item1.info.text')}
                  />
                </span>
              </>
            }
            name="titel"
            rules={[
              {
                required: true,
                message: t('zeit.newZeitplanungsvorlage.modal.page3.item1.requiredMsg'),
              },
              {
                max: Constants.TEXT_BOX_LENGTH,
                message: t('zeit.newZeitplanungsvorlage.modal.page3.item1.errorLength', {
                  maxChars: Constants.TEXT_BOX_LENGTH,
                }),
              },
            ]}
          >
            <Input type="text" />
          </FormItemWithInfo>
          <FormItemWithInfo
            customName={`${props.name}_beschreibung`}
            label={
              <>
                {t('zeit.newZeitplanungsvorlage.modal.page3.item2.label')}
                <span style={{ position: 'absolute', marginLeft: '10px' }}>
                  <HiddenInfoComponent
                    title={t('zeit.newZeitplanungsvorlage.modal.page3.item2.info.title')}
                    text={t('zeit.newZeitplanungsvorlage.modal.page3.item2.info.text')}
                  />
                </span>
              </>
            }
            name="beschreibung"
            rules={[
              {
                max: Constants.TEXT_AREA_LENGTH,
                message: t('zeit.newZeitplanungsvorlage.modal.page3.item2.errorLength', {
                  maxChars: Filters.numberToDecimalString(Constants.TEXT_AREA_LENGTH),
                }),
              },
            ]}
          >
            <TextArea id={`${props.name}_beschreibung`} autoSize={{ minRows: 4, maxRows: 4 }} />
          </FormItemWithInfo>
        </>
      </BasicFormPageComponent>
    </>
  );
}
