// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../../../general-zeitelement/review-component.less';

import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { ZeitplanungsvorlageEntityShortDTO } from '@plateg/rest-api';

export interface Page4ReviewComponentProps {
  zeitplanungsVorlage?: ZeitplanungsvorlageEntityShortDTO;
  selectedZeitplanungsVorlage?: ZeitplanungsvorlageEntityShortDTO;
  isZeitplanungsVorlage?: boolean;
}

export function Page4ReviewComponent(props: Page4ReviewComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const noAnswer = <em>{t(`zeit.newZeitplanungsvorlage.modal.page4.noAnswer`)}</em>;
  const titleKey = !props.isZeitplanungsVorlage ? 'titelDerZeitplanung' : 'titelDerZeitplanungsVorlage';

  return (
    <div className="review-section" tabIndex={0}>
      <Title level={2}>{t('zeit.newZeitplanungsvorlage.modal.page4.title')}</Title>
      <dl>
        <div className="review-row">
          <dt>{t('zeit.newZeitplanungsvorlage.modal.page4.definitionTitles.vorhabentyp')}</dt>
          <dd>
            {props.zeitplanungsVorlage?.vorhabenart
              ? t(`zeit.newZeitplanungsvorlage.modal.page2.item1.options.${props.zeitplanungsVorlage.vorhabenart}`)
              : noAnswer}
          </dd>
        </div>

        {!props.isZeitplanungsVorlage && (
          <div className="review-row zeitplanungsvorlage">
            <dt>{t('zeit.newZeitplanungsvorlage.modal.page4.definitionTitles.verwendeteZeitplanungsvorlage')}</dt>
            <dd>{props.selectedZeitplanungsVorlage?.titel ? props.selectedZeitplanungsVorlage.titel : noAnswer}</dd>
          </div>
        )}

        <div className="review-row">
          <dt>{t(`zeit.newZeitplanungsvorlage.modal.page4.definitionTitles.${titleKey}`)}</dt>
          <dd>{props.zeitplanungsVorlage?.titel ? props.zeitplanungsVorlage.titel : noAnswer}</dd>
        </div>
        <div className="review-row">
          <dt>{t('zeit.newZeitplanungsvorlage.modal.page4.definitionTitles.beschreibung')}</dt>
          <dd>{props?.zeitplanungsVorlage?.beschreibung ? props.zeitplanungsVorlage.beschreibung : noAnswer}</dd>
        </div>
      </dl>
    </div>
  );
}
