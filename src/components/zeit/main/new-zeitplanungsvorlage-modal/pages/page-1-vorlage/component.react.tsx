// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../../../general-zeitelement/new-zeitplanungsvorlage.less';

import { Form } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { ZeitplanungsvorlageEntityListResponseShortDTO } from '@plateg/rest-api';
import { CloseOutlined, FormItemWithInfo, HiddenInfoComponent, SelectWrapper } from '@plateg/theme';

import { ZeitFormComponent } from '../../../general-zeitelement/zeit-form-component/component.react';

export const zplVorlagenOptionsOrder = [
  'Gesetz mit Regelfristen',
  'Gesetz mit verkürzten Fristen',
  'Gesetz mit verlängerten Fristen',
  'Verordnung mit Regelfristen',
  'Verordnung mit verkürzten Fristen',
  'Verordnung mit verlängerten Fristen',
  'Verwaltungsvorschrift mit Regelfristen',
  'Verwaltungsvorschrift mit verkürzten Fristen',
  'Verwaltungsvorschrift mit verlängerten Fristen',
];

export interface Page1VorlageComponentProps {
  name: string;
  selectedZeitplanungsVorlageIndex?: number;
  setSelectedZeitplanungsVorlageIndex: (selectedZeitplanungsVorlageIndex: number) => void;
  activePageIndex: number;
  setActivePageIndex: (activePageIndex: number) => void;
  zeitplanungsvorlagen?: ZeitplanungsvorlageEntityListResponseShortDTO;
}

export function Page1VorlageComponent(props: Page1VorlageComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const [form] = Form.useForm();

  return (
    <>
      <ZeitFormComponent
        form={form}
        name={props.name}
        title={t('zeit.newZeitplanungsvorlage.modal.page1.title')}
        activePageIndex={props.activePageIndex}
        setActivePageIndex={props.setActivePageIndex}
        values={{ zeitplanungsVorlage: props.selectedZeitplanungsVorlageIndex }}
        setValues={(value: { zeitplanungsVorlage: number }) => {
          if (value.zeitplanungsVorlage !== props.selectedZeitplanungsVorlageIndex) {
            props.setSelectedZeitplanungsVorlageIndex(value.zeitplanungsVorlage);
          }
        }}
      >
        <FormItemWithInfo
          customName={`${props.name}_zeitplanungsVorlage`}
          label={
            <>
              {t('zeit.newZeitplanungsvorlage.modal.page1.item1.label')}
              <HiddenInfoComponent
                title={t('zeit.newZeitplanungsvorlage.modal.page1.item1.info.title')}
                text={t('zeit.newZeitplanungsvorlage.modal.page1.item1.info.text')}
              />
            </>
          }
          name="zeitplanungsVorlage"
        >
          <SelectWrapper
            id={`${props.name}_zeitplanungsVorlage`}
            allowClear={{ clearIcon: <CloseOutlined /> }}
            placeholder={t('zeit.newZeitplanungsvorlage.modal.page1.item1.placeholder')}
            options={props.zeitplanungsvorlagen?.dtos
              .sort(
                (a, b) => zplVorlagenOptionsOrder.indexOf(a.dto.titel) - zplVorlagenOptionsOrder.indexOf(b.dto.titel),
              )
              .map((element, index) => {
                const title = element.dto.titel;
                return {
                  label: (
                    <span key={`option-${title}-${index}`} aria-label={title}>
                      {title}
                    </span>
                  ),
                  value: index,
                  title,
                };
              })}
          />
        </FormItemWithInfo>
      </ZeitFormComponent>
    </>
  );
}
