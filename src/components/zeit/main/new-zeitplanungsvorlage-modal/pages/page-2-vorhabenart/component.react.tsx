// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../../../general-zeitelement/new-zeitplanungsvorlage.less';

import { FormInstance, Radio } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { RegelungsvorhabenTypType, ZeitplanungsvorlageEntityShortDTO } from '@plateg/rest-api';
import { FormItemWithInfo, HiddenInfoComponent } from '@plateg/theme';

import {
  BasicFormPageComponent,
  BasicGeneralFormPageComponentProps,
} from '../../../general-zeitelement/pages/basic-form-page/component.react';

export function Page2VorhabenartComponent(props: BasicGeneralFormPageComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const vorhabenart = props?.data?.hasOwnProperty('vorhabenart')
    ? (props.data as ZeitplanungsvorlageEntityShortDTO).vorhabenart
    : props.vorhabenart;
  const [formInstance, setFormInstance] = useState<FormInstance>();
  useEffect(() => {
    if (vorhabenart) {
      formInstance?.setFieldValue('vorhabenart', vorhabenart);
    }
  }, [vorhabenart]);
  return (
    <BasicFormPageComponent formProps={props} setFormInstance={setFormInstance}>
      <fieldset className="fieldset-form-items">
        <legend className="seo">{t('zeit.newZeitplanungsvorlage.modal.page2.item1.label')}</legend>
        <FormItemWithInfo
          label={
            <>
              {t('zeit.newZeitplanungsvorlage.modal.page2.item1.label')}
              <span style={{ position: 'absolute', marginLeft: '10px' }}>
                <HiddenInfoComponent
                  title={t('zeit.newZeitplanungsvorlage.modal.page2.info.title')}
                  text={t('zeit.newZeitplanungsvorlage.modal.page2.info.text')}
                />
              </span>
            </>
          }
          name="vorhabenart"
          rules={[
            {
              required: true,
              message: t('zeit.newZeitplanungsvorlage.modal.page2.item1.requiredMsg'),
            },
          ]}
          initialValue={vorhabenart}
        >
          <Radio.Group name={'radiogroup-vorhabenart'} className="horizontal-radios">
            <Radio
              id="ezeit-newZeitplanungsVorlageModalPage2Item1OptionGesetz-radio"
              value={RegelungsvorhabenTypType.Gesetz}
              disabled={vorhabenart !== undefined && RegelungsvorhabenTypType.Gesetz.toString() !== vorhabenart}
            >
              {t(`zeit.newZeitplanungsvorlage.modal.page2.item1.options.${RegelungsvorhabenTypType.Gesetz}`)}
            </Radio>
            <Radio
              id="ezeit-newZeitplanungsVorlageModalPage2Item1OptionRechtsverordnung-radio"
              value={RegelungsvorhabenTypType.Rechtsverordnung}
              disabled={
                vorhabenart !== undefined && RegelungsvorhabenTypType.Rechtsverordnung.toString() !== vorhabenart
              }
            >
              {t(`zeit.newZeitplanungsvorlage.modal.page2.item1.options.${RegelungsvorhabenTypType.Rechtsverordnung}`)}
            </Radio>
            <Radio
              id="ezeit-newZeitplanungsVorlageModalPage2Item1OptionVerwaltungsvorschrift-radio"
              value={RegelungsvorhabenTypType.Verwaltungsvorschrift}
              disabled={
                vorhabenart !== undefined && RegelungsvorhabenTypType.Verwaltungsvorschrift.toString() !== vorhabenart
              }
            >
              {t(
                `zeit.newZeitplanungsvorlage.modal.page2.item1.options.${RegelungsvorhabenTypType.Verwaltungsvorschrift}`,
              )}
            </Radio>
          </Radio.Group>
        </FormItemWithInfo>
      </fieldset>
    </BasicFormPageComponent>
  );
}
