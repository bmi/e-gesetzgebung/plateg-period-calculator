// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import chai, { expect } from 'chai';
import chaiString from 'chai-string';
import { Observable } from 'rxjs';
import sinon from 'sinon';

import {
  Configuration,
  ErstellerType,
  RegelungsvorhabenTypType,
  ZeitplanungControllerApi,
  ZeitplanungsvorlageControllerApi,
  ZeitplanungsvorlageEntityResponseDTO,
  ZeitplanungsvorlageEntityShortDTO,
} from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme/src/shares';

import { ZeitplanungsvorlagenModalController } from './controller';

chai.use(chaiString);

const zeitplanungsvorlagenController = GlobalDI.getOrRegister(
  'zeitplanungsvorlagenController',
  () => new ZeitplanungsvorlagenModalController(),
);

const zeitplanungsvorlageController = GlobalDI.getOrRegister(
  'zeitplanunsvorlageRestController',
  () => new ZeitplanungsvorlageControllerApi(new Configuration()),
);

const zeitplanungRestController = GlobalDI.getOrRegister(
  'zeitplanungRestController',
  () => new ZeitplanungControllerApi(new Configuration()),
);

const zeitplanungsvorlage: ZeitplanungsvorlageEntityShortDTO = {
  titel: 'Zeitplanungsvorlage XYZ',
  beschreibung: 'Beschreibung XYZ',
  vorhabenart: RegelungsvorhabenTypType.Gesetz,
};

const zeitplanungsvorlageExtern: ZeitplanungsvorlageEntityShortDTO = {
  titel: 'Zeitplanungsvorlage XYZ',
  beschreibung: 'Beschreibung XYZ',
  vorhabenart: RegelungsvorhabenTypType.Gesetz,
};

const date1 = '2021-04-21T16:05:30.958156';
const date2 = '2028-04-16T14:02:38.958156';

const zeitplanungsvorlageResponse: ZeitplanungsvorlageEntityResponseDTO = {
  base: {
    id: '1232rj43843fn34',
    erstelltAm: date1,
    bearbeitetAm: date2,
  },
  dto: { ...zeitplanungsvorlage, erstellerType: ErstellerType.Selbst, elemente: [], readOnlyAccess: false },
};

const zeitplanungsvorlageExternResponse: ZeitplanungsvorlageEntityResponseDTO = {
  base: {
    id: '1232rj43843fn35',
    erstelltAm: date1,
    bearbeitetAm: date2,
  },
  dto: {
    ...zeitplanungsvorlageExtern,
    erstellerType: ErstellerType.Selbst,
    elemente: [],
    readOnlyAccess: false,
  },
};

describe('TEST: createZeitplanungsvorlageCall', () => {
  let createZeitplanungsvorlageCallStub: sinon.SinonStub;
  before(() => {
    createZeitplanungsvorlageCallStub = sinon.stub(zeitplanungsvorlageController, 'createZeitplanungsvorlage');
  });
  after(() => {
    createZeitplanungsvorlageCallStub.restore();
  });
  it('check if REST API is called', () => {
    zeitplanungsvorlagenController.createZeitplanungsvorlageCall(zeitplanungsvorlage);
    sinon.assert.calledOnce(createZeitplanungsvorlageCallStub);
    createZeitplanungsvorlageCallStub.resetHistory();
  });
});

describe('TEST: saveAlsZeitplanungsvorlageCall', () => {
  let saveAlsZeitplanungsvorlageCallStub: sinon.SinonStub;
  before(() => {
    saveAlsZeitplanungsvorlageCallStub = sinon.stub(zeitplanungRestController, 'createZeitplanungsvorlage1');
  });
  after(() => {
    saveAlsZeitplanungsvorlageCallStub.restore();
  });
  it('check if REST API is called', () => {
    zeitplanungsvorlagenController.saveAlsZeitplanungsvorlageCall(zeitplanungsvorlage);
    sinon.assert.calledOnce(saveAlsZeitplanungsvorlageCallStub);
    saveAlsZeitplanungsvorlageCallStub.resetHistory();
  });
});

describe('TEST: getSuccessMsg - create correct success string', () => {
  it('can create success string ', () => {
    expect(zeitplanungsvorlagenController.getSuccessMsg(zeitplanungsvorlageResponse)).to.equal(
      'Sie haben die neue Zeitplanungsvorlage „Zeitplanungsvorlage XYZ“ angelegt.',
    );
  });

  it('can create success string - copy ', () => {
    expect(zeitplanungsvorlagenController.getSuccessMsg(zeitplanungsvorlageResponse, true)).to.equal(
      'Sie haben die Kopie als neue Zeitplanungsvorlage „Zeitplanungsvorlage XYZ“ angelegt.',
    );
  });
});

describe('TEST: saveZeitplanungsvorlage', () => {
  let handleSaveStub: sinon.SinonStub;
  let createZeitplanungsvorlageCallStub: sinon.SinonStub;
  before(() => {
    handleSaveStub = sinon.stub(zeitplanungsvorlagenController, 'handleSave');
    createZeitplanungsvorlageCallStub = sinon.stub(zeitplanungsvorlageController, 'createZeitplanungsvorlage').returns(
      new Observable(function (observer) {
        observer.next();
        observer.complete();
      }),
    );
  });
  after(() => {
    createZeitplanungsvorlageCallStub.restore();
    handleSaveStub.restore();
  });

  afterEach(() => {
    createZeitplanungsvorlageCallStub.resetHistory();
    handleSaveStub.resetHistory();
  });

  it('check if handler is called', () => {
    const setIsVisible = sinon.spy((isVisible: boolean) => {});
    zeitplanungsvorlagenController.saveZeitplanungsvorlage(setIsVisible, true, zeitplanungsvorlage);

    sinon.assert.calledOnce(setIsVisible);
    sinon.assert.calledOnce(createZeitplanungsvorlageCallStub);
    sinon.assert.calledOnce(handleSaveStub);
  });

  it('check if nothing is called, if newZeitplanungsVorlage is undefined', () => {
    zeitplanungsvorlagenController.saveZeitplanungsvorlage((isVisible: boolean) => {}, true);
    sinon.assert.notCalled(createZeitplanungsvorlageCallStub);
    sinon.assert.notCalled(handleSaveStub);
  });
});
