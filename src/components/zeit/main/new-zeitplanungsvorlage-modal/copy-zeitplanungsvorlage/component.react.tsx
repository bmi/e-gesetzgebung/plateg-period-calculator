// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../../general-zeitelement/new-zeitplanungsvorlage.less';

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { ZeitplanungsvorlageEntityShortDTO } from '@plateg/rest-api';
import { MultiPageModalComponent } from '@plateg/theme';
import { GlobalDI } from '@plateg/theme/src/shares';

import { ZeitLightLocalController } from '../../../../zeit-light/controller-local';
import { NewZeitplanungsvorlageModalProps } from '../component.react';
import { ZeitplanungsvorlagenModalController } from '../controller';
import { Page3BenennungComponent } from '../pages/page-3-vorhabenart-benennung/component.react';
import { Page4ReviewComponent } from '../pages/page-4-review/component.react';

export interface CopyZeitplanungsvorlageComponentProps extends NewZeitplanungsvorlageModalProps {
  selectedZeitplanungsvorlage?: ZeitplanungsvorlageEntityShortDTO;
  selectedId?: string;
}

export function CopyZeitplanungsvorlageComponent(props: CopyZeitplanungsvorlageComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const zeitplanungsvorlagenController = GlobalDI.getOrRegister(
    'zeitplanungsvorlagenController',
    () => new ZeitplanungsvorlagenModalController(),
  );
  const zeitLightLocalController = GlobalDI.getOrRegister(
    'zeitLightLocalController',
    () => new ZeitLightLocalController(),
  );
  const [activePageIndex, setActivePageIndex] = useState<number>(0);
  const [newZeitplanungsVorlage, setNewZeitplanungsVorlage] = useState<ZeitplanungsvorlageEntityShortDTO>();

  useEffect(() => {
    setActivePageIndex(0);
    setNewZeitplanungsVorlage(undefined);
  }, [props.isVisible]);

  const mergeZeitplanungsVorlagen = (vorlage: ZeitplanungsvorlageEntityShortDTO) => {
    setNewZeitplanungsVorlage({ ...props.selectedZeitplanungsvorlage, ...vorlage, sourceId: props.selectedId });
  };

  return (
    <>
      {props.selectedZeitplanungsvorlage && (
        <MultiPageModalComponent
          key={`copy-zeitplanungsvorlage-${props.isVisible.toString()}`}
          isVisible={props.isVisible}
          setIsVisible={props.setIsVisible}
          activePageIndex={activePageIndex}
          setActivePageIndex={setActivePageIndex}
          title={t('zeit.newZeitplanungsvorlage.modal.copyZeitplanungsvorlage.title')}
          cancelBtnText={t('zeit.newZeitplanungsvorlage.modal.cancelBtnText')}
          nextBtnText={t('zeit.newZeitplanungsvorlage.modal.nextBtnText')}
          prevBtnText={t('zeit.newZeitplanungsvorlage.modal.prevBtnText')}
          componentReferenceContext="copyZeitplanungsvorlage"
          pages={[
            {
              formName: 'copyZeitplanungsvorlage.page1',
              content: (
                <Page3BenennungComponent
                  name="copyZeitplanungsvorlage.page1"
                  activePageIndex={activePageIndex}
                  setActivePageIndex={setActivePageIndex}
                  setData={mergeZeitplanungsVorlagen}
                  data={newZeitplanungsVorlage}
                  zeitType="newZeitplanungsvorlage"
                />
              ),
              primaryInsteadNextBtn: {
                buttonText: t('zeit.newZeitplanungsvorlage.modal.page3.nextBtn'),
              },
            },
            {
              content: (
                <Page4ReviewComponent
                  zeitplanungsVorlage={newZeitplanungsVorlage}
                  selectedZeitplanungsVorlage={props.selectedZeitplanungsvorlage}
                />
              ),
              primaryInsteadNextBtn: {
                buttonText: t('zeit.newZeitplanungsvorlage.modal.copyZeitplanungsvorlage.page2.nextBtn'),
              },
              nextOnClick: () => {
                const saveMethod = props.isLight
                  ? zeitLightLocalController.createNewVorlageLight
                  : zeitplanungsvorlagenController.saveZeitplanungsvorlage;
                saveMethod(props.setIsVisible, true, newZeitplanungsVorlage);
              },
            },
          ]}
        />
      )}
    </>
  );
}
