// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';
import { Route, Switch } from 'react-router-dom';

import { routes } from '../../../shares/routes';
import { HomeTabsZeit } from './home-tabs-zeit/component.react';
import { ZeitplanungenTabViewComponent } from './zeitplanung-tab-view/component.react';
import { ZeitplanungsvorlagenTabViewComponent } from './zeitplanungsvorlage-tab-view/component.react';

export function MainRoutesZeit(): React.ReactElement {
  return (
    <Switch>
      <Route
        exact
        path={[
          '/zeit',
          `/zeit/${routes.MEINE_ZEITPLANUNGEN}`,
          `/zeit/${routes.BILLIGUNGSANFRAGEN}`,
          `/zeit/${routes.ZEITPLANUNGSVORLAGEN}`,
          `/zeit/${routes.SYSTEMVORLAGEN}`,
          `/zeit/${routes.ARCHIV}`,
          `/zeit/${routes.NEUEZEITPLANUNG}/${routes.REGELUNGSVORHABEN}/:id`,
        ]}
      >
        <HomeTabsZeit />
      </Route>
      <Route
        path={[`/zeit/${routes.MEINE_ZEITPLANUNGEN}/:id`, `/zeit/${routes.ARCHIV}/${routes.MEINE_ZEITPLANUNGEN}/:id`]}
      >
        <ZeitplanungenTabViewComponent />
      </Route>
      <Route
        path={[
          `/zeit/${routes.ZEITPLANUNGSVORLAGEN}/:id`,
          `/zeit/${routes.ARCHIV}/${routes.ZEITPLANUNGSVORLAGEN}/:id`,
          `/zeit/${routes.SYSTEMVORLAGEN}/:id`,
        ]}
      >
        <ZeitplanungsvorlagenTabViewComponent />
      </Route>
    </Switch>
  );
}
