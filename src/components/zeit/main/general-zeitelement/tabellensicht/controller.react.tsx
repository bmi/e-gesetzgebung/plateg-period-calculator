// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Tooltip } from 'antd';
import i18n from 'i18next';
import React, { ReactElement } from 'react';
import { Link } from 'react-router-dom';

import { BASE_PATH, ZeitplanungselementHinweisType } from '@plateg/rest-api';
import {
  CalendarOutlined,
  ClockOutlined,
  ExclamationCircleFilled,
  IconImportant,
  IconLocked,
  InfoComponent,
} from '@plateg/theme';

import { routes } from '../../../../../shares/routes';
import { ZeitplanungRow } from '../../zeitplanung-tab-view/tabs/table-view/controller.react';
import { VorlageRow } from '../../zeitplanungsvorlage-tab-view/tabs/table-view/controller.react';

export class TabellensichtController {
  public readonly titelRenderer = (record: VorlageRow | ZeitplanungRow): ReactElement => {
    const typeString = i18n.t(
      `zeit.zeitplanungsvorlagenTable.tabs.tabellensicht.types.${record.isTermin ? 'termin' : 'phase'}`,
    );
    const info = record.infoKey ? (
      <InfoComponent
        key={`info-${record.id}-${record.infoKey}`}
        id={`info-${record.id}-${record.infoKey}`}
        title={record.titel}
        children={
          <p
            dangerouslySetInnerHTML={{
              __html: i18n.t(`zeit.zeitplanungsvorlagenTable.tabs.tabellensicht.infos.${record.infoKey}`, {
                interpolation: { escapeValue: false },
                baseUrl: BASE_PATH,
              }),
            }}
          />
        }
      />
    ) : (
      <></>
    );
    const icon = (
      <Tooltip
        trigger={['focus', 'hover']}
        autoAdjustOverflow={true}
        placement={'top'}
        overlayStyle={{ fontSize: '14px', lineHeight: '16px' }}
        key={`info-type-${record.id}`}
        id={`info-type-${record.id}`}
        title={typeString}
      >
        <span>{record.isTermin ? <ClockOutlined label={typeString} /> : <CalendarOutlined label={typeString} />}</span>
      </Tooltip>
    );
    let ariaLabel = record.titel;
    if ((record.children && record.children?.length > 0) || (record.level !== undefined && record.level > 0)) {
      ariaLabel = `Element Ebene ${(record.level || 0) + 1} ${record.titel}`;
    }
    return (
      <span className="element-title">
        <Link
          aria-label={ariaLabel}
          id={`ezeit-generalZeitElement-${record.id}-link`}
          to={`${routes.TABELLENSICHT}/${routes.UEBERSICHT}/${record.id}`}
        >
          {icon}
          {record.titel}
        </Link>
        &nbsp;{info}
      </span>
    );
  };
  public readonly eigenschaftenRenderer = (record: VorlageRow | ZeitplanungRow): ReactElement => {
    let infoWarningIcon = null;
    if (record.hinweise?.length) {
      record.hinweise.forEach((hinweis: ZeitplanungselementHinweisType) => {
        if (hinweis === ZeitplanungselementHinweisType.UnzulaessigeAbstandsverkuerzung) {
          infoWarningIcon = (
            <Tooltip
              trigger={['focus', 'hover']}
              autoAdjustOverflow={true}
              placement={'top'}
              overlayStyle={{ fontSize: '14px', lineHeight: '16px' }}
              key={`info-warning-${record.id}`}
              id={`info-warning-${record.id}`}
              title={i18n.t(`zeit.zeitplanungsvorlagenTable.tabs.tabellensicht.eigenschaften.abstand.title`)}
            >
              <span>
                <button
                  aria-label={i18n.t(`zeit.zeitplanungsvorlagenTable.tabs.tabellensicht.eigenschaften.abstand.title`)}
                  type="button"
                  className="eigenschaften-icon"
                >
                  <ExclamationCircleFilled />
                </button>
              </span>
            </Tooltip>
          );
        }
      });
    }
    let wichtigIcon = null;
    if (record.wichtig) {
      wichtigIcon = (
        <Tooltip
          trigger={['focus', 'hover']}
          autoAdjustOverflow={true}
          placement={'top'}
          overlayStyle={{ fontSize: '14px', lineHeight: '16px' }}
          key={`info-important-${record.id}`}
          id={`info-important-${record.id}`}
          title={i18n.t(`zeit.zeitplanungsvorlagenTable.tabs.tabellensicht.eigenschaften.wichtig.title`)}
        >
          <span>
            <button
              aria-label={i18n.t(`zeit.zeitplanungsvorlagenTable.tabs.tabellensicht.eigenschaften.wichtig.title`)}
              type="button"
              className="eigenschaften-icon"
            >
              <IconImportant
                label={i18n.t(`zeit.zeitplanungsvorlagenTable.tabs.tabellensicht.eigenschaften.wichtig.title`)}
              />
            </button>
          </span>
        </Tooltip>
      );
    }
    let gesperrtIcon = null;
    if ((record as ZeitplanungRow)?.gesperrt && record.level == 0) {
      const itemType = record.isTermin ? 'termin' : 'phase';
      const title = i18n.t(
        `zeit.zeitplanungsvorlagenTable.tabs.tabellensicht.eigenschaften.gesperrt.${itemType}.title`,
      );
      gesperrtIcon = (
        <Tooltip
          trigger={['focus', 'hover']}
          autoAdjustOverflow={true}
          placement={'top'}
          overlayStyle={{ fontSize: '14px', lineHeight: '16px' }}
          key={`info-gesperrt-${record.id}`}
          id={`info-gesperrt-${record.id}`}
          title={title}
        >
          <span>
            <button aria-label={title} type="button" className="eigenschaften-icon">
              <IconLocked label={title} />
            </button>
          </span>
        </Tooltip>
      );
    }
    return (
      <div className="important-icon">
        <span> {wichtigIcon}</span>
        <span> {infoWarningIcon}</span>
        <span>{gesperrtIcon}</span>
      </div>
    );
  };
}
