// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../../../general-zeitelement/new-zeitplanungsvorlage.less';

import { Form, FormInstance } from 'antd';
import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';

import { ZeitplanungEntityShortDTO, ZeitplanungsvorlageEntityShortDTO } from '@plateg/rest-api';

import { ZeitFormComponent } from '../../zeit-form-component/component.react';

export interface BasicGeneralFormPageComponentProps {
  name: string;
  data?: ZeitplanungsvorlageEntityShortDTO | ZeitplanungEntityShortDTO;
  setData: Function;
  activePageIndex: number;
  setActivePageIndex: (activePageIndex: number) => void;
  zeitType: string;
  vorhabenart?: string;
}

export interface BasicFormPageComponentProps {
  formProps: BasicGeneralFormPageComponentProps;
  children: React.ReactElement;
  setFormInstance?: (formInstance: FormInstance) => void;
}

export function BasicFormPageComponent(props: BasicFormPageComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const [form] = Form.useForm();
  useEffect(() => {
    props.setFormInstance?.(form);
  }, [form]);

  return (
    <ZeitFormComponent
      form={form}
      name={props.formProps.name}
      activePageIndex={props.formProps.activePageIndex}
      setActivePageIndex={props.formProps.setActivePageIndex}
      values={props.formProps.data}
      title={t(`zeit.${props.formProps.zeitType}.modal.${props.formProps.name}.title`)}
      setValues={(values: any) => {
        props.formProps.setData({ ...props.formProps.data, ...values });
      }}
    >
      {props.children}
    </ZeitFormComponent>
  );
}
