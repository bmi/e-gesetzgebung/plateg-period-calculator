// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Route, Switch, useRouteMatch } from 'react-router';

import { ZeitplanungEntityResponseDTO, ZeitplanungsvorlageEntityResponseDTO } from '@plateg/rest-api';
import { BreadcrumbComponent, HeaderController } from '@plateg/theme';
import { GlobalDI } from '@plateg/theme/src/shares';

import { routes } from '../../../../../shares/routes';
import { ZeitHelpLink } from '../../../component.react';
import { ElementLink } from '../../zeitplanung-tab-view/header/element-link/component.react';
import { ParentLink } from '../../zeitplanung-tab-view/header/parent-link/component.react';
import { ZeitplanungsLink } from '../../zeitplanung-tab-view/header/zeitplanung-link/component.react';
import { TabText } from '../../zeitplanungsvorlage-tab-view/header/tab-text/component.react';
import { NewZeitElementPruefenController } from '../new-time-element/element-pruefen/controller';
import {
  CreateNewElementComponent,
  ZeitplanungsElement,
  ZeitplanungsvorlageElement,
} from '../new-time-element/new-element/component.react';
import { ElementPruefenComponent } from './element-pruefen/component.react';

export interface NewTimeElementProps {
  currentZeitParent?: ZeitplanungsvorlageEntityResponseDTO | ZeitplanungEntityResponseDTO;
  reloadZeitParent: (redirectURL?: string) => void;
  isLight?: boolean;
}

export function NewZeitplanungsvorlageElement(props: NewTimeElementProps): React.ReactElement {
  const { t } = useTranslation();
  const headerController = GlobalDI.getOrRegister('ezeitHeaderController', () => new HeaderController());
  const newZeitElementPruefenController = GlobalDI.getOrRegister(
    'newZeitElementPruefenController',
    () => new NewZeitElementPruefenController(),
  );

  const routeMatcher = useRouteMatch<{
    archiv?: string;
    vorlageType: string;
    vorlageId: string;
    action: string;
    elementId?: string;
    elementAction?: string;
  }>(
    `/zeit/:archiv?/:vorlageType/:vorlageId/(${routes.TABELLENSICHT}|${routes.KALENDERSICHT})/:action/:elementId?/:elementAction?`,
  );
  const [element, setElement] = useState<ZeitplanungsvorlageElement | ZeitplanungsElement>();
  const vorlageId = routeMatcher?.params.vorlageId ?? '';
  const action = routeMatcher?.params.action ?? '';
  const tab = routeMatcher?.params.vorlageType;
  const viewMode = routeMatcher?.url.includes(routes.TABELLENSICHT) ? routes.TABELLENSICHT : routes.KALENDERSICHT;

  const setHeader = (
    value: ZeitplanungsvorlageEntityResponseDTO | ZeitplanungEntityResponseDTO,
    isPruefenPage: boolean,
  ) => {
    const breadcrumbItems = [];
    const isArchive = routeMatcher?.params.archiv;

    // Parent Link to home zeit page
    const parentTabLinkUrl = `/zeit/${isArchive ? routes.ARCHIV : tab}`;
    const parentTabLink = <ParentLink keyTab={isArchive ? routes.ARCHIV : tab} url={parentTabLinkUrl} />;
    breadcrumbItems.push(parentTabLink);

    // Link to zeitplanungen list page

    let zeitplanungsLinkUrl = `${parentTabLinkUrl}/${vorlageId}/${viewMode}`;
    if (isArchive) {
      zeitplanungsLinkUrl = `${parentTabLinkUrl}/${routes.MEINE_ZEITPLANUNGEN}/${vorlageId}/${viewMode}`;
    }
    const zeitplanungsLink = (
      <ZeitplanungsLink key="zeit-tabellensicht" url={zeitplanungsLinkUrl}>
        <>
          {value?.dto.titel} - <TabText key="zeit-tab" activeTab={viewMode} originalTable="zeitplanungenTable" />
        </>
      </ZeitplanungsLink>
    );
    breadcrumbItems.push(zeitplanungsLink);

    // Link to element List
    let elementLinkUrl;
    // If this is a edit or pruffung page set url
    if (action === routes.ELEMENT_BEARBEITEN) {
      elementLinkUrl = `${parentTabLinkUrl}/${vorlageId}/${viewMode}/${routes.UEBERSICHT}/${
        routeMatcher?.params.elementId as string
      }`;
    }
    if (action === routes.NEUES_ELEMENT && isPruefenPage) {
      elementLinkUrl = `${parentTabLinkUrl}/${vorlageId}/${viewMode}/${routes.NEUES_ELEMENT}`;
    }
    const elementLink = (
      <ElementLink
        url={elementLinkUrl}
        text={t(`zeit.newElementGeneral.action.${action}.breadcrumbTitle`, {
          type: element?.termin ? 'Termin' : 'Phase',
          name: element?.titel,
        })}
      />
    );
    breadcrumbItems.push(elementLink);

    // If this is edit page add item to breadcrumbs with 'Edit' text
    if (action === routes.ELEMENT_BEARBEITEN && !isPruefenPage) {
      const actionLink = <ElementLink text={t(`zeit.newElementGeneral.action.${action}.breadcrumbActionTitle`)} />;
      breadcrumbItems.push(actionLink);
    }

    // If this is Pruefen page add item to breadcrumbs with 'Edit' link and 'Pruefen' text
    if (action === routes.ELEMENT_BEARBEITEN && isPruefenPage) {
      const actionLinkUrl = `${parentTabLinkUrl}/${vorlageId}/${viewMode}/${routes.ELEMENT_BEARBEITEN}/${
        routeMatcher?.params.elementId as string
      }`;
      const actionLink = (
        <ElementLink url={actionLinkUrl} text={t(`zeit.newElementGeneral.action.${action}.breadcrumbActionTitle`)} />
      );
      breadcrumbItems.push(actionLink);

      // Add 'Pruefen' text
      const pruefungLink = <ElementLink text={t(`zeit.newElementGeneral.elementPruefen.breadcrumbTitle`)} />;
      breadcrumbItems.push(pruefungLink);
    }
    if (action === routes.NEUES_ELEMENT && isPruefenPage) {
      // Add 'Pruefen' text
      const pruefungLink = <ElementLink text={t(`zeit.newElementGeneral.elementPruefen.breadcrumbTitle`)} />;
      breadcrumbItems.push(pruefungLink);
    }
    headerController.setHeaderProps({
      headerLeft: [<BreadcrumbComponent key="breadcrumb" items={[...breadcrumbItems]} />],
      headerRight: [],
      headerLast: [<ZeitHelpLink key="zeit-help-link" isLight={props.isLight} />],
    });
  };

  useEffect(() => {
    setElement(
      newZeitElementPruefenController.findVorlageById(
        props.currentZeitParent?.dto.elemente,
        routeMatcher?.params.elementId,
      )?.dto,
    );
  }, [routeMatcher?.params.action, props.currentZeitParent]);

  return (
    <Switch>
      <Route
        exact
        path={[
          `/zeit/${tab}/:vorlageId/(${routes.TABELLENSICHT}|${routes.KALENDERSICHT})/${routes.NEUES_ELEMENT}`,
          `/zeit/${tab}/:vorlageId/(${routes.TABELLENSICHT}|${routes.KALENDERSICHT})/${routes.ELEMENT_BEARBEITEN}/:elementId`,
        ]}
      >
        {routeMatcher?.params.elementId && !element ? (
          <></>
        ) : (
          <CreateNewElementComponent
            parentId={vorlageId}
            action={action}
            element={element}
            setElement={setElement}
            currentZeitParent={props.currentZeitParent}
            elementId={routeMatcher?.params.elementId}
            setHeader={setHeader}
            isZeitplanungElement={tab === routes.MEINE_ZEITPLANUNGEN}
            viewMode={viewMode}
            isLight={props.isLight}
          />
        )}
      </Route>
      <Route
        exact
        path={[
          `/zeit/${tab}/:id/(${routes.TABELLENSICHT}|${routes.KALENDERSICHT})/${routes.NEUES_ELEMENT}/${routes.ELEMENT_PRUEFEN}`,
          `/zeit/${tab}/:id/(${routes.TABELLENSICHT}|${routes.KALENDERSICHT})/${routes.ELEMENT_BEARBEITEN}/:elementId/${routes.ELEMENT_PRUEFEN}`,
          `/zeit/${tab}/:id/${routes.TABELLENSICHT}/${routes.UEBERSICHT}/:elementId/`,
          `/zeit/${routes.ARCHIV}/${tab}/:id/${routes.TABELLENSICHT}/${routes.UEBERSICHT}/:elementId/`,
        ]}
      >
        {routeMatcher?.params.elementId && !element ? (
          <></>
        ) : (
          <ElementPruefenComponent
            element={element}
            action={action}
            vorlageId={vorlageId}
            reloadZeitParent={props.reloadZeitParent}
            currentZeitParent={props.currentZeitParent}
            elementId={routeMatcher?.params.elementId}
            setHeader={setHeader}
            isZeitplanungElement={tab === routes.MEINE_ZEITPLANUNGEN}
            isArchived={routeMatcher?.params.archiv ? true : false}
            viewMode={viewMode}
            isLight={props.isLight}
            tab={tab}
          />
        )}
      </Route>
      )
    </Switch>
  );
}
