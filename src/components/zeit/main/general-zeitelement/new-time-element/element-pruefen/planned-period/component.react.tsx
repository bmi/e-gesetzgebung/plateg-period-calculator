// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Typography } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { PhaseEntityDTO } from '@plateg/rest-api';
import { Filters } from '@plateg/theme/src/shares/filters';

import { ZeitplanungsElement } from '../../new-element/component.react';
import { RepositionElementsReviewComponent } from '../reposition-elements/component.react';
interface PlannedPeriodReviewComponentProps {
  element?: ZeitplanungsElement;
  isZeitplanungElement?: boolean;
}

export function PlannedPeriodReviewComponent(props: PlannedPeriodReviewComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const { Title } = Typography;
  const sectionIsVisible = !props.element?.termin === true && props.isZeitplanungElement;

  return (
    <>
      {sectionIsVisible && (
        <section className="review-section">
          <Title level={2}>{t('zeit.newElementGeneral.elementPruefen.plannendPeriod.title')}</Title>
          <dl>
            <dt>{t('zeit.newElementGeneral.elementPruefen.plannendPeriod.items.from')}</dt>
            <dd>
              {(props.element as PhaseEntityDTO).beginn
                ? Filters.dateFromString((props.element as PhaseEntityDTO).beginn)
                : '-'}
            </dd>
            <dt>{t('zeit.newElementGeneral.elementPruefen.plannendPeriod.items.to')}</dt>
            <dd>
              {(props.element as PhaseEntityDTO).ende
                ? Filters.dateFromString((props.element as PhaseEntityDTO).ende)
                : '-'}
            </dd>
          </dl>
          <RepositionElementsReviewComponent element={props.element} />
        </section>
      )}
    </>
  );
}
