// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';
import { useTranslation } from 'react-i18next';

import { TerminEntityDTO } from '@plateg/rest-api';
import { Filters } from '@plateg/theme/src/shares/filters';

import { ZeitplanungsElement } from '../../new-element/component.react';
import { RepositionElementsReviewComponent } from '../reposition-elements/component.react';
interface DateReviewComponentProps {
  element?: ZeitplanungsElement;
  isZeitplanungElement?: boolean;
}

export function DateReviewComponent(props: DateReviewComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const sectionIsVisible = props.element?.termin === true && props.isZeitplanungElement;

  return (
    <>
      {sectionIsVisible && (
        <>
          <dt>{t('zeit.newElementGeneral.elementPruefen.date.items.date')}</dt>
          <dd>{Filters.dateFromString((props.element as TerminEntityDTO).datum ?? 0)}</dd>
          <RepositionElementsReviewComponent element={props.element} />
        </>
      )}
    </>
  );
}
