// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import chai, { expect } from 'chai';
import chaiString from 'chai-string';
import sinon from 'sinon';

import {
  PhasenvorlageEntityResponseDTO,
  ZeitplanungselementEntityResponseDTOZeitplanungselementEntityDTO,
  ZeitplanungsvorlagenBauplan,
} from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme/src/shares';

import { NewZeitElementPruefenController } from './controller';
chai.use(chaiString);
const ctrl = GlobalDI.getOrRegister('newZeitElementPruefenController', () => new NewZeitElementPruefenController());
describe('TEST: findVorlageById', () => {
  const date = '2021-06-08 08:03:09';
  const zeitvorlagen: ZeitplanungselementEntityResponseDTOZeitplanungselementEntityDTO[] = [
    {
      base: { id: '1', bearbeitetAm: date, erstelltAm: date },
      dto: {
        termin: false,
        wichtig: false,
        titel: 'Phase1',
        vorherigeElementeIds: [],
        bauplan: ZeitplanungsvorlagenBauplan.GAnnahmeUndInterneVerteilungGesetzbeschluss,
        untergeordneteElemente: [
          {
            base: { id: '2', bearbeitetAm: date, erstelltAm: date },
            dto: { termin: true, wichtig: true, titel: 'Termin1.1', vorherigeElementeIds: [] },
          },
        ],
      },
    },
    {
      base: { id: '3', bearbeitetAm: date, erstelltAm: date },
      dto: { termin: true, wichtig: true, titel: 'Termin2', vorherigeElementeIds: [] },
    },
  ];
  it('no zeitvorlagen passed in, undefined should be returned', () => {
    expect(ctrl.findVorlageById(undefined, 'id')).undefined;
  });

  it('no id passed in, undefined should be returned', () => {
    expect(ctrl.findVorlageById(zeitvorlagen, undefined)).undefined;
  });

  it('nothing passed in, undefined should be returned', () => {
    expect(ctrl.findVorlageById(zeitvorlagen, undefined)).undefined;
  });

  it('search for existing id, correct zeitvorlage should be returned', () => {
    expect(ctrl.findVorlageById(zeitvorlagen, '1')).to.deep.include(zeitvorlagen[0]);
  });

  it('search for existing nested id, correct zeitvorlage should be returned', () => {
    expect(ctrl.findVorlageById(zeitvorlagen, '2')).to.deep.include(
      (zeitvorlagen[0] as PhasenvorlageEntityResponseDTO).dto.untergeordneteElemente[0],
    );
  });
  it('search for not existing id, undefined should be returned', () => {
    expect(ctrl.findVorlageById(zeitvorlagen, '4')).undefined;
  });
});
