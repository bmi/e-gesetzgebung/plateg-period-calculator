// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import {
  PhasenvorlageEntityDTO,
  ZeitplanungselementEntityResponseDTOZeitplanungselementEntityDTO,
  ZeitplanungsvorlagenelementEntityResponseDTOZeitplanungsvorlagenelementEntityDTO,
} from '@plateg/rest-api/models';
export class NewZeitElementPruefenController {
  public findVorlageById = (
    zeitvorlagen?: Array<
      | ZeitplanungselementEntityResponseDTOZeitplanungselementEntityDTO
      | ZeitplanungsvorlagenelementEntityResponseDTOZeitplanungsvorlagenelementEntityDTO
    >,
    id?: string | undefined,
  ):
    | undefined
    | ZeitplanungselementEntityResponseDTOZeitplanungselementEntityDTO
    | ZeitplanungsvorlagenelementEntityResponseDTOZeitplanungsvorlagenelementEntityDTO => {
    if (!zeitvorlagen || !zeitvorlagen.length || !id) {
      return undefined;
    }
    return (
      zeitvorlagen.find((zeitvorlage) => zeitvorlage.base.id === id) ||
      this.findVorlageById(
        zeitvorlagen.flatMap(
          (zeitvorlage) =>
            (!zeitvorlage.dto.termin
              ? (zeitvorlage.dto as PhasenvorlageEntityDTO).untergeordneteElemente
              : undefined) || [],
        ),
        id,
      ) ||
      undefined
    );
  };
}
