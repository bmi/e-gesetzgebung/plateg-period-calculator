// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Typography } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { PhasenvorlageEntityDTO } from '@plateg/rest-api';

import { ZeitplanungsvorlageElement } from '../../new-element/component.react';

interface DurationReviewComponentProps {
  element?: ZeitplanungsvorlageElement;
  isZeitplanungElement?: boolean;
}

export function DurationReviewComponent(props: DurationReviewComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const { Title } = Typography;
  const sectionIsVisible = props.element?.termin === false && !props.isZeitplanungElement;
  const elementDauer = (props.element as PhasenvorlageEntityDTO).dauer;

  return (
    <>
      {sectionIsVisible && (
        <section className="review-section">
          <Title level={2}>{t('zeit.newElementGeneral.elementPruefen.duration.title')}</Title>
          <dl>
            <dt>{t('zeit.newElementGeneral.elementPruefen.duration.items.weeks')}</dt>
            <dd>{elementDauer?.weeks}</dd>
            <dt>{t('zeit.newElementGeneral.elementPruefen.duration.items.days')}</dt>
            <dd>{elementDauer?.days}</dd>
          </dl>
        </section>
      )}
    </>
  );
}
