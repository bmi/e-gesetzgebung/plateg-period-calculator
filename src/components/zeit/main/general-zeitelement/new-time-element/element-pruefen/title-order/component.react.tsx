// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Typography } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { ZeitplanungEntityResponseDTO, ZeitplanungsvorlageEntityResponseDTO } from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme/src/shares';

import { ZeitplanungsvorlageElement } from '../../new-element/component.react';
import { NewZeitElementPruefenController } from '../controller';
import { DateReviewComponent } from '../date/component.react';
import { PhaseReviewComponent } from './phase/component.react';
import { TitleReviewComponent } from './title/component.react';
import { TypeReviewComponent } from './type/component.react';
interface TitleOrderComponentProps {
  element?: ZeitplanungsvorlageElement;
  currentZeitParent?: ZeitplanungsvorlageEntityResponseDTO | ZeitplanungEntityResponseDTO;
  isZeitplanungElement?: boolean;
}

export function TitleOrderComponent(props: TitleOrderComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const { Title } = Typography;
  const ctrl = GlobalDI.getOrRegister('newZeitElementPruefenController', () => new NewZeitElementPruefenController());
  const selectedType = props.element?.termin ? 'termin' : 'phase';
  const uebergeordnetePhase = ctrl.findVorlageById(
    props.currentZeitParent?.dto.elemente,
    props.element?.uebergeordneteElementeId,
  )?.dto.titel;
  const order = (() => {
    const previousElementTitle = ctrl.findVorlageById(
      props.currentZeitParent?.dto.elemente,
      props.element?.vorherigeElementeIds?.length ? props.element?.vorherigeElementeIds[0] : undefined,
    )?.dto.titel;
    if (previousElementTitle) {
      return `Nach: ${previousElementTitle}`;
    } else {
      return uebergeordnetePhase
        ? t('zeit.newElementGeneral.elementPruefen.titleOrder.items.order.firstInPhase')
        : t('zeit.newElementGeneral.elementPruefen.titleOrder.items.order.firstInVorlage');
    }
  })();

  return (
    <section className="review-section">
      <Title level={2}>{t('zeit.newElementGeneral.elementPruefen.titleOrder.title')}</Title>
      <dl>
        <TitleReviewComponent element={props.element} />
        <TypeReviewComponent element={props.element} selectedType={selectedType} />
        <PhaseReviewComponent uebergeordnetePhase={uebergeordnetePhase} />
        <dt>{t('zeit.newElementGeneral.elementPruefen.titleOrder.items.order.label')}</dt>
        <dd>{order}</dd>
        <DateReviewComponent element={props.element} isZeitplanungElement={props.isZeitplanungElement} />
      </dl>
    </section>
  );
}
