// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';
import { useTranslation } from 'react-i18next';

import { InfoComponent } from '@plateg/theme';

import { ZeitplanungsvorlageElement } from '../../../new-element/component.react';
interface TitleReviewComponentProps {
  element?: ZeitplanungsvorlageElement;
}

export function TitleReviewComponent(props: TitleReviewComponentProps): React.ReactElement {
  const { t } = useTranslation();

  return (
    <>
      <dt>{t('zeit.newElementGeneral.elementPruefen.titleOrder.items.title.label')}</dt>
      <dd>
        {props.element?.titel}
        {props.element?.bauplan && (
          <InfoComponent title={props.element?.titel ?? ''}>
            <p
              dangerouslySetInnerHTML={{
                __html: t(`zeit.zeitplanungsvorlagenTable.tabs.tabellensicht.infos.${props.element?.bauplan}`, {
                  interpolation: { escapeValue: false },
                }),
              }}
            ></p>
          </InfoComponent>
        )}
      </dd>
    </>
  );
}
