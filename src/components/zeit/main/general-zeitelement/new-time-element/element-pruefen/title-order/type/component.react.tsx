// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';
import { useTranslation } from 'react-i18next';

import { InfoComponent } from '@plateg/theme';

import { ZeitplanungsvorlageElement } from '../../../new-element/component.react';
interface TypeReviewComponentProps {
  selectedType: string;
  element?: ZeitplanungsvorlageElement;
}

export function TypeReviewComponent(props: TypeReviewComponentProps): React.ReactElement {
  const { t } = useTranslation();

  return (
    <>
      <dt>
        {t(`zeit.newElementGeneral.elementPruefen.titleOrder.items.type.${props.selectedType}.label`)}
        <InfoComponent
          title={t(`zeit.newElementGeneral.titleAndOrder.type.specificType.${props.selectedType}.hint.title`)}
        >
          <p
            dangerouslySetInnerHTML={{
              __html: t(`zeit.newElementGeneral.titleAndOrder.type.specificType.${props.selectedType}.hint.content`, {
                interpolation: { escapeValue: false },
              }),
            }}
          ></p>
        </InfoComponent>
      </dt>
      <dd>
        {props.element?.bauplan
          ? t(`zeit.newElementGeneral.elementPruefen.titleOrder.items.type.${props.selectedType}.copyLabel`)
          : t(`zeit.newElementGeneral.elementPruefen.titleOrder.items.type.${props.selectedType}.newLabel`)}
      </dd>
    </>
  );
}
