// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';
import { useTranslation } from 'react-i18next';
interface PhaseReviewComponentProps {
  uebergeordnetePhase?: string;
}

export function PhaseReviewComponent(props: PhaseReviewComponentProps): React.ReactElement {
  const { t } = useTranslation();

  return (
    <>
      <dt>{t('zeit.newElementGeneral.elementPruefen.titleOrder.items.phase.label')}</dt>
      <dd>{props.uebergeordnetePhase ?? t('zeit.newElementGeneral.elementPruefen.titleOrder.items.phase.noPhase')}</dd>
    </>
  );
}
