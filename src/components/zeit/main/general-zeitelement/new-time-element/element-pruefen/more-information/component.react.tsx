// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { List, Typography } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { TerminvorlageEntityDTO } from '@plateg/rest-api';
import { IconImportant, IconLocked } from '@plateg/theme';

import { ZeitplanungsElement, ZeitplanungsvorlageElement } from '../../new-element/component.react';

interface MoreInformationComponentProps {
  element?: ZeitplanungsvorlageElement | ZeitplanungsElement;
}

export function MoreInformationReviewComponent(props: MoreInformationComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const { Title } = Typography;
  const properties = (() => {
    const items = [];
    if (props.element?.termin && (props.element as TerminvorlageEntityDTO).wichtig) {
      items.push(
        <List.Item>
          <IconImportant />
          {t(`zeit.newElementGeneral.elementPruefen.moreInformation.properties.items.important.label`)}
        </List.Item>,
      );
    }
    if ((props.element as ZeitplanungsElement)?.gesperrt) {
      items.push(
        <List.Item>
          <IconLocked />
          {t(
            `zeit.newElementGeneral.elementPruefen.moreInformation.properties.items.lock.${
              props.element?.termin ? 'termin' : 'phase'
            }.${props.element?.uebergeordneteElementeId == null ? 'label' : 'labelChild'}`,
          )}
        </List.Item>,
      );
    }
    return items;
  })();

  const propertyList = properties.length ? (
    <>
      <List className="property-list">{properties}</List>
    </>
  ) : undefined;

  const comment = props.element?.kommentar ? (
    <dl>
      <dt>{t('zeit.newElementGeneral.elementPruefen.moreInformation.comment.title')}</dt>
      <dd>{props.element?.kommentar}</dd>
    </dl>
  ) : undefined;

  const sectionIsVisible = comment || properties;

  return (
    <>
      {sectionIsVisible && (
        <section className="review-section">
          <Title level={2}>{t('zeit.newElementGeneral.elementPruefen.moreInformation.title')}</Title>
          {propertyList}
          {comment}
        </section>
      )}
    </>
  );
}
