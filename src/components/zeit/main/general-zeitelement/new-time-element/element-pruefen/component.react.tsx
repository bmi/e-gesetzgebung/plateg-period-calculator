// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../../new-time-element/new-zeit-element.less';

import { Button } from 'antd';
import Title from 'antd/lib/typography/Title';
import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router';

import {
  AktionType,
  RolleLokalType,
  ZeitplanungEntityResponseDTO,
  ZeitplanungsvorlageEntityResponseDTO,
} from '@plateg/rest-api';
import { CombinedTitle, displayMessage, LeftOutlined } from '@plateg/theme';
import { GlobalDI } from '@plateg/theme/src/shares';

import { routes } from '../../../../../../shares/routes';
import { ZeitplanungsElement, ZeitplanungsvorlageElement } from '../new-element/component.react';
import { NewZeitElementController } from '../new-element/controller';
import { DurationReviewComponent } from './duration/component.react';
import { MoreInformationReviewComponent } from './more-information/component.react';
import { PlannedPeriodReviewComponent } from './planned-period/component.react';
import { TitleOrderComponent } from './title-order/component.react';

export interface ElementPruefenComponentProps {
  vorlageId: string;
  action: string;
  element?: ZeitplanungsvorlageElement | ZeitplanungsElement;
  currentZeitParent?: ZeitplanungsvorlageEntityResponseDTO | ZeitplanungEntityResponseDTO;
  reloadZeitParent: (redirectURL?: string) => void;
  elementId?: string;
  setHeader: Function;
  isZeitplanungElement?: boolean;
  isArchived?: boolean;
  viewMode: string;
  isLight?: boolean;
  tab?: string;
}

export function ElementPruefenComponent(props: ElementPruefenComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const history = useHistory();
  const newZeitElementController = GlobalDI.getOrRegister(
    'newZeitElementController',
    () => new NewZeitElementController(),
  );
  const modify = props.action === routes.ELEMENT_BEARBEITEN;
  const overview = props.action === routes.UEBERSICHT;
  const tab = props.isZeitplanungElement ? routes.MEINE_ZEITPLANUNGEN : props.tab;

  useEffect(() => {
    if (props.currentZeitParent) {
      props.setHeader(props.currentZeitParent, !overview);
    }
  }, []);

  useEffect(() => {
    if (props.currentZeitParent) {
      props.setHeader(props.currentZeitParent, !overview);
    }
  }, [overview]);

  const finishCallback = () => {
    const typeText = props.element?.termin ? 'den Termin' : 'die Phase';
    const successMsg = t(`zeit.newElementGeneral.action.${props.action}.msgs.success`, {
      type: typeText,
      name: props.element?.titel,
    });
    displayMessage(successMsg, 'success');
    props.reloadZeitParent(`/zeit/${tab}/${props.vorlageId}/${props.viewMode}`);
  };
  const onFinish = () => {
    const id = modify ? props.elementId : props.vorlageId;
    if (props.isZeitplanungElement) {
      newZeitElementController.saveElement(
        finishCallback,
        modify,
        id,
        props.element as ZeitplanungsElement,
        props.isLight,
        props.vorlageId,
      );
    } else {
      newZeitElementController.saveVorlageElement(
        finishCallback,
        modify,
        id,
        props.element,
        props.isLight,
        props.vorlageId,
      );
    }
  };
  const terminOrPhase = props.element?.termin ? 'Termin' : 'Phase';
  return (
    <div className="new-zeit-element element-pruefen">
      <Title level={1}>
        {overview ? (
          `${terminOrPhase} „${props.element?.titel || ''}“`
        ) : (
          <CombinedTitle title={props.element?.titel} suffix={t('zeit.newElementGeneral.elementPruefen.title')} />
        )}
        &nbsp;
      </Title>
      <TitleOrderComponent
        element={props.element}
        currentZeitParent={props.currentZeitParent}
        isZeitplanungElement={props.isZeitplanungElement}
      />
      <DurationReviewComponent element={props.element} isZeitplanungElement={props.isZeitplanungElement} />
      <PlannedPeriodReviewComponent element={props.element} isZeitplanungElement={props.isZeitplanungElement} />
      <MoreInformationReviewComponent element={props.element} />
      <div className="form-control-buttons">
        {overview && (
          <Button
            id="ezeit-newElementElementPruefen-btn"
            type="text"
            size={'large'}
            className="btn-prev text-home"
            onClick={() =>
              history.push(
                `/zeit/${props.isArchived ? routes.ARCHIV + '/' : ''}${tab}/${props.vorlageId}/${props.viewMode}`,
              )
            }
          >
            <LeftOutlined />
            {t(`zeit.newElementGeneral.elementPruefen.backLink.${tab}`)}
          </Button>
        )}

        {!overview && (
          <Button id="ezeit-newElementElementPruefenSubmit-btn" type="primary" onClick={onFinish} size={'large'}>
            {modify
              ? t('zeit.newElementGeneral.elementPruefen.submitEdit')
              : t('zeit.newElementGeneral.elementPruefen.submit')}
          </Button>
        )}
        {!props.currentZeitParent?.dto.readOnlyAccess &&
          !(
            (props.element as ZeitplanungsElement)?.gesperrt &&
            props.currentZeitParent?.dto.rolleTyp !== RolleLokalType.Federfuehrer
          ) && (
            <Button
              id="ezeit-newElementElementPruefenEditOverview-btn"
              size={'large'}
              disabled={!props.currentZeitParent?.dto.aktionen?.includes(AktionType.Schreiben)}
              onClick={() => {
                if (!(modify || overview)) {
                  history.push(`/zeit/${tab}/${props.vorlageId}/${props.viewMode}/${routes.NEUES_ELEMENT}/`);
                } else {
                  history.push(
                    `/zeit/${tab}/${props.vorlageId}/${props.viewMode}/${routes.ELEMENT_BEARBEITEN}/${
                      props.elementId as string
                    }`,
                  );
                }
              }}
            >
              {t(`zeit.newElementGeneral.elementPruefen.edit.overview.${overview.toString()}`, {
                type: terminOrPhase,
              })}
            </Button>
          )}
      </div>
    </div>
  );
}
