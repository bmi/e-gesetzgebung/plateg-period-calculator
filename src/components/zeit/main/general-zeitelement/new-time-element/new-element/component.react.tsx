// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../new-zeit-element.less';

import { Button, Col, Form, Row } from 'antd';
import { setHours } from 'date-fns';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router';
import { AjaxError } from 'rxjs/ajax';

import {
  PhaseEntityDTO,
  PhasenvorlageEntityDTO,
  TerminEntityDTO,
  TerminvorlageEntityDTO,
  ZeitplanungEntityResponseDTO,
  ZeitplanungselementEntityResponseDTOZeitplanungselementEntityDTO,
  ZeitplanungselementHinweisType,
  ZeitplanungsvorlageEntityResponseDTO,
  ZeitvorlagevorlagenBauplanDTO,
} from '@plateg/rest-api';
import {
  CombinedTitle,
  ErrorBox,
  ErrorController,
  GeneralFormWrapper,
  LoadingStatusController,
  RouteLeavingGuard,
} from '@plateg/theme';
import { GlobalDI } from '@plateg/theme/src/shares';

import { routes } from '../../../../../../shares/routes';
import { DurationComponent } from '../../../zeitplanungsvorlage-tab-view/tabs/table-view/new-time-element/new-element/sections/duration/component.react';
import { NewZeitElementController } from './controller';
import { MoreInformationComponent } from './sections/more-information/component.react';
import { PeriodComponent } from './sections/period/component.react';
import { PeriodController } from './sections/period/controller';
import { RepositionElementsComponent } from './sections/reposition-elements/component.react';
import { StartDateComponent } from './sections/start-date/component.react';
import { TitleAndOrderComponent } from './sections/title-and-order/component.react';

export type NullableDate = Date | null | undefined;
export type ZeitplanungsElement = PhaseEntityDTO | TerminEntityDTO;
export type ZeitplanungsvorlageElement = TerminvorlageEntityDTO | PhasenvorlageEntityDTO;

export interface ZeitplanungsvorlageTableViewComponentProps {
  parentId: string;
  currentZeitParent?: ZeitplanungsvorlageEntityResponseDTO | ZeitplanungEntityResponseDTO;
  action: string;
  element?: ZeitplanungsvorlageElement | ZeitplanungsElement;
  elementId?: string;
  setElement: (element?: ZeitplanungsvorlageElement | ZeitplanungsElement) => void;
  setHeader: Function;
  isZeitplanungElement?: boolean;
  viewMode: string;
  isLight?: boolean;
}

export interface NewElementFormVals extends TerminEntityDTO, PhaseEntityDTO {
  selectedPreviousId: string;
  period?: [];
  abstand: ZeitplanungselementHinweisType;
}

export function CreateNewElementComponent(props: ZeitplanungsvorlageTableViewComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const history = useHistory();
  const [form] = Form.useForm();
  const newZeitElementController = GlobalDI.getOrRegister(
    'newZeitElementController',
    () => new NewZeitElementController(),
  );
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  const periodController = GlobalDI.getOrRegister('periodController', () => new PeriodController());

  const tab = props.isZeitplanungElement ? routes.MEINE_ZEITPLANUNGEN : routes.ZEITPLANUNGSVORLAGEN;
  const isModifyAction = props.action !== routes.NEUES_ELEMENT;
  const [isLeavingPageBlocked, setIsLeavingPageBlocked] = useState(false);
  const [isFinished, setIsFinished] = useState(false);
  const [isTermin, setIsTermin] = useState(props.element?.termin ?? true);
  const [isCustomElement, seIsCustomElement] = useState(props.element ? !props.element?.bauplan : false);
  const [selectedVorlageBauplan, setSelectedVorlageBauplan] = useState<ZeitvorlagevorlagenBauplanDTO>(
    {} as ZeitvorlagevorlagenBauplanDTO,
  );
  const [possibleVorlagen, setPossibleVorlagen] = useState<ZeitvorlagevorlagenBauplanDTO[]>([]);
  const [selectedVorgaenerId, setSelectedVorgaengerId] = useState<string>();
  const [selectedVorgaenger, setSelectedVorgaenger] = useState<ZeitplanungsElement | undefined>();

  const initialDates = newZeitElementController.getInitialDatesValues(
    props.isZeitplanungElement,
    isTermin,
    props.element,
  );
  const initialValues = { ...props.element, termin: isTermin, 'custom-element': false, ...initialDates };

  useEffect(() => {
    if (props.currentZeitParent) {
      props.setHeader(props.currentZeitParent, false);
    }
  }, []);

  useEffect(() => {
    if (props.currentZeitParent) {
      loadingStatusController.setLoadingStatus(true);
      newZeitElementController
        .getInitialDataObservable(props.isZeitplanungElement, props.currentZeitParent, props.isLight)
        .subscribe({
          next: (vorlagen: ZeitvorlagevorlagenBauplanDTO[]) => {
            setSelectedVorlageBauplan(
              vorlagen.find((vorlage) => {
                return vorlage.bauplan === props.element?.bauplan;
              }),
            );
            setPossibleVorlagen(vorlagen);
            loadingStatusController.setLoadingStatus(false);
          },
          error: (error: AjaxError) => {
            errorCtrl.displayErrorMsg(error, 'zeit.generalErrorMsg');
            console.log(error);
            loadingStatusController.setLoadingStatus(false);
          },
        });
    }
  }, [props.currentZeitParent]);

  useEffect(() => {
    form.resetFields();
    form.setFieldsValue(initialValues);
  }, [props.action]);

  useEffect(() => {
    if (tab === routes.MEINE_ZEITPLANUNGEN) {
      setSelectedVorgaenger(
        periodController.findPreviousElement(
          props.currentZeitParent?.dto.elemente as ZeitplanungselementEntityResponseDTOZeitplanungselementEntityDTO[],
          selectedVorgaenerId,
        ),
      );
    }
  }, [selectedVorgaenerId]);

  const onFinish = () => {
    const value = form.getFieldsValue(true) as NewElementFormVals;

    if (!isCustomElement) {
      value.titel =
        possibleVorlagen.find((vorlage) => {
          return vorlage.bauplan === value.bauplan;
        })?.titel || '';
    }
    value.datum = setHours(new Date(value.datum), 12) as unknown as string;
    // Set list of hinweises
    value.hinweise = value?.hinweise || [];
    if (value.abstand) {
      value.hinweise = [...value.hinweise, value.abstand];
    }

    // Convert dauer to weeks and days if input days are more than 7 days
    const dauer = (value as ZeitplanungsvorlageElement)?.dauer;
    const extendedValue = value;
    if (dauer?.days && dauer?.days >= 7) {
      const newDauer = {
        ...dauer,
        // Convert dauer?.weeks to Number as after manual changes on form input it is a string
        weeks: Number(dauer?.weeks || 0) + Math.trunc(dauer.days / 7),
        days: Number(dauer.days % 7 || 0),
      };
      (extendedValue as ZeitplanungsvorlageElement)['dauer'] = newDauer;
    }

    props.setElement(extendedValue);
    setIsLeavingPageBlocked(false);
    setIsFinished(true);
  };

  useEffect(() => {
    if (isFinished && !isLeavingPageBlocked) {
      if (!isModifyAction) {
        history.push(
          `/zeit/${tab}/${props.parentId}/${props.viewMode}/${routes.NEUES_ELEMENT}/${routes.ELEMENT_PRUEFEN}`,
        );
      } else {
        history.push(
          `/zeit/${tab}/${props.parentId}/${props.viewMode}/${routes.ELEMENT_BEARBEITEN}/${props.elementId as string}/${
            routes.ELEMENT_PRUEFEN
          }`,
        );
      }
      setIsFinished(false);
    }
  }, [isFinished, isLeavingPageBlocked]);

  const onFinishFailed = () => {
    document?.getElementById('errorBox')?.focus();
  };

  const onFormValuesChanged = () => {
    setIsLeavingPageBlocked(true);
  };

  const displayPeriodOrTerminComp = () => {
    if (!props.isZeitplanungElement) {
      return <></>;
    }
    if (isTermin) {
      return (
        <StartDateComponent
          form={form}
          isZeitplanungElement={props.isZeitplanungElement}
          isTermin={isTermin}
          selectedVorgaenger={selectedVorgaenger}
          elements={props.currentZeitParent?.dto.elemente}
          elementId={props.elementId}
          isLight={props.isLight}
          bauplanType={props.element?.bauplan}
          datum={(props.element as ZeitplanungsElement)?.datum}
        />
      );
    } else {
      return (
        <PeriodComponent
          form={form}
          isZeitplanungElement={props.isZeitplanungElement}
          isTermin={isTermin}
          selectedVorlageBauplan={selectedVorlageBauplan}
          elemente={props.currentZeitParent?.dto.elemente}
          selectedVorgaenger={selectedVorgaenger}
          elementId={props.elementId}
          isLight={props.isLight}
          vorhabenart={props.currentZeitParent?.dto.vorhabenart}
        />
      );
    }
  };

  return (
    <Row className="new-zeit-element">
      <Col
        xs={{ span: 22, offset: 1 }}
        md={{ span: 14, offset: 2 }}
        lg={{ span: 16, offset: 3 }}
        xl={{ span: 12, offset: 3 }}
        xxl={{ span: 9, offset: 8 }}
      >
        <RouteLeavingGuard
          navigate={(path: string) => history.push(path)}
          shouldBlockNavigation={() => {
            return isLeavingPageBlocked;
          }}
          title={t('zeit.newElementGeneral.confirmModal.title')}
          btnOk={t('zeit.newElementGeneral.confirmModal.btnCancelConfirmOk')}
          btnCancel={t('zeit.newElementGeneral.confirmModal.btnCancelConfirmNo')}
        />
        <GeneralFormWrapper
          initialValues={initialValues}
          form={form}
          layout="vertical"
          scrollToFirstError={true}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          className="rv-form"
          onValuesChange={onFormValuesChanged}
          noValidate
        >
          <CombinedTitle
            title={props.element?.titel}
            suffix={t(`zeit.newElementGeneral.action.${props.action}.mainTitle`, {
              type: props.element?.termin ? 'Termin' : 'Phase',
            })}
          />
          <p className="ant-typography p-no-style">
            Pflichtfelder sind mit einem <sup>*</sup> gekennzeichnet.
          </p>
          <Form.Item shouldUpdate noStyle>
            {() => <ErrorBox fieldsErrors={form.getFieldsError()} />}
          </Form.Item>
          <TitleAndOrderComponent
            isTermin={isTermin}
            setIsTermin={setIsTermin}
            isCustomElement={isCustomElement}
            setIsCustomElement={seIsCustomElement}
            form={form}
            possibleVorlagen={possibleVorlagen}
            selectedVorlageBauplan={selectedVorlageBauplan}
            setSelectedVorlageBauplan={setSelectedVorlageBauplan}
            setSelectedVorgaengerId={setSelectedVorgaengerId}
            elements={props.currentZeitParent?.dto.elemente}
            isModifyAction={isModifyAction}
            element={props.element}
          />
          <DurationComponent
            form={form}
            isTermin={isTermin}
            isZeitplanungElement={props.isZeitplanungElement}
            selectedVorlageBauplan={selectedVorlageBauplan}
            elements={props.currentZeitParent?.dto.elemente}
            elementId={props.elementId}
          />
          {displayPeriodOrTerminComp()}
          {<RepositionElementsComponent />}
          <MoreInformationComponent
            form={form}
            isTermin={isTermin}
            isZeitplanungElement={props.isZeitplanungElement}
            currentZeitParent={props.currentZeitParent}
            isLight={props.isLight}
            element={props.element}
          />
          <div className="form-control-buttons">
            <Button id="ezeit-newElementGeneralSubmit-btn" type="primary" htmlType="submit" size={'large'}>
              {t('zeit.newElementGeneral.submit')}
            </Button>

            <Button
              id="ezeit-newElementGeneralCancel-btn"
              onClick={() => {
                history.push(`/zeit/${tab}/${props.parentId}/${props.viewMode}`);
              }}
              size={'large'}
            >
              {t('zeit.newElementGeneral.cancel')}
            </Button>
          </div>
        </GeneralFormWrapper>
      </Col>
    </Row>
  );
}
