// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Observable } from 'rxjs';
import { AjaxError } from 'rxjs/ajax';

import { ZeitplanungControllerApi, ZeitplanungsvorlageControllerApi } from '@plateg/rest-api';
import {
  PhaseEntityDTO,
  PhasenvorlageEntityDTO,
  RegelungsvorhabenTypType,
  TerminEntityDTO,
  TerminvorlageEntityDTO,
  ZeitplanungEntityResponseDTO,
  ZeitplanungselementEntityDTO,
  ZeitplanungsvorlageEntityResponseDTO,
  ZeitvorlagevorlagenBauplanDTO,
} from '@plateg/rest-api/models';
import { ErrorController, LoadingStatusController } from '@plateg/theme';
import { GlobalDI } from '@plateg/theme/src/shares';
import { Filters } from '@plateg/theme/src/shares/filters';

import { ZeitLighBackendController } from '../../../../../zeit-light/controller-backend';
import { ZeitLightController } from '../../../../../zeit-light/controller-light';
import { ZeitplanungsElement, ZeitplanungsvorlageElement } from './component.react';

export class NewZeitElementController {
  private readonly zeitLighBackendController = GlobalDI.getOrRegister(
    'zeitLighBackendController',
    () => new ZeitLighBackendController(),
  );
  private readonly errorCallback = (error: AjaxError) => {
    const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
    const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
    loadingStatusController.setLoadingStatus(false);
    console.error(error, error);
    errorCtrl.displayErrorMsg(error, 'enorm.generalErrorMsg');
  };

  private addDateToZeitplanungsvorlageCall(
    this: void,
    date: {
      id: string;
      terminvorlageEntityDTO: TerminvorlageEntityDTO;
    },
  ): Observable<void> {
    const zeitplanungsvorlageController = GlobalDI.get<ZeitplanungsvorlageControllerApi>(
      'zeitplanunsvorlageRestController',
    );
    return zeitplanungsvorlageController.addDateToZeitplanungsvorlage(date);
  }

  private addDateToZeitplanungsvorlageCallLight(
    this: void,
    date: {
      id: string;
      terminvorlageEntityDTO: TerminvorlageEntityDTO;
    },
  ): Observable<void> {
    const zeitLightController = GlobalDI.getOrRegister('zeitLightController', () => new ZeitLightController());
    return zeitLightController.addDateToZeitplanungsvorlageLight(date);
  }

  private addPhaseToZeitplanungsvorlageCall(
    this: void,
    date: {
      id: string;
      phasenvorlageEntityDTO: PhasenvorlageEntityDTO;
    },
  ): Observable<void> {
    const zeitplanungsvorlageController = GlobalDI.get<ZeitplanungsvorlageControllerApi>(
      'zeitplanunsvorlageRestController',
    );
    return zeitplanungsvorlageController.addPhaseToZeitplanungsvorlage(date);
  }

  private addPhaseToZeitplanungsvorlageCallLight(
    this: void,
    date: {
      id: string;
      phasenvorlageEntityDTO: PhasenvorlageEntityDTO;
    },
  ): Observable<void> {
    const zeitLightController = GlobalDI.getOrRegister('zeitLightController', () => new ZeitLightController());
    return zeitLightController.addPhaseToZeitplanungsvorlageLight(date);
  }

  private addDateToZeitplanungCall(
    this: void,
    date: {
      id: string;
      terminEntityDTO: TerminEntityDTO;
    },
  ): Observable<void> {
    const zeitplanungController = GlobalDI.get<ZeitplanungControllerApi>('zeitplanungRestController');
    return zeitplanungController.addDateToZeitplanung(date);
  }

  private addDateToZeitplanungCallLight(
    this: void,
    date: {
      id: string;
      terminEntityDTO: TerminEntityDTO;
    },
  ): Observable<void> {
    const zeitLightController = GlobalDI.getOrRegister('zeitLightController', () => new ZeitLightController());
    return zeitLightController.addDateToZeitplanungLight(date);
  }

  private addPhaseToZeitplanungCall(
    this: void,
    date: {
      id: string;
      phaseEntityDTO: PhaseEntityDTO;
    },
  ): Observable<void> {
    const zeitplanungController = GlobalDI.get<ZeitplanungControllerApi>('zeitplanungRestController');
    return zeitplanungController.addPhaseToZeitplanung(date);
  }

  private addPhaseToZeitplanungCallLight(
    this: void,
    date: {
      id: string;
      phaseEntityDTO: PhaseEntityDTO;
    },
  ): Observable<void> {
    const zeitLightController = GlobalDI.getOrRegister('zeitLightController', () => new ZeitLightController());
    return zeitLightController.addPhaseToZeitplanungLight(date);
  }
  public modifyDateToZeitplanungsvorlageCall(
    this: void,
    date: {
      elementId: string;
      terminvorlageEntityDTO: TerminvorlageEntityDTO;
      vorlageId: string | undefined;
    },
  ): Observable<void> {
    const zeitplanungsvorlageController = GlobalDI.get<ZeitplanungsvorlageControllerApi>(
      'zeitplanunsvorlageRestController',
    );
    return zeitplanungsvorlageController.modifyDateInZeitplanungsvorlage(date);
  }

  public modifyDateToZeitplanungsvorlageCallLight(
    this: void,
    date: {
      elementId: string;
      terminvorlageEntityDTO: TerminvorlageEntityDTO;
      vorlageId: string | undefined;
    },
  ): Observable<void> {
    const zeitLightController = GlobalDI.getOrRegister('zeitLightController', () => new ZeitLightController());
    return zeitLightController.modifyDateToZeitplanungsvorlageLight(date);
  }

  private modifyPhaseToZeitplanungsvorlageCall(
    this: void,
    date: {
      elementId: string;
      phasenvorlageEntityDTO: PhasenvorlageEntityDTO;
      vorlageId: string | undefined;
    },
  ): Observable<void> {
    const zeitplanungsvorlageController = GlobalDI.get<ZeitplanungsvorlageControllerApi>(
      'zeitplanunsvorlageRestController',
    );
    return zeitplanungsvorlageController.modifyPhaseInZeitplanungsvorlage(date);
  }

  private modifyPhaseToZeitplanungsvorlageCallLight = (date: {
    elementId: string;
    phasenvorlageEntityDTO: PhasenvorlageEntityDTO;
    vorlageId: string | undefined;
  }): Observable<void> => {
    const zeitLightController = GlobalDI.getOrRegister('zeitLightController', () => new ZeitLightController());
    return zeitLightController.modifyPhaseToZeitplanungsvorlageCallLight(date);
  };

  private modifyDateInZeitplanungCall(
    this: void,
    date: {
      elementId: string;
      terminEntityDTO: TerminEntityDTO;
    },
  ): Observable<void> {
    const zeitplanungController = GlobalDI.get<ZeitplanungControllerApi>('zeitplanungRestController');
    return zeitplanungController.modifyDateInZeitplanung(date);
  }

  private modifyDateInZeitplanungCallLight(
    this: void,
    date: {
      elementId: string;
      terminEntityDTO: TerminEntityDTO;
      zeitplanungId: string;
    },
  ): Observable<void> {
    const zeitLightController = GlobalDI.getOrRegister('zeitLightController', () => new ZeitLightController());
    return zeitLightController.modifyDateInZeitplanungCallLight(date);
  }

  private modifyPhaseInZeitplanungCall(
    this: void,
    date: {
      elementId: string;
      phaseEntityDTO: PhaseEntityDTO;
    },
  ): Observable<void> {
    const zeitplanungController = GlobalDI.get<ZeitplanungControllerApi>('zeitplanungRestController');
    return zeitplanungController.modifyPhaseInZeitplanung(date);
  }

  private modifyPhaseInZeitplanungCallLight(
    this: void,
    date: {
      elementId: string;
      phaseEntityDTO: PhaseEntityDTO;
      zeitplanungId: string;
    },
  ): Observable<void> {
    const zeitLightController = GlobalDI.getOrRegister('zeitLightController', () => new ZeitLightController());
    return zeitLightController.modifyPhaseInZeitplanungCallLight(date);
  }

  public getZeitvorlagenvorlagenForVorhabenartCall = (
    vorhabenart: RegelungsvorhabenTypType,
  ): Observable<Array<ZeitvorlagevorlagenBauplanDTO>> => {
    const zeitplanungsvorlageController = GlobalDI.get<ZeitplanungsvorlageControllerApi>(
      'zeitplanunsvorlageRestController',
    );
    return zeitplanungsvorlageController.getBauplaeneForVorhabenart({ vorhabenart });
  };

  private checkDefinedValue = (arg: unknown, resTruthy: unknown, resFalsy: unknown) => (arg ? resTruthy : resFalsy);

  public saveVorlageElement = (
    callBack: Function,
    modify: boolean,
    id?: string,
    element?: PhasenvorlageEntityDTO | TerminvorlageEntityDTO,
    isLight?: boolean,
    vorlageId?: string,
  ): void => {
    if (element && id) {
      const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
      loadingStatusController.setLoadingStatus(true);
      if (element.termin) {
        //cast as termin, because property termin is true
        const terminElement = element as TerminvorlageEntityDTO;
        const newTermin: TerminvorlageEntityDTO = {
          termin: element.termin,
          titel: element.titel,
          wichtig: terminElement.wichtig,
          vorherigeElementeIds: element.vorherigeElementeIds || [],
          uebergeordneteElementeId: element.uebergeordneteElementeId,
          bauplan: element.bauplan,
          kommentar: element.kommentar,
          hinweise: element.hinweise,
        };

        const modifyCall = this.checkDefinedValue(
          isLight,
          this.modifyDateToZeitplanungsvorlageCallLight,
          this.modifyDateToZeitplanungsvorlageCall,
        ) as (
          this: void,
          date: {
            elementId: string;
            terminvorlageEntityDTO: TerminvorlageEntityDTO;
            vorlageId: string | undefined;
          },
        ) => Observable<void>;

        const addDateCall = this.checkDefinedValue(
          isLight,
          this.addDateToZeitplanungsvorlageCallLight,
          this.addDateToZeitplanungsvorlageCall,
        ) as (
          this: void,
          date: {
            id: string;
            terminvorlageEntityDTO: TerminvorlageEntityDTO;
          },
        ) => Observable<void>;

        const call = modify
          ? modifyCall({
              elementId: id,
              terminvorlageEntityDTO: newTermin,
              vorlageId: vorlageId,
            })
          : addDateCall({
              id,
              terminvorlageEntityDTO: newTermin,
            });

        call.subscribe({
          next: () => {
            callBack();
          },
          error: this.errorCallback,
        });
      } else {
        //cast as phase, because property termin is false
        const phasenElement = element as PhasenvorlageEntityDTO;
        const newPhase: PhasenvorlageEntityDTO = {
          termin: element.termin,
          titel: element.titel,
          vorherigeElementeIds: element.vorherigeElementeIds || [],
          uebergeordneteElementeId: element.uebergeordneteElementeId,
          untergeordneteElemente: [],
          bauplan: element.bauplan,
          kommentar: element.kommentar,
          dauer: phasenElement.dauer,
          wichtig: false,
          hinweise: element.hinweise,
        };

        const modifyCall = this.checkDefinedValue(
          isLight,
          this.modifyPhaseToZeitplanungsvorlageCallLight,
          this.modifyPhaseToZeitplanungsvorlageCall,
        ) as (date: {
          elementId: string;
          phasenvorlageEntityDTO: PhasenvorlageEntityDTO;
          vorlageId: string | undefined;
        }) => Observable<void>;

        const addDateCall = this.checkDefinedValue(
          isLight,
          this.addPhaseToZeitplanungsvorlageCallLight,
          this.addPhaseToZeitplanungsvorlageCall,
        ) as (
          this: void,
          date: {
            id: string;
            phasenvorlageEntityDTO: PhasenvorlageEntityDTO;
          },
        ) => Observable<void>;

        const call = modify
          ? modifyCall({
              elementId: id,
              phasenvorlageEntityDTO: newPhase,
              vorlageId: vorlageId,
            })
          : addDateCall({
              id,
              phasenvorlageEntityDTO: newPhase,
            });

        call.subscribe({
          next: () => {
            callBack();
          },
          error: this.errorCallback,
        });
      }
    }
  };

  public saveElement = (
    callBack: Function,
    modify: boolean,
    id?: string,
    element?: ZeitplanungsElement,
    isLight?: boolean,
    zeitplanungId?: string,
  ): void => {
    if (element && id) {
      const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
      loadingStatusController.setLoadingStatus(true);
      if (element.termin) {
        //cast as termin, because property termin is true
        const terminElement = element as TerminEntityDTO;
        const newTermin: TerminEntityDTO = {
          termin: element.termin,
          titel: element.titel,
          wichtig: (element as TerminEntityDTO).wichtig,
          gesperrt: (element as TerminEntityDTO).gesperrt,
          vorherigeElementeIds: element.vorherigeElementeIds || [],
          uebergeordneteElementeId: element.uebergeordneteElementeId,
          bauplan: element.bauplan,
          kommentar: element.kommentar,
          datum: Filters.shortIsoDateFromString(terminElement.datum),
          zeitplanungAnpassen: element.zeitplanungAnpassen ?? false,
          hinweise: element.hinweise,
        };

        const modifyCall = this.checkDefinedValue(
          isLight,
          this.modifyDateInZeitplanungCallLight,
          this.modifyDateInZeitplanungCall,
        ) as (date: {
          elementId: string;
          terminEntityDTO: ZeitplanungselementEntityDTO;
          zeitplanungId: string | undefined;
        }) => Observable<void>;

        const addDateCall = this.checkDefinedValue(
          isLight,
          this.addDateToZeitplanungCallLight,
          this.addDateToZeitplanungCall,
        ) as (
          this: void,
          date: {
            id: string;
            terminEntityDTO: ZeitplanungselementEntityDTO;
          },
        ) => Observable<void>;
        const call = modify
          ? modifyCall({
              elementId: id,
              terminEntityDTO: newTermin,
              zeitplanungId,
            })
          : addDateCall({
              id,
              terminEntityDTO: newTermin,
            });

        call.subscribe({
          next: () => {
            callBack();
          },
          error: this.errorCallback,
        });
      } else {
        //cast as phase, because property termin is false
        const phasenElement = element as PhaseEntityDTO;
        const newPhase: PhaseEntityDTO = {
          termin: element.termin,
          titel: element.titel,
          vorherigeElementeIds: element.vorherigeElementeIds || [],
          uebergeordneteElementeId: element.uebergeordneteElementeId,
          untergeordneteElemente: [],
          bauplan: element.bauplan,
          kommentar: element.kommentar,
          wichtig: false,
          gesperrt: (element as TerminEntityDTO).gesperrt,
          beginn: Filters.shortIsoDateFromString(phasenElement.beginn),
          ende: Filters.shortIsoDateFromString(phasenElement.ende),
          zeitplanungAnpassen: element.zeitplanungAnpassen ?? false,
          hinweise: element.hinweise,
        };

        const modifyCall = this.checkDefinedValue(
          isLight,
          this.modifyPhaseInZeitplanungCallLight,
          this.modifyPhaseInZeitplanungCall,
        ) as (date: {
          elementId: string;
          phaseEntityDTO: PhaseEntityDTO;
          zeitplanungId: string | undefined;
        }) => Observable<void>;

        const addDateCall = this.checkDefinedValue(
          isLight,
          this.addPhaseToZeitplanungCallLight,
          this.addPhaseToZeitplanungCall,
        ) as (
          this: void,
          date: {
            id: string;
            phaseEntityDTO: PhaseEntityDTO;
          },
        ) => Observable<void>;

        const call = modify
          ? modifyCall({
              elementId: id,
              phaseEntityDTO: newPhase,
              zeitplanungId,
            })
          : addDateCall({
              id,
              phaseEntityDTO: newPhase,
            });
        call.subscribe({
          next: () => {
            callBack();
          },
          error: this.errorCallback,
        });
      }
    }
  };

  public getInitialDataObservable = (
    isZeitplanungElement?: boolean,
    currentZeitParent?: ZeitplanungsvorlageEntityResponseDTO | ZeitplanungEntityResponseDTO,
    isLight?: boolean,
  ): Observable<ZeitvorlagevorlagenBauplanDTO[]> => {
    const art =
      isZeitplanungElement && !isLight
        ? (currentZeitParent as ZeitplanungEntityResponseDTO).dto.regelungsvorhaben?.dto.vorhabenart
        : (currentZeitParent as ZeitplanungsvorlageEntityResponseDTO).dto.vorhabenart;

    const apiCall = isLight
      ? this.zeitLighBackendController.getBauplaeneForVorhabenartLightCall
      : this.getZeitvorlagenvorlagenForVorhabenartCall;

    return apiCall(art);
  };

  public getInitialDatesValues = (
    isZeitplanungElement: boolean | undefined,
    isTermin: boolean,
    element: ZeitplanungsvorlageElement | ZeitplanungsElement | undefined,
  ) => {
    return {
      beginn:
        isZeitplanungElement && !isTermin && element && (element as PhaseEntityDTO).beginn
          ? new Date((element as PhaseEntityDTO).beginn)
          : undefined,
      ende:
        isZeitplanungElement && !isTermin && element && (element as PhaseEntityDTO).ende
          ? new Date((element as PhaseEntityDTO).ende)
          : undefined,
      datum: isZeitplanungElement && isTermin && element ? new Date((element as TerminEntityDTO).datum) : undefined,
      dauer:
        !isZeitplanungElement && !isTermin && element
          ? {
              months: (element as PhasenvorlageEntityDTO).dauer.months ?? 0,
              weeks: (element as PhasenvorlageEntityDTO).dauer.weeks ?? 0,
              days: (element as PhasenvorlageEntityDTO).dauer.days ?? 0,
            }
          : undefined,
    };
  };
}
