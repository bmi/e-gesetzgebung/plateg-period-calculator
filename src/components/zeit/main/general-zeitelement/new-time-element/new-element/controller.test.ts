// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import chai, { assert, expect } from 'chai';
import chaiString from 'chai-string';
import { Observable } from 'rxjs';
import sinon from 'sinon';

import {
  Configuration,
  PhaseEntityDTO,
  PhasenvorlageEntityDTO,
  RegelungsvorhabenTypType,
  TerminEntityDTO,
  ZeitplanungControllerApi,
  ZeitplanungsvorlageControllerApi,
} from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme/src/shares';

import { setLoadingStatusStub } from '../../../../../../general.test';
import { NewZeitElementController } from './controller';
chai.use(chaiString);

const newZeitElementController = GlobalDI.getOrRegister(
  'newZeitElementController',
  () => new NewZeitElementController(),
);
const zeitplanungRestController = GlobalDI.getOrRegister(
  'zeitplanungRestController',
  () => new ZeitplanungControllerApi(new Configuration()),
);

const zeitplanungsvorlageController = GlobalDI.getOrRegister(
  'zeitplanunsvorlageRestController',
  () => new ZeitplanungsvorlageControllerApi(new Configuration()),
);

describe('TEST: getZeitvorlagenvorlagenForVorhabenartCall', () => {
  let getZeitvorlagenvorlagenForVorhabenartCallStub: sinon.SinonStub;

  before(() => {
    getZeitvorlagenvorlagenForVorhabenartCallStub = sinon.stub(
      zeitplanungsvorlageController,
      'getBauplaeneForVorhabenart',
    );
  });
  after(() => {
    getZeitvorlagenvorlagenForVorhabenartCallStub.restore();
  });

  it('check if REST API is called', () => {
    newZeitElementController.getZeitvorlagenvorlagenForVorhabenartCall(RegelungsvorhabenTypType.Gesetz);
    sinon.assert.calledOnce(getZeitvorlagenvorlagenForVorhabenartCallStub);
  });
});

describe('TEST: saveVorlageElement', () => {
  let addDateToZeitplanungsvorlageStub: sinon.SinonStub;
  let addPhaseToZeitplanungsvorlageStub: sinon.SinonStub;
  let modifyDateInZeitplanungsvorlageStub: sinon.SinonStub;
  let modifyPhaseInZeitplanungsvorlageStub: sinon.SinonStub;
  let callBackStub: sinon.SinonStub;
  const phase = {
    id: 'test',
    titel: 'Termin',
    wichtig: false,
    termin: false,
    vorherigeElementeIds: [],
    untergeordneteZeitvorlagen: [],
  };
  const termin = {
    id: 'test2',
    titel: 'Phase',
    wichtig: false,
    termin: true,
    vorherigeElementeIds: [],
    untergeordneteZeitvorlagen: [],
  };

  before(() => {
    addPhaseToZeitplanungsvorlageStub = sinon
      .stub(zeitplanungsvorlageController, 'addPhaseToZeitplanungsvorlage')
      .returns(
        new Observable(function (observer) {
          observer.next();
          observer.complete();
        }),
      );
    addDateToZeitplanungsvorlageStub = sinon
      .stub(zeitplanungsvorlageController, 'addDateToZeitplanungsvorlage')
      .returns(
        new Observable(function (observer) {
          observer.next();
          observer.complete();
        }),
      );
    modifyPhaseInZeitplanungsvorlageStub = sinon
      .stub(zeitplanungsvorlageController, 'modifyPhaseInZeitplanungsvorlage')
      .returns(
        new Observable(function (observer) {
          observer.next();
          observer.complete();
        }),
      );
    modifyDateInZeitplanungsvorlageStub = sinon
      .stub(zeitplanungsvorlageController, 'modifyDateInZeitplanungsvorlage')
      .returns(
        new Observable(function (observer) {
          observer.next();
          observer.complete();
        }),
      );
    callBackStub = sinon.stub();
  });
  after(() => {
    addPhaseToZeitplanungsvorlageStub.restore();
    addDateToZeitplanungsvorlageStub.restore();
    modifyDateInZeitplanungsvorlageStub.restore();
    modifyPhaseInZeitplanungsvorlageStub.restore();
    setLoadingStatusStub.resetHistory();
  });
  afterEach(() => {
    addPhaseToZeitplanungsvorlageStub.resetHistory();
    addDateToZeitplanungsvorlageStub.resetHistory();
    modifyDateInZeitplanungsvorlageStub.resetHistory();
    modifyPhaseInZeitplanungsvorlageStub.resetHistory();
    setLoadingStatusStub.resetHistory();
    callBackStub.resetHistory();
  });

  it('undefined element, nothing should be called', () => {
    newZeitElementController.saveVorlageElement(callBackStub, false, 'abc');
    sinon.assert.notCalled(addPhaseToZeitplanungsvorlageStub);
    sinon.assert.notCalled(addDateToZeitplanungsvorlageStub);
    sinon.assert.notCalled(callBackStub);
  });

  it('phase, callback + correct creation call should be called', () => {
    newZeitElementController.saveVorlageElement(callBackStub, false, 'abc', phase);
    sinon.assert.calledOnce(addPhaseToZeitplanungsvorlageStub);
    sinon.assert.calledOnce(callBackStub);
    sinon.assert.calledOnce(setLoadingStatusStub);
    sinon.assert.notCalled(addDateToZeitplanungsvorlageStub);
  });

  it('termin, callback + correct creation call should be called', () => {
    newZeitElementController.saveVorlageElement(callBackStub, false, 'abc', termin);
    sinon.assert.calledOnce(addDateToZeitplanungsvorlageStub);
    sinon.assert.calledOnce(callBackStub);
    sinon.assert.calledOnce(setLoadingStatusStub);
    sinon.assert.notCalled(addPhaseToZeitplanungsvorlageStub);
  });

  it('modify phase, callback + correct creation call should be called', () => {
    newZeitElementController.saveVorlageElement(callBackStub, true, 'abc', phase);
    sinon.assert.calledOnce(modifyPhaseInZeitplanungsvorlageStub);
    sinon.assert.calledOnce(callBackStub);
    sinon.assert.calledOnce(setLoadingStatusStub);
    sinon.assert.notCalled(modifyDateInZeitplanungsvorlageStub);
  });

  it('modify termin, callback + correct creation call should be called', () => {
    newZeitElementController.saveVorlageElement(callBackStub, true, 'abc', termin);
    sinon.assert.calledOnce(modifyDateInZeitplanungsvorlageStub);
    sinon.assert.calledOnce(callBackStub);
    sinon.assert.calledOnce(setLoadingStatusStub);
    sinon.assert.notCalled(modifyPhaseInZeitplanungsvorlageStub);
  });
});

describe('TEST: saveElement', () => {
  let addDateToZeitplanungeStub: sinon.SinonStub;
  let addPhaseToZeitplanungStub: sinon.SinonStub;
  let modifyDateInZeitplanungeStub: sinon.SinonStub;
  let modifyPhaseInZeitplanungStub: sinon.SinonStub;
  let callBackStub: sinon.SinonStub;
  const phase: PhaseEntityDTO = {
    titel: 'Termin',
    wichtig: false,
    termin: false,
    vorherigeElementeIds: [],
    zeitplanungAnpassen: false,
  };
  const termin: TerminEntityDTO = {
    titel: 'Phase',
    wichtig: false,
    termin: true,
    vorherigeElementeIds: [],
    datum: '01.01.2001',
    zeitplanungAnpassen: true,
  };

  before(() => {
    addPhaseToZeitplanungStub = sinon.stub(zeitplanungRestController, 'addPhaseToZeitplanung').returns(
      new Observable(function (observer) {
        observer.next();
        observer.complete();
      }),
    );
    addDateToZeitplanungeStub = sinon.stub(zeitplanungRestController, 'addDateToZeitplanung').returns(
      new Observable(function (observer) {
        observer.next();
        observer.complete();
      }),
    );
    modifyPhaseInZeitplanungStub = sinon.stub(zeitplanungRestController, 'modifyPhaseInZeitplanung').returns(
      new Observable(function (observer) {
        observer.next();
        observer.complete();
      }),
    );
    modifyDateInZeitplanungeStub = sinon.stub(zeitplanungRestController, 'modifyDateInZeitplanung').returns(
      new Observable(function (observer) {
        observer.next();
        observer.complete();
      }),
    );
    callBackStub = sinon.stub();
  });
  after(() => {
    addPhaseToZeitplanungStub.restore();
    addDateToZeitplanungeStub.restore();
    modifyDateInZeitplanungeStub.restore();
    modifyPhaseInZeitplanungStub.restore();
    setLoadingStatusStub.resetHistory();
  });
  afterEach(() => {
    addPhaseToZeitplanungStub.resetHistory();
    addDateToZeitplanungeStub.resetHistory();
    modifyDateInZeitplanungeStub.resetHistory();
    modifyPhaseInZeitplanungStub.resetHistory();
    setLoadingStatusStub.resetHistory();
    callBackStub.resetHistory();
  });

  it('undefined element, nothing should be called', () => {
    newZeitElementController.saveElement(callBackStub, false, 'abc');
    sinon.assert.notCalled(addPhaseToZeitplanungStub);
    sinon.assert.notCalled(addDateToZeitplanungeStub);
    sinon.assert.notCalled(callBackStub);
  });

  it('phase, callback + correct creation call should be called', () => {
    newZeitElementController.saveElement(callBackStub, false, 'abc', phase);
    sinon.assert.calledOnce(addPhaseToZeitplanungStub);
    sinon.assert.calledOnce(callBackStub);
    sinon.assert.calledOnce(setLoadingStatusStub);
    sinon.assert.notCalled(addDateToZeitplanungeStub);
  });

  it('termin, callback + correct creation call should be called', () => {
    newZeitElementController.saveElement(callBackStub, false, 'abc', termin);
    sinon.assert.calledOnce(addDateToZeitplanungeStub);
    sinon.assert.calledOnce(callBackStub);
    sinon.assert.calledOnce(setLoadingStatusStub);
    sinon.assert.notCalled(addPhaseToZeitplanungStub);
  });

  it('modify phase, callback + correct creation call should be called', () => {
    newZeitElementController.saveElement(callBackStub, true, 'abc', phase);
    sinon.assert.calledOnce(modifyPhaseInZeitplanungStub);
    sinon.assert.calledOnce(callBackStub);
    sinon.assert.calledOnce(setLoadingStatusStub);
    sinon.assert.notCalled(addPhaseToZeitplanungStub);
  });

  it('modify termin, callback + correct creation call should be called', () => {
    newZeitElementController.saveElement(callBackStub, true, 'abc', termin);
    sinon.assert.calledOnce(modifyDateInZeitplanungeStub);
    sinon.assert.calledOnce(callBackStub);
    sinon.assert.calledOnce(setLoadingStatusStub);
    sinon.assert.notCalled(addDateToZeitplanungeStub);
  });
});

describe('TEST: getInitialDatesValues', () => {
  const phase: PhaseEntityDTO = {
    termin: false,
    wichtig: false,
    titel: 'Phase',
    vorherigeElementeIds: [],
    zeitplanungAnpassen: false,
    beginn: '2021-09-04',
    ende: '2021-09-26',
    datum: '2021-09-26',
  };
  const termin: TerminEntityDTO = {
    titel: 'Termin',
    wichtig: false,
    termin: true,
    vorherigeElementeIds: [],
    datum: '01.01.2001',
    zeitplanungAnpassen: true,
  };

  const phasenvorlage: PhasenvorlageEntityDTO = {
    termin: false,
    wichtig: false,
    titel: 'Befassung Hausleitung',
    vorherigeElementeIds: [],
    kommentar: 'main phase',
    dauer: { months: 0, weeks: 4, days: 0 },
  };

  it('has no element', () => {
    const initialDate = newZeitElementController.getInitialDatesValues(true, true, undefined);
    expect(initialDate.beginn).to.eql(undefined);
    expect(initialDate.ende).to.eql(undefined);
    expect(initialDate.datum).to.eql(undefined);
    expect(initialDate.dauer).to.eql(undefined);
  });

  it('has dauer', () => {
    const initialDate = newZeitElementController.getInitialDatesValues(false, false, phasenvorlage);
    expect(initialDate.beginn).to.eql(undefined);
    expect(initialDate.ende).to.eql(undefined);
    expect(initialDate.datum).to.eql(undefined);
    expect(initialDate.dauer).to.eql({
      months: 0,
      weeks: 4,
      days: 0,
    });
  });

  it('has datum', () => {
    const initialDate = newZeitElementController.getInitialDatesValues(true, true, termin);
    expect(initialDate.beginn).to.eql(undefined);
    expect(initialDate.ende).to.eql(undefined);
    expect(initialDate.datum).to.eql(new Date(termin.datum as string));
    expect(initialDate.dauer).to.eql(undefined);
  });

  it('has beginn and end', () => {
    const initialDate = newZeitElementController.getInitialDatesValues(true, false, phase);
    expect(initialDate.beginn).to.eql(new Date(phase.beginn as string));
    expect(initialDate.ende).to.eql(new Date(phase.ende as string));
    expect(initialDate.datum).to.eql(undefined);
    expect(initialDate.dauer).to.eql(undefined);
  });
});
