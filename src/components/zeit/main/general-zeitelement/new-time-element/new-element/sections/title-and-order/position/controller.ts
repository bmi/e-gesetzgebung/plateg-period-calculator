// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import i18n from 'i18next';
import { DataNode } from 'rc-tree/lib/interface';

import {
  PhasenvorlageEntityResponseDTO,
  ZeitplanungselementEntityResponseDTOZeitplanungselementEntityDTO,
  ZeitplanungsvorlagenBauplan,
  ZeitvorlagevorlagenBauplanDTO,
} from '@plateg/rest-api/models';

import { ZeitplanungsvorlageElement } from '../../../component.react';
export interface TreeNode extends DataNode {
  data: {
    vorherigeElementeIds: string[];
    uebergeordneteElementeId: string | null;
    vorherigerBauplan: ZeitplanungsvorlagenBauplan | null;
    uebergeordneterBauplan: ZeitplanungsvorlagenBauplan | null;
  };
}
export class PositionController {
  public getTreeData = (
    zeitvorlagen?: ZeitplanungselementEntityResponseDTOZeitplanungselementEntityDTO[],
    toBeExcludedElement?: ZeitplanungsvorlageElement,
  ): TreeNode[] => {
    return [
      {
        title: i18n.t('zeit.newElementGeneral.titleAndOrder.position.firstElement'),
        key: '0',
        value: '0',
        data: {
          vorherigeElementeIds: [],
          uebergeordneteElementeId: null,
          vorherigerBauplan: null,
          uebergeordneterBauplan: null,
        },
      } as TreeNode,
      ...this.createTreeData(zeitvorlagen, undefined, toBeExcludedElement),
    ];
  };

  private readonly createTreeData = (
    zeitvorlagen?: ZeitplanungselementEntityResponseDTOZeitplanungselementEntityDTO[],
    parentBauplan?: ZeitplanungsvorlagenBauplan,
    toBeExcludedElement?: ZeitplanungsvorlageElement,
  ): TreeNode[] => {
    if (!zeitvorlagen) {
      return [];
    }
    return zeitvorlagen
      .map((element) => {
        if (
          element.dto.uebergeordneteElementeId === toBeExcludedElement?.uebergeordneteElementeId &&
          element.dto.vorherigeElementeIds?.length === toBeExcludedElement?.vorherigeElementeIds?.length &&
          element.dto.vorherigeElementeIds.every((id, index) => id === toBeExcludedElement?.vorherigeElementeIds[index])
        ) {
          return undefined;
        }
        const isTermin = element.dto.termin;
        return {
          key: element.base.id,
          title: `Nach: ${isTermin ? 'Termin' : 'Phase'} ${element.dto.titel}`,
          value: element.base.id,
          data: {
            vorherigeElementeIds: [element.base.id],
            uebergeordneteElementeId: element.dto.uebergeordneteElementeId ?? null,
            vorherigerBauplan: element.dto.bauplan || null,
            uebergeordneterBauplan: parentBauplan || null,
          },
          children: isTermin
            ? undefined
            : [
                {
                  key: `${element.base.id}-beginning`,
                  value: `${element.base.id}-beginning`,
                  data: {
                    vorherigeElementeIds: [],
                    uebergeordneteElementeId: element.base.id,
                    vorherigerBauplan: null,
                    uebergeordneterBauplan: element.dto.bauplan || null,
                  },
                  title: i18n.t('zeit.newElementGeneral.titleAndOrder.position.firstElementInPhase'),
                },
                ...this.createTreeData(
                  (element as PhasenvorlageEntityResponseDTO).dto.untergeordneteElemente,
                  element.dto.bauplan,
                  toBeExcludedElement,
                ),
              ],
        };
      })
      .filter((elem) => elem !== undefined) as TreeNode[];
  };

  public findNodeForVorlage = (
    nodes: TreeNode[],
    selectedVorlageBauplan?: ZeitvorlagevorlagenBauplanDTO | undefined,
  ): TreeNode | undefined => {
    if (!nodes.length || !selectedVorlageBauplan) {
      return undefined;
    }
    return (
      //default/preferred: find previous node and return it -> the new element can be inserted in the right position
      //fallback: if previous node is not present, look if element of bauplan is already present and return it -> the new element can be inserted after the element of the same bauplan
      nodes.find(
        (node) =>
          (node.data.uebergeordneterBauplan === selectedVorlageBauplan.uebergeordnetesElement &&
            node.data.vorherigerBauplan === selectedVorlageBauplan.vorherigesElement) ||
          node.data.vorherigerBauplan === selectedVorlageBauplan.bauplan,
      ) ||
      // if nothing is found, check children
      this.findNodeForVorlage(
        nodes.flatMap((node) => (node.children as TreeNode[]) || []),
        selectedVorlageBauplan,
      ) ||
      undefined
    );
  };

  public findNodeForEntity = (
    nodes: TreeNode[],
    element?: ZeitplanungsvorlageElement | undefined,
  ): TreeNode | undefined => {
    if (!nodes.length || !element) {
      return undefined;
    }
    return (
      nodes.find(
        (node) =>
          node.data.uebergeordneteElementeId === element.uebergeordneteElementeId &&
          node.data.vorherigeElementeIds?.length === element.vorherigeElementeIds?.length &&
          node.data.vorherigeElementeIds.every((id, index) => id === element.vorherigeElementeIds[index]),
      ) ||
      // if nothing is found, check children
      this.findNodeForEntity(
        nodes.flatMap((node) => (node.children as TreeNode[]) || []),
        element,
      ) ||
      undefined
    );
  };
  public findNodeTitleById = (
    nodes: TreeNode[],
    id: string,
  ): { node: TreeNode; order: number; length: number } | null => {
    if (!nodes.length || !id) {
      return null;
    }
    const foundNode = nodes.find((node) => node.key === id);
    if (foundNode) {
      return { node: foundNode, order: nodes.indexOf(foundNode) + 1, length: nodes.length };
    } else {
      let foundSubNode = { node: {} as TreeNode, order: 0, length: 0 };
      for (const element of nodes) {
        if (element.children) {
          const childrenNode = this.findNodeTitleById((element.children as TreeNode[]) || [], id);
          if (childrenNode) {
            foundSubNode = childrenNode;
          }
        }
      }
      if (foundSubNode.length) {
        return foundSubNode;
      } else {
        return null;
      }
    }
  };
}
