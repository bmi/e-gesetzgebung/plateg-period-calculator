// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { KalenderDTO, FeiertageDTO, FerienDTO, SitzungenDTO, KalenderType, InitiantType } from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme/src/shares';
import { expect, assert } from 'chai';
import { KalenderController } from './controller';

describe('TEST: check gernerate extern calender data', () => {
  const testData = [
    {
      id: '61f37182-171c-364b-b00c-9a83e9e05b19',
      titel: 'Bundesweite Feiertage',
      kalendereintraege: [
        {
          id: '05fa09ff-8875-4f53-98f1-be53da29f93f',
          titel: 'Neujahr in Deutschland',
          beginn: '2022-01-01',
          ende: '2022-01-01',
          ganztaegig: true,
        },
      ],
      bundeslandType: null,
      typ: 'FEIERTAGE',
    },
    {
      id: '9bed63a3-0931-37fd-a019-91e2f1174bc9',
      titel: 'Feiertage Brandenburg',
      kalendereintraege: [
        {
          id: '51a80507-c92e-4ed1-a3e8-a7d7c087124e',
          titel: 'Neujahr in Deutschland',
          beginn: `${new Date().getFullYear() - 1}-01-01`,
          ende: `${new Date().getFullYear() - 1}-01-01`,
          ganztaegig: true,
        },
        {
          id: 'd702f46d-89f6-4a67-9147-7c39f8e35192',
          titel: 'Karfreitag in Deutschland',
          beginn: `${new Date().getFullYear()}-04-15`,
          ende: `${new Date().getFullYear()}-04-15`,
          ganztaegig: true,
        },
      ],
      bundeslandType: 'BB',
      typ: 'FEIERTAGE',
    },
    {
      id: 'e64776e4-3c5f-3600-a627-8e326d249708',
      titel: 'Plenarsitzungen',
      bearbeitetAm: '2022-12-15',
      kalendereintraege: [
        {
          id: '1651562580371-20946@ical.marudot.com',
          titel: 'Sitzungswoche Deutscher Bundestag',
          beginn: `${new Date().getFullYear()}-11-27`,
          ende: `${new Date().getFullYear()}-12-01`,
          ganztaegig: true,
        },
        {
          id: '1651562589891-43071@ical.marudot.com',
          titel: 'Sitzungswoche Deutscher Bundestag',
          beginn: `${new Date().getFullYear()}-12-11`,
          ende: `${new Date().getFullYear()}-12-15`,
          ganztaegig: true,
        },
      ],
      initiantType: 'BUNDESTAG',
      sitzungType: 'PLENAR',
      typ: 'SITZUNGEN',
    },
    {
      id: 'f4398b69-6326-34db-a087-5742adea3b8b',
      titel: 'Plenarsitzungen',
      bearbeitetAm: '2022-12-16',
      kalendereintraege: [
        {
          id: '040000008200E00074C5B7101A82E0080000000020D0918D3B7BD801000000000000000',
          titel: 'Plenarsitzung des Bundesrates',
          beginn: `${new Date().getFullYear() - 1}-10-27`,
          ende: `${new Date().getFullYear() - 1}-10-27`,
          ganztaegig: false,
        },
        {
          id: '040000008200E00074C5B7101A82E008000000001081B4AB3B7BD801000000000000000',
          titel: 'Plenarsitzung des Bundesrates',
          beginn: `${new Date().getFullYear()}-11-24`,
          ende: `${new Date().getFullYear()}-11-24`,
          ganztaegig: false,
        },
        {
          id: '040000008200E00074C5B7101A82E0080000000090066AB93B7BD801000000000000000',
          titel: 'Plenarsitzung des Bundesrates',
          beginn: `${new Date().getFullYear() + 1}-12-15`,
          ende: `${new Date().getFullYear() + 1}-12-15`,
          ganztaegig: false,
        },
      ],
      initiantType: 'BUNDESRAT',
      sitzungType: 'PLENAR',
      typ: 'SITZUNGEN',
    },
  ] as (KalenderDTO | FeiertageDTO | FerienDTO | SitzungenDTO)[];
  const kalenderController = GlobalDI.getOrRegister('ezeitKalenderController', () => new KalenderController());

  it('get holidays that belong to Bundeslands only for current or later years', () => {
    expect(kalenderController.getFeierTage(testData, KalenderType.Feiertage)).lengthOf(1);
  });
  it('get meetings detalis for Bundesrat for current or later years', () => {
    expect(kalenderController.getSetzungKalender(testData, InitiantType.Bundesrat)).lengthOf(2);
  });
  it('get stand date for Bundestag', () => {
    assert.deepEqual(kalenderController.getStandDate(testData, InitiantType.Bundestag), new Date('2022-12-15'));
  });
});
