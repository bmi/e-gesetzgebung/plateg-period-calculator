// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';

interface KalenderContentProps {
  data: {
    year: string;
    details: {
      month: string;
      dates: {
        start: Date;
        end: Date;
      }[];
    }[];
  }[];
  title: string;
  stand?: Date | null;
}

export function BundesratKalender(props: KalenderContentProps): React.ReactElement {
  const { t } = useTranslation();
  return (
    <div className="calender-content">
      <Title level={3}>{props.title}</Title>
      {props.data.map((item) => (
        <React.Fragment key={`bundesrat-sitzung-${item.year}`}>
          <Title level={3} className="year-title">
            {item.year}
          </Title>
          <div className="months-container">
            {item.details.map((detail) =>
              detail.dates.map((date) => (
                <span key={`${crypto.randomUUID()}`}>
                  {date.start.toLocaleDateString('de-DE', { month: '2-digit', day: '2-digit', year: 'numeric' })}{' '}
                  {t(`zeit.newElementGeneral.kalendarDrawer.plenarsitzung`)}
                </span>
              )),
            )}
          </div>
        </React.Fragment>
      ))}
      {props.stand && (
        <p>{`${t('zeit.newElementGeneral.kalendarDrawer.stand')} ${props.stand.toLocaleDateString('de-DE')}`}</p>
      )}
    </div>
  );
}
