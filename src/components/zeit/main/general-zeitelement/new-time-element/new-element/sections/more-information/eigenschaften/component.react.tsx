// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './eigenschaften.less';

import { FormInstance } from 'antd';
import { CheckboxValueType } from 'antd/lib/checkbox/Group';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { RolleLokalType, ZeitplanungEntityResponseDTO, ZeitplanungsvorlageEntityResponseDTO } from '@plateg/rest-api';
import { CheckboxGroupWithInfo, FormItemWithInfo, IconImportant, IconLocked } from '@plateg/theme';
import { CheckboxItem } from '@plateg/theme/src/components/checkbox-group-with-info/component.react';

import { ZeitplanungsElement, ZeitplanungsvorlageElement } from '../../../component.react';

export interface EigenschaftenComponentProps {
  form: FormInstance;
  isTermin: boolean;
  isZeitplanungElement?: boolean;
  currentZeitParent?: ZeitplanungsvorlageEntityResponseDTO | ZeitplanungEntityResponseDTO;
  isLight?: boolean;
  element?: ZeitplanungsElement | ZeitplanungsvorlageElement;
}

export function EigenschaftenComponent(props: EigenschaftenComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const possibleEigenschaftenProps = ['wichtig', 'gesperrt'];
  const [items, setItems] = useState<CheckboxItem[]>([]);
  const isHauptElement = props.element?.uebergeordneteElementeId === null;

  useEffect(() => {
    props.form.setFieldsValue({
      eigenschaften: possibleEigenschaftenProps.filter((eigenschaft) => {
        return props.form.getFieldValue(eigenschaft) === true;
      }),
    });
  }, [props.form.getFieldsValue()]);

  useEffect(() => {
    const eigenschaftenItems: CheckboxItem[] = [];
    props.form.setFieldsValue({ wichtig: undefined });
    if (props.isTermin) {
      eigenschaftenItems.push({
        label: (
          <span className="eigenschaft-checkbox">
            <IconImportant />
            {t(`zeit.newElementGeneral.moreInformation.properties.options.important.label`)}
          </span>
        ),
        value: possibleEigenschaftenProps[0],
        title: t(`zeit.newElementGeneral.moreInformation.properties.options.important.label`),
      });
    }
    if (
      !props.isLight &&
      props.isZeitplanungElement &&
      props.currentZeitParent?.dto.rolleTyp === RolleLokalType.Federfuehrer &&
      (isHauptElement || (!isHauptElement && (props.element as ZeitplanungsElement)?.gesperrt))
    ) {
      const itemType = props.isTermin ? 'termin' : 'phase';
      const itemText = t(
        `zeit.newElementGeneral.moreInformation.properties.options.lock.${itemType}.${isHauptElement ? 'label' : 'labelChild'}`,
      );
      eigenschaftenItems.push({
        label: (
          <span className="eigenschaft-checkbox">
            <IconLocked />
            {itemText}
          </span>
        ),
        value: possibleEigenschaftenProps[1],
        title: itemText,
        disabled: !isHauptElement,
      });
    }
    setItems(eigenschaftenItems);
  }, [props.isTermin]);

  const handleEigenschaftChange = (checkedValue: CheckboxValueType[]) => {
    possibleEigenschaftenProps.forEach((eigenschaft) => {
      props.form.setFieldsValue({ [eigenschaft]: checkedValue.includes(eigenschaft) });
    });
  };

  return (
    <>
      {items.length > 0 && (
        <FormItemWithInfo name="eigenschaften" className="zeit-element-eigenschaften">
          <CheckboxGroupWithInfo onChange={handleEigenschaftChange} items={items} id="ezeit-newElement-moreInfo" />
        </FormItemWithInfo>
      )}
    </>
  );
}
