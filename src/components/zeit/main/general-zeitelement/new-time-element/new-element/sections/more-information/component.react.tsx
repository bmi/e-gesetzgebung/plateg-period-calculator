// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { FormInstance } from 'antd';
import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { ZeitplanungEntityResponseDTO, ZeitplanungsvorlageEntityResponseDTO } from '@plateg/rest-api';

import { ZeitplanungsElement, ZeitplanungsvorlageElement } from '../../component.react';
import { CommentComponent } from './comment/component.react';
import { EigenschaftenComponent } from './eigenschaften/component.react';

export interface MoreInformationComponentProps {
  form: FormInstance;
  isTermin: boolean;
  isZeitplanungElement?: boolean;
  currentZeitParent?: ZeitplanungsvorlageEntityResponseDTO | ZeitplanungEntityResponseDTO;
  isLight?: boolean;
  element?: ZeitplanungsvorlageElement | ZeitplanungsElement;
}

export function MoreInformationComponent(props: MoreInformationComponentProps): React.ReactElement {
  const { t } = useTranslation();
  return (
    <div className="more-information">
      <Title level={2}>{t('zeit.newElementGeneral.moreInformation.title')}</Title>
      <fieldset className="fieldset-form-items">
        <legend className="seo">{t('zeit.newElementGeneral.moreInformation.title')}</legend>
        <EigenschaftenComponent
          form={props.form}
          isTermin={props.isTermin}
          isZeitplanungElement={props.isZeitplanungElement}
          currentZeitParent={props.currentZeitParent}
          isLight={props.isLight}
          element={props.element}
        />
      </fieldset>
      <CommentComponent form={props.form} />
    </div>
  );
}
