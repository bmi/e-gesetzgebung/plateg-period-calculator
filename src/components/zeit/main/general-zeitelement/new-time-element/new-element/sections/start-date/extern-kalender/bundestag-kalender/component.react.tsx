// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';

interface KalenderContentProps {
  data: {
    year: string;
    details: {
      month: string;
      dates: {
        start: Date;
        end: Date;
      }[];
    }[];
  }[];
  title: string;
  stand?: Date | null;
}
export function BundestagKalender(props: KalenderContentProps): React.ReactElement {
  const { t } = useTranslation();
  return (
    <div className="calender-content">
      <Title level={3}>{props.title}</Title>
      {props.data.map((item) => (
        <React.Fragment key={`Bundestag-setzung-${item.year}`}>
          <Title level={3} className="year-title">
            {item.year}
          </Title>
          {item.details.map((detail) => (
            <React.Fragment key={crypto.randomUUID()}>
              <Title level={4}>{detail.month}</Title>
              <div className="months-container">
                {detail.dates.map((date) => (
                  <span key={crypto.randomUUID()}>
                    <span>
                      {date.start.toLocaleDateString('de-DE', { month: '2-digit', day: '2-digit' })} -
                      {date.end.toLocaleDateString('de-DE', { month: '2-digit', day: '2-digit', year: 'numeric' })}
                    </span>
                  </span>
                ))}
              </div>
            </React.Fragment>
          ))}
        </React.Fragment>
      ))}
      {props.stand && (
        <p>{`${t('zeit.newElementGeneral.kalendarDrawer.stand')} ${props.stand.toLocaleDateString('de-DE')}`}</p>
      )}
    </div>
  );
}
