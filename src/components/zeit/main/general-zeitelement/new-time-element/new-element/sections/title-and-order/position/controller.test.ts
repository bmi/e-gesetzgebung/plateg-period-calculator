// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import chai, { expect } from 'chai';
import chaiString from 'chai-string';
import i18n from 'i18next';

import {
  PhasenvorlageEntityDTO,
  ZeitplanungselementEntityResponseDTOZeitplanungselementEntityDTO,
  ZeitplanungsvorlagenBauplan,
  ZeitvorlagevorlagenBauplanDTO,
} from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme/src/shares';

import { PositionController, TreeNode } from './controller';
chai.use(chaiString);
const positionController = GlobalDI.getOrRegister('ezeitPositionController', () => new PositionController());

describe('TEST: getTreeData', () => {
  const date = '2021-06-08 08:03:09';
  const zeitvorlagen: ZeitplanungselementEntityResponseDTOZeitplanungselementEntityDTO[] = [
    {
      base: { id: '1', bearbeitetAm: date, erstelltAm: date },
      dto: {
        termin: false,
        wichtig: false,
        titel: 'Phase1',
        vorherigeElementeIds: [],
        bauplan: ZeitplanungsvorlagenBauplan.GAnnahmeUndInterneVerteilungGesetzbeschluss,
        untergeordneteElemente: [
          {
            base: { id: '2', bearbeitetAm: date, erstelltAm: date },
            dto: { termin: true, wichtig: true, titel: 'Termin1.1', vorherigeElementeIds: [] },
          },
        ],
      },
    },
    {
      base: { id: '3', bearbeitetAm: date, erstelltAm: date },
      dto: { termin: true, wichtig: true, titel: 'Termin2', vorherigeElementeIds: ['1'] },
    },
  ];

  it('no zeitvorlagen passed in, empty array should be returned', () => {
    expect(positionController.getTreeData()).lengthOf(1);
  });

  it('zeitvorlagen passend in, result should be correct', () => {
    const result = positionController.getTreeData(zeitvorlagen);
    const result1Children = result[1].children || [];
    expect(result.length).to.eql(3);
    expect(result[0]).to.deep.include({
      title: i18n.t('zeit.newElementGeneral.titleAndOrder.position.firstElement'),
      key: '0',
      data: {
        vorherigeElementeIds: [],
        uebergeordneteElementeId: null,
        vorherigerBauplan: null,
        uebergeordneterBauplan: null,
      },
    });

    expect(result[1]).to.deep.include({
      key: '1',
      value: '1',
      title: 'Nach: Phase Phase1',
      data: {
        vorherigeElementeIds: ['1'],
        uebergeordneteElementeId: null,
        vorherigerBauplan: ZeitplanungsvorlagenBauplan.GAnnahmeUndInterneVerteilungGesetzbeschluss,
        uebergeordneterBauplan: null,
      },
    });

    expect(result1Children[0]).to.deep.include({
      key: '1-beginning',
      value: '1-beginning',
      title: i18n.t('zeit.newElementGeneral.titleAndOrder.position.firstElementInPhase'),
      data: {
        vorherigeElementeIds: [],
        uebergeordneteElementeId: '1',
        vorherigerBauplan: null,
        uebergeordneterBauplan: ZeitplanungsvorlagenBauplan.GAnnahmeUndInterneVerteilungGesetzbeschluss,
      },
    });

    expect(result1Children[1]).to.deep.include({
      key: '2',
      value: '2',
      title: 'Nach: Termin Termin1.1',
      data: {
        vorherigeElementeIds: ['2'],
        uebergeordneteElementeId: null,
        vorherigerBauplan: null,
        uebergeordneterBauplan: ZeitplanungsvorlagenBauplan.GAnnahmeUndInterneVerteilungGesetzbeschluss,
      },
    });

    expect(result[2]).to.deep.include({
      key: '3',
      value: '3',
      title: 'Nach: Termin Termin2',
      data: {
        vorherigeElementeIds: ['3'],
        uebergeordneteElementeId: null,
        vorherigerBauplan: null,
        uebergeordneterBauplan: null,
      },
    });
  });

  it('zeitvorlagen + exlcusion passend in, result should be correct', () => {
    const result = positionController.getTreeData(zeitvorlagen, zeitvorlagen[1].dto);
    const result1Children = result[1].children || [];
    expect(result.length).to.eql(2);
    expect(result[0]).to.deep.include({
      title: i18n.t('zeit.newElementGeneral.titleAndOrder.position.firstElement'),
      key: '0',
      data: {
        vorherigeElementeIds: [],
        uebergeordneteElementeId: null,
        vorherigerBauplan: null,
        uebergeordneterBauplan: null,
      },
    });

    expect(result[1]).to.deep.include({
      key: '1',
      value: '1',
      title: 'Nach: Phase Phase1',
      data: {
        vorherigeElementeIds: ['1'],
        uebergeordneteElementeId: null,
        vorherigerBauplan: ZeitplanungsvorlagenBauplan.GAnnahmeUndInterneVerteilungGesetzbeschluss,
        uebergeordneterBauplan: null,
      },
    });

    expect(result1Children[0]).to.deep.include({
      key: '1-beginning',
      value: '1-beginning',
      title: i18n.t('zeit.newElementGeneral.titleAndOrder.position.firstElementInPhase'),
      data: {
        vorherigeElementeIds: [],
        uebergeordneteElementeId: '1',
        vorherigerBauplan: null,
        uebergeordneterBauplan: ZeitplanungsvorlagenBauplan.GAnnahmeUndInterneVerteilungGesetzbeschluss,
      },
    });

    expect(result1Children[1]).to.deep.include({
      key: '2',
      value: '2',
      title: 'Nach: Termin Termin1.1',
      data: {
        vorherigeElementeIds: ['2'],
        uebergeordneteElementeId: null,
        vorherigerBauplan: null,
        uebergeordneterBauplan: ZeitplanungsvorlagenBauplan.GAnnahmeUndInterneVerteilungGesetzbeschluss,
      },
    });
  });
});

describe('TEST: findNodeForVorlage', () => {
  const nodes: TreeNode[] = [
    {
      key: '1',
      data: {
        vorherigeElementeIds: ['1'],
        uebergeordneteElementeId: null,
        vorherigerBauplan: ZeitplanungsvorlagenBauplan.GAnnahmeUndInterneVerteilungGesetzbeschluss,
        uebergeordneterBauplan: ZeitplanungsvorlagenBauplan.GAusfertigungDurchBpr,
      },
    },
  ];
  const zeitvorlageDTO: ZeitvorlagevorlagenBauplanDTO = {
    termin: false,
    titel: 'Phase1',
    bauplan: ZeitplanungsvorlagenBauplan.GAnnahmeUndInterneVerteilungGesetzbeschluss,
    uebergeordnetesElement: ZeitplanungsvorlagenBauplan.GAusfertigungDurchBpr,
    dauerMin: {},
  };
  const zeitvorlageDTONotInNodes: ZeitvorlagevorlagenBauplanDTO = {
    termin: false,
    titel: 'Phase2',
    bauplan: ZeitplanungsvorlagenBauplan.GAusfertigungDurchBpr,
    dauerMin: {},
  };

  it('no nodes passed in, empty array should be returned', () => {
    expect(positionController.findNodeForVorlage([], zeitvorlageDTO)).undefined;
  });

  it('no zeitvorlageDTO passed in, empty array should be returned', () => {
    expect(positionController.findNodeForVorlage(nodes)).undefined;
  });

  it('should return node', () => {
    expect(positionController.findNodeForVorlage(nodes, zeitvorlageDTO)).to.deep.include(nodes[0]);
  });

  it('no matching node present, should return undefined', () => {
    expect(positionController.findNodeForVorlage(nodes, zeitvorlageDTONotInNodes)).undefined;
  });
});

describe('TEST: findNodeForEntity', () => {
  const nodes: TreeNode[] = [
    {
      key: '0',
      data: {
        vorherigeElementeIds: [],
        uebergeordneteElementeId: undefined,
        vorherigerBauplan: null,
        uebergeordneterBauplan: undefined,
      },
    },
    {
      key: '1',
      data: {
        vorherigeElementeIds: ['1'],
        uebergeordneteElementeId: undefined,
        vorherigerBauplan: ZeitplanungsvorlagenBauplan.GAnnahmeUndInterneVerteilungGesetzbeschluss,
        uebergeordneterBauplan: undefined,
      },
    },
  ];
  const phasenvorlageEntityDTO: PhasenvorlageEntityDTO = {
    termin: false,
    titel: 'Phase1',
    bauplan: ZeitplanungsvorlagenBauplan.GAnnahmeUndInterneVerteilungGesetzbeschluss,
    uebergeordneteElementeId: undefined,
    vorherigeElementeIds: ['1'],
    untergeordneteElemente: [],
    wichtig: false,
  };
  const phasenvorlageEntityDTONotInNodes: PhasenvorlageEntityDTO = {
    termin: false,
    titel: 'Phase2',
    bauplan: ZeitplanungsvorlagenBauplan.GAnnahmeUndInterneVerteilungGesetzbeschluss,
    uebergeordneteElementeId: undefined,
    vorherigeElementeIds: ['3'],
    untergeordneteElemente: [],
    wichtig: false,
  };

  it('no nodes passed in, empty array should be returned', () => {
    expect(positionController.findNodeForEntity([], phasenvorlageEntityDTO)).undefined;
  });

  it('no entity passed in, empty array should be returned', () => {
    expect(positionController.findNodeForEntity(nodes)).undefined;
  });

  it('should return node', () => {
    expect(positionController.findNodeForEntity(nodes, phasenvorlageEntityDTO)).to.deep.include(nodes[1]);
  });

  it('no matching node present, should return undefined', () => {
    expect(positionController.findNodeForEntity(nodes, phasenvorlageEntityDTONotInNodes)).undefined;
  });
});

describe('TEST: findNodeTitleById', () => {
  const nodes: TreeNode[] = [
    {
      key: '0',
      data: {
        vorherigeElementeIds: [],
        uebergeordneteElementeId: undefined,
        vorherigerBauplan: null,
        uebergeordneterBauplan: undefined,
      },
    },
    {
      key: '1',
      data: {
        vorherigeElementeIds: ['1'],
        uebergeordneteElementeId: undefined,
        vorherigerBauplan: ZeitplanungsvorlagenBauplan.GAnnahmeUndInterneVerteilungGesetzbeschluss,
        uebergeordneterBauplan: undefined,
      },
    },
  ];
  it('no id passed in, null should be returned', () => {
    expect(positionController.findNodeTitleById([], '')).null;
  });

  it('no nodes passed in, null should be returned', () => {
    expect(positionController.findNodeTitleById([], '0')).null;
  });

  it('should return node', () => {
    expect(positionController.findNodeTitleById(nodes, '0')).to.not.be.null;
  });

  it('no matching node present, should return undefined', () => {
    expect(positionController.findNodeTitleById(nodes, '5')).null;
  });
});
