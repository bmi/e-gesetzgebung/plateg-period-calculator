// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { endOfDay, format, isAfter, isBefore, startOfDay } from 'date-fns';
import addDays from 'date-fns/addDays';
import i18n from 'i18next';

import {
  PhaseEntityDTO,
  PhaseEntityResponseDTO,
  TerminEntityDTO,
  TerminEntityResponseDTO,
  ZeitplanungselementEntityResponseDTOZeitplanungselementEntityDTO,
} from '@plateg/rest-api/models';

import { ZeitplanungsElement } from '../../component.react';

interface ChildItemInterface {
  date: Date;
  title: string;
}
type ModeType = 'beginn' | 'ende';
export const DATE_FORMAT = 'dd.MM.yyyy';
export class PeriodController {
  //get initial date for range picker (start): termin -> datum +1, phase: ende +1
  private dateFormat = DATE_FORMAT;

  public getInitialDateValue(prevElement?: ZeitplanungsElement): Date | undefined {
    if (!prevElement) {
      return undefined;
    }

    const date = prevElement?.termin ? (prevElement as TerminEntityDTO)?.datum : (prevElement as PhaseEntityDTO)?.ende;
    return date ? addDays(new Date(date), 1) : undefined;
  }

  public findPreviousElement = (
    elemente?: ZeitplanungselementEntityResponseDTOZeitplanungselementEntityDTO[],
    selectedVorgaenerId?: string,
  ): ZeitplanungsElement | undefined => {
    if (!elemente || !selectedVorgaenerId) {
      return undefined;
    }
    return (
      this.findPreviousElementCurrentLevel(elemente, selectedVorgaenerId) ||
      this.lookupChildrenForPreviousElement(elemente, selectedVorgaenerId || undefined)
    );
  };

  public dateValidator = (
    startDate?: Date,
    prevElement?: ZeitplanungsElement | undefined,
    isElementTermin?: boolean,
  ): Promise<any> => {
    if (!prevElement || !startDate) {
      return Promise.resolve();
    }
    const prevDate = prevElement?.termin
      ? new Date((prevElement as TerminEntityDTO).datum)
      : new Date((prevElement as PhaseEntityDTO).beginn);
    if (prevDate > startDate) {
      return Promise.reject(
        i18n.t(`zeit.newElementGeneral.startDate.validationError`, {
          elementType: isElementTermin ? 'Datum' : 'Startdatum',
          type: prevElement?.termin ? 'Datum' : 'Startdatum',
          prevDate: `${format(prevDate, this.dateFormat)}`,
        }),
      );
    } else {
      return Promise.resolve();
    }
  };

  public terminDateValidator = (selectedDate: Date, parentPhase: PhaseEntityDTO | undefined): Promise<any> => {
    // element in root level, don't need to validate
    if (!parentPhase) {
      return Promise.resolve();
    }

    const parentStartDate = startOfDay(new Date(parentPhase.beginn));
    const parentEndDate = startOfDay(new Date(parentPhase.ende));
    const selectedDateAsBeginigOfDay = startOfDay(selectedDate);

    if (selectedDateAsBeginigOfDay < parentStartDate) {
      // if termins date is earlier then parentStartDate
      return Promise.reject(
        i18n.t(`zeit.newElementGeneral.startDate.validationErrorElement.beginn`, {
          dateType: 'Datum',
          newDate: `${format(selectedDate, this.dateFormat)}`,
          parentDate: `${format(parentStartDate, this.dateFormat)}`,
        }),
      );
    } else if (selectedDateAsBeginigOfDay > parentEndDate) {
      // if termins date is later then parentEndDate
      return Promise.reject(
        i18n.t(`zeit.newElementGeneral.startDate.validationErrorElement.ende`, {
          dateType: 'Datum',
          newDate: `${format(selectedDate, this.dateFormat)}`,
          parentDate: `${format(parentEndDate, this.dateFormat)}`,
        }),
      );
    } else {
      return Promise.resolve();
    }
  };

  public elementDateValidator = (
    phaseDate: Date,
    parentPhase: PhaseEntityDTO | undefined,
    mode: ModeType,
  ): Promise<any> => {
    // element on root level, don't need to validate
    if (!parentPhase) {
      return Promise.resolve();
    }

    const parentDate = startOfDay(new Date(parentPhase[mode]));
    const elementDate = startOfDay(phaseDate);
    const condition = mode === 'beginn' ? elementDate < parentDate : elementDate > parentDate;

    if (condition) {
      return Promise.reject(
        i18n.t(`zeit.newElementGeneral.startDate.validationErrorElement.${mode}`, {
          dateType: mode === 'beginn' ? 'Startdatum' : 'Enddatum',
          newDate: `${format(elementDate, this.dateFormat)}`,
          parentDate: `${format(parentDate, this.dateFormat)}`,
        }),
      );
    } else {
      return Promise.resolve();
    }
  };

  public parentDateValidator = (
    phaseDate: Date,
    childrenList: ZeitplanungselementEntityResponseDTOZeitplanungselementEntityDTO[] | undefined,
    mode: ModeType,
  ): Promise<any> => {
    // has no children - don't do this validate
    if (!childrenList || !childrenList.length) {
      return Promise.resolve();
    }

    const childElement = this.getChildDate(childrenList, mode);
    const childDate = startOfDay(childElement.date);
    const elementDate = startOfDay(phaseDate);
    const condition = mode === 'beginn' ? elementDate > childDate : elementDate < childDate;

    if (condition) {
      return Promise.reject(
        i18n.t(`zeit.newElementGeneral.startDate.validationErrorParentPhase.${mode}`, {
          newDate: `${format(elementDate, this.dateFormat)}`,
          childDate: `${format(childDate, this.dateFormat)}`,
          childTitle: childElement.title,
        }),
      );
    } else {
      return Promise.resolve();
    }
  };

  private getChildDate = (
    list: ZeitplanungselementEntityResponseDTOZeitplanungselementEntityDTO[],
    mode: ModeType,
  ): ChildItemInterface => {
    const childrenList = list
      .flatMap((elem: PhaseEntityResponseDTO | TerminEntityResponseDTO) => {
        let date: Date;

        // Depends on element type and mode take a corresponding date
        if (elem.dto.termin) {
          date = new Date((elem as TerminEntityResponseDTO).dto.datum);
        } else {
          date = new Date((elem as PhaseEntityResponseDTO).dto[mode]);
        }

        return {
          date: date,
          title: elem.dto.titel,
        };
      })
      // Sort children by date
      .sort((a: ChildItemInterface, b: ChildItemInterface) => a.date.getTime() - b.date.getTime());

    if (mode === 'beginn') {
      return childrenList[0];
    } else {
      return childrenList[childrenList.length - 1];
    }
  };

  private findPreviousElementCurrentLevel = (
    elemente?: ZeitplanungselementEntityResponseDTOZeitplanungselementEntityDTO[],
    selectedVorgaenerId?: string,
  ): ZeitplanungsElement | undefined => {
    return elemente?.find((elem) => {
      return elem.base.id === selectedVorgaenerId;
    })?.dto;
  };

  private lookupChildrenForPreviousElement = (
    elemente?: ZeitplanungselementEntityResponseDTOZeitplanungselementEntityDTO[],
    selectedVorgaenerId?: string,
  ): ZeitplanungsElement | undefined => {
    const children = elemente
      ?.filter((element) => {
        return element.dto.termin === false;
      })
      .flatMap((elem) => (elem.dto as PhaseEntityDTO).untergeordneteElemente || []);
    return this.findPreviousElement(children?.length ? children : undefined, selectedVorgaenerId);
  };

  public disabledDate = (possibleDate: Date): boolean => {
    const allowedDate = new Date('2000-01-01');
    return isBefore(endOfDay(possibleDate), endOfDay(allowedDate));
  };

  public disabledPast = (possibleDate: Date, startDate: Date | undefined): boolean => {
    if (!startDate) {
      return false;
    }
    return isBefore(endOfDay(possibleDate), endOfDay(startDate));
  };
  public disabledFuture = (possibleDate: Date, endDate: Date | undefined): boolean => {
    const allowedDate = new Date('2000-01-01');
    if (!endDate) {
      return isBefore(endOfDay(possibleDate), endOfDay(allowedDate));
    }
    return (
      isAfter(endOfDay(possibleDate), endOfDay(endDate)) || isBefore(endOfDay(possibleDate), endOfDay(allowedDate))
    );
  };
}
