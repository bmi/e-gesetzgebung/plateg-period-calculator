// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { FormInstance } from 'antd';
import { PickerDateProps } from 'antd/es/date-picker/generatePicker';
import { Rule } from 'antd/lib/form';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { CustomDatepicker, TooltipComponent } from '@plateg/theme';

import { FeiertageDataType } from '../start-date/extern-kalender/controller';

// Datepicker from antd was implemented as dateRender doesnt work with the Datepicker from theme (date-fns)
// but it might need to be refactored as Moment is not supported in antd 5

interface Props extends Omit<PickerDateProps<Date>, 'picker'> {
  federalHolidays: FeiertageDataType | undefined;
  checkDisabledDates: (date1: Date) => boolean;
  onChange?: (value: Date | null | undefined) => void;
  form: FormInstance;
  required: boolean;
  additionalValidators?: Rule[];
  aliasDate: string;
  label: string | React.ReactElement;
}

export function DatePickerWHolidays({
  federalHolidays,
  checkDisabledDates,
  onChange,
  form,
  required,
  additionalValidators,
  aliasDate,
  label,
}: Props): React.ReactElement {
  const { t } = useTranslation();
  const [date, setDate] = useState<Date | null>();

  useEffect(() => {
    onChange?.(date);
  }, [date]);

  const customRender = (current: Date) => {
    let isFederalHoliday = false;
    let classnameUnderline = '';
    let tooltipTitle = '';
    federalHolidays?.[0].details
      .filter((item) => item.lands.length === 16)
      .forEach((item) => {
        if (new Date(item.startDate).toDateString() == current.toDateString()) {
          tooltipTitle = item.name.replace('in Deutschland', '');
          isFederalHoliday = true;
          classnameUnderline = 'picker-underline';
        }
      });

    return isFederalHoliday ? (
      <TooltipComponent placement="top" title={tooltipTitle}>
        <div aria-label={`${tooltipTitle} ${current.toString()}`} className="ant-picker-cell-inner picker-cell-bigger">
          {current.getDate()}
          <div className={classnameUnderline} />
        </div>
      </TooltipComponent>
    ) : (
      <div aria-label={current.toString()} className="ant-picker-cell-inner picker-cell-bigger">
        {current.getDate()}
      </div>
    );
  };
  const errorTextDateFormat = t(`zeit.newElementGeneral.errorDateFormat`);
  return (
    <CustomDatepicker
      disabledDate={checkDisabledDates}
      setDate={setDate}
      aliasDate={aliasDate}
      aliasTime={''}
      form={form}
      required={required}
      timePlaceholderName={'timePlaceholderName'}
      additionalValidators={additionalValidators || []}
      errorRequired={''}
      errorDateTime={''}
      errorInvalidFormat={errorTextDateFormat}
      errorDisabledDate={''}
      customCellRender={customRender}
      customLabel={label}
    />
  );
}
