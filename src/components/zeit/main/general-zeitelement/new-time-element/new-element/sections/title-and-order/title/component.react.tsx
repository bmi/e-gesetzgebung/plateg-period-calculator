// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, FormInstance, Input, Select } from 'antd';
import { Rule } from 'antd/lib/form';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { ZeitplanungsvorlagenBauplan, ZeitvorlagevorlagenBauplanDTO } from '@plateg/rest-api';
import { SelectWrapper } from '@plateg/theme/src/components';

export interface NewElementTitleProps {
  form: FormInstance;
  isTermin: boolean;
  isCustomElement: boolean;
  possibleVorlagen: ZeitvorlagevorlagenBauplanDTO[];
  selectedVorlageBauplan?: ZeitvorlagevorlagenBauplanDTO;
  setSelectedVorlageBauplan: (selectedVorlageBauplan?: ZeitvorlagevorlagenBauplanDTO) => void;
}

export function NewElementTitle(props: NewElementTitleProps): React.ReactElement {
  const { t } = useTranslation();
  const [selectedType, setSelectedType] = useState(props.isTermin ? 'termin' : 'phase');
  const [formItemContent, setFormItemContent] = useState<React.ReactElement>();
  const labelContent = <span>{t(`zeit.newElementGeneral.titleAndOrder.mainTitle.${selectedType}.title`)}</span>;
  const bauplanRules: Rule[] = [
    {
      required: !props.isCustomElement,
      message: t(`zeit.newElementGeneral.titleAndOrder.mainTitle.error`),
    },
  ];

  const titelRules: Rule[] = [
    {
      required: props.isCustomElement,
      whitespace: true,
      message: t(`zeit.newElementGeneral.titleAndOrder.mainTitle.error`),
    },
  ];

  useEffect(() => {
    setSelectedType(props.isTermin ? 'termin' : 'phase');
    setFormItemContent(getFormItemContent());
  }, [props.isTermin, props.isCustomElement, props.possibleVorlagen]);

  useEffect(() => {
    props.setSelectedVorlageBauplan(undefined);
    props.form.setFieldsValue({ bauplan: undefined, titel: undefined });
  }, [props.isTermin, props.isCustomElement]);

  useEffect(() => {
    if (props.isCustomElement) {
      void props.form.validateFields(['bauplan']);
    } else {
      void props.form.validateFields(['titel']);
    }
  }, [props.isCustomElement]);

  const onChange = (bauplan: ZeitplanungsvorlagenBauplan) => {
    props.setSelectedVorlageBauplan(
      props.possibleVorlagen.find((vorlage) => {
        return vorlage.bauplan === bauplan;
      }),
    );
  };

  const getFormItemContent = (): React.ReactElement => {
    const vorlagenForSelectedType = props.possibleVorlagen.filter((vorlage) => {
      return vorlage.termin === props.isTermin;
    });
    if (!props.isCustomElement) {
      return (
        <SelectWrapper
          onChange={(bauplan: ZeitplanungsvorlagenBauplan) => onChange(bauplan)}
          placeholder={t(`zeit.newElementGeneral.placeholder.select`)}
        >
          {vorlagenForSelectedType.map((vorlage, index) => {
            return (
              <Select.Option
                id="ezeit-newElementGeneralVorlageBauplanOption-select"
                value={vorlage.bauplan || ''}
                key={`titel-select-${index}`}
              >
                {vorlage.titel}
              </Select.Option>
            );
          })}
        </SelectWrapper>
      );
    } else {
      return <Input type="text" placeholder={t(`zeit.newElementGeneral.placeholder.input`)} />;
    }
  };

  //need to return both and hide one to keep correct order of error msgs
  return (
    <>
      {formItemContent && (
        <>
          <Form.Item hidden={props.isCustomElement} name="bauplan" label={labelContent} rules={bauplanRules}>
            {formItemContent}
          </Form.Item>
          <Form.Item hidden={!props.isCustomElement} name="titel" label={labelContent} rules={titelRules}>
            {formItemContent}
          </Form.Item>
        </>
      )}
    </>
  );
}
