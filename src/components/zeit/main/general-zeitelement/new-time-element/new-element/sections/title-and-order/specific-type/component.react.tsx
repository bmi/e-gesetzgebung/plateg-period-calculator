// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { FormInstance, Radio } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { FormItemWithInfo, InfoComponent } from '@plateg/theme';

export interface CustomElementTypeComponentProps {
  form: FormInstance;
  isTermin: boolean;
  isCustomElement: boolean;
  setIsCustomElement: (isCustomElement: boolean) => void;
}

export function CustomElementTypeComponent(props: CustomElementTypeComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const [selectedType, setSelectedType] = useState(props.isTermin ? 'termin' : 'phase');

  useEffect(() => {
    setSelectedType(props.isTermin ? 'termin' : 'phase');
  }, [props.isTermin]);

  const onChange = (isCustomElement: boolean) => {
    props.setIsCustomElement(isCustomElement);
  };

  return (
    <>
      <fieldset className="fieldset-form-items">
        <legend className="seo">
          {t(`zeit.newElementGeneral.titleAndOrder.type.specificType.${selectedType}.hint.title`)}
        </legend>
        <FormItemWithInfo
          name="custom-element"
          className="type-select"
          label={
            <span>
              {t(`zeit.newElementGeneral.titleAndOrder.type.specificType.${selectedType}.hint.title`)}
              <InfoComponent
                title={t(`zeit.newElementGeneral.titleAndOrder.type.specificType.${selectedType}.hint.title`)}
              >
                <p
                  dangerouslySetInnerHTML={{
                    __html: t(`zeit.newElementGeneral.titleAndOrder.type.specificType.${selectedType}.hint.content`, {
                      interpolation: { escapeValue: false },
                    }),
                  }}
                ></p>
              </InfoComponent>
            </span>
          }
          rules={[
            {
              required: true,
              message: t('zeit.newElementGeneral.titleAndOrder.type.elementType.error'),
            },
          ]}
        >
          <Radio.Group
            name={'radiogroup-custom-element'}
            onChange={(e) => {
              onChange(e.target.value as boolean);
            }}
            className="horizontal-radios"
          >
            <Radio id="ezeit-newElementGeneralOptionCopy-radio" value={false}>
              {t(`zeit.newElementGeneral.titleAndOrder.type.specificType.${selectedType}.options.copy`)}
            </Radio>
            <Radio id="ezeit-newElementGeneralOptionCustom-radio" value={true}>
              {t(`zeit.newElementGeneral.titleAndOrder.type.specificType.${selectedType}.options.custom`)}
            </Radio>
          </Radio.Group>
        </FormItemWithInfo>
      </fieldset>
    </>
  );
}
