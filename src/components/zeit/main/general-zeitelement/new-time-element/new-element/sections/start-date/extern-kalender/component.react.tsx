// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './extern-kalender.less';

import { TreeSelect } from 'antd';
import Title from 'antd/lib/typography/Title';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useRouteMatch } from 'react-router';
import { AjaxError } from 'rxjs/ajax';

import {
  FeiertageDTO,
  FerienDTO,
  InitiantType,
  KalenderDTO,
  KalenderType,
  PhaseEntityDTO,
  SitzungenDTO,
  TerminEntityDTO,
  ZeitplanungselementEntityResponseDTOZeitplanungselementEntityDTO,
} from '@plateg/rest-api';
import { CalendarOutlined, ErrorController, GlobalDI, InfoComponent } from '@plateg/theme';
import { SelectDown } from '@plateg/theme/src/components/icons/SelectDown';

import { routes } from '../../../../../../../../../shares/routes';
import {
  ZeitLightCollectionsKeys,
  ZeitLightLocalController,
} from '../../../../../../../../zeit-light/controller-local';
import { ZeitplanungController } from '../../../../../../home-tabs-zeit/controller';
import { SelectExternalCalendarController } from '../../../../../../zeitplanung-tab-view/tabs/calendar-view/ select-calendar/controller';
import { BundesratKalender } from './bundesrat-kalender/component.react';
import { BundestagKalender } from './bundestag-kalender/component.react';
import { BundesweiteFeiertageKalender } from './bundesweitefeiertage-kalender/component.react';
import { KalenderController } from './controller';
import { SchulferienKalender } from './schulferien-kalender/component.react';
import { TermineUndPhasenKalender } from './termine-phasen-kalender/component.react';

interface TreeData {
  title: string;
  value: string;
  children?: TreeData[];
  selectable?: boolean;
}
interface ExternKalenderState {
  treeData: TreeData[];
  kalenderData: (KalenderDTO | FeiertageDTO | FerienDTO | SitzungenDTO | PhaseEntityDTO | TerminEntityDTO)[];
  kalenderValue: string;
}

const kalenderController = GlobalDI.getOrRegister('ezeitKalenderController', () => new KalenderController());

interface Props {
  isLight?: boolean;
  setFederalHolidays?: Function;
}

export function ExternKalender(props: Props): React.ReactElement {
  const zeitplanungController = GlobalDI.getOrRegister('zeitplanungController', () => new ZeitplanungController());
  const selectExternalCalendarController = GlobalDI.getOrRegister(
    'selectExternalCalendarController',
    () => new SelectExternalCalendarController(),
  );

  const zeitLightLocalController = GlobalDI.getOrRegister(
    'zeitLightLocalController',
    () => new ZeitLightLocalController(),
  );

  const { t } = useTranslation();
  const dataNode: TreeData[] = [
    {
      title: t(`zeit.newElementGeneral.kalendarDrawer.eigeneTermineUndPhasen`),
      value: 'Termine-und-Phasen-dieser-Zeitplanung',
      children: [],
    },
    {
      title: t(`zeit.newElementGeneral.kalendarDrawer.sitzungswochenBundestags`),
      value: 'Sitzungswochen-des-Deutschen-Bundestags',
    },
    {
      title: t(`zeit.newElementGeneral.kalendarDrawer.termineBundesrats`),
      value: 'Termine-des-Bundesrats',
    },
    {
      title: t(`zeit.newElementGeneral.kalendarDrawer.schulferienBundesland`),
      value: `Schulferien-nach-Bundesland ${t('zeit.newElementGeneral.kalendarDrawer.erweiterbar')}`,
      selectable: false,
      children: [],
    },
    {
      title: t(`zeit.newElementGeneral.kalendarDrawer.gesetzlicheFeiertage`),
      value: `Gesetzliche-Feiertage ${t('zeit.newElementGeneral.kalendarDrawer.erweiterbar')}`,
      selectable: false,
      children: [],
    },
  ];
  const defaultState: ExternKalenderState = {
    treeData: dataNode,
    kalenderData: [],
    kalenderValue: dataNode[0].value,
  };
  const [state, setState] = useState<ExternKalenderState>(defaultState);
  const [ownPlan, setOwnPlan] = useState<ZeitplanungselementEntityResponseDTOZeitplanungselementEntityDTO>([]);
  const routeMatcher = useRouteMatch<{ archiv?: string; id: string; tabKey: string; action?: string }>(
    `/zeit/:archiv?/${routes.MEINE_ZEITPLANUNGEN}/:id/:tabKey/:action?`,
  );
  const id = routeMatcher?.params.id as string;
  useEffect(() => {
    const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
    const apiCall = props.isLight
      ? zeitLightLocalController.getItemByIDObservable(id, ZeitLightCollectionsKeys.Zeitplanung)
      : zeitplanungController.getZeitplanungCall(id);
    const calendarCall = props.isLight
      ? selectExternalCalendarController.getCalendarLightCall()
      : selectExternalCalendarController.getCalendarCall();

    calendarCall.subscribe({
      next: (data) => {
        data.forEach((item) => {
          switch (item.typ) {
            case KalenderType.Ferien:
              dataNode[3].children?.push({
                value: item.id,
                title: item.titel,
              });
              break;
            case KalenderType.Feiertage:
              dataNode[4].children?.push({
                value: item.id,
                title: item.titel,
              });
              break;
            default:
              break;
          }
        });
        setState({ ...state, treeData: dataNode, kalenderData: data });
        props.setFederalHolidays?.(kalenderController.getFeierTage(data, KalenderType.Feiertage));
      },
      error: (error: AjaxError) => {
        errorCtrl.displayErrorMsg(error, t(`zeit.newElementGeneral.kalendarDrawer.errorMessage`));
      },
    });
    apiCall.subscribe({
      next: (data) => {
        setOwnPlan(data.dto.elemente);
      },
      error: (error: AjaxError) => {
        errorCtrl.displayErrorMsg(error, t(`zeit.newElementGeneral.kalendarDrawer.errorMessage`));
      },
    });
  }, []);

  let contentElement = null;
  switch (state.kalenderValue) {
    case state.treeData[0].value: {
      contentElement = (
        <TermineUndPhasenKalender
          title={t(`zeit.newElementGeneral.kalendarDrawer.eigeneTermineUndPhasen`)}
          data={kalenderController.generatePhasenData(ownPlan)}
        />
      );
      break;
    }
    case state.treeData[1].value: {
      contentElement = (
        <BundestagKalender
          title={t(`zeit.newElementGeneral.kalendarDrawer.sitzungswochenBundestags`)}
          data={kalenderController.getSetzungKalender(state.kalenderData, InitiantType.Bundestag)}
          stand={kalenderController.getStandDate(state.kalenderData, InitiantType.Bundestag)}
        />
      );
      break;
    }
    case state.treeData[2].value: {
      contentElement = (
        <BundesratKalender
          title={t(`zeit.newElementGeneral.kalendarDrawer.bundesratContentTitle`)}
          data={kalenderController.getSetzungKalender(state.kalenderData, InitiantType.Bundesrat)}
          stand={kalenderController.getStandDate(state.kalenderData, InitiantType.Bundesrat)}
        />
      );
      break;
    }
    default:
      {
        const selectedItem = state.kalenderData.find((item) => item.id === state.kalenderValue);
        const federalHolidays = kalenderController.getFeierTage(state.kalenderData, KalenderType.Feiertage);
        if (selectedItem) {
          if (selectedItem.typ === KalenderType.Feiertage) {
            if ((selectedItem as FeiertageDTO).bundeslandType === null) {
              contentElement = (
                <BundesweiteFeiertageKalender
                  title={t(`zeit.newElementGeneral.kalendarDrawer.bundesweiteFeiertageTitle`)}
                  data={federalHolidays}
                />
              );
            } else {
              const landName = selectedItem.titel.replace('Feiertage ', '');
              contentElement = (
                <BundesweiteFeiertageKalender
                  title={state.kalenderData.find((item) => item.id === state.kalenderValue)?.titel || ''}
                  data={federalHolidays}
                  landName={landName}
                />
              );
            }
          } else {
            if ((selectedItem as FeiertageDTO).bundeslandType && selectedItem?.typ === KalenderType.Ferien) {
              const landName = selectedItem.titel.replace('Schulferien ', '');
              contentElement = (
                <SchulferienKalender
                  title={state.kalenderData.find((item) => item.id === state.kalenderValue)?.titel || ''}
                  landName={landName}
                  data={kalenderController.getFeierTage(state.kalenderData, KalenderType.Ferien)}
                />
              );
            }
          }
        }
      }
      break;
  }

  const onkeyUp = () => {
    const accessibilityElement = document
      .querySelector('.kalender-drawer-container')
      ?.querySelector('[aria-live="assertive"]');
    if (accessibilityElement) {
      const config = { attributes: true, childList: true, subtree: true, characterDataOldValue: true };

      const observer = new MutationObserver(() => {
        // uuid format regex pattern
        const regexExp = /^[0-9a-fA-F]{8}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{12}$/gi;
        if (regexExp.test(accessibilityElement.textContent || '')) {
          accessibilityElement.textContent =
            state.kalenderData.find((item) => item.id === accessibilityElement.textContent)?.titel || '';
        }
      });
      observer.observe(accessibilityElement, config);
    }
  };
  return (
    <InfoComponent
      buttonId="kalender-drawer-btn"
      title={t(`zeit.newElementGeneral.kalendarDrawer.title`)}
      titleWithoutPrefix
      buttonText={
        <>
          <CalendarOutlined />
          {t(`zeit.newElementGeneral.kalendarDrawer.buttonText`)}
        </>
      }
    >
      <div className="kalender-drawer-container">
        <p>{t(`zeit.newElementGeneral.kalendarDrawer.introText`)}</p>
        <Title level={2}>{t(`zeit.newElementGeneral.kalendarDrawer.selectLabel`)}</Title>

        <TreeSelect
          suffixIcon={<SelectDown className="ant-select-suffix" style={{ width: 14, height: 14, marginRight: 4 }} />}
          onKeyUp={onkeyUp}
          style={{ width: '100%', marginBottom: '10px' }}
          dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
          treeData={state.treeData}
          getPopupContainer={() => document.querySelector('.kalender-drawer-container') as HTMLElement}
          onChange={(value: string) => setState({ ...state, kalenderValue: value })}
          value={state.kalenderValue}
          popupClassName="custom-select-dropdown dropDownClassName"
        />
        {contentElement}
      </div>
    </InfoComponent>
  );
}
