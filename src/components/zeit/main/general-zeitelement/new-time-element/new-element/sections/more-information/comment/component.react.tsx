// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { FormInstance } from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { Constants, FormItemWithInfo, InfoComponent } from '@plateg/theme';
import { Filters } from '@plateg/theme/src/shares/filters';

export interface CommentComponentProps {
  form: FormInstance;
}

export function CommentComponent(props: CommentComponentProps): React.ReactElement {
  const { t } = useTranslation();

  return (
    <FormItemWithInfo
      name="kommentar"
      label={
        <span>
          <>
            {t('zeit.newElementGeneral.moreInformation.commentBox.title')}
            <span>
              {t('zeit.newElementGeneral.moreInformation.commentBox.hint', {
                maxChars: Filters.numberToDecimalString(Constants.TEXT_AREA_LENGTH),
              })}
            </span>
          </>
          <InfoComponent title={t(`zeit.newElementGeneral.moreInformation.commentBox.info.title`)}>
            <p
              dangerouslySetInnerHTML={{
                __html: t(`zeit.newElementGeneral.moreInformation.commentBox.info.content`, {
                  interpolation: { escapeValue: false },
                }),
              }}
            ></p>
          </InfoComponent>
        </span>
      }
      rules={[
        {
          max: Constants.TEXT_AREA_LENGTH,
          message: t('zeit.newElementGeneral.moreInformation.commentBox.error', {
            maxChars: Filters.numberToDecimalString(Constants.TEXT_AREA_LENGTH),
          }),
        },
      ]}
    >
      <TextArea rows={4} />
    </FormItemWithInfo>
  );
}
