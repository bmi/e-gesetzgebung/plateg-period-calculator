// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { FormInstance, Radio } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { FormItemWithInfo, InfoComponent } from '@plateg/theme';

export interface TypeComponentProps {
  form: FormInstance;
  isTermin: boolean;
  setIsTermin: (isTermin: boolean) => void;
}

export function TypeComponent(props: TypeComponentProps): React.ReactElement {
  const { t } = useTranslation();

  const onChange = (isTermin: boolean) => {
    props.setIsTermin(isTermin);
  };

  return (
    <>
      <fieldset className="fieldset-form-items">
        <legend className="seo">{t(`zeit.newElementGeneral.titleAndOrder.type.elementType.hint.title`)}</legend>
        <FormItemWithInfo
          className="type-select"
          name="termin"
          label={
            <span>
              {t(`zeit.newElementGeneral.titleAndOrder.type.elementType.hint.title`)}
              <InfoComponent title={t(`zeit.newElementGeneral.titleAndOrder.type.elementType.hint.title`)}>
                <p
                  dangerouslySetInnerHTML={{
                    __html: t(`zeit.newElementGeneral.titleAndOrder.type.elementType.hint.content`, {
                      interpolation: { escapeValue: false },
                    }),
                  }}
                ></p>
              </InfoComponent>
            </span>
          }
          rules={[
            {
              required: true,
              message: t('zeit.newElementGeneral.titleAndOrder.type.elementType.error'),
            },
          ]}
        >
          <Radio.Group
            name={'radiogroup-is-termin'}
            onChange={(e) => onChange(e.target.value as boolean)}
            className="horizontal-radios"
          >
            <Radio id="ezeit-newElementGeneralOptionTermin-radio" value={true}>
              {t('zeit.newElementGeneral.titleAndOrder.type.elementType.options.termin')}
            </Radio>
            <Radio id="ezeit-newElementGeneralOptionPhase-radio" value={false}>
              {t('zeit.newElementGeneral.titleAndOrder.type.elementType.options.phase')}
            </Radio>
          </Radio.Group>
        </FormItemWithInfo>
      </fieldset>
    </>
  );
}
