// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './period.less';

import { Col, FormInstance, Row } from 'antd';
import Title from 'antd/lib/typography/Title';
import { parse } from 'date-fns';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import {
  PhaseEntityDTO,
  RegelungsvorhabenTypType,
  ZeitplanungselementEntityResponseDTOZeitplanungselementEntityDTO,
  ZeitplanungsvorlagenelementEntityResponseDTOZeitplanungsvorlagenelementEntityDTO,
  ZeitvorlagevorlagenBauplanDTO,
} from '@plateg/rest-api';
import { GlobalDI, HinweisComponent, InfoComponent } from '@plateg/theme';

import { NewZeitElementPruefenController } from '../../../element-pruefen/controller';
import { NullableDate, ZeitplanungsElement } from '../../component.react';
import { DatePickerWHolidays } from '../datepicker-w-holidays/component.react';
import { ExternKalender } from '../start-date/extern-kalender/component.react';
import { FeiertageDataType } from '../start-date/extern-kalender/controller';
import { DATE_FORMAT, PeriodController } from './controller';

export interface PeriodComponentProps {
  form: FormInstance;
  isTermin: boolean;
  vorhabenart: RegelungsvorhabenTypType;
  isZeitplanungElement?: boolean;
  selectedVorlageBauplan?: ZeitvorlagevorlagenBauplanDTO;
  elemente?:
    | ZeitplanungselementEntityResponseDTOZeitplanungselementEntityDTO[]
    | ZeitplanungsvorlagenelementEntityResponseDTOZeitplanungsvorlagenelementEntityDTO[];
  selectedVorgaenger?: ZeitplanungsElement;
  elementId?: string;
  isLight?: boolean;
}

export function PeriodComponent(props: PeriodComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const periodController = GlobalDI.getOrRegister('periodController', () => new PeriodController());
  const [infoText, setInfoText] = useState<{ title: string; content: string } | undefined>();
  const ctrl = GlobalDI.getOrRegister('newZeitElementPruefenController', () => new NewZeitElementPruefenController());
  const [startDate, setStartDate] = useState<Date>(props.form.getFieldValue('beginn') as Date);
  const [endDate, setEndDate] = useState<Date>(props.form.getFieldValue('ende') as Date);

  const [federalHolidays, setFederalHolidays] = useState<FeiertageDataType>();

  const createInfoText = () => {
    if (props.selectedVorlageBauplan && !props.isTermin) {
      if (props.vorhabenart === RegelungsvorhabenTypType.Verwaltungsvorschrift) {
        setInfoText({
          title: t(`zeit.newElementGeneral.period.hinweis.title`),
          content: t(`zeit.newElementGeneral.period.hinweis.content_VV`, {
            min: props.selectedVorlageBauplan.dauerMin?.weeks,
            max: props.selectedVorlageBauplan.dauerMax?.weeks,
          }),
        });
      } else {
        setInfoText({
          title: t(`zeit.newElementGeneral.period.hinweis.title`),
          content: t(`zeit.newElementGeneral.period.hinweis.content_G_RV`),
        });
      }
    } else {
      setInfoText(undefined);
    }
  };

  const onChangeStart = (value: NullableDate) => {
    value && setStartDate(value);
  };
  const onChangeEnd = (value: NullableDate) => {
    value && setEndDate(value);
  };

  useEffect(() => {
    createInfoText();
  }, [props.selectedVorlageBauplan]);

  useEffect(() => {
    const shouldSetStartDate = props.isZeitplanungElement && props.form.isFieldTouched('selectedPreviousId');
    if (shouldSetStartDate) {
      const date = periodController.getInitialDateValue(props.selectedVorgaenger);
      props.form.setFieldsValue({
        beginn: date,
        ende: undefined,
      });
    }
  }, [props.selectedVorgaenger, props.isTermin]);

  const getParentPhase = (): PhaseEntityDTO => {
    return ctrl.findVorlageById(
      props.elemente,
      props.form.getFieldValue('uebergeordneteElementeId') as string | undefined,
    )?.dto as PhaseEntityDTO;
  };

  const getChildrenOfCurrentElement = ():
    | ZeitplanungselementEntityResponseDTOZeitplanungselementEntityDTO[]
    | undefined => {
    return ctrl.findVorlageById(props.elemente, props.elementId)?.dto
      ? (ctrl.findVorlageById(props.elemente, props.elementId)?.dto as PhaseEntityDTO).untergeordneteElemente
      : [];
  };

  return (
    <>
      <Title level={2}>
        <span>
          {t(`zeit.newElementGeneral.period.info.title`)}
          <InfoComponent title={t(`zeit.newElementGeneral.period.info.title`)}>
            <p
              dangerouslySetInnerHTML={{
                __html: t(`zeit.newElementGeneral.period.info.content`, {
                  interpolation: { escapeValue: false },
                }),
              }}
            ></p>
          </InfoComponent>
        </span>
      </Title>
      <div className="period-date-picker-container">
        <Row gutter={8} className="date-time-holder">
          <Col span={12}>
            <DatePickerWHolidays
              checkDisabledDates={(currentDate) => periodController.disabledFuture(currentDate, endDate)}
              federalHolidays={federalHolidays}
              required={true}
              form={props.form}
              aliasDate={'beginn'}
              onChange={(value: Date | null | undefined) => onChangeStart(value)}
              additionalValidators={[
                {
                  required: true,
                  message: t(`zeit.newElementGeneral.period.error`, {
                    type: 'Startdatum',
                  }),
                },
                {
                  validator: (_rule, value: string) => {
                    const startDate = value ? parse(value, DATE_FORMAT, new Date()) : undefined;
                    return periodController.dateValidator(startDate, props.selectedVorgaenger, props.isTermin);
                  },
                  validateTrigger: 'submit',
                },
                {
                  validator: (_rule, value: string) => {
                    // Validate start of Phase according to parent
                    const parentPhase = getParentPhase();
                    const startPhaseDate = value ? parse(value, DATE_FORMAT, new Date()) : new Date();
                    return periodController.elementDateValidator(startPhaseDate, parentPhase, 'beginn');
                  },
                  validateTrigger: 'submit',
                },
              ]}
              label={t(`zeit.newElementGeneral.period.startLabel`)}
            />
          </Col>
          <Col span={12}>
            <DatePickerWHolidays
              checkDisabledDates={(currentDate) => periodController.disabledPast(currentDate, startDate)}
              federalHolidays={federalHolidays}
              required={true}
              form={props.form}
              aliasDate={'ende'}
              onChange={(value: Date | null | undefined) => onChangeEnd(value)}
              additionalValidators={[
                {
                  required: true,
                  message: t(`zeit.newElementGeneral.period.error`, {
                    type: 'Enddatum',
                  }),
                },
                {
                  validator: (_rule, value: string) => {
                    // Validate end of Phase according to parent
                    const parentPhase = getParentPhase();
                    const endPhaseDate = value ? parse(value, DATE_FORMAT, new Date()) : new Date();
                    return periodController.elementDateValidator(endPhaseDate, parentPhase, 'ende');
                  },
                  validateTrigger: 'submit',
                },
                {
                  validator: (_rule, value: string) => {
                    // is parent phase endDate is earlier then latest child
                    const startPhaseDate = value ? parse(value, DATE_FORMAT, new Date()) : new Date();
                    return periodController.parentDateValidator(startPhaseDate, getChildrenOfCurrentElement(), 'ende');
                  },
                  validateTrigger: 'submit',
                },
              ]}
              label={t(`zeit.newElementGeneral.period.endLabel`)}
            />
          </Col>
        </Row>
        <ExternKalender setFederalHolidays={setFederalHolidays} isLight={props.isLight} />
        {infoText && <HinweisComponent title={infoText.title} content={<p>{infoText.content}</p>} />}
      </div>
    </>
  );
}
