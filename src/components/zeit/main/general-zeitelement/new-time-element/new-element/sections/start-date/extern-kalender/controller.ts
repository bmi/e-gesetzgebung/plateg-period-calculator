// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import {
  FeiertageDTO,
  FerienDTO,
  InitiantType,
  KalenderDTO,
  KalenderType,
  PhaseEntityDTO,
  SitzungenDTO,
  TerminEntityDTO,
} from '@plateg/rest-api';

export type FeiertageDataType = {
  year: string;
  details: { startDate: Date; endDate: Date; name: string; lands: string[] }[];
}[];
const germanMonthPattern = [
  'Januar',
  'Februar',
  'März',
  'April',
  'Mai',
  'Juni',
  'Juli',
  'August',
  'September',
  'Oktober',
  'November',
  'Dezember',
];
export class KalenderController {
  public getStandDate = (
    data: (KalenderDTO | FeiertageDTO | FerienDTO | SitzungenDTO)[],
    setzungType: InitiantType,
  ): Date | null => {
    const result = data.find(
      (kalenderItem) =>
        kalenderItem.typ === KalenderType.Sitzungen && (kalenderItem as SitzungenDTO).initiantType === setzungType,
    )?.bearbeitetAm;
    return result ? new Date(result) : null;
  };
  public getSetzungKalender = (
    data: (KalenderDTO | FeiertageDTO | FerienDTO | SitzungenDTO)[],
    setzungType: InitiantType,
  ) => {
    return this.generateRenderData(
      data.find((item) => item.typ === KalenderType.Sitzungen && (item as SitzungenDTO).initiantType === setzungType),
    );
  };
  public getFeierTage = (
    data: (KalenderDTO | FeiertageDTO | FerienDTO | SitzungenDTO)[],
    kalenderType: KalenderType,
  ) => {
    return this.generateFeiertageData(
      data.filter((item) => item.typ === kalenderType && (item as FeiertageDTO).bundeslandType),
    );
  };

  private generateRenderData = (
    bundesitems: KalenderDTO | FeiertageDTO | FerienDTO | SitzungenDTO | undefined,
  ): { year: string; details: { month: string; dates: { start: Date; end: Date }[] }[] }[] => {
    const returnData: { year: string; details: { month: string; dates: { start: Date; end: Date }[] }[] }[] = [];
    bundesitems?.kalendereintraege.forEach((eintrag) => {
      const endDate = new Date(eintrag.ende);
      const beginDate = new Date(eintrag.beginn);
      if (endDate.getFullYear() >= new Date().getFullYear()) {
        const year = eintrag.beginn.split('-')[0];
        const month = endDate.toLocaleString('de-DE', { month: 'long' });
        const foundYear = returnData.find((item) => item.year === year);
        if (foundYear) {
          const foundMonth = foundYear.details.find((item) => item.month === month);
          if (foundMonth) {
            foundMonth.dates.push({
              start: beginDate,
              end: endDate,
            });
          } else {
            foundYear.details.push({
              month: month,
              dates: [
                {
                  start: beginDate,
                  end: endDate,
                },
              ],
            });
          }
        } else {
          returnData.push({
            year: year,
            details: [
              {
                month: month,
                dates: [
                  {
                    start: beginDate,
                    end: endDate,
                  },
                ],
              },
            ],
          });
        }
      }
    });
    returnData.sort((a, b) => Number(a.year) - Number(b.year));
    returnData.forEach((item) => {
      item.details.sort(
        (a, b) => (germanMonthPattern.indexOf(a.month) || Infinity) - (germanMonthPattern.indexOf(b.month) || Infinity),
      );
      item.details.forEach((detail) => {
        detail.dates.sort((a, b) => Number(a.start) - Number(b.start));
      });
    });
    return returnData;
  };

  private generateFeiertageData = (
    feiertageItems: (KalenderDTO | FeiertageDTO | FerienDTO | SitzungenDTO | undefined)[],
  ): FeiertageDataType => {
    const returnData: FeiertageDataType = [];
    feiertageItems.forEach((feiertageItem) => {
      feiertageItem?.kalendereintraege.forEach((eintrag) => {
        const endDate = new Date(eintrag.ende);
        const beginDate = new Date(eintrag.beginn);
        if (endDate.getFullYear() >= new Date().getFullYear()) {
          const year = eintrag.beginn.split('-')[0];
          const foundYear = returnData.find((item) => item.year === year);
          const foundDetail = foundYear?.details.find(
            (detail) => detail.startDate.getTime() === beginDate.getTime() && detail.name === eintrag.titel,
          );
          if (foundDetail) {
            if (
              !foundDetail.lands.includes(
                (feiertageItem as FeiertageDTO).titel.replace('Feiertage ', '').replace('Schulferien ', ''),
              )
            ) {
              foundDetail.lands.push(
                (feiertageItem as FeiertageDTO).titel.replace('Feiertage ', '').replace('Schulferien ', ''),
              );
            }
          } else {
            foundYear?.details.push({
              startDate: beginDate,
              endDate: endDate,
              name: eintrag.titel,
              lands: [(feiertageItem as FeiertageDTO).titel.replace('Feiertage ', '').replace('Schulferien ', '')],
            });
          }
          if (!foundYear) {
            returnData.push({
              year: year,
              details: [
                {
                  startDate: beginDate,
                  endDate: endDate,
                  name: eintrag.titel,
                  lands: [(feiertageItem as FeiertageDTO).titel.replace('Feiertage ', '').replace('Schulferien ', '')],
                },
              ],
            });
          }
        }
      });
    });
    returnData.sort((a, b) => Number(a.year) - Number(b.year));
    returnData.forEach((item) => {
      item.details.sort((a, b) => Number(a.startDate) - Number(b.startDate));
    });
    return returnData;
  };

  public generatePhasenData = (
    phasenData: (PhaseEntityDTO | TerminEntityDTO)[],
  ): { details: { dates: { start: Date; end: Date; name: string; termin: boolean }[] }[] }[] => {
    const returnData: {
      details: { dates: { start: Date; end: Date; name: string; termin: boolean }[] }[];
    }[] = [];
    phasenData.forEach((phase: { dto: PhaseEntityDTO | TerminEntityDTO }) => {
      const endDate = new Date(phase.dto.ende);
      const beginDate = new Date(phase.dto.beginn);
      returnData.push({
        details: [
          {
            dates: [
              {
                start: beginDate,
                end: endDate,
                name: phase.dto.titel,
                termin: phase.dto.termin,
              },
            ],
          },
        ],
      });
    });
    return returnData;
  };
}
