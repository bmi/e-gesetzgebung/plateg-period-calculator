// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import chai, { assert, expect } from 'chai';
import addDays from 'date-fns/addDays';

import { PhaseEntityDTO, ZeitplanungselementEntityResponseDTOZeitplanungselementEntityDTO } from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme/src/shares';

import { PeriodController } from './controller';
const periodController = GlobalDI.getOrRegister('periodController', () => new PeriodController());

const date1 = '01.01.2001';
const date2 = '02.02.2002';
const date3 = '02.02.4002';
const elemente: ZeitplanungselementEntityResponseDTOZeitplanungselementEntityDTO[] = [
  {
    base: { id: 'a', bearbeitetAm: '1', erstelltAm: '1' },
    dto: {
      termin: false,
      vorherigeElementeIds: [],
      titel: 'a',
      wichtig: false,
      zeitplanungAnpassen: false,
      ende: date1,
    },
  },
  {
    base: { id: 'b', bearbeitetAm: '2', erstelltAm: '2' },
    dto: {
      datum: date2,
      termin: true,
      vorherigeElementeIds: [],
      titel: 'b',
      wichtig: false,
      zeitplanungAnpassen: false,
      ende: date2,
    },
  },
  {
    base: { id: 'c', bearbeitetAm: '3', erstelltAm: '3' },
    dto: {
      datum: date3,
      termin: true,
      vorherigeElementeIds: [],
      titel: 'c',
      wichtig: false,
      zeitplanungAnpassen: false,
      ende: date3,
    },
  },
];

const childrenList = [
  {
    base: {
      id: '1',
      erstelltAm: '2021-09-01T06:41:44.640905Z',
      bearbeitetAm: '2021-09-01T06:51:00.411249Z',
    },
    dto: {
      termin: false,
      wichtig: false,
      titel: 'unter841',
      uebergeordneteElementeId: '136e1d9c-6e9f-4ce1-9b00-92f586326cec',
      vorherigeElementeIds: ['2'],
      zeitplanungAnpassen: false,
      beginn: '2021-09-06',
      ende: '2021-09-12',
      untergeordneteElemente: [],
      datum: '2021-09-12',
    },
  },
  {
    base: {
      id: '2',
      erstelltAm: '2021-09-01T06:51:00.403199Z',
      bearbeitetAm: '2021-09-01T09:38:17.733335Z',
    },
    dto: {
      termin: true,
      wichtig: false,
      titel: 'termin850',
      uebergeordneteElementeId: '136e1d9c-6e9f-4ce1-9b00-92f586326cec',
      vorherigeElementeIds: [],
      zeitplanungAnpassen: false,
      datum: '2021-09-26',
    },
  },
];

const parentPhase: PhaseEntityDTO = {
  termin: false,
  wichtig: false,
  titel: 'test840',
  vorherigeElementeIds: [],
  zeitplanungAnpassen: false,
  beginn: '2021-09-04',
  ende: '2021-09-26',
  untergeordneteElemente: childrenList,
  datum: '2021-09-26',
};

describe('TEST: getInitialDateValue', () => {
  it('no element provided, should return undefined', () => {
    expect(periodController.getInitialDateValue(undefined)).undefined;
  });

  it('(phase) should return date1 + 1', () => {
    expect(periodController.getInitialDateValue(elemente[0].dto)).to.deep.equal(addDays(new Date(date1), 1));
  });

  it('(termin) should return date2 + 1', () => {
    expect(periodController.getInitialDateValue(elemente[1].dto)).to.deep.equal(addDays(new Date(date2), 1));
  });
});

describe('TEST: findPreviousElement', () => {
  it('no element provided, should return undefined', () => {
    expect(periodController.findPreviousElement(undefined, 'a')).undefined;
  });

  it('no id provided, should return undefined', () => {
    expect(periodController.findPreviousElement(elemente, undefined)).undefined;
  });

  it('should return correct phase', () => {
    expect(periodController.findPreviousElement(elemente, 'a')).to.deep.equal(elemente[0].dto);
  });

  it('should return correct termin', () => {
    expect(periodController.findPreviousElement(elemente, 'b')).to.deep.equal(elemente[1].dto);
  });
});

describe('TEST: dateValidator', () => {
  it('no startDate provided, should resolve', () => {
    periodController.dateValidator(undefined, elemente[0].dto).then(
      () => assert(true),
      () => assert(false),
    );
  });

  it('no element provided, should resolve', () => {
    return periodController.dateValidator(new Date(), undefined).then(
      () => assert(true),
      () => assert(false),
    );
  });

  it('prevDate < startDate, should resolve', () => {
    return periodController.dateValidator(new Date(), elemente[0].dto).then(
      () => assert(true),
      () => assert(false),
    );
  });

  it('prevDate > startDate, should reject', () => {
    return periodController.dateValidator(new Date(), elemente[2].dto).then(
      () => assert(false),
      () => assert(true),
    );
  });
});

describe('TEST: terminDateValidator', () => {
  it('no parentPhase provided, should resolve', () => {
    const selectedDate = new Date('2021-09-30');
    return periodController.terminDateValidator(selectedDate, undefined).then(
      () => assert(true),
      () => assert(false),
    );
  });

  it('if termins date is earlier then parentStartDate, should reject', () => {
    const selectedDate = new Date('2021-09-02');
    return periodController.terminDateValidator(selectedDate, parentPhase).then(
      () => assert(false),
      () => assert(true),
    );
  });

  it('if termins date is later then parentEndDate, should reject', () => {
    const selectedDate = new Date('2021-09-28');
    return periodController.terminDateValidator(selectedDate, parentPhase).then(
      () => assert(false),
      () => assert(true),
    );
  });

  it('if termins date is fits parents duration, should resolve', () => {
    const selectedDate = new Date('2021-09-15');
    return periodController.terminDateValidator(selectedDate, parentPhase).then(
      () => assert(true),
      () => assert(false),
    );
  });
});

describe('TEST: elementDateValidator', () => {
  it('no parentPhase provided, should resolve', () => {
    const selectedDate = new Date('2021-09-30');
    return periodController.elementDateValidator(selectedDate, undefined, 'beginn').then(
      () => assert(true),
      () => assert(false),
    );
  });

  it('if element date is earlier then parentStartDate, should reject', () => {
    const selectedDate = new Date('2021-09-02');
    return periodController.elementDateValidator(selectedDate, parentPhase, 'beginn').then(
      () => assert(false),
      () => assert(true),
    );
  });

  it('if element date is later then parentEndDate, should reject', () => {
    const selectedDate = new Date('2021-09-29');
    return periodController.elementDateValidator(selectedDate, parentPhase, 'ende').then(
      () => assert(false),
      () => assert(true),
    );
  });

  it('if elment date is fits parents duration, should resolve', () => {
    const selectedDate = new Date('2021-09-15');
    return periodController.elementDateValidator(selectedDate, parentPhase, 'beginn').then(
      () => assert(true),
      () => assert(false),
    );
  });
});

describe('TEST: parentDateValidator', () => {
  it('no childrenList provided, should resolve', () => {
    const selectedDate = new Date('2021-09-15');
    return periodController.parentDateValidator(selectedDate, undefined, 'beginn').then(
      () => assert(true),
      () => assert(false),
    );
  });

  it('empty childrenList provided, should resolve', () => {
    const selectedDate = new Date('2021-09-15');
    return periodController.parentDateValidator(selectedDate, [], 'beginn').then(
      () => assert(true),
      () => assert(false),
    );
  });

  it('if element startDate is later then startDate of first child, should reject', () => {
    const selectedDate = new Date('2021-09-08');
    return periodController.parentDateValidator(selectedDate, childrenList, 'beginn').then(
      () => assert(false),
      () => assert(true),
    );
  });

  it('if element endDate is earlier then endDate of last child, should reject', () => {
    const selectedDate = new Date('2021-09-12');
    return periodController.parentDateValidator(selectedDate, childrenList, 'ende').then(
      () => assert(false),
      () => assert(true),
    );
  });

  it('if elment startDate covers startDate of first child, should resolve', () => {
    const selectedDate = new Date('2021-09-02');
    return periodController.parentDateValidator(selectedDate, childrenList, 'beginn').then(
      () => assert(true),
      () => assert(false),
    );
  });

  it('if elment endDate covers endDate of last child, should resolve', () => {
    const selectedDate = new Date('2021-09-30');
    return periodController.parentDateValidator(selectedDate, childrenList, 'ende').then(
      () => assert(true),
      () => assert(false),
    );
  });
});

describe('TEST: disabledDate', () => {
  it('date in past', () => {
    const possibleDate = new Date('1999-09-11');
    expect(periodController.disabledDate(possibleDate)).to.be.true;
  });

  it('date in future', () => {
    const possibleDate = addDays(new Date(), 2);
    expect(periodController.disabledDate(possibleDate)).to.be.false;
  });
});

describe('TEST: disabledPast', () => {
  it('startDate is undefined', () => {
    const possibleDate = new Date('2021-12-14');
    expect(periodController.disabledPast(possibleDate, undefined)).to.be.false;
  });

  it('date is before startDate', () => {
    const possibleDate = new Date('2021-12-16');
    const startDate = new Date('2021-12-14');
    expect(periodController.disabledPast(possibleDate, startDate)).to.be.false;
  });

  it('date is after startDate', () => {
    const possibleDate = new Date('2021-12-12');
    const startDate = new Date('2021-12-14');
    expect(periodController.disabledPast(possibleDate, startDate)).to.be.true;
  });
});

describe('TEST: disabledFuture', () => {
  it('endDate is undefined and date is before 2000', () => {
    const possibleDate = new Date('1998-12-14');
    expect(periodController.disabledFuture(possibleDate, undefined)).to.be.true;
  });

  it('endDate is undefined and date is after 2000', () => {
    const possibleDate = new Date('2021-12-14');
    expect(periodController.disabledFuture(possibleDate, undefined)).to.be.false;
  });

  it('date is after endDate', () => {
    const possibleDate = new Date('2021-12-16');
    const endDate = new Date('2021-12-14');
    expect(periodController.disabledFuture(possibleDate, endDate)).to.be.true;
  });

  it('date is before endDate', () => {
    const possibleDate = new Date('2021-12-12');
    const endDate = new Date('2021-12-14');
    expect(periodController.disabledFuture(possibleDate, endDate)).to.be.false;
  });
});
