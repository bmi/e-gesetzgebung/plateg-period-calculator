// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import Title from 'antd/lib/typography/Title';
import React from 'react';

interface KalenderContentProps {
  data: {
    year: string;
    details: {
      startDate: Date;
      endDate: Date;
      name: string;
      lands: string[];
    }[];
  }[];
  landName: string;
  title: string;
}

export function SchulferienKalender(props: KalenderContentProps): React.ReactElement {
  return (
    <div className="calender-content">
      <Title level={3}>{props.title}</Title>
      {props.data.map((item) => (
        <React.Fragment key={`${props.landName}-ferien-${item.year}`}>
          <Title level={3} className="year-title">
            {item.year}
          </Title>
          <div className="months-container">
            {item.details
              .filter((filterDetail) => filterDetail.lands.includes(props.landName))
              .map((detail) => (
                <span key={`${crypto.randomUUID()}`}>
                  {`${detail.startDate.toLocaleDateString('de-DE', {
                    month: '2-digit',
                    day: '2-digit',
                  })} - ${detail.endDate.toLocaleDateString('de-DE', {
                    month: '2-digit',
                    day: '2-digit',
                  })} - ${detail.name}`}
                </span>
              ))}
          </div>
        </React.Fragment>
      ))}
    </div>
  );
}
