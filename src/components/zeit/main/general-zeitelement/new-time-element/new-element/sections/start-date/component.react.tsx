// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './start-date.less';

import { Col, FormInstance, Row } from 'antd';
import { parse } from 'date-fns';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import {
  PhaseEntityDTO,
  ZeitplanungsvorlagenBauplan,
  ZeitplanungsvorlagenelementEntityResponseDTOZeitplanungsvorlagenelementEntityDTO,
} from '@plateg/rest-api';
import { GlobalDI, HinweisComponent, InfoComponent } from '@plateg/theme';

import { NewZeitElementPruefenController } from '../../../element-pruefen/controller';
import { NullableDate, ZeitplanungsElement } from '../../component.react';
import { DatePickerWHolidays } from '../datepicker-w-holidays/component.react';
import { DATE_FORMAT, PeriodController } from '../period/controller';
import { ExternKalender } from './extern-kalender/component.react';
import { FeiertageDataType } from './extern-kalender/controller';

export interface StartDateComponentProps {
  form: FormInstance;
  isTermin: boolean;
  isZeitplanungElement?: boolean;
  selectedVorgaenger?: ZeitplanungsElement;
  elements?: ZeitplanungsvorlagenelementEntityResponseDTOZeitplanungsvorlagenelementEntityDTO[];
  elementId?: string;
  isLight?: boolean;
  bauplanType?: ZeitplanungsvorlagenBauplan;
  datum?: string;
}

export function StartDateComponent(props: StartDateComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const periodController = GlobalDI.getOrRegister('periodController', () => new PeriodController());
  const ctrl = GlobalDI.getOrRegister('newZeitElementPruefenController', () => new NewZeitElementPruefenController());
  const [infoBoxText, setInfoBoxText] = useState<{ title: string; content: string } | undefined>();

  const [federalHolidays, setFederalHolidays] = useState<FeiertageDataType>();

  const isKabinettbeschlussAll =
    props.bauplanType === ZeitplanungsvorlagenBauplan.VvKabinettbeschluss ||
    props.bauplanType === ZeitplanungsvorlagenBauplan.RvKabinettbeschluss ||
    props.bauplanType === ZeitplanungsvorlagenBauplan.GKabinettbeschluss;

  const handleDatePickerChange = (value: NullableDate) => {
    if (value?.getDay() !== 3 && isKabinettbeschlussAll) {
      setInfoBoxText({
        title: t(`zeit.newElementGeneral.startDate.infobox.title`),
        content: t(`zeit.newElementGeneral.startDate.infobox.content`),
      });
    } else {
      setInfoBoxText(undefined);
    }
  };

  useEffect(() => {
    if (props.datum) {
      handleDatePickerChange(new Date(props.datum));
    }
  }, [props.datum]);

  const getParentPhase = (): PhaseEntityDTO => {
    return ctrl.findVorlageById(
      props.elements,
      props.form.getFieldValue('uebergeordneteElementeId') as string | undefined,
    )?.dto as PhaseEntityDTO;
  };

  return (
    <div className="start-date-picker-container">
      <Row gutter={8} className="date-time-holder">
        <Col span={12}>
          <DatePickerWHolidays
            onChange={handleDatePickerChange}
            checkDisabledDates={(date) => periodController.disabledDate(date)}
            federalHolidays={federalHolidays}
            required={true}
            form={props.form}
            aliasDate={'datum'}
            additionalValidators={[
              {
                required: true,
                message: t(`zeit.newElementGeneral.startDate.error`),
              },
              {
                validator: (_rule, value: string) => {
                  const startDate = parse(value, DATE_FORMAT, new Date());

                  return periodController.dateValidator(startDate, props.selectedVorgaenger, props.isTermin);
                },
                validateTrigger: 'submit',
              },
              {
                validator: (_rule, value: string) => {
                  const selectedDate = parse(value, DATE_FORMAT, new Date());
                  const parentPhase = getParentPhase();

                  return periodController.terminDateValidator(selectedDate, parentPhase);
                },
                validateTrigger: 'submit',
              },
            ]}
            label={
              <>
                {t(`zeit.newElementGeneral.startDate.title`)}
                <span style={{ position: 'absolute', marginLeft: '10px' }}>
                  <InfoComponent title={t(`zeit.newElementGeneral.startDate.info.title`)}>
                    <p
                      dangerouslySetInnerHTML={{
                        __html: t(`zeit.newElementGeneral.startDate.info.content`, {
                          interpolation: { escapeValue: false },
                        }),
                      }}
                    ></p>
                  </InfoComponent>
                </span>
              </>
            }
          />
        </Col>
      </Row>
      <ExternKalender setFederalHolidays={setFederalHolidays} isLight={props.isLight} />
      {infoBoxText && <HinweisComponent title={infoBoxText.title} content={<p>{infoBoxText.content}</p>} />}
    </div>
  );
}
