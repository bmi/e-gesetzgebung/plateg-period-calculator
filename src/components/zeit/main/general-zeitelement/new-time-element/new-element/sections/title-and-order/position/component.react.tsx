// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { FormInstance, TreeSelect } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import {
  ZeitplanungselementHinweisType,
  ZeitplanungsvorlagenelementEntityResponseDTOZeitplanungsvorlagenelementEntityDTO,
  ZeitvorlagevorlagenBauplanDTO,
} from '@plateg/rest-api';
import { FormItemWithInfo, HinweisComponent, InfoComponent } from '@plateg/theme';
import { SelectDown } from '@plateg/theme/src/components/icons/SelectDown';
import { GlobalDI } from '@plateg/theme/src/shares';

import { ZeitplanungsvorlageElement } from '../../../component.react';
import { PositionController, TreeNode } from './controller';

export interface PositionComponentProps {
  form: FormInstance;
  isTermin: boolean;
  elements?: ZeitplanungsvorlagenelementEntityResponseDTOZeitplanungsvorlagenelementEntityDTO[];
  selectedVorlageBauplan?: ZeitvorlagevorlagenBauplanDTO;
  element?: ZeitplanungsvorlageElement;
  setSelectedVorgaengerId: (selectedVorgaengerId?: string) => void;
  isModifyAction: boolean;
}

export function PositionComponent(props: PositionComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const positionController = GlobalDI.getOrRegister('ezeitPositionController', () => new PositionController());
  const treeData = positionController.getTreeData(props.elements, props.isModifyAction ? props.element : undefined);
  const [warningIsVisible, setWarningIsVisible] = useState(false);
  const [selectedNode, setSelectedNode] = useState<TreeNode>();
  const [nodeForSelectedVorlage, setNodeForSelectedVorlage] = useState<TreeNode | undefined>();

  useEffect(() => {
    let matchingNode;
    if (
      (props.element && !props.form.isFieldsTouched()) ||
      (!props.selectedVorlageBauplan && props.element) ||
      (props.isModifyAction && props.element)
    ) {
      matchingNode = positionController.findNodeForEntity(treeData, props.element);
    } else {
      matchingNode = positionController.findNodeForVorlage(treeData, props.selectedVorlageBauplan);
    }
    setNodeForSelectedVorlage(matchingNode);
    props.form.setFieldsValue({
      ...matchingNode?.data,
    });
    props.form.setFieldsValue({
      selectedPreviousId: matchingNode?.key,
    });
    setSelectedNode(matchingNode);
    props.setSelectedVorgaengerId(matchingNode?.data?.vorherigeElementeIds[0]);
  }, [props.selectedVorlageBauplan]);

  useEffect(() => {
    if (props.selectedVorlageBauplan) {
      const isWarning =
        selectedNode && nodeForSelectedVorlage ? selectedNode.key !== nodeForSelectedVorlage?.key : true;
      setWarningIsVisible(isWarning);
      props.form.setFieldsValue({
        abstand: isWarning ? ZeitplanungselementHinweisType.UnzulaessigeAbstandsverkuerzung : null,
      });
    } else {
      setWarningIsVisible(false);
      props.form.setFieldsValue({
        abstand: null,
      });
    }
  }, [props.selectedVorlageBauplan, selectedNode]);

  const onSelect = (value: string, option: {}) => {
    setSelectedNode(option as TreeNode);
    props.setSelectedVorgaengerId((option as TreeNode).data.vorherigeElementeIds[0]);
    props.form.setFieldsValue({
      ...(option as TreeNode).data,
    });
  };
  const dropdownRender = (
    menu: React.ReactElement<any, string | React.JSXElementConstructor<any>>,
  ): React.ReactElement<any, string | React.JSXElementConstructor<any>> => {
    document
      .querySelector('.position-tree-select')
      ?.querySelector('[aria-autocomplete="list"]')
      ?.removeAttribute('aria-autocomplete');

    return menu;
  };
  const onkeyUp = () => {
    const accessibilityElement = document
      .querySelector('.position-tree-select')
      ?.querySelector('[aria-live="assertive"]');
    if (accessibilityElement) {
      const config = { attributes: true, childList: true, subtree: true, characterDataOldValue: true };

      const observer = new MutationObserver(() => {
        // uuid format regex pattern
        const regexExp = /^[0-9a-fA-F]{8}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{12}$/gi;
        if (
          regexExp.test(accessibilityElement.textContent || '') ||
          accessibilityElement.textContent === '0' ||
          accessibilityElement.textContent?.includes('-beginning')
        ) {
          const node = positionController.findNodeTitleById(treeData, accessibilityElement.textContent || '');
          accessibilityElement.textContent = `${node?.node?.title as string} ${node?.order || ''} von ${
            node?.length || ''
          }`;
          if (node?.node?.children?.length) {
            accessibilityElement.textContent = accessibilityElement.textContent + ' erweiterbar';
          }
        }
      });
      observer.observe(accessibilityElement, config);
    }
  };
  return (
    <>
      <FormItemWithInfo
        className="position-select has-hinweis"
        name="selectedPreviousId"
        label={
          <span>
            {t(`zeit.newElementGeneral.titleAndOrder.position.title`, {
              type: props.isTermin ? 'Termin' : 'Phase',
            })}

            <InfoComponent
              title={t(
                `zeit.newElementGeneral.titleAndOrder.position.hint.${props.isTermin ? 'termin' : 'phase'}.title`,
              )}
            >
              <p
                dangerouslySetInnerHTML={{
                  __html: t(
                    `zeit.newElementGeneral.titleAndOrder.position.hint.${props.isTermin ? 'termin' : 'phase'}.content`,
                    {
                      interpolation: { escapeValue: false },
                    },
                  ),
                }}
              ></p>
            </InfoComponent>
          </span>
        }
        rules={[
          {
            required: true,
            message: t('zeit.newElementGeneral.titleAndOrder.position.error'),
          },
        ]}
      >
        <TreeSelect
          suffixIcon={<SelectDown className="ant-select-suffix" style={{ width: 16, height: 16, marginRight: 4 }} />}
          size="large"
          getPopupContainer={() => document.querySelector('.position-tree-select') as HTMLElement}
          dropdownRender={dropdownRender}
          onKeyUp={onkeyUp}
          className="position-tree-select"
          onSelect={onSelect}
          treeData={treeData}
        />
      </FormItemWithInfo>
      {warningIsVisible && (
        <HinweisComponent
          mode="warning"
          title={t(`zeit.newElementGeneral.titleAndOrder.position.msg.title`)}
          content={
            <p
              dangerouslySetInnerHTML={{
                __html: t(`zeit.newElementGeneral.titleAndOrder.position.msg.content`, {
                  interpolation: { escapeValue: false },
                }),
              }}
            />
          }
        />
      )}
    </>
  );
}
