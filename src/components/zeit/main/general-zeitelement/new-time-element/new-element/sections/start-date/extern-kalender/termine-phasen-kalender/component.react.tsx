// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import Title from 'antd/lib/typography/Title';
import React from 'react';

interface TerminUndPhasenKalenderProps {
  data: {
    details: {
      month: string;
      dates: {
        start: Date;
        end: Date;
        name: string;
        termin: boolean;
      }[];
    }[];
  }[];
  title: string;
}

export function TermineUndPhasenKalender(props: TerminUndPhasenKalenderProps): React.ReactElement {
  return (
    <div className="calender-content">
      <Title level={3}>{props.title}</Title>
      <br></br>
      {props.data.map((item, index) => (
        <React.Fragment key={index}>
          <div className="phasen-container">
            {item.details.map((detail) =>
              detail.dates.map((date, index) => (
                <span key={index}>
                  <Title level={5}>
                    {date.termin ? 'Termin: ' : 'Phase: '} {date.name}
                  </Title>
                  {date.start.toLocaleDateString('de-DE', { month: '2-digit', day: '2-digit', year: 'numeric' })}
                  {date.termin === true
                    ? ''
                    : ` - ${date.end.toLocaleDateString('de-DE', {
                        month: '2-digit',
                        day: '2-digit',
                        year: 'numeric',
                      })}`}
                </span>
              )),
            )}
          </div>
        </React.Fragment>
      ))}
    </div>
  );
}
