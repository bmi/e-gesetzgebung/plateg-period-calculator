// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { FormInstance } from 'antd';
import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';

import {
  ZeitplanungsvorlagenelementEntityResponseDTOZeitplanungsvorlagenelementEntityDTO,
  ZeitvorlagevorlagenBauplanDTO,
} from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme/src/shares';

import { NewZeitElementPruefenController } from '../../../element-pruefen/controller';
import { PhaseReviewComponent } from '../../../element-pruefen/title-order/phase/component.react';
import { TitleReviewComponent } from '../../../element-pruefen/title-order/title/component.react';
import { TypeReviewComponent } from '../../../element-pruefen/title-order/type/component.react';
import { ZeitplanungsvorlageElement } from '../../component.react';
import { PositionComponent } from './position/component.react';
import { CustomElementTypeComponent } from './specific-type/component.react';
import { NewElementTitle } from './title/component.react';
import { TypeComponent } from './type/component.react';
export interface TitleAndOrderComponentProps {
  form: FormInstance;
  isTermin: boolean;
  setIsTermin: (isTermin: boolean) => void;
  isCustomElement: boolean;
  setIsCustomElement: (isCustomElement: boolean) => void;
  possibleVorlagen: ZeitvorlagevorlagenBauplanDTO[];
  selectedVorlageBauplan?: ZeitvorlagevorlagenBauplanDTO;
  setSelectedVorlageBauplan: (selectedVorlageBauplan?: ZeitvorlagevorlagenBauplanDTO) => void;
  setSelectedVorgaengerId: (selectedVorgaengerId?: string) => void;
  elements?: ZeitplanungsvorlagenelementEntityResponseDTOZeitplanungsvorlagenelementEntityDTO[];
  isModifyAction: boolean;
  element?: ZeitplanungsvorlageElement;
}

export function TitleAndOrderComponent(props: TitleAndOrderComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const ctrl = GlobalDI.getOrRegister('newZeitElementPruefenController', () => new NewZeitElementPruefenController());
  return (
    <div className="title-and-order">
      <Title level={2}>{t('zeit.newElementGeneral.titleAndOrder.title')}</Title>
      {props.isModifyAction && (
        <section className="review-section">
          <dl>
            <TitleReviewComponent element={props.element} />
            <TypeReviewComponent element={props.element} selectedType={props.element?.termin ? 'termin' : 'phase'} />
            <PhaseReviewComponent
              uebergeordnetePhase={
                ctrl.findVorlageById(
                  props.elements,
                  props.element?.vorherigeElementeIds?.length ? props.element?.vorherigeElementeIds[0] : undefined,
                )?.dto.titel
              }
            />
          </dl>
        </section>
      )}

      {!props.isModifyAction && (
        <>
          <TypeComponent isTermin={props.isTermin} setIsTermin={props.setIsTermin} form={props.form} />
          <CustomElementTypeComponent
            isTermin={props.isTermin}
            isCustomElement={props.isCustomElement}
            setIsCustomElement={props.setIsCustomElement}
            form={props.form}
          />
          <NewElementTitle
            form={props.form}
            isTermin={props.isTermin}
            isCustomElement={props.isCustomElement}
            possibleVorlagen={props.possibleVorlagen}
            selectedVorlageBauplan={props.selectedVorlageBauplan}
            setSelectedVorlageBauplan={props.setSelectedVorlageBauplan}
          />
        </>
      )}
      <PositionComponent
        form={props.form}
        isTermin={props.isTermin}
        elements={props.elements}
        selectedVorlageBauplan={props.selectedVorlageBauplan}
        element={props.element}
        setSelectedVorgaengerId={props.setSelectedVorgaengerId}
        isModifyAction={props.isModifyAction}
      />
    </div>
  );
}
