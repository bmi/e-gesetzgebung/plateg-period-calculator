// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';

interface KalenderContentProps {
  data: {
    year: string;
    details: {
      startDate: Date;
      endDate: Date;
      name: string;
      lands: string[];
    }[];
  }[];
  landName?: string;
  all?: boolean;
  title: string;
}

export function BundesweiteFeiertageKalender(props: KalenderContentProps): React.ReactElement {
  const { t } = useTranslation();
  let key = '';
  if (props.landName) {
    key = props.landName;
  } else {
    if (props.all) {
      key = 'Deutschland';
    } else {
      key = 'Bundesweite';
    }
  }
  return (
    <div className="calender-content">
      <Title level={3}>{props.title}</Title>
      {props.data.map((item) => (
        <React.Fragment key={`Feiertage-${key}-${item.year}`}>
          <Title level={3} className="year-title">
            {item.year}
          </Title>
          <div className="months-container">
            {item.details
              .filter((filterDetail) => {
                if (props.all) {
                  return true;
                }
                if (!props.landName) {
                  return filterDetail.lands.length === 16;
                } else {
                  return filterDetail.lands.includes(props.landName);
                }
              })
              .map((detail) => (
                <span key={`${item.year}-${detail.name}`}>
                  <span>
                    {`${detail.startDate.toLocaleDateString('de-DE', {
                      month: '2-digit',
                      day: '2-digit',
                    })} - ${detail.name}`}
                  </span>
                  {detail.lands.length < 16 && props.all && (
                    <span className="light-span">{`(${t(
                      `zeit.newElementGeneral.kalendarDrawer.nurIn`,
                    )} ${detail.lands.join(', ')})`}</span>
                  )}
                </span>
              ))}
          </div>
        </React.Fragment>
      ))}
    </div>
  );
}
