// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './delete-reposition.less';

import { Switch } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { FormItemWithInfo, HiddenInfoComponent } from '@plateg/theme';
export function DeleteRepositionElementsComponent(): React.ReactElement {
  const { t } = useTranslation();
  return (
    <>
      <FormItemWithInfo
        className="delete-reposition-elements"
        name="zeitplanungAnpassen"
        valuePropName="checked"
        label={
          <span>
            {t(`zeit.deleteModal.zeitplanungAnpassen.label`)}
            <HiddenInfoComponent
              title={t('zeit.deleteModal.zeitplanungAnpassen.infoTitle')}
              text={t('zeit.deleteModal.zeitplanungAnpassen.info')}
            />
          </span>
        }
      >
        <Switch />
      </FormItemWithInfo>
    </>
  );
}
