// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Observable } from 'rxjs';
import { AjaxError } from 'rxjs/ajax';

import { RegelungsvorhabenTypType, ZeitvorlagevorlagenBauplanDTO } from '@plateg/rest-api';
import { displayMessage, ErrorController, GeneralFormWrapper, LoadingStatusController } from '@plateg/theme';
import { ModalWrapper } from '@plateg/theme/src/components/modal-wrapper/component.react';
import { GlobalDI } from '@plateg/theme/src/shares';

import { ZeitLightController } from '../../../../zeit-light/controller-light';
import { ZeitplanungController } from '../../home-tabs-zeit/controller';
import { DeleteRepositionElementsComponent } from './delete-reposition-elements/component.react';

export interface OpenConfirmModalComponentProps {
  isVisible: boolean;
  setIsVisible: (isVisible: boolean) => void;
  target: ZeitplanungDeleteInterface;
  vorhabenart?: RegelungsvorhabenTypType;
  reloadData: () => void;
  isZeitplanung: boolean;
  isLight?: boolean;
}
export interface ZeitplanungDeleteInterface {
  titel: string;
  isTermin: boolean;
  infoKey?: string;
  id: string;
  parentId: string;
}
export interface DeleteConfirmModalPropsInterface {
  isVisible: boolean;
  target: ZeitplanungDeleteInterface;
}

export function DeleteConfirmModalComponent(props: OpenConfirmModalComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const zeitplanungController = GlobalDI.getOrRegister('zeitplanungController', () => new ZeitplanungController());
  const zeitLightController = GlobalDI.getOrRegister('zeitLightController', () => new ZeitLightController());
  const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  const [isInGGO, setIsInGGO] = useState<boolean>(false);
  const [vorlagenartList, setVorlagenartList] = useState<Array<ZeitvorlagevorlagenBauplanDTO>>([]);
  const initialValues = { zeitplanungAnpassen: false };
  const [elementName, setElementName] = useState('');

  useEffect(() => {
    if (props.vorhabenart) {
      zeitplanungController.getVorlagenListByArt(props.vorhabenart).subscribe((data) => {
        setVorlagenartList(data);
      });
    }
  }, []);

  useEffect(() => {
    if (props.target.infoKey) {
      const isGGO = vorlagenartList.some((item) => item.bauplan === props.target.infoKey);
      setIsInGGO(isGGO);
    } else {
      setIsInGGO(false);
    }
  }, [props.target.infoKey]);

  useEffect(() => {
    if (props.target.titel) {
      setElementName(props.target.titel);
      form.setFieldsValue({ zeitplanungAnpassen: false });
    }
  }, [props.target.titel]);

  const errorCallback = (error: AjaxError) => {
    loadingStatusController.setLoadingStatus(false);
    console.error(error, error);
    errorCtrl.displayErrorMsg(error, 'zeit.generalErrorMsg');
    props.setIsVisible(!props.isVisible);
  };

  const cancelHandler = () => {
    props.setIsVisible(!props.isVisible);
    loadingStatusController.setLoadingStatus(false);
    form.setFieldsValue({ zeitplanungAnpassen: false });
  };

  const deleteHandler = () => {
    loadingStatusController.setLoadingStatus(true);
    props.setIsVisible(!props.isVisible);
    let obs: Observable<void>;
    if (props.isZeitplanung) {
      // Zeitplanung
      const zeitplanungAnpassen = form.getFieldValue('zeitplanungAnpassen') as boolean;
      obs = props.isLight
        ? zeitLightController.deleteZeitplanungselementFromZeitplanungCallLight(
            props.target.id,
            props.target.parentId,
            zeitplanungAnpassen,
          )
        : zeitplanungController.deleteZeitplanungselementFromZeitplanungCall(props.target.id, zeitplanungAnpassen);
    } else {
      // Zeitplanungsvorlage
      obs = props.isLight
        ? zeitLightController.deleteZeitvorlageFromZeitplanungsvorlageCallLight(props.target.id, props.target.parentId)
        : zeitplanungController.deleteZeitvorlageFromZeitplanungsvorlageCall(props.target.id);
    }

    obs.subscribe({
      next: () => {
        loadingStatusController.setLoadingStatus(false);
        const vorlageType = t(
          `zeit.zeitplanungsvorlagenTable.tabs.tabellensicht.deleteModal.type.${
            props.target.isTermin ? 'termin' : 'phase'
          }`,
        );
        const vorlageName = props.target.titel;
        displayMessage(
          t('zeit.zeitplanungsvorlagenTable.tabs.tabellensicht.deleteModal.deleteMsg', {
            type: vorlageType,
            name: vorlageName,
          }),
          'success',
        );
        props.reloadData();
      },
      error: errorCallback,
    });
  };

  return (
    <ModalWrapper
      width="590px"
      key="open-confirm-modal"
      open={props.isVisible}
      closable={false}
      title={<h3>{`${t('zeit.zeitplanungsvorlagenTable.tabs.tabellensicht.deleteModal.title')} ${elementName}`}</h3>}
      cancelText={t('zeit.zeitplanungsvorlagenTable.tabs.tabellensicht.deleteModal.btnCancel')}
      okText={t('zeit.zeitplanungsvorlagenTable.tabs.tabellensicht.deleteModal.btnOk')}
      cancelButtonProps={{ onClick: cancelHandler, type: 'default' }}
      okButtonProps={{ onClick: deleteHandler, type: 'primary' }}
    >
      <span>{t(`zeit.zeitplanungsvorlagenTable.tabs.tabellensicht.deleteModal.${isInGGO ? 'textGGO' : 'text'}`)}</span>
      {/* Don't need reposition for Zeitplanungsvorlage */}
      {props.isZeitplanung && (
        <GeneralFormWrapper initialValues={initialValues} form={form} layout="vertical" noValidate>
          <DeleteRepositionElementsComponent />
        </GeneralFormWrapper>
      )}
    </ModalWrapper>
  );
}
