// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';
import { useTranslation } from 'react-i18next';

import { ZeitplanungEntityResponseDTO, ZeitplanungsvorlageEntityResponseDTO } from '@plateg/rest-api';
import { getDateTimeString } from '@plateg/theme';

export interface SaveInfoComponentProps {
  zeitplanungBase?: ZeitplanungEntityResponseDTO | ZeitplanungsvorlageEntityResponseDTO;
}
export function SaveInfoComponent(props: SaveInfoComponentProps): React.ReactElement {
  const { t } = useTranslation();

  if (!props.zeitplanungBase?.base.bearbeitetAm) {
    return <></>;
  }
  return (
    <span key="edit-information" className="edit-information">
      {t('zeit.zeitplanungenTable.header.lastSavedAt', {
        dateTime: getDateTimeString(props.zeitplanungBase?.base.bearbeitetAm),
      })}
    </span>
  );
}
