// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './new-element-button.less';

import { Button } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router';

import { PlusOutlined } from '@plateg/theme';

export interface NewElementButtonProps {
  disabled: boolean;
  path: string;
}
export function NewElementButton(props: NewElementButtonProps): React.ReactElement {
  const history = useHistory();
  const { t } = useTranslation();

  return (
    <Button
      id="ezeit-newElement-btn"
      type="text"
      disabled={props.disabled}
      className="new-zeit-element-btn"
      onClick={() => history.push(props.path)}
    >
      <PlusOutlined />
      {t('zeit.newElementGeneral.newElementBtn')}
    </Button>
  );
}
