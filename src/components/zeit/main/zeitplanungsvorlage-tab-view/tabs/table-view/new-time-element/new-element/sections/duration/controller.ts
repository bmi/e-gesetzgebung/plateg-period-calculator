// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import i18n from 'i18next';

import {
  PhasenvorlageEntityDTO,
  ZeitplanungsvorlagenelementEntityResponseDTOZeitplanungsvorlagenelementEntityDTO,
} from '@plateg/rest-api';
export interface DurationValueInterface {
  [s: string]: number;
}
export class DurationController {
  public rangeValidator(min: number, max: number, value: any): Promise<void> {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
    if (isNaN(value)) {
      return Promise.resolve();
    }
    if (value !== undefined) {
      const inputNumber = Number(value);
      if (!Number.isInteger(inputNumber) || inputNumber < min || inputNumber > max) {
        return Promise.reject();
      }
      return Promise.resolve();
    } else {
      return Promise.reject();
    }
  }

  public numberValidator(value: any): Promise<void> {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
    if (!isNaN(value)) {
      return Promise.resolve();
    } else {
      return Promise.reject();
    }
  }

  public durationIsPresentValidator(months?: string, weeks?: string, days?: string): Promise<void> {
    if ((!months || months === '0') && (!weeks || weeks === '0') && (!days || days === '0')) {
      return Promise.reject();
    } else {
      return Promise.resolve();
    }
  }

  public parentDurationTooShortValidator(
    parentPhase: PhasenvorlageEntityDTO | undefined,
    elementId: string | undefined,
    childrenList: ZeitplanungsvorlagenelementEntityResponseDTOZeitplanungsvorlagenelementEntityDTO[],
    durationValues: DurationValueInterface,
  ): Promise<void> {
    // if new phase on root level and has no parent - do not need a validation
    if (!elementId && !parentPhase) {
      return Promise.resolve();
    }

    const durationOfChildren = this.getDurationOfChildren(childrenList);
    const elementDuration = this.getDurationInDays(
      Number(durationValues.months || 0),
      Number(durationValues.weeks || 0),
      Number(durationValues.days || 0),
    );

    if (elementDuration < durationOfChildren) {
      return Promise.reject(
        i18n.t(
          `zeit.zeitplanungsvorlagenTable.tabs.tabellensicht.newElement.sections.duration.errorsList.parentDurationToShort`,
          {
            phaseDuration: this.prepareDurationText({
              months: durationValues.months,
              weeks: durationValues.weeks,
              days: durationValues.days,
            }),
            childrenDuration: this.converDurationToString(durationOfChildren),
          },
        ),
      );
    } else {
      return Promise.resolve();
    }
  }

  public childrenDurationTooLongValidator(
    parentPhase: PhasenvorlageEntityDTO | undefined,
    durationValues: DurationValueInterface,
    elementId?: string,
  ): Promise<void> {
    if (!parentPhase) {
      // Phase on root level
      return Promise.resolve();
    }

    const elementDuration = this.getDurationInDays(
      Number(durationValues.months || 0),
      Number(durationValues.weeks || 0),
      Number(durationValues.days || 0),
    );
    const durationOfParent = this.getDurationInDays(
      parentPhase.dauer.months,
      parentPhase.dauer.weeks,
      parentPhase.dauer.days,
    );
    let durationOfChildren: number;

    if (elementId) {
      // edited existing phase
      const restOfChildrenList = parentPhase.untergeordneteElemente.filter((child) => {
        // Calculate only for phases and exclude current phase
        return child.dto.termin === false && child.base.id !== elementId;
      });
      durationOfChildren = this.getDurationOfChildren(restOfChildrenList);
    } else {
      // new phase added to parent phase
      durationOfChildren = this.getDurationOfChildren(parentPhase.untergeordneteElemente);
    }

    if (durationOfParent < durationOfChildren + elementDuration) {
      return Promise.reject(
        i18n.t(
          `zeit.zeitplanungsvorlagenTable.tabs.tabellensicht.newElement.sections.duration.errorsList.childrenDurationTooLong`,
          {
            childrenDuration: this.converDurationToString(durationOfChildren + elementDuration),
            phaseDuration: this.prepareDurationText({
              months: parentPhase.dauer.months || 0,
              weeks: parentPhase.dauer.weeks || 0,
              days: parentPhase.dauer.days || 0,
            }),
          },
        ),
      );
    } else {
      return Promise.resolve();
    }
  }

  private getDurationInDays(months?: number, weeks?: number, days?: number): number {
    const monthsInDays = (months || 0) * 28;
    const weeksInDays = (weeks || 0) * 7;
    return monthsInDays + weeksInDays + (days || 0);
  }

  private getDurationOfChildren(
    childrenList: ZeitplanungsvorlagenelementEntityResponseDTOZeitplanungsvorlagenelementEntityDTO[],
  ): number {
    return (
      childrenList
        .map((child: ZeitplanungsvorlagenelementEntityResponseDTOZeitplanungsvorlagenelementEntityDTO) => {
          if (!child.dto.termin) {
            // prepare duration in days for each child
            const durationOfChild = (child.dto as PhasenvorlageEntityDTO).dauer;
            return this.getDurationInDays(durationOfChild.months, durationOfChild.weeks, durationOfChild.days);
          } else {
            return 0;
          }
        })
        // Get sum for all children
        .reduce((acc, itemDuration) => acc + itemDuration, 0)
    );
  }

  private prepareDurationText(values: DurationValueInterface): string {
    let finalLine = '';

    Object.keys(values).forEach((key) => {
      if (values[key] && values[key] > 0) {
        // Get array of names from translations
        const label: string[] = i18n.t(`zeit.newElementGeneral.elementPruefen.duration.durationLabels.${key}`, {
          returnObjects: true,
        });
        // Decide singular or plural translation to use
        finalLine += `${values[key]} ${values[key] > 1 ? label[0] : label[1]} `;
      }
    });
    return finalLine.trim();
  }

  private converDurationToString(duration: number) {
    const weeksAndDays = duration % 28;

    const values: DurationValueInterface = {
      months: Math.floor(duration / 28),
      weeks: Math.floor(weeksAndDays / 7),
      days: weeksAndDays % 7,
    };

    return this.prepareDurationText(values);
  }
}
