// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './duration-component.less';

import { Col, Form, FormInstance, Input, Row } from 'antd';
import Title from 'antd/lib/typography/Title';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import {
  PhasenvorlageEntityDTO,
  ZeitplanungsvorlagenelementEntityResponseDTOZeitplanungsvorlagenelementEntityDTO,
  ZeitvorlagevorlagenBauplanDTO,
} from '@plateg/rest-api';
import { HinweisComponent, InfoComponent } from '@plateg/theme';
import { GlobalDI } from '@plateg/theme/src/shares';

import { NewZeitElementPruefenController } from '../../../../../../../general-zeitelement/new-time-element/element-pruefen/controller';
import { DurationController, DurationValueInterface } from './controller';

export interface DurationComponentProps {
  form: FormInstance;
  isTermin: boolean;
  isZeitplanungElement?: boolean;
  selectedVorlageBauplan?: ZeitvorlagevorlagenBauplanDTO;
  elements?: ZeitplanungsvorlagenelementEntityResponseDTOZeitplanungsvorlagenelementEntityDTO[];
  elementId?: string;
}
export function DurationComponent(props: DurationComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const durationController = GlobalDI.getOrRegister('ezeitDurationController', () => new DurationController());
  const [infoText, setInfoText] = useState<{ title: string; content: string } | undefined>();
  const ctrl = GlobalDI.getOrRegister('newZeitElementPruefenController', () => new NewZeitElementPruefenController());

  const createInfoText = () => {
    if (props.selectedVorlageBauplan && !props.isTermin) {
      setInfoText({
        title: t(`zeit.zeitplanungsvorlagenTable.tabs.tabellensicht.newElement.sections.duration.hinweis.title`),
        content: t(`zeit.zeitplanungsvorlagenTable.tabs.tabellensicht.newElement.sections.duration.hinweis.content`, {
          min: props.selectedVorlageBauplan.dauerMin?.weeks,
          max: props.selectedVorlageBauplan.dauerMax?.weeks,
        }),
      });
    } else {
      setInfoText(undefined);
    }
  };
  useEffect(() => {
    createInfoText();
  }, []);

  useEffect(() => {
    createInfoText();
  }, [props.selectedVorlageBauplan]);

  useEffect(() => {
    props.form.setFieldsValue({ dauer: { months: 0, weeks: 0, days: 0 } });
  }, [props.isTermin]);

  const getParentPhase = (): PhasenvorlageEntityDTO => {
    return ctrl.findVorlageById(
      props.elements,
      props.form.getFieldValue('uebergeordneteElementeId') as string | undefined,
    )?.dto as PhasenvorlageEntityDTO;
  };

  const childrenOfCurrentElement: ZeitplanungsvorlagenelementEntityResponseDTOZeitplanungsvorlagenelementEntityDTO[] =
    ctrl.findVorlageById(props.elements, props.elementId)?.dto
      ? (ctrl.findVorlageById(props.elements, props.elementId)?.dto as PhasenvorlageEntityDTO).untergeordneteElemente
      : [];

  return (
    <>
      {!props.isTermin && !props.isZeitplanungElement && (
        <div className="duration-component">
          <Title level={2}>
            {t('zeit.zeitplanungsvorlagenTable.tabs.tabellensicht.newElement.sections.duration.title')}
            <InfoComponent
              title={t('zeit.zeitplanungsvorlagenTable.tabs.tabellensicht.newElement.sections.duration.hint.title')}
            >
              <p>{t('zeit.zeitplanungsvorlagenTable.tabs.tabellensicht.newElement.sections.duration.infoText')}</p>
            </InfoComponent>
          </Title>

          {infoText && <HinweisComponent mode="warning" title={infoText.title} content={<p>{infoText.content}</p>} />}

          <Form.List
            name="dauer"
            rules={[
              {
                validator: (_rule, value) => {
                  const parentPhase = getParentPhase();
                  return durationController.parentDurationTooShortValidator(
                    parentPhase,
                    props.elementId,
                    childrenOfCurrentElement,
                    value as DurationValueInterface,
                  );
                },
              },
              {
                validator: (_rule, value) => {
                  const parentPhase = getParentPhase();
                  return durationController.childrenDurationTooLongValidator(
                    parentPhase,
                    value as DurationValueInterface,
                    props.elementId,
                  );
                },
              },
            ]}
          >
            {() => (
              <Row className="duration-select">
                <Col span={8} className="duration-select-element">
                  <Form.Item
                    name={['weeks']}
                    label={
                      <span>
                        {t(
                          `zeit.zeitplanungsvorlagenTable.tabs.tabellensicht.newElement.sections.duration.items.weeks.title`,
                        )}
                      </span>
                    }
                    rules={[
                      {
                        validator: (_rule, value) => durationController.numberValidator(value),
                        message: t(
                          `zeit.zeitplanungsvorlagenTable.tabs.tabellensicht.newElement.sections.duration.nanError`,
                        ),
                        validateTrigger: 'onChange',
                      },
                      {
                        validator: (_rule, value) => durationController.rangeValidator(0, 52, value),
                        message: t(
                          `zeit.zeitplanungsvorlagenTable.tabs.tabellensicht.newElement.sections.duration.items.weeks.error`,
                        ),
                        validateTrigger: 'onChange',
                      },
                    ]}
                  >
                    <Input placeholder="0" />
                  </Form.Item>
                </Col>
                <Col span={8} className="duration-select-element">
                  <Form.Item
                    name={['days']}
                    label={
                      <span>
                        {t(
                          `zeit.zeitplanungsvorlagenTable.tabs.tabellensicht.newElement.sections.duration.items.days.title`,
                        )}
                      </span>
                    }
                    rules={[
                      {
                        validator: (_rule, value) => durationController.numberValidator(value),
                        message: t(
                          `zeit.zeitplanungsvorlagenTable.tabs.tabellensicht.newElement.sections.duration.nanError`,
                        ),
                        validateTrigger: 'onChange',
                      },
                      {
                        validator: (_rule, value) => durationController.rangeValidator(0, 365, value),
                        message: t(
                          `zeit.zeitplanungsvorlagenTable.tabs.tabellensicht.newElement.sections.duration.items.days.error`,
                        ),
                        validateTrigger: 'onChange',
                      },
                      {
                        validator: (_rule, value) =>
                          durationController.durationIsPresentValidator(
                            props.form.getFieldValue(['dauer', 'months']) as string | undefined,
                            props.form.getFieldValue(['dauer', 'weeks']) as string | undefined,
                            value as string | undefined,
                          ),
                        message: t(
                          `zeit.zeitplanungsvorlagenTable.tabs.tabellensicht.newElement.sections.duration.error`,
                        ),
                        validateTrigger: 'submit',
                      },
                    ]}
                  >
                    <Input placeholder="0" />
                  </Form.Item>
                </Col>
                <Col span={8} className="placeholder-col"></Col>
              </Row>
            )}
          </Form.List>
        </div>
      )}
    </>
  );
}
