// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import chai, { assert } from 'chai';
import chaiString from 'chai-string';
import sinon from 'sinon';

import {
  PhasenvorlageEntityDTO,
  ZeitplanungsvorlagenelementEntityResponseDTOZeitplanungsvorlagenelementEntityDTO,
} from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme/src/shares';

import { DurationController, DurationValueInterface } from './controller';
const durationController = GlobalDI.getOrRegister('ezeitDurationController', () => new DurationController());
const elementId = '123';
const childrenList: ZeitplanungsvorlagenelementEntityResponseDTOZeitplanungsvorlagenelementEntityDTO[] = [
  {
    base: {
      id: '1',
      erstelltAm: '2021-08-30T08:02:48.320306Z',
      bearbeitetAm: '2021-08-30T08:02:48.320306Z',
    },
    dto: {
      termin: false,
      wichtig: false,
      titel: 'Ergänzung der Vorhabenplanung der BReg',
      uebergeordneteElementeId: '2',
      vorherigeElementeIds: [],
      kommentar: 'unter phase level 1',
      dauer: { months: 0, weeks: 2, days: 0 },
      untergeordneteElemente: [
        {
          base: {
            id: '3',
            erstelltAm: '2021-08-30T08:03:39.808400Z',
            bearbeitetAm: '2021-08-30T08:03:39.808400Z',
          },
          dto: {
            termin: false,
            wichtig: false,
            titel: 'Erstellung des Referentenentwurfs',
            uebergeordneteElementeId: '1',
            vorherigeElementeIds: [],
            kommentar: 'unter phase level 2',
            dauer: { months: 0, weeks: 2, days: 0 },
            untergeordneteElemente: [],
          },
        },
      ],
    },
  },
  {
    base: {
      id: elementId,
      erstelltAm: '2021-08-30T08:01:54.385208Z',
      bearbeitetAm: '2021-08-30T08:02:48.363312Z',
    },
    dto: {
      termin: false,
      wichtig: false,
      titel: 'Konzeptionelle Überlegungen',
      uebergeordneteElementeId: '2',
      vorherigeElementeIds: ['1'],
      kommentar: 'phase level 1',
      dauer: { months: 0, weeks: 1 },
      untergeordneteElemente: [],
    },
  },
];
const parentPhase: PhasenvorlageEntityDTO = {
  termin: false,
  wichtig: false,
  titel: 'Befassung Hausleitung',
  vorherigeElementeIds: [],
  kommentar: 'main phase',
  dauer: { months: 0, weeks: 4, days: 0 },
  untergeordneteElemente: childrenList,
};
describe('TEST: rangeValidator', () => {
  const min = 0;
  const max = 12;

  it('12 in range, promise should be resolved', () => {
    return durationController.rangeValidator(min, max, 12).then(
      () => assert(true),
      () => assert(false),
    );
  });
  it('6 in range, promise should be resolved', () => {
    return durationController.rangeValidator(min, max, 6).then(
      () => assert(true),
      () => assert(false),
    );
  });
  it('0 in range, promise should be resolved', () => {
    return durationController.rangeValidator(min, max, 0).then(
      () => assert(true),
      () => assert(false),
    );
  });
  it('-12 not in range, promise should be rejected', () => {
    return durationController.rangeValidator(min, max, -12).then(
      () => assert(false),
      () => assert(true),
    );
  });

  it('500 not in range, promise should be rejected', () => {
    return durationController.rangeValidator(min, max, 500).then(
      () => assert(false),
      () => assert(true),
    );
  });

  it('5.1 not round, promise should be rejected', () => {
    return durationController.rangeValidator(min, max, 5.1).then(
      () => assert(false),
      () => assert(true),
    );
  });

  it('"abc" NaN, promise should be resolved', () => {
    return durationController.rangeValidator(min, max, 'abc').then(
      () => assert(true),
      () => assert(false),
    );
  });
});

describe('TEST: numberValidator', () => {
  it('12 is  a number, promise should be resolved', () => {
    return durationController.numberValidator(12).then(
      () => assert(true),
      () => assert(false),
    );
  });
  it('10000 is  a number, promise should be resolved', () => {
    return durationController.numberValidator(10000).then(
      () => assert(true),
      () => assert(false),
    );
  });
  it('099999 is  a number, promise should be resolved', () => {
    return durationController.numberValidator('99999').then(
      () => assert(true),
      () => assert(false),
    );
  });
  it('abc is not a number, promise should be rejected', () => {
    return durationController.numberValidator('abc').then(
      () => assert(false),
      () => assert(true),
    );
  });
  it('12abc is not a number, promise should be rejected', () => {
    return durationController.numberValidator('12abc').then(
      () => assert(false),
      () => assert(true),
    );
  });
});

describe('TEST: durationIsPresentValidator', () => {
  it('all three parameters exist, promise should be resolved', () => {
    return durationController.durationIsPresentValidator('2', '3', '12').then(
      () => assert(true),
      () => assert(false),
    );
  });

  it('has no parameters, promise should be rereject', () => {
    return durationController.durationIsPresentValidator().then(
      () => assert(false),
      () => assert(true),
    );
  });

  it('one parameter missed, promise should be resolved', () => {
    return durationController.durationIsPresentValidator('6', '3').then(
      () => assert(true),
      () => assert(false),
    );
  });
  it('two parameters missed, promise should be resolved', () => {
    return durationController.durationIsPresentValidator('3').then(
      () => assert(true),
      () => assert(false),
    );
  });
});

describe('TEST: parentDurationTooShortValidator', () => {
  it('no parentPhase and no elementId, promise should be resolved', () => {
    const durationValues: DurationValueInterface = {
      months: 3,
      weeks: 0,
      days: 0,
    };
    return durationController.parentDurationTooShortValidator(undefined, undefined, [], durationValues).then(
      () => assert(true),
      () => assert(false),
    );
  });

  it('element duration equal or more than children duration, promise should be resolved', () => {
    const durationValues: DurationValueInterface = {
      months: 0,
      weeks: 4,
      days: 0,
    };
    return durationController.parentDurationTooShortValidator(undefined, elementId, childrenList, durationValues).then(
      () => assert(true),
      () => assert(false),
    );
  });

  it('element duration less than children duration, promise should be rejected', () => {
    const durationValues: DurationValueInterface = {
      months: 0,
      weeks: 2,
      days: 6,
    };
    return durationController.parentDurationTooShortValidator(undefined, elementId, childrenList, durationValues).then(
      () => assert(false),
      () => assert(true),
    );
  });
});

describe('TEST: childrenDurationTooLongValidator', () => {
  it('no parentPhase promise should be resolved', () => {
    const durationValues: DurationValueInterface = {
      months: 3,
      weeks: 0,
      days: 0,
    };
    return durationController.childrenDurationTooLongValidator(undefined, durationValues, elementId).then(
      () => assert(true),
      () => assert(false),
    );
  });

  it('new element with duration that fits parent duration - should be resolved', () => {
    const durationValues: DurationValueInterface = {
      months: 0,
      weeks: 1,
      days: 0,
    };
    return durationController.childrenDurationTooLongValidator(parentPhase, durationValues).then(
      () => assert(true),
      () => assert(false),
    );
  });

  it('exist element duration + rest of child duration fits parent duration , promise should be rejected', () => {
    const durationValues: DurationValueInterface = {
      months: 0,
      weeks: 1,
      days: 1,
    };
    return durationController.childrenDurationTooLongValidator(parentPhase, durationValues, elementId).then(
      () => assert(true),
      () => assert(false),
    );
  });
  it('exist element duration + rest of child duration more that parent duration , promise should be rejected', () => {
    const durationValues: DurationValueInterface = {
      months: 0,
      weeks: 2,
      days: 1,
    };
    return durationController.childrenDurationTooLongValidator(parentPhase, durationValues, elementId).then(
      () => assert(false),
      () => assert(true),
    );
  });
});
