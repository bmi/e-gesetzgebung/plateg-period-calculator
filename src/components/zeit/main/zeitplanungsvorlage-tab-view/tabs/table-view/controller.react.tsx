// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { ColumnsType } from 'antd/lib/table';
import Text from 'antd/lib/typography/Text';
import { createHashHistory } from 'history';
import i18n from 'i18next';
import React, { ReactElement } from 'react';
import { AjaxError } from 'rxjs/ajax';

import {
  AktionType,
  ErstellerType,
  PhasenvorlageEntityResponseDTO,
  PlategPeriod,
  TerminEntityDTO,
  TerminvorlageEntityResponseDTO,
  ZeitplanungselementEntityResponseDTOZeitplanungselementEntityDTO,
  ZeitplanungselementHinweisType,
  ZeitplanungsvorlageEntityResponseDTO,
  ZeitplanungsvorlagenelementEntityResponseDTOZeitplanungsvorlagenelementEntityDTO,
} from '@plateg/rest-api';
import {
  CommonRow,
  displayMessage,
  ErrorController,
  GlobalDI,
  LoadingStatusController,
  TableComponentProps,
} from '@plateg/theme';
import {
  DropdownMenu,
  DropdownMenuItem,
} from '@plateg/theme/src/components/table-component/table-sub-components/dropdown-button-component/component.react';

import { routes } from '../../../../../../shares/routes';
import { NewZeitElementPruefenController } from '../../../general-zeitelement/new-time-element/element-pruefen/controller';
import { NewZeitElementController } from '../../../general-zeitelement/new-time-element/new-element/controller';
import { TabellensichtController } from '../../../general-zeitelement/tabellensicht/controller.react';

export interface VorlageRow extends CommonRow {
  titel: string;
  isTermin: boolean;
  wichtig: boolean;
  dauer?: string;
  infoKey?: string;
  children?: VorlageRow[];
  hinweise?: ZeitplanungselementHinweisType[];
  parentId?: string;
  isLight?: boolean;
  level?: number;
}
export interface VorlageDeleteInterface {
  titel: string;
  isTermin: boolean;
  infoKey?: string;
  id: string;
  parentId: string;
}
export class ZeitplanungsvorlageTableViewController {
  private readonly loadingstatusController = GlobalDI.getOrRegister(
    'loadingStatusController',
    () => new LoadingStatusController(),
  );
  private readonly newZeitElementController = GlobalDI.getOrRegister(
    'newZeitElementController',
    () => new NewZeitElementController(),
  );
  private readonly errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  private readonly newZeitElementPruefenController = GlobalDI.getOrRegister(
    'newZeitElementPruefenController',
    () => new NewZeitElementPruefenController(),
  );
  private readonly dauerRenderer = (record: VorlageRow): ReactElement => {
    return record?.isTermin ? (
      i18n.t('zeit.zeitplanungsvorlagenTable.tabs.tabellensicht.terminDauer')
    ) : (
      <Text>{record?.dauer}</Text>
    );
  };

  private readonly dropdownRenderer = (
    record: VorlageRow,
    setDeleteConfirmModalProps: (openConfirmModalProps: { isVisible: boolean; target: VorlageDeleteInterface }) => void,
    owner: ErstellerType,
    disabled: boolean,
    elemente: Array<ZeitplanungsvorlagenelementEntityResponseDTOZeitplanungsvorlagenelementEntityDTO>,
    reloadData: () => void,
    aktionen: AktionType[],
  ): ReactElement => {
    const history = createHashHistory();
    const items: DropdownMenuItem[] = [];
    const bearbeitenAction = {
      element: i18n.t('zeit.zeitplanungsvorlagenTable.tabs.tabellensicht.actions.bearbeiten'),
      onClick: () => {
        history.push(`${routes.TABELLENSICHT}/${routes.ELEMENT_BEARBEITEN}/${record.id}/`);
      },
      disabled: () => !aktionen.includes(AktionType.Schreiben),
    };

    items.push(bearbeitenAction);

    if (record.isTermin) {
      const importantAction = {
        element: i18n.t(
          `zeit.zeitplanungsvorlagenTable.tabs.tabellensicht.actions.${!record?.wichtig ? 'important' : 'unimportant'}`,
        ),
        disabled: () => !aktionen.includes(AktionType.Schreiben),
        onClick: () => {
          this.changeTerminImportantStatus(record.id, elemente, reloadData, record.parentId as string, record.isLight);
        },
      };
      items.push(importantAction);
    }

    const deleteAction = {
      element: i18n.t('zeit.zeitplanungsvorlagenTable.tabs.tabellensicht.actions.delete'),
      disabled: () => !aktionen.includes(AktionType.Schreiben),
      onClick: () => {
        setDeleteConfirmModalProps({
          isVisible: true,
          target: {
            id: record.id,
            titel: record.titel,
            isTermin: record.isTermin,
            infoKey: record.infoKey,
            parentId: record.parentId as string,
          },
        });
      },
    };
    items.push(deleteAction);

    return (
      <>
        <DropdownMenu
          openLink={`${routes.TABELLENSICHT}/${routes.UEBERSICHT}/${record.id}`}
          items={items}
          elementId={record.id}
        />
      </>
    );
  };

  private readonly getDauerString = (dauer?: PlategPeriod): string => {
    const getMonths = () => {
      if (!dauer?.months) {
        return undefined;
      } else {
        return dauer.months !== 1 ? `${dauer.months} Monate` : `${dauer.months} Monat`;
      }
    };
    const getWochen = () => {
      if (!dauer?.weeks) {
        return undefined;
      } else {
        return dauer.weeks !== 1 ? `${dauer.weeks} Wochen` : `${dauer.weeks} Woche`;
      }
    };
    const getDays = () => {
      if (!dauer?.days) {
        return undefined;
      } else {
        return dauer.days !== 1 ? `${dauer.days} Tage` : `${dauer.days} Tag`;
      }
    };

    const vals = [getMonths(), getWochen(), getDays()].filter((x) => x !== undefined);

    return vals.join(', ');
  };

  private readonly changeTerminImportantStatus = (
    elementId: string,
    elemente: Array<ZeitplanungsvorlagenelementEntityResponseDTOZeitplanungsvorlagenelementEntityDTO>,
    callBack: () => void,
    parentId: string,
    isLight?: boolean,
  ) => {
    const termin = this.newZeitElementPruefenController.findVorlageById(elemente, elementId);
    this.loadingstatusController.setLoadingStatus(true);
    const apiCall = isLight
      ? this.newZeitElementController.modifyDateToZeitplanungsvorlageCallLight
      : this.newZeitElementController.modifyDateToZeitplanungsvorlageCall;
    apiCall({
      elementId,
      terminvorlageEntityDTO: { ...(termin?.dto as TerminEntityDTO), wichtig: !termin?.dto.wichtig },
      vorlageId: parentId,
    }).subscribe({
      next: () => {
        displayMessage(
          i18n.t(
            `zeit.zeitplanungsvorlagenTable.tabs.tabellensicht.actions.msgs.${
              !termin?.dto.wichtig ? 'important' : 'unimportant'
            }`,
            { name: termin?.dto.titel },
          ),
          'success',
        );
        callBack();
        this.loadingstatusController.setLoadingStatus(false);
      },
      error: (error: AjaxError) => {
        console.log('Error sub', error);
        this.errorCtrl.displayErrorMsg(error, 'zeit.generalErrorMsg');
        this.loadingstatusController.setLoadingStatus(false);
      },
    });
  };

  private readonly createVorlageRows = (
    content:
      | ZeitplanungselementEntityResponseDTOZeitplanungselementEntityDTO[]
      | ZeitplanungsvorlagenelementEntityResponseDTOZeitplanungsvorlagenelementEntityDTO[],
    parentId: string,
    isLight?: boolean,
    level = 0,
  ): VorlageRow[] => {
    return content.map((element) => {
      const isTermin = element.dto.termin;
      return {
        id: element.base.id,
        isTermin: isTermin,
        titel: element.dto.titel,
        wichtig: isTermin ? (element as TerminvorlageEntityResponseDTO).dto.wichtig : false,
        dauer: isTermin ? undefined : this.getDauerString((element as PhasenvorlageEntityResponseDTO).dto.dauer),
        children:
          isTermin || !(element as PhasenvorlageEntityResponseDTO).dto.untergeordneteElemente.length
            ? undefined
            : this.createVorlageRows(
                (element as PhasenvorlageEntityResponseDTO).dto.untergeordneteElemente,
                parentId,
                isLight,
                level + 1,
              ),
        level: level,
        infoKey: element.dto.bauplan,
        parentId: parentId,
        isLight: isLight,
        hinweise: element.dto.hinweise,
      };
    });
  };

  public getZeitplanungsvorlageTableVals(
    content: ZeitplanungsvorlageEntityResponseDTO,
    setDeleteConfirmModalProps: (openConfirmModalProps: { isVisible: boolean; target: VorlageDeleteInterface }) => void,
    disabled: boolean,
    reloadData: () => void,
    isLight?: boolean,
  ): TableComponentProps<VorlageRow> {
    const ctrl = GlobalDI.getOrRegister('tabellensichtController', () => new TabellensichtController());
    const rows = this.createVorlageRows(content.dto.elemente, content.base.id, isLight);
    const columns: ColumnsType<VorlageRow> = [
      {
        title: i18n.t('zeit.zeitplanungsvorlagenTable.tabs.tabellensicht.tableHeads.phasenUndTermine'),
        key: 'c1',
        fixed: 'left',
        className: 'title-column',
        render: ctrl.titelRenderer,
      },
      {
        title: i18n.t('zeit.zeitplanungsvorlagenTable.tabs.tabellensicht.tableHeads.dauer'),
        key: 'c2',
        render: this.dauerRenderer,
      },
      {
        title: i18n.t('zeit.zeitplanungsvorlagenTable.tabs.tabellensicht.tableHeads.eigenschaften'),
        key: 'c3',
        render: ctrl.eigenschaftenRenderer,
      },
      {
        title: i18n.t('zeit.zeitplanungsvorlagenTable.tabs.tabellensicht.tableHeads.aktionen'),
        key: 'c4',
        render: (record: VorlageRow): ReactElement => {
          return this.dropdownRenderer(
            record,
            setDeleteConfirmModalProps,
            content.dto.erstellerType,
            disabled,
            content.dto.elemente,
            reloadData,
            content.dto.aktionen,
          );
        },
      },
    ];

    return {
      id: '',
      expandable: true,
      expandableCondition: (record: VorlageRow) => {
        if (record.children) {
          return true;
        }
        return false;
      },
      columns,
      content: rows,
      className: 'hidden-expanded-rows',
    };
  }
}
