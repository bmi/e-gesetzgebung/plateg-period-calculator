// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './zeitplanungsvorlage-table-view.less';

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { AktionType, ZeitplanungsvorlageEntityResponseDTO } from '@plateg/rest-api';
import { TableComponent, TableComponentProps } from '@plateg/theme';
import { GlobalDI } from '@plateg/theme/src/shares';

import { routes } from '../../../../../../shares/routes';
import { NewElementButton } from '../../../general-zeitelement/ new-element-button/component.react';
import { DeleteConfirmModalComponent } from '../../../general-zeitelement/delete-confirm-modal/component.react';
import { VorlageDeleteInterface, VorlageRow, ZeitplanungsvorlageTableViewController } from './controller.react';

export interface ZeitplanungsvorlageTableViewComponentProps {
  vorlage?: ZeitplanungsvorlageEntityResponseDTO;
  reloadData: () => void;
  disabled?: boolean;
  isLight?: boolean;
}

export function ZeitplanungsvorlageTableViewComponent(
  props: ZeitplanungsvorlageTableViewComponentProps,
): React.ReactElement {
  const zeitplanungsvorlageTableViewController = GlobalDI.getOrRegister(
    'zeitplanungsvorlageTableViewController',
    () => new ZeitplanungsvorlageTableViewController(),
  );
  const { t } = useTranslation();
  const [vorlage, setVorlage] = useState(props.vorlage);
  const [phasenTableVals, setPhasenTableVals] = useState<TableComponentProps<VorlageRow>>({
    id: '',
    columns: [],
    content: [],
    expandable: false,
    sorterOptions: [],
    customDefaultSortIndex: 0,
    expandableCondition: undefined,
    className: '',
  });

  const [deleteConfirmModalProps, setDeleteConfirmModalProps] = useState<{
    isVisible: boolean;
    target: VorlageDeleteInterface;
  }>({
    isVisible: false,
    target: {} as VorlageDeleteInterface,
  });

  useEffect(() => {
    setVorlage(props.vorlage);
  }, [props.vorlage]);

  useEffect(() => {
    if (vorlage) {
      setPhasenTableVals(
        zeitplanungsvorlageTableViewController.getZeitplanungsvorlageTableVals(
          vorlage,
          setDeleteConfirmModalProps,
          props.disabled || false,
          props.reloadData,
          props.isLight,
        ),
      );
    }
  }, [vorlage]);

  return (
    <div className="zeitplanungsvorlage-table">
      <>
        <DeleteConfirmModalComponent
          isVisible={deleteConfirmModalProps?.isVisible}
          target={deleteConfirmModalProps?.target}
          vorhabenart={vorlage?.dto.vorhabenart}
          setIsVisible={(isVisible: boolean) => {
            setDeleteConfirmModalProps({ isVisible: isVisible, target: deleteConfirmModalProps?.target });
          }}
          reloadData={props.reloadData}
          isZeitplanung={false}
          isLight={props.isLight}
        />
        <TableComponent
          id="period-zeitplanungsvorlage-view-table"
          columns={phasenTableVals.columns}
          content={phasenTableVals.content}
          filteredColumns={phasenTableVals.filteredColumns}
          sorterOptions={phasenTableVals.sorterOptions}
          customDefaultSortIndex={phasenTableVals.customDefaultSortIndex}
          className={phasenTableVals.className}
          expandable={phasenTableVals.expandable}
          expandableCondition={phasenTableVals.expandableCondition}
          expandedRowRender={phasenTableVals.expandedRowRender}
          expandAllRowsButtonIsVisible={true}
          expandAllRowsButtonCustomTexts={{
            open: t('zeit.newElementGeneral.expandAllRows'),
            close: t('zeit.newElementGeneral.closeAllRows'),
          }}
          rowClassName={(record: VorlageRow) => {
            return record.wichtig ? 'important-row' : '';
          }}
          hiddenRowCount={true}
          customButtonAriaLables={{
            opened: t('zeit.zeitplanungsvorlagenTable.tabs.tabellensicht.ariaLabels.buttonOpened'),
            closed: t('zeit.zeitplanungsvorlagenTable.tabs.tabellensicht.ariaLabels.buttonClosed'),
          }}
          additionalRightControl={
            vorlage && (
              <div className="button-container">
                <NewElementButton
                  disabled={!vorlage?.dto.aktionen.includes(AktionType.Schreiben)}
                  path={`/zeit/${routes.ZEITPLANUNGSVORLAGEN}/${vorlage?.base.id}/${routes.TABELLENSICHT}/${routes.NEUES_ELEMENT}`}
                />
              </div>
            )
          }
          isLight={props.isLight}
        />
      </>
    </div>
  );
}
