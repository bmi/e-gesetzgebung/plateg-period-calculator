// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';
import { useTranslation } from 'react-i18next';
export interface TabTextProps {
  activeTab: string;
  originalTable: string;
}

export function TabText(props: TabTextProps): React.ReactElement {
  const { t } = useTranslation();
  return <span key="zeit-vorlagen-tab">{t(`zeit.${props.originalTable}.tabs.${props.activeTab}.tabNav`)}</span>;
}
