// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './zeitplanungsvoralage-tab-view.less';

import { Button, Col, Row, TabsProps } from 'antd';
import Title from 'antd/lib/typography/Title';
import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Route, Switch, useHistory, useRouteMatch } from 'react-router-dom';
import { AjaxError } from 'rxjs/ajax';

import { AktionType, ErstellerType, ZeitplanungsvorlageEntityResponseDTO } from '@plateg/rest-api';
import {
  BreadcrumbComponent,
  BtnBackComponent,
  DropdownMenu,
  ErrorController,
  HeaderController,
  LoadingStatusController,
  TabsWrapper,
  TitleWrapperComponent,
} from '@plateg/theme';
import { GlobalDI } from '@plateg/theme/src/shares';

import { routes } from '../../../../shares/routes';
import { ZeitLightCollectionsKeys, ZeitLightLocalController } from '../../../zeit-light/controller-local';
import { ZeitHelpLink } from '../../component.react';
import { NewZeitplanungsvorlageElement } from '../general-zeitelement/new-time-element/component.react';
import { ZeitplanungController } from '../home-tabs-zeit/controller';
import { ZeitFreigabeModalRBAC } from '../home-tabs-zeit/freigabe-rbac-modal/component.react';
import { CopyZeitplanungsvorlageComponent } from '../new-zeitplanungsvorlage-modal/copy-zeitplanungsvorlage/component.react';
import { CommentsContainerComponent } from '../zeitplanung-tab-view/comments/component.react';
import { ParentLink } from '../zeitplanung-tab-view/header/parent-link/component.react';
import { ZeitplanungsLink } from '../zeitplanung-tab-view/header/zeitplanung-link/component.react';
import { TabText } from './header/tab-text/component.react';
import { ZeitplanungsvorlageTableViewComponent } from './tabs/table-view/component.react';

export function ZeitplanungsvorlagenTabViewComponent(props: Readonly<{ isLight?: boolean }>): React.ReactElement {
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const headerController = GlobalDI.getOrRegister('ezeitHeaderController', () => new HeaderController());
  const zeitplanungController = GlobalDI.getOrRegister('zeitplanungController', () => new ZeitplanungController());
  const zeitLightLocalController = GlobalDI.getOrRegister(
    'zeitLightLocalController',
    () => new ZeitLightLocalController(),
  );
  const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  const { t } = useTranslation();
  const history = useHistory();
  const routeMatcher = useRouteMatch<{
    vorlageType: string;
    id: string;
    tabKey: string;
    action?: string;
    archiv?: string;
  }>(`/zeit/:archiv?/:vorlageType(${routes.ZEITPLANUNGSVORLAGEN}|${routes.SYSTEMVORLAGEN})/:id/:tabKey/:action?`);
  const id = routeMatcher?.params.id as string;
  const vorlageType = routeMatcher?.params.vorlageType;
  const [activeTab, setActiveTab] = useState<string>(routeMatcher?.params?.tabKey ?? routes.TABELLENSICHT);
  const [zeitplanungsVorlage, setZeitplanungsVorlage] = useState<ZeitplanungsvorlageEntityResponseDTO>();
  const [copyZeitplanungsvorlageModalIsVisible, setCopyZeitplanungsvorlageModalIsVisible] = useState<boolean>(false);
  const copyZeitplanungsvorlageButtonRef = useRef<HTMLButtonElement>(null);

  useEffect(() => {
    loadData();
  }, [id]);

  useEffect(() => {
    const tabKey: string = routeMatcher?.params?.tabKey as string;
    if (tabKey !== activeTab) {
      setActiveTab(tabKey);
    }
  }, [routeMatcher?.params?.tabKey]);

  useEffect(() => {
    if (zeitplanungsVorlage) setBreadcrumb(zeitplanungsVorlage);
  }, [activeTab, zeitplanungsVorlage]);

  useEffect(() => {
    if (!routeMatcher?.params?.action && zeitplanungsVorlage) {
      setBreadcrumb(zeitplanungsVorlage);
    }
  }, [routeMatcher?.params?.action, zeitplanungsVorlage]);

  useEffect(() => {
    copyZeitplanungsvorlageButtonRef.current?.focus();
  }, [copyZeitplanungsvorlageModalIsVisible]);

  const errorHandler = (error: AjaxError) => {
    console.log('Error sub', error);
    errorCtrl.displayErrorMsg(error, 'zeit.generalErrorMsg');
    loadingStatusController.setLoadingStatus(false);
  };

  const loadData = (redirectURL?: string) => {
    loadingStatusController.setLoadingStatus(true);
    const vorlageCall =
      vorlageType === routes.SYSTEMVORLAGEN
        ? zeitplanungController.getSystemVorlageCall
        : zeitplanungController.getZeitplanungsvorlageCall;
    const apiCall = props.isLight
      ? zeitLightLocalController.getItemByIDObservable(id, `${vorlageType}Collection` as ZeitLightCollectionsKeys)
      : vorlageCall(id);
    const subscription = apiCall.subscribe({
      next: (data) => {
        setZeitplanungsVorlage(data as ZeitplanungsvorlageEntityResponseDTO);
        loadingStatusController.setLoadingStatus(false);
        if (redirectURL) {
          history.push(redirectURL);
        }
      },
      error: errorHandler,
    });
    return function cleanup() {
      subscription.unsubscribe();
    };
  };

  const setBreadcrumb = (zplanungsVorlage: ZeitplanungsvorlageEntityResponseDTO) => {
    const usersList =
      zeitplanungsVorlage?.dto.erstellerType !== ErstellerType.Admin && !props.isLight ? (
        <ZeitFreigabeModalRBAC
          disabled={!zeitplanungsVorlage?.dto?.aktionen?.includes(AktionType.BerechtigungenAendern)}
          objectTitle={zplanungsVorlage.dto.titel}
          objectId={zplanungsVorlage.base.id}
          isZeitplanungvorlage={true}
          leserechtOnly
          loadContent={() => history.push(`/zeit/${routes.ZEITPLANUNGSVORLAGEN}`)}
          allowChanges={[AktionType.BerechtigungenAendern, AktionType.EigeneBerechtigungenLoeschen].some((aktion) =>
            zeitplanungsVorlage?.dto?.aktionen?.includes(aktion),
          )}
          revokeObserverRight={zeitplanungsVorlage?.dto?.aktionen?.includes(AktionType.EigeneBerechtigungenLoeschen)}
        />
      ) : (
        <span key="users-icon-list-empty"></span>
      );
    const isArchive = routeMatcher?.params.archiv;
    const parentTabLinkUrl = `/zeit/${isArchive ? routes.ARCHIV : vorlageType}`;
    const parentTab = <ParentLink keyTab={isArchive ? routes.ARCHIV : vorlageType} url={parentTabLinkUrl} />;
    const zeitplanungsLink = (
      <ZeitplanungsLink key="zeit-vorlagen-tab">
        <>
          {zeitplanungsVorlage?.dto.titel} -{' '}
          <TabText key="zeit-tab" activeTab={activeTab} originalTable="zeitplanungsvorlagenTable" />
        </>
      </ZeitplanungsLink>
    );
    headerController.setHeaderProps({
      headerLeft: [<BreadcrumbComponent key="breadcrumb" items={[parentTab, zeitplanungsLink]} />],
      headerLast: [usersList, <ZeitHelpLink key="zeit-help-link" />],
      headerCenter: [],
      headerRight: [
        <Button
          id="ezeit-zeitplanungenVorlagenTableHeaderExport-btn"
          key="export-button"
          onClick={() => {
            zeitplanungController.exportZeitplanung(zeitplanungsVorlage?.dto, true);
          }}
          disabled={!zeitplanungsVorlage?.dto.aktionen?.includes(AktionType.Exportieren)}
        >
          {t('zeit.header.exportButton')}
        </Button>,
        <Button
          id="ezeit-zeitplanungenVorlagenTableHeaderSpeichern-btn"
          key="speichern-button"
          onClick={() => setCopyZeitplanungsvorlageModalIsVisible(true)}
          ref={copyZeitplanungsvorlageButtonRef}
          disabled={!zeitplanungsVorlage?.dto.aktionen?.includes(AktionType.NeueVersionErstellen)}
        >
          {t('zeit.zeitplanungsvorlagenTable.header.btnSpeichern')}
        </Button>,
        <DropdownMenu
          items={[
            {
              element: t('zeit.header.exportButton'),
              onClick: () => {
                zeitplanungController.exportZeitplanung(zeitplanungsVorlage?.dto, true);
              },
              disabled: () => !zeitplanungsVorlage?.dto.aktionen?.includes(AktionType.Exportieren),
            },
            {
              element: t('zeit.zeitplanungsvorlagenTable.header.btnSpeichern'),
              onClick: () => setCopyZeitplanungsvorlageModalIsVisible(true),
              disabled: () => !zeitplanungsVorlage?.dto.aktionen?.includes(AktionType.BerechtigungenLesen),
            },
          ]}
          elementId={'headerRightAlternative'}
          overlayClass={'headerRightAlternative-overlay'}
        />,
      ],
    });
  };
  const tabItems: TabsProps['items'] = [
    {
      key: routes.TABELLENSICHT,
      label: t('zeit.zeitplanungsvorlagenTable.tabs.tabellensicht.tabNav'),
      children: (
        <ZeitplanungsvorlageTableViewComponent
          vorlage={zeitplanungsVorlage}
          reloadData={loadData}
          disabled={zeitplanungsVorlage?.dto.readOnlyAccess}
          isLight={props.isLight}
        />
      ),
    },
  ];
  if (!props.isLight && tabItems.length === 1) {
    tabItems.push({
      key: routes.KOMMENTARE,
      label: t('zeit.zeitplanungsvorlagenTable.tabs.kommentare.tabNav'),
      children: zeitplanungsVorlage ? (
        <CommentsContainerComponent
          zeitItemId={zeitplanungsVorlage.base.id}
          isReadOnly={!zeitplanungsVorlage.dto.aktionen.includes(AktionType.Kommentieren)}
          isZeitplanungsVorlage={true}
        />
      ) : undefined,
    });
  }
  return (
    <>
      <CopyZeitplanungsvorlageComponent
        isVisible={copyZeitplanungsvorlageModalIsVisible}
        setIsVisible={setCopyZeitplanungsvorlageModalIsVisible}
        resetTab={() => loadingStatusController.setLoadingStatus(false)}
        selectedZeitplanungsvorlage={zeitplanungsVorlage?.dto}
        selectedId={zeitplanungsVorlage?.base.id}
        isLight={props.isLight}
      />
      <Switch>
        <Route
          path={[
            `/zeit/${routes.ZEITPLANUNGSVORLAGEN}/:id/${routes.TABELLENSICHT}/${routes.NEUES_ELEMENT}`,
            `/zeit/${routes.ZEITPLANUNGSVORLAGEN}/:id/${routes.TABELLENSICHT}/${routes.ELEMENT_BEARBEITEN}`,
            `/zeit/${routes.ZEITPLANUNGSVORLAGEN}/:id/${routes.TABELLENSICHT}/${routes.UEBERSICHT}`,
            `/zeit/${routes.SYSTEMVORLAGEN}/:id/${routes.TABELLENSICHT}/${routes.UEBERSICHT}`,
            `/zeit/${routes.ARCHIV}/${routes.ZEITPLANUNGSVORLAGEN}/:id/${routes.TABELLENSICHT}/${routes.UEBERSICHT}`,
          ]}
        >
          <NewZeitplanungsvorlageElement
            currentZeitParent={zeitplanungsVorlage}
            reloadZeitParent={loadData}
            isLight={props.isLight}
          />
        </Route>
        <Route
          path={[
            `*/${routes.ZEITPLANUNGSVORLAGEN}/:id/${routes.TABELLENSICHT}`,
            `*/${routes.SYSTEMVORLAGEN}/:id/${routes.TABELLENSICHT}`,
            `*/${routes.ZEITPLANUNGSVORLAGEN}/:id/${routes.KOMMENTARE}`,
          ]}
        >
          <div className="zeitplanungsvorlage-tabs">
            <TitleWrapperComponent>
              <Row>
                <Col xs={{ span: 22, offset: 1 }}>
                  <div className="heading-holder">
                    <Title className="main-title" level={1}>
                      {zeitplanungsVorlage?.dto.titel}
                    </Title>
                  </div>
                </Col>
              </Row>
            </TitleWrapperComponent>
            <Row>
              <Col xs={{ span: 22, offset: 1 }}>
                <TabsWrapper
                  activeKey={activeTab}
                  className="my-votes-tabs standard-tabs"
                  onChange={(key: string) => {
                    const archivePath = routeMatcher?.params.archiv ? `${routes.ARCHIV}/` : '';
                    history.push(`/zeit/${archivePath}zeitplanungsvorlagen/${id}/${key}`);
                  }}
                  moduleName="Zeitplanung"
                  items={tabItems}
                />

                <BtnBackComponent
                  id="ezeit-zeitplanung-back-btn"
                  url={`/zeit/${routeMatcher?.params.archiv ? routes.ARCHIV : vorlageType}`}
                />
              </Col>
            </Row>
          </div>
        </Route>
      </Switch>
    </>
  );
}
