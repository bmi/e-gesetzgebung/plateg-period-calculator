// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Observable } from 'rxjs';

import {
  RegelungsvorhabenTypType,
  ZeitplanungsvorlageEntityResponseDTO,
  ZeitplanungsvorlageLightControllerApi,
  ZeitvorlagevorlagenBauplanDTO,
} from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme/src/shares';
export class ZeitLighBackendController {
  private readonly zeitplanungsvorlageLightControllerApi = GlobalDI.getOrRegister(
    'zeitplanungsvorlageLightControllerApi',
    () => new ZeitplanungsvorlageLightControllerApi(),
  );

  public getZeitplanungsvorlagenAlleLightCall = (): Observable<ZeitplanungsvorlageEntityResponseDTO[]> => {
    return this.zeitplanungsvorlageLightControllerApi.getZeitplanungsvorlagenAlleLight();
  };

  public getBauplaeneForVorhabenartLightCall = (
    vorhabenart: RegelungsvorhabenTypType,
  ): Observable<ZeitvorlagevorlagenBauplanDTO[]> => {
    return this.zeitplanungsvorlageLightControllerApi.getBauplaeneForVorhabenartLight({ vorhabenart });
  };
}
