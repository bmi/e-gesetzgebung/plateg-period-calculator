// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { setHours } from 'date-fns';
import { createHashHistory } from 'history';
import { Observable, of } from 'rxjs';
import { v4 as uuidv4 } from 'uuid';

import {
  AktionType,
  ErstellerType,
  NaechstesElementDTO,
  RegelungsvorhabenTypType,
  RolleLokalType,
  UserEntityDTOStatusEnum,
  UserEntityResponseDTO,
  ZeitplanungEntityResponseDTO,
  ZeitplanungEntityShortDTO,
  ZeitplanungLightControllerApi,
  ZeitplanungsvorlageEntityDTO,
  ZeitplanungsvorlageEntityListResponseShortDTO,
  ZeitplanungsvorlageEntityResponseDTO,
  ZeitplanungsvorlageEntityShortDTO,
  ZeitplanungsvorlagenelementEntityResponseDTOZeitplanungsvorlagenelementEntityDTO,
  ZeitplanungsvorlageTableDTO,
  ZeitplanungTableDTO,
} from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme';

import { routes } from '../../shares/routes';
import { ZeitplanungFormVal } from '../zeit/main/new-zeitplanung-modal/component.react';

export const collectionKey = 'zeitplanungCollection';
export enum ZeitLightCollectionsKeys {
  Zeitplanung = 'zeitplanungCollection',
  Zeitplanungsvorlagen = 'zeitplanungsvorlagenCollection',
  Systemvorlagen = 'systemvorlagenCollection',
}

export interface ZeitCollectionInterface {
  [key: string]: ZeitplanungsvorlageEntityResponseDTO | ZeitplanungEntityResponseDTO;
}

export class ZeitLightLocalController {
  private readonly history = createHashHistory();

  private _getDraftCollection(collectionKey: ZeitLightCollectionsKeys): ZeitCollectionInterface {
    const draftCollectionString: string = global.window.localStorage.getItem(collectionKey) || '{}';
    return JSON.parse(draftCollectionString) as ZeitCollectionInterface;
  }

  private _setDraftCollection(collection: ZeitCollectionInterface, collectionKey: ZeitLightCollectionsKeys): void {
    global.window.localStorage.setItem(collectionKey, JSON.stringify(collection));
  }

  public getScrollCalendarStatusObservable(): Observable<boolean> {
    const status = global.window.localStorage.getItem('scrollCalendarStatus') || 'true';
    return of(JSON.parse(status) as boolean);
  }
  public setScrollCalendarStatusObservable(status: boolean): Observable<void> {
    global.window.localStorage.setItem('scrollCalendarStatus', JSON.stringify(status));
    return of(void 0);
  }

  public getDraftListObservable(
    collectionKey: ZeitLightCollectionsKeys,
  ): Observable<ZeitplanungsvorlageEntityListResponseShortDTO> {
    return of({ dtos: this._getDraftList(collectionKey) });
  }

  private _getDraftList(collectionKey: ZeitLightCollectionsKeys): ZeitplanungsvorlageEntityResponseDTO[] {
    const draftCollection: ZeitCollectionInterface = this._getDraftCollection(collectionKey);
    const draftList: ZeitplanungsvorlageEntityResponseDTO[] = [];
    Object.keys(draftCollection).forEach((key) => {
      const tmpRegelungsentwurf: ZeitplanungsvorlageEntityResponseDTO = draftCollection[key];
      tmpRegelungsentwurf['id'] = key;
      draftList.push(tmpRegelungsentwurf);
    });

    return draftList;
  }

  /**
   * Get item by it from selected category
   * @return Observable<ZeitplanungsvorlageEntityResponseDTO>
   */
  public getItemByIDObservable(
    id: string,
    collectionKey: ZeitLightCollectionsKeys,
  ): Observable<ZeitplanungsvorlageEntityResponseDTO | ZeitplanungEntityResponseDTO> {
    return of(this.getItemByID(id, collectionKey));
  }

  public getItemByID(
    id: string,
    collectionKey: ZeitLightCollectionsKeys,
  ): ZeitplanungsvorlageEntityResponseDTO | ZeitplanungEntityResponseDTO {
    return this._getDraftCollection(collectionKey)[id];
  }

  /**
   * Syncing data from BE with already created data from user in local storage
   * @param data
   * @param collectionKey
   */
  public syncLocalDrafts(
    data: ZeitplanungsvorlageEntityResponseDTO[] | ZeitplanungEntityResponseDTO[],
    collectionKey: ZeitLightCollectionsKeys,
  ) {
    const exisitngData: ZeitCollectionInterface = this._getDraftCollection(collectionKey);
    if (Object.keys(exisitngData).length === 0) {
      data.forEach((item) => (exisitngData[item.base.id] = item));
      this._setDraftCollection(exisitngData, collectionKey);
    }
  }

  /**
   * Update item with new bearbeitetAm value ans store to Local Storage
   * @param item
   * @param collectionKey
   */
  public updateItem(
    item: ZeitplanungsvorlageEntityResponseDTO | ZeitplanungEntityResponseDTO,
    collectionKey: ZeitLightCollectionsKeys,
  ) {
    item.base.bearbeitetAm = new Date().toISOString();
    item.dto.erstellerType = ErstellerType.Selbst;
    item.dto.readOnlyAccess = false;
    item.dto.erstelltVon = userGuest;

    const exisitngData: ZeitCollectionInterface = this._getDraftCollection(collectionKey);
    exisitngData[item.base.id] = item;
    this._setDraftCollection(exisitngData, collectionKey);
  }

  public getVorlagenTableData(vorlagenType?: ZeitLightCollectionsKeys): ZeitplanungsvorlageTableDTO[] {
    const exisitngData: ZeitCollectionInterface = this._getDraftCollection(
      vorlagenType || ZeitLightCollectionsKeys.Zeitplanungsvorlagen,
    );
    let preparedData: ZeitplanungsvorlageTableDTO[] = [];
    if (Object.keys(exisitngData).length !== 0) {
      preparedData = Object.values(exisitngData).map((vorhabe) => {
        return {
          id: vorhabe.base.id,
          titel: vorhabe.dto.titel,
          erstelltAm: vorhabe.base.erstelltAm,
          erstelltVon: vorhabe.dto.erstelltVon,
          bearbeitetAm: vorhabe.base.bearbeitetAm,
          bearbeitetVon: vorhabe.dto.erstelltVon,
          erstellerType: vorhabe.dto.erstellerType,
          vorhabenart: (vorhabe.dto as ZeitplanungsvorlageEntityDTO).vorhabenart,
          dauer: (vorhabe.dto as ZeitplanungsvorlageEntityDTO).dauer,
          freigaben: [],
          rolleTyp: RolleLokalType.Gast,
          aktionen: [
            vorlagenType === ZeitLightCollectionsKeys.Systemvorlagen ? AktionType.Lesen : AktionType.Schreiben,
          ],
        };
      });
    }
    return preparedData;
  }

  public getZeitplanungenTableData(): ZeitplanungTableDTO[] {
    const exisitngData: { [key: string]: ZeitplanungEntityResponseDTO } = this._getDraftCollection(
      ZeitLightCollectionsKeys.Zeitplanung,
    ) as { [key: string]: ZeitplanungEntityResponseDTO };
    let preparedData: ZeitplanungTableDTO[] = [];
    if (Object.keys(exisitngData).length !== 0) {
      preparedData = Object.values(exisitngData).map((zeit) => {
        return {
          id: zeit.base.id,
          titel: zeit.dto.titel,
          erstelltAm: zeit.base.erstelltAm,
          erstelltVon: zeit.dto.erstelltVon,
          bearbeitetAm: zeit.base.bearbeitetAm,
          bearbeitetVon: zeit.dto.erstelltVon,
          erstellerType: zeit.dto.erstellerType,
          beschreibung: zeit.dto.beschreibung,
          naechstesElement: zeit.dto.naechstesElement as NaechstesElementDTO,
          erstePhaseBeginntAm: zeit.dto.erstePhaseBeginntAm as string,
          letztePhaseEndetAm: zeit.dto.letztePhaseEndetAm as string,
        } as ZeitplanungTableDTO;
      });
    }
    return preparedData;
  }

  public createNewVorlageLight = (
    setIsVisible: (isVisible: boolean) => void,
    copy: boolean,
    newZeitplanungsVorlage?: ZeitplanungsvorlageEntityShortDTO,
  ): void => {
    if (newZeitplanungsVorlage) {
      setIsVisible(false);
      const newId = uuidv4();
      const date = new Date().toISOString();
      const tempVorlage: ZeitplanungsvorlageEntityResponseDTO = {
        base: {
          id: newId,
          erstelltAm: date,
          bearbeitetAm: date,
        },
        dto: {
          ...newZeitplanungsVorlage,
          erstellerType: ErstellerType.Selbst,
          erstelltVon: userGuest,
          readOnlyAccess: false,
          elemente: this._getElementeList(newZeitplanungsVorlage.sourceId),
          dauer: this._getDauer(newZeitplanungsVorlage.sourceId),
          aktionen: [AktionType.Lesen, AktionType.Schreiben, AktionType.Exportieren, AktionType.NeueVersionErstellen],
          rolleTyp: RolleLokalType.Gast,
        },
      };
      const exisitngData: ZeitCollectionInterface = this._getDraftCollection(
        ZeitLightCollectionsKeys.Zeitplanungsvorlagen,
      );

      exisitngData[newId] = tempVorlage;
      this._setDraftCollection(exisitngData, ZeitLightCollectionsKeys.Zeitplanungsvorlagen);
      this.handleSaveZeitplanungsvorlage(newId);
    }
  };

  private readonly zeitplanungLightControllerApi = GlobalDI.getOrRegister(
    'zeitplanungLightControllerApi',
    () => new ZeitplanungLightControllerApi(),
  );
  public createNewZeitplanungLight = (
    setIsVisible: (isVisible: boolean) => void,
    resetTab?: (key: string) => void,
    newZeitplanung?: ZeitplanungFormVal,
  ): void => {
    if (newZeitplanung) {
      newZeitplanung.startdatum = setHours(newZeitplanung.startdatumDate, 12) as unknown as string;
      setIsVisible(false);
      const newId = uuidv4();
      const tempZeitplanung: ZeitplanungEntityShortDTO = {
        sourceId: newZeitplanung.sourceId,
        titel: newZeitplanung.titel,
        beschreibung: newZeitplanung.beschreibung,
        regelungsvorhabenId: '',
        startdatum: newZeitplanung.startdatum,
      };
      this.zeitplanungLightControllerApi
        .createZeitplanungLight({
          zeitplanungCreateFromVorlageRequestDTO: {
            zeitplanungsvorlage: tempZeitplanung.sourceId
              ? ((
                  this.getItemByID(tempZeitplanung.sourceId, ZeitLightCollectionsKeys.Zeitplanungsvorlagen) ||
                  this.getItemByID(tempZeitplanung.sourceId, ZeitLightCollectionsKeys.Systemvorlagen)
                ).dto as ZeitplanungsvorlageEntityDTO)
              : ({} as ZeitplanungsvorlageEntityDTO),
            zeitplanungRequest: tempZeitplanung,
          },
        })
        .subscribe((data: ZeitplanungEntityResponseDTO) => {
          const zeitplanungNew = { ...data };
          zeitplanungNew.base.id = newId;
          zeitplanungNew.dto.erstelltVon = userGuest;
          zeitplanungNew.dto.regelungsvorhabenId = '';
          zeitplanungNew.dto.vorhabenart = newZeitplanung.vorhabenart as RegelungsvorhabenTypType;
          const exisitngData: ZeitCollectionInterface = this._getDraftCollection(ZeitLightCollectionsKeys.Zeitplanung);

          exisitngData[newId] = zeitplanungNew;
          this._setDraftCollection(exisitngData, ZeitLightCollectionsKeys.Zeitplanung);
          this.handleSaveZeitplanung(newId, resetTab);
        });
    }
  };

  private _getElementeList = (
    id: string | undefined,
  ): ZeitplanungsvorlagenelementEntityResponseDTOZeitplanungsvorlagenelementEntityDTO[] => {
    let result = [] as ZeitplanungsvorlagenelementEntityResponseDTOZeitplanungsvorlagenelementEntityDTO[];
    if (id) {
      const item =
        this.getItemByID(id, ZeitLightCollectionsKeys.Zeitplanungsvorlagen) ||
        this.getItemByID(id, ZeitLightCollectionsKeys.Systemvorlagen);
      result = item.dto.elemente;
    }
    return result;
  };

  private _getDauer = (id: string | undefined): number => {
    let dauer = 0;
    if (id) {
      const item =
        this.getItemByID(id, ZeitLightCollectionsKeys.Zeitplanungsvorlagen) ||
        this.getItemByID(id, ZeitLightCollectionsKeys.Systemvorlagen);
      dauer = (item.dto as ZeitplanungsvorlageEntityDTO).dauer;
    }
    return dauer;
  };

  public handleSaveZeitplanungsvorlage = (id: string, copy?: boolean): void => {
    this.history.push(`/zeit/${routes.ZEITPLANUNGSVORLAGEN}/${id}/${routes.TABELLENSICHT}`);
  };

  public handleSaveZeitplanung = (id: string, resetTab?: (key: string) => void): void => {
    resetTab?.(routes.MEINE_ZEITPLANUNGEN);
    if (!location.pathname.endsWith('tabellensicht')) {
      this.history.push(`/zeit/${routes.MEINE_ZEITPLANUNGEN}/${id}/tabellensicht`);
    }
  };
}
const userGuest: UserEntityResponseDTO = {
  base: {
    id: 'user-guest',
    erstelltAm: '',
    bearbeitetAm: '',
  },
  dto: {
    name: 'Gast',
    email: 'Gast',
    status: UserEntityDTOStatusEnum.Ok,
  },
};
