// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { map, Observable, of } from 'rxjs';

import {
  AktionType,
  PhaseEntityDTO,
  PhasenvorlageEntityDTO,
  TerminEntityDTO,
  TerminvorlageEntityDTO,
  ZeitplanungEntityDTO,
  ZeitplanungEntityResponseDTO,
  ZeitplanungLightControllerApi,
  ZeitplanungPhaseRequest,
  ZeitplanungsvorlageEntityDTO,
  ZeitplanungsvorlageEntityResponseDTO,
  ZeitplanungsvorlageLightControllerApi,
  ZeitplanungsvorlagePhaseRequest,
  ZeitplanungsvorlageTerminRequest,
  ZeitplanungTerminRequest,
} from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme';

import { ZeitLightCollectionsKeys, ZeitLightLocalController } from './controller-local';

export const collectionKey = 'zeitplanungCollection';

export interface ZeitCollectionInterface {
  [key: string]: ZeitplanungsvorlageEntityResponseDTO;
}

export class ZeitLightController {
  private readonly zeitplanungsvorlageLightControllerApi = GlobalDI.getOrRegister(
    'zeitplanungsvorlageLightControllerApi',
    () => new ZeitplanungsvorlageLightControllerApi(),
  );
  private readonly zeitplanungLightControllerApi = GlobalDI.getOrRegister(
    'zeitplanungLightControllerApi',
    () => new ZeitplanungLightControllerApi(),
  );
  private readonly zeitLightLocalController = GlobalDI.getOrRegister(
    'zeitLightLocalController',
    () => new ZeitLightLocalController(),
  );
  public addDateToZeitplanungsvorlageLight = (data: {
    id: string;
    terminvorlageEntityDTO: TerminvorlageEntityDTO;
  }): Observable<void> => {
    const vorlage = this.zeitLightLocalController.getItemByID(data.id, ZeitLightCollectionsKeys.Zeitplanungsvorlagen);
    const zeitplanungsvorlageTerminRequest: ZeitplanungsvorlageTerminRequest = {
      zeitplanungsvorlage: vorlage.dto,
      terminvorlage: data.terminvorlageEntityDTO,
    };
    return this.zeitplanungsvorlageLightControllerApi
      .addDateToZeitplanungsvorlageLight({
        zeitplanungsvorlageTerminRequest: zeitplanungsvorlageTerminRequest,
      })
      .pipe(map((data) => this.updateVorlage(vorlage, data.dto)));
  };

  public addDateToZeitplanungLight = (data: { id: string; terminEntityDTO: TerminEntityDTO }): Observable<void> => {
    const zeitplanung = this.zeitLightLocalController.getItemByID(
      data.id,
      ZeitLightCollectionsKeys.Zeitplanung,
    ) as ZeitplanungEntityResponseDTO;
    const zeitplanungTerminRequest: ZeitplanungTerminRequest = {
      zeitplanung: zeitplanung.dto,
      termin: data.terminEntityDTO,
    };
    return this.zeitplanungLightControllerApi
      .addDateToZeitplanungLight({
        zeitplanungTerminRequest: zeitplanungTerminRequest,
      })
      .pipe(map((value) => this.updateZeitplanung(zeitplanung, value.dto)));
  };

  public modifyDateToZeitplanungsvorlageLight = (data: {
    elementId: string;
    terminvorlageEntityDTO: TerminvorlageEntityDTO;
    vorlageId: string | undefined;
  }): Observable<void> => {
    const vorlage = this.zeitLightLocalController.getItemByID(
      data.vorlageId as string,
      ZeitLightCollectionsKeys.Zeitplanungsvorlagen,
    );
    const zeitplanungsvorlageTerminRequest: ZeitplanungsvorlageTerminRequest = {
      zeitplanungsvorlage: vorlage.dto,
      terminvorlage: data.terminvorlageEntityDTO,
    };

    return this.zeitplanungsvorlageLightControllerApi
      .modifyDateInZeitplanungsvorlageLight({
        elementId: data.elementId,
        zeitplanungsvorlageTerminRequest: zeitplanungsvorlageTerminRequest,
      })
      .pipe(map((data) => this.updateVorlage(vorlage, data.dto)));
  };

  public modifyDateInZeitplanungCallLight = (data: {
    elementId: string;
    terminEntityDTO: TerminEntityDTO;
    zeitplanungId: string;
  }): Observable<void> => {
    const zeitplanung = this.zeitLightLocalController.getItemByID(
      data.zeitplanungId,
      ZeitLightCollectionsKeys.Zeitplanung,
    ) as ZeitplanungEntityResponseDTO;
    const zeitplanungTerminRequest: ZeitplanungTerminRequest = {
      zeitplanung: zeitplanung.dto,
      termin: data.terminEntityDTO,
    };
    return this.zeitplanungLightControllerApi
      .modifyDateInZeitplanungLight({
        elementId: data.elementId,
        zeitplanungTerminRequest: zeitplanungTerminRequest,
      })
      .pipe(map((data) => this.updateZeitplanung(zeitplanung, data.dto)));
  };

  public modifyPhaseInZeitplanungCallLight = (data: {
    elementId: string;
    phaseEntityDTO: PhaseEntityDTO;
    zeitplanungId: string;
  }): Observable<void> => {
    const zeitplanung = this.zeitLightLocalController.getItemByID(
      data.zeitplanungId,
      ZeitLightCollectionsKeys.Zeitplanung,
    ) as ZeitplanungEntityResponseDTO;
    const zeitplanungPhaseRequest: ZeitplanungPhaseRequest = {
      zeitplanung: zeitplanung.dto,
      phase: data.phaseEntityDTO,
    };
    return this.zeitplanungLightControllerApi
      .modifyPhaseInZeitplanungLight({
        elementId: data.elementId,
        zeitplanungPhaseRequest: zeitplanungPhaseRequest,
      })
      .pipe(map((data) => this.updateZeitplanung(zeitplanung, data.dto)));
  };

  public addPhaseToZeitplanungsvorlageLight = (data: {
    id: string;
    phasenvorlageEntityDTO: PhasenvorlageEntityDTO;
  }): Observable<void> => {
    const vorlage = this.zeitLightLocalController.getItemByID(data.id, ZeitLightCollectionsKeys.Zeitplanungsvorlagen);
    const zeitplanungsvorlagePhaseRequest: ZeitplanungsvorlagePhaseRequest = {
      zeitplanungsvorlage: vorlage.dto,
      phasenvorlage: data.phasenvorlageEntityDTO,
    };
    return this.zeitplanungsvorlageLightControllerApi
      .addPhaseToZeitplanungsvorlageLight({
        zeitplanungsvorlagePhaseRequest: zeitplanungsvorlagePhaseRequest,
      })
      .pipe(map((data) => this.updateVorlage(vorlage, data.dto)));
  };

  public addPhaseToZeitplanungLight = (data: { id: string; phaseEntityDTO: PhaseEntityDTO }): Observable<void> => {
    const zeitplanung = this.zeitLightLocalController.getItemByID(
      data.id,
      ZeitLightCollectionsKeys.Zeitplanung,
    ) as ZeitplanungEntityResponseDTO;
    const zeitplanungPhaseRequest: ZeitplanungPhaseRequest = {
      zeitplanung: zeitplanung.dto,
      phase: data.phaseEntityDTO,
    };
    return this.zeitplanungLightControllerApi
      .addPhaseToZeitplanungLight({
        zeitplanungPhaseRequest: zeitplanungPhaseRequest,
      })
      .pipe(map((data) => this.updateZeitplanung(zeitplanung, data.dto)));
  };

  public modifyPhaseToZeitplanungsvorlageCallLight = (data: {
    elementId: string;
    phasenvorlageEntityDTO: PhasenvorlageEntityDTO;
    vorlageId: string | undefined;
  }): Observable<void> => {
    const vorlage = this.zeitLightLocalController.getItemByID(
      data.vorlageId as string,
      ZeitLightCollectionsKeys.Zeitplanungsvorlagen,
    );
    const zeitplanungsvorlagePhaseRequest: ZeitplanungsvorlagePhaseRequest = {
      zeitplanungsvorlage: vorlage.dto,
      phasenvorlage: data.phasenvorlageEntityDTO,
    };
    return this.zeitplanungsvorlageLightControllerApi
      .modifyPhaseInZeitplanungsvorlageLight({
        elementId: data.elementId,
        zeitplanungsvorlagePhaseRequest: zeitplanungsvorlagePhaseRequest,
      })
      .pipe(map((data) => this.updateVorlage(vorlage, data.dto)));
  };

  public deleteZeitvorlageFromZeitplanungsvorlageCallLight = (
    elementId: string,
    parentId: string,
  ): Observable<void> => {
    const vorlage = this.zeitLightLocalController.getItemByID(parentId, ZeitLightCollectionsKeys.Zeitplanungsvorlagen);
    return this.zeitplanungsvorlageLightControllerApi
      .deleteZeitplanungsvorlagenelementFromZeitplanungsvorlageLight({
        elementId: elementId,
        zeitplanungsvorlageEntityDTO: vorlage.dto,
      })
      .pipe(map((data) => this.updateVorlage(vorlage, data.dto)));
  };

  public deleteZeitplanungselementFromZeitplanungCallLight = (
    elementId: string,
    parentId: string,
    zeitplanungAnpassen: boolean,
  ): Observable<void> => {
    const zeitplanung = this.zeitLightLocalController.getItemByID(parentId, ZeitLightCollectionsKeys.Zeitplanung);
    return this.zeitplanungLightControllerApi
      .deleteZeitplanungselementFromZeitplanungLight({
        elementId: elementId,
        zeitplanungAnpassen,
        zeitplanungEntityDTO: zeitplanung.dto,
      })
      .pipe(map((data) => this.updateZeitplanung(zeitplanung, data.dto)));
  };

  public updateVorlage = (
    vorlage: ZeitplanungsvorlageEntityResponseDTO,
    dto: ZeitplanungsvorlageEntityDTO,
  ): Observable<void> => {
    const dtoWithAktionen = { ...dto, aktionen: [AktionType.Lesen, AktionType.Schreiben] };
    const updatedVorlage = { ...vorlage, dto: dtoWithAktionen };
    this.zeitLightLocalController.updateItem(updatedVorlage, ZeitLightCollectionsKeys.Zeitplanungsvorlagen);
    return of(void 0);
  };

  public updateZeitplanung = (
    zeitplanung: ZeitplanungEntityResponseDTO,
    dto: ZeitplanungEntityDTO,
  ): Observable<void> => {
    const updatedZeitplanung = { ...zeitplanung, dto: dto };
    this.zeitLightLocalController.updateItem(updatedZeitplanung, ZeitLightCollectionsKeys.Zeitplanung);
    return of(void 0);
  };
}
