// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import chai, { expect } from 'chai';
import chaiString from 'chai-string';
import { Observable } from 'rxjs';
import sinon from 'sinon';

import {
  ErstellerType,
  RegelungsvorhabenTypType,
  ZeitplanungEntityResponseDTO,
  ZeitplanungLightControllerApi,
  ZeitplanungsvorlageEntityResponseDTO,
  ZeitplanungsvorlagenBauplan,
} from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme';

import { ZeitCollectionInterface, ZeitLightCollectionsKeys, ZeitLightLocalController } from './controller-local';

chai.use(chaiString);
describe('createNewVorlageLight', () => {
  const ctrl = new ZeitLightLocalController();
  let handleSaveStub: sinon.SinonStub;
  const testNewVorlage = {
    erstellerType: 'KEINE_ANGABE',
    erstelltVon: undefined,
    titel: 'TEST NEW Vorlage',
    beschreibung: undefined,
    readOnlyAccess: true,
    vorhabenart: RegelungsvorhabenTypType.Gesetz,
    sourceId: 'test-vorlage-id',
  };
  beforeEach(() => {
    handleSaveStub = sinon.stub(ctrl, 'handleSaveZeitplanungsvorlage');
    ctrl.syncLocalDrafts([testVorlage], ZeitLightCollectionsKeys.Zeitplanungsvorlagen);
  });
  afterEach(() => {
    handleSaveStub.restore();
    ctrl.syncLocalDrafts([], ZeitLightCollectionsKeys.Zeitplanungsvorlagen);
  });

  it('Add to Storage', () => {
    ctrl.createNewVorlageLight(() => {}, false, testNewVorlage);

    const draftCollectionString: string =
      global.window.localStorage.getItem(ZeitLightCollectionsKeys.Zeitplanungsvorlagen) || '';
    const draftCollection: ZeitCollectionInterface = JSON.parse(draftCollectionString) as ZeitCollectionInterface;
    expect(Object.keys(draftCollection)).to.have.lengthOf(2);
  });
});

describe('createNewZeitplanungLight', () => {
  const ctrl = new ZeitLightLocalController();
  const zeitplanungLightControllerApi = GlobalDI.getOrRegister(
    'zeitplanungLightControllerApi',
    () => new ZeitplanungLightControllerApi(),
  );
  let handleSaveStub: sinon.SinonStub;
  let createZeitplanungLightCallStub: sinon.SinonStub;
  const testZeitplanung = {
    titel: 'New zeit',
    beschreibung: undefined,
    readOnlyAccess: true,
    sourceId: 'test-vorlage-id',
    startdatum: '2022-11-30T11:40:02.364Z',
    startdatumDate: new Date('2022-11-30T09:40:02.364Z'),
    regelungsvorhabenId: '',
  };
  const testResponse = {
    base: {
      id: '',
      erstelltAm: 'string',
      bearbeitetAm: 'string',
    },
    dto: {
      titel: 'New zeit',
      beschreibung: undefined,
      readOnlyAccess: true,
      sourceId: 'test-vorlage-id',
      startdatum: '2022-11-30T11:40:02.364Z',
      startdatumDate: new Date('2022-11-30T09:40:02.364Z'),
      regelungsvorhabenId: '',
    },
  };
  beforeEach(() => {
    createZeitplanungLightCallStub = sinon.stub(zeitplanungLightControllerApi, 'createZeitplanungLight').returns(
      new Observable(function (observer) {
        observer.next(testResponse);
        observer.complete();
      }),
    );
    handleSaveStub = sinon.stub(ctrl, 'handleSaveZeitplanung');
    ctrl.syncLocalDrafts([testVorlage], ZeitLightCollectionsKeys.Zeitplanung);
  });
  afterEach(() => {
    handleSaveStub.restore();
    createZeitplanungLightCallStub.restore();
    ctrl.syncLocalDrafts([], ZeitLightCollectionsKeys.Zeitplanungsvorlagen);
    createZeitplanungLightCallStub.resetHistory();
  });

  it('createNewZeitplanungLight Add to Storage', () => {
    ctrl.createNewZeitplanungLight(() => {}, undefined, testZeitplanung);
    sinon.assert.calledOnce(createZeitplanungLightCallStub);
    const draftCollectionString: string =
      global.window.localStorage.getItem(ZeitLightCollectionsKeys.Zeitplanungsvorlagen) || '';
    const draftCollection: ZeitCollectionInterface = JSON.parse(draftCollectionString) as ZeitCollectionInterface;
    expect(Object.keys(draftCollection)).to.have.lengthOf(2);
  });
});

describe('getVorlagenTableData', () => {
  const ctrl = new ZeitLightLocalController();
  let handleSaveStub: sinon.SinonStub;
  beforeEach(() => {
    handleSaveStub = sinon.stub(ctrl, 'handleSaveZeitplanungsvorlage');
    ctrl.syncLocalDrafts([testVorlage], ZeitLightCollectionsKeys.Zeitplanungsvorlagen);
  });
  afterEach(() => {
    handleSaveStub.restore();
    ctrl.syncLocalDrafts([], ZeitLightCollectionsKeys.Zeitplanungsvorlagen);
  });

  it('Get list for table', () => {
    const list = ctrl.getVorlagenTableData();
    expect(list).to.have.lengthOf(2);
    expect(list[0].dauer).to.equal(3);
  });
});

describe('getDraftListObservable', () => {
  const ctrl = new ZeitLightLocalController();
  let handleSaveStub: sinon.SinonStub;
  beforeEach(() => {
    handleSaveStub = sinon.stub(ctrl, 'handleSaveZeitplanungsvorlage');
    ctrl.syncLocalDrafts([testVorlage], ZeitLightCollectionsKeys.Zeitplanungsvorlagen);
  });
  afterEach(() => {
    handleSaveStub.restore();
    ctrl.syncLocalDrafts([], ZeitLightCollectionsKeys.Zeitplanungsvorlagen);
  });

  it('Get list for table', (done) => {
    ctrl.getDraftListObservable(ZeitLightCollectionsKeys.Zeitplanungsvorlagen).subscribe({
      next: (data) => {
        expect(data.dtos).to.have.lengthOf(2);
        done();
      },
    });
  });

  it('Get item by Id', (done) => {
    ctrl.getItemByIDObservable('test-vorlage-id', ZeitLightCollectionsKeys.Zeitplanungsvorlagen).subscribe({
      next: (data) => {
        expect(data.dto.titel).to.equal(testVorlage.dto.titel);
        done();
      },
    });
  });
});

describe('updateItem', () => {
  const ctrl = new ZeitLightLocalController();
  beforeEach(() => {
    ctrl.syncLocalDrafts([testVorlage], ZeitLightCollectionsKeys.Zeitplanungsvorlagen);
  });
  afterEach(() => {
    ctrl.syncLocalDrafts([], ZeitLightCollectionsKeys.Zeitplanungsvorlagen);
  });
  it('test functionality updateItem', () => {
    const tempVorlage = { ...testVorlage, base: { ...testVorlage.base, bearbeitetAm: '2022-11-23T10:23:59.054Z' } };
    tempVorlage.base.id = 'item-for-update';
    ctrl.syncLocalDrafts([tempVorlage], ZeitLightCollectionsKeys.Zeitplanungsvorlagen);
    ctrl.updateItem(tempVorlage, ZeitLightCollectionsKeys.Zeitplanungsvorlagen);
    const updatedItem = ctrl.getItemByID('item-for-update', ZeitLightCollectionsKeys.Zeitplanungsvorlagen);
    expect(updatedItem.base.bearbeitetAm).to.not.equal(testVorlage.base.bearbeitetAm);
  });
});

describe('getScrollCalendarStatusObservable', () => {
  const ctrl = new ZeitLightLocalController();
  beforeEach(() => {
    global.window.localStorage.setItem('scrollCalendarStatus', 'false');
  });
  afterEach(() => {
    global.window.localStorage.setItem('scrollCalendarStatus', '');
  });
  it('Initial view status should be false', (done) => {
    ctrl.getScrollCalendarStatusObservable().subscribe({
      next: (data) => {
        expect(data).to.be.false;
        done();
      },
    });
  });
  it('set status to true', (done) => {
    ctrl.setScrollCalendarStatusObservable(true).subscribe({
      next: () => {
        expect(JSON.parse(global.window.localStorage.getItem('scrollCalendarStatus') as string)).to.be.true;
        done();
      },
    });
  });
});

export const testVorlage: ZeitplanungsvorlageEntityResponseDTO = {
  base: {
    id: 'test-vorlage-id',
    erstelltAm: '2022-11-23T10:23:59.054Z',
    bearbeitetAm: '2022-11-23T10:23:59.054Z',
  },
  dto: {
    erstellerType: ErstellerType.Selbst,
    erstelltVon: {
      base: {
        id: 'user-guest',
        erstelltAm: '2022-06-16T12:34:26.538579Z',
        bearbeitetAm: '2022-08-24T11:41:49.951635Z',
      },
      dto: {
        name: 'Gast',
        email: 'Gast',
      },
    },
    titel: 'Draft 1',
    beschreibung: undefined,
    readOnlyAccess: false,
    vorhabenart: RegelungsvorhabenTypType.Gesetz,
    elemente: [
      {
        base: {
          id: '6b443066-5c94-32c3-b5d3-4863ed029114',
          erstelltAm: '',
          bearbeitetAm: '',
        },
        dto: {
          termin: false,
          wichtig: false,
          bauplan: ZeitplanungsvorlagenBauplan.GVorphase,
          titel: 'Vorphase',
          vorherigeElementeIds: [],
          dauer: {
            weeks: 8,
          },
          untergeordneteElemente: [
            {
              base: {
                id: '31ca69b4-b184-3336-b5ec-678c960fe83e',
                erstelltAm: '',
                bearbeitetAm: '',
              },
              dto: {
                termin: false,
                wichtig: false,
                bauplan: ZeitplanungsvorlagenBauplan.GKonzeptionelleUeberlegungen,
                titel: 'Konzeptionelle Überlegungen',
                uebergeordneteElementeId: '6b443066-5c94-32c3-b5d3-4863ed029114',
                vorherigeElementeIds: [],
                dauer: {
                  weeks: 2,
                },
                untergeordneteElemente: [
                  {
                    base: {
                      id: '2df3faaa-829c-3658-9153-92643a7c89f4',
                      erstelltAm: '',
                      bearbeitetAm: '',
                    },
                    dto: {
                      termin: false,
                      wichtig: false,
                      bauplan:
                        ZeitplanungsvorlagenBauplan.GPruefungDerPolitischenInhaltlichenUndZeitlichenRahmenbedingungen,
                      titel: 'Prüfung der politischen, inhaltlichen und zeitlichen Rahmenbedingungen',
                      uebergeordneteElementeId: '31ca69b4-b184-3336-b5ec-678c960fe83e',
                      vorherigeElementeIds: [],
                      dauer: {
                        weeks: 1,
                      },
                      untergeordneteElemente: [],
                    },
                  },
                  {
                    base: {
                      id: 'd2af52fb-22c4-3ecc-aada-bda7c543a72f',
                      erstelltAm: 'null',
                      bearbeitetAm: 'null',
                    },
                    dto: {
                      termin: false,
                      wichtig: false,
                      bauplan: ZeitplanungsvorlagenBauplan.GPruefungVonRegelungsalternativen,
                      titel: 'Prüfung von Regelungsalternativen',
                      uebergeordneteElementeId: '31ca69b4-b184-3336-b5ec-678c960fe83e',
                      vorherigeElementeIds: ['2df3faaa-829c-3658-9153-92643a7c89f4'],

                      dauer: {
                        weeks: 1,
                      },
                      untergeordneteElemente: [],
                    },
                  },
                  {
                    base: {
                      id: '52178ff5-604c-33da-8edf-13f5fa30e880',
                      erstelltAm: '',
                      bearbeitetAm: '',
                    },
                    dto: {
                      termin: false,
                      wichtig: false,
                      bauplan: ZeitplanungsvorlagenBauplan.RvPruefungVonFolgenInklErfuellungsaufwand,
                      titel: 'Prüfung von Gesetzesfolgen inkl. Erfüllungsaufwand',
                      uebergeordneteElementeId: '31ca69b4-b184-3336-b5ec-678c960fe83e',
                      vorherigeElementeIds: ['d2af52fb-22c4-3ecc-aada-bda7c543a72f'],

                      dauer: {
                        weeks: 1,
                      },
                      untergeordneteElemente: [],
                    },
                  },
                ],
              },
            },
            {
              base: {
                id: 'e1b8d856-0780-34f0-9752-3ddc3d775a89',
                erstelltAm: '',
                bearbeitetAm: '',
              },
              dto: {
                termin: false,
                wichtig: false,
                bauplan: ZeitplanungsvorlagenBauplan.GErgaenzungDerVorhabenplanungDerBreg,
                titel: 'Ergänzung der Vorhabenplanung der BReg',
                uebergeordneteElementeId: '6b443066-5c94-32c3-b5d3-4863ed029114',
                vorherigeElementeIds: ['31ca69b4-b184-3336-b5ec-678c960fe83e'],

                dauer: {
                  weeks: 4,
                },
                untergeordneteElemente: [],
              },
            },
            {
              base: {
                id: 'b7c15b16-46b6-363c-af26-835a269ffa95',
                erstelltAm: '',
                bearbeitetAm: '',
              },
              dto: {
                termin: false,
                wichtig: false,
                bauplan: ZeitplanungsvorlagenBauplan.GErstellungDesReferentenentwurfs,
                titel: 'Erstellung des Referentenentwurfs',
                uebergeordneteElementeId: '6b443066-5c94-32c3-b5d3-4863ed029114',
                vorherigeElementeIds: ['e1b8d856-0780-34f0-9752-3ddc3d775a89'],

                dauer: {
                  weeks: 2,
                },
                untergeordneteElemente: [],
              },
            },
          ],
        },
      },
      {
        base: {
          id: 'ff791ac7-dda4-3178-8acf-1910595063bc',
          erstelltAm: 'null',
          bearbeitetAm: 'null',
        },
        dto: {
          termin: true,
          wichtig: false,
          bauplan: ZeitplanungsvorlagenBauplan.GInkrafttretenDesGesetzes,
          titel: 'Inkrafttreten des Gesetzes',
          vorherigeElementeIds: ['16454c54-50f7-3537-80bc-efbc78b00e38'],
          dauer: {
            days: 1,
          },
        },
      },
    ],
    dauer: 3,
  },
};
export const testZeitplanung: ZeitplanungEntityResponseDTO = {
  base: {
    id: 'testZeitplanung',
    erstelltAm: '2022-12-06T22:36:11.280600Z',
    bearbeitetAm: '2022-12-07T09:40:33.086Z',
  },
  dto: {
    erstellerType: ErstellerType.Selbst,
    erstelltVon: {
      base: {
        id: 'user-guest',
        erstelltAm: '',
        bearbeitetAm: '',
      },
      dto: {
        name: 'Gast',
        email: 'Gast',
      },
    },
    titel: 'TEST',
    beschreibung: 'asdasd',
    regelungsvorhabenId: '',
    readOnlyAccess: false,
    vorhabenart: RegelungsvorhabenTypType.Gesetz,
    elemente: [
      {
        base: {
          id: 'phase1',
          erstelltAm: '',
          bearbeitetAm: '',
        },
        dto: {
          termin: false,
          wichtig: false,
          bauplan: ZeitplanungsvorlagenBauplan.GVorphase,
          titel: 'Vorphase',
          uebergeordneteElementeId: '',
          vorherigeElementeIds: [],
          kommentar: '',
          zeitplanungAnpassen: false,
          hinweise: [],
          gesperrt: false,
          beginn: '2022-12-06',
          ende: '2023-04-24',
          untergeordneteElemente: [],
          datum: '2023-04-24',
        },
      },
      {
        base: {
          id: 'termin1',
          erstelltAm: '',
          bearbeitetAm: '',
        },
        dto: {
          termin: true,
          wichtig: false,
          bauplan: ZeitplanungsvorlagenBauplan.GKabinettbeschluss,
          titel: 'Kabinettbeschluss',
          uebergeordneteElementeId: '',
          vorherigeElementeIds: ['0d2040f0-a9b7-3b75-9b5e-6a6d9a88ae1e'],
          kommentar: '',
          zeitplanungAnpassen: false,
          hinweise: [],
          gesperrt: false,
          datum: '2023-10-04',
          beginn: '2023-10-04',
          ende: '2023-10-04',
        },
      },
      {
        base: {
          id: 'termin2',
          erstelltAm: '',
          bearbeitetAm: '',
        },
        dto: {
          termin: true,
          wichtig: false,
          bauplan: ZeitplanungsvorlagenBauplan.GKabinettbeschluss,
          titel: 'termin2',
          uebergeordneteElementeId: '',
          vorherigeElementeIds: ['0d2040f0-a9b7-3b75-9b5e-6a6d9a88ae1e'],
          kommentar: '',
          zeitplanungAnpassen: false,
          hinweise: [],
          gesperrt: false,
          datum: '2023-10-04',
          beginn: '2023-10-04',
          ende: '2023-10-04',
        },
      },
    ],
    naechstesElement: {
      id: 'ccd9501e-05c9-3ead-9871-d48aa278052d',
      beginn: '2023-04-25',
      ende: '2023-05-11',
      titel: 'Hausabstimmung',
      termin: false,
      faelligIn: 140,
    },
    erstePhaseBeginntAm: '2022-12-06',
    letztePhaseEndetAm: '2024-05-12',
  },
};
