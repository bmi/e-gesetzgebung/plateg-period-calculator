// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

import {
  Configuration,
  ExportControllerApi,
  RegelungsvorhabenControllerApi,
  UserControllerApi,
  ZeitplanungControllerApi,
  ZeitplanungLightControllerApi,
  ZeitplanungsvorlageControllerApi,
  ZeitplanungsvorlageLightControllerApi,
} from '@plateg/rest-api';
import { configureRestApi, HeaderController, LoadingStatusController } from '@plateg/theme';
import { GlobalDI } from '@plateg/theme/src/shares';

import { LayoutWrapperZeit } from '../zeit/component.react';

export function AppLightZeit(): React.ReactElement {
  GlobalDI.getOrRegister('loadingStatusController', () => new LoadingStatusController());
  GlobalDI.getOrRegister('ezeitHeaderController', () => new HeaderController());
  configureRestApi(registerRestApis);

  return <LayoutWrapperZeit isLight={true} />;
}

function registerRestApis(configRestCalls: Configuration) {
  GlobalDI.getOrRegister('userController', () => new UserControllerApi(configRestCalls));
  GlobalDI.getOrRegister('zeitplanungRestController', () => new ZeitplanungControllerApi(configRestCalls));
  GlobalDI.getOrRegister(
    'zeitplanunsvorlageRestController',
    () => new ZeitplanungsvorlageControllerApi(configRestCalls),
  );
  GlobalDI.getOrRegister('regelungsvorhabenController', () => new RegelungsvorhabenControllerApi(configRestCalls));
  GlobalDI.getOrRegister('exportControllerApi', () => new ExportControllerApi(configRestCalls));
  GlobalDI.getOrRegister('zeitplanungLightControllerApi', () => new ZeitplanungLightControllerApi(configRestCalls));
  GlobalDI.getOrRegister(
    'zeitplanungsvorlageLightControllerApi',
    () => new ZeitplanungsvorlageLightControllerApi(configRestCalls),
  );
}
