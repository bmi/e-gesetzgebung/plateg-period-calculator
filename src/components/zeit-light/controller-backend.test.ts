// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import chai from 'chai';
import chaiString from 'chai-string';
import sinon from 'sinon';

import { ZeitplanungsvorlageLightControllerApi } from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme/src/shares';

import { ZeitLighBackendController } from './controller-backend';
chai.use(chaiString);

const zeitLighBackendController = GlobalDI.getOrRegister(
  'zeitLighBackendController',
  () => new ZeitLighBackendController(),
);

const zeitplanungsvorlageLightControllerApi = GlobalDI.getOrRegister(
  'zeitplanungsvorlageLightControllerApi',
  () => new ZeitplanungsvorlageLightControllerApi(),
);

describe('TEST: getZeitplanungsvorlagenAlleLightCall', () => {
  let getZeitplanungsvorlagenAlleLightStub: sinon.SinonStub;
  before(() => {
    getZeitplanungsvorlagenAlleLightStub = sinon.stub(
      zeitplanungsvorlageLightControllerApi,
      'getZeitplanungsvorlagenAlleLight',
    );
  });
  after(() => {
    getZeitplanungsvorlagenAlleLightStub.restore();
  });
  it('check if REST API is called', () => {
    zeitLighBackendController.getZeitplanungsvorlagenAlleLightCall();
    sinon.assert.calledOnce(getZeitplanungsvorlagenAlleLightStub);
    getZeitplanungsvorlagenAlleLightStub.resetHistory();
  });
});
