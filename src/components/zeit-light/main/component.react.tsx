// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';
import { Route, Switch } from 'react-router-dom';

import { routes } from '../../../shares/routes';
import { ZeitplanungenTabViewComponent } from '../../zeit/main/zeitplanung-tab-view/component.react';
import { ZeitplanungsvorlagenTabViewComponent } from '../../zeit/main/zeitplanungsvorlage-tab-view/component.react';
import { HomeTabsLightZeit } from './home-tabs-light-zeit/component.react';

export function MainRoutesLightZeit(): React.ReactElement {
  return (
    <Switch>
      <Route
        exact
        path={[
          '/zeit',
          `/zeit/${routes.MEINE_ZEITPLANUNGEN}`,
          `/zeit/${routes.ZEITPLANUNGSVORLAGEN}`,
          `/zeit/${routes.SYSTEMVORLAGEN}`,
        ]}
      >
        <HomeTabsLightZeit />
      </Route>
      <Route
        path={[`/zeit/${routes.MEINE_ZEITPLANUNGEN}/:id`, `/zeit/${routes.ARCHIV}/${routes.MEINE_ZEITPLANUNGEN}/:id`]}
      >
        <ZeitplanungenTabViewComponent isLight={true} />
      </Route>
      <Route path={[`/zeit/${routes.ZEITPLANUNGSVORLAGEN}/:id`, `/zeit/${routes.SYSTEMVORLAGEN}/:id`]}>
        <ZeitplanungsvorlagenTabViewComponent isLight={true} />
      </Route>
    </Switch>
  );
}
