// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { ColumnsType } from 'antd/lib/table';
import Text from 'antd/lib/typography/Text';
import i18n from 'i18next';
import React, { ReactElement } from 'react';
import { Link } from 'react-router-dom';

import { ZeitplanungsvorlageTableDTO } from '@plateg/rest-api';
import { compareDates, getDateTimeString, TableComponentProps, TwoLineText } from '@plateg/theme';
import {
  DropdownMenu,
  DropdownMenuItem,
} from '@plateg/theme/src/components/table-component/table-sub-components/dropdown-button-component/component.react';

import { routes } from '../../../../../shares/routes';
import { getBeschreibungRenderer } from '../../../../zeit/main/home-tabs-zeit/meine-zeitplanungen-tab/controller.react';

export function getZeitplanungsvorlagenTableVals(
  content: ZeitplanungsvorlageTableDTO[],
): TableComponentProps<ZeitplanungsvorlageTableDTO> {
  const columns: ColumnsType<ZeitplanungsvorlageTableDTO> = [
    {
      title: i18n.t('zeit.myZeitHome.tabs.zeitplanungsvorlagen.table.menuItemVorhabentyp').toString(),
      key: 'c1',
      fixed: 'left',
      render: (record: ZeitplanungsvorlageTableDTO): ReactElement => {
        return (
          <Text strong>
            {i18n.t(`zeit.myZeitHome.tabs.zeitplanungsvorlagen.table.vorhabenart.${record.vorhabenart}`).toString()}
          </Text>
        );
      },
    },
    {
      title: i18n.t('zeit.myZeitHome.tabs.zeitplanungsvorlagen.table.menuItemZeitplanungsvorlage').toString(),
      key: 'c2',
      render: (record: ZeitplanungsvorlageTableDTO): ReactElement => {
        const target = `/zeit/${routes.ZEITPLANUNGSVORLAGEN}/${record.id}/${routes.TABELLENSICHT}`;
        return (
          <>
            <Link id={`ezeit-zeitplanungsVorlageTab-${record.id}-link`} to={target}>
              {record.titel}
            </Link>
          </>
        );
      },
    },
    {
      title: i18n.t('zeit.myZeitHome.tabs.zeitplanungsvorlagen.table.menuItemDauer').toString(),
      key: 'c3',
      sorter: (a, b) => a.dauer - b.dauer,
      render: (record: ZeitplanungsvorlageTableDTO): ReactElement => {
        return (
          <Text>{`${record?.dauer?.toString()} ${i18n.t(
            'zeit.myZeitHome.tabs.zeitplanungsvorlagen.table.monate',
          )}`}</Text>
        );
      },
    },
    {
      title: i18n.t('zeit.myZeitHome.tabs.zeitplanungsvorlagen.table.menuItemErsteller').toString(),
      key: 'c4',
      sorter: (a, b) => compareDates(a.erstelltAm, b.erstelltAm),
      render: (record: ZeitplanungsvorlageTableDTO): ReactElement => {
        return (
          <TwoLineText
            firstRow={getDateTimeString(record.erstelltAm)}
            firstRowBold={false}
            secondRow={'Gast'}
            secondRowLight={true}
            secondRowBold={false}
            elementId={`erstellt-${record.id}`}
          />
        );
      },
    },
    {
      title: i18n.t('zeit.myZeitHome.tabs.zeitplanungsvorlagen.table.menuItemZuletztBearbeitet').toString(),
      key: 'c5',
      sorter: (a, b) => compareDates(a.bearbeitetAm || a.erstelltAm, b.bearbeitetAm || b.erstelltAm),
      render: (record: ZeitplanungsvorlageTableDTO): ReactElement => {
        return (
          <TwoLineText
            firstRow={getDateTimeString(record.bearbeitetAm || record.erstelltAm)}
            firstRowBold={false}
            secondRow={'Gast'}
            secondRowLight={true}
            secondRowBold={false}
            elementId={`bearbeitet-${record.id}`}
          />
        );
      },
    },
    {
      title: i18n.t('zeit.myZeitHome.tabs.zeitplanungsvorlagen.table.menuItemAktionen').toString(),
      key: 'c6',
      render: (record: ZeitplanungsvorlageTableDTO): ReactElement => {
        const items: DropdownMenuItem[] = [];
        return (
          <DropdownMenu
            openLink={`/zeit/${routes.ZEITPLANUNGSVORLAGEN}/${record.id}/${routes.TABELLENSICHT}`}
            items={items}
            elementId={record.id}
          />
        );
      },
    },
  ];

  return {
    id: '',
    expandable: true,
    expandedRowRender: (record) => getBeschreibungRenderer(record, routes.ZEITPLANUNGSVORLAGEN),
    expandableCondition: (record: ZeitplanungsvorlageTableDTO) => {
      if (record.beschreibung) {
        return true;
      }
      return false;
    },
    columns,
    content,
    filteredColumns: [{ name: 'vorhabenart', columnIndex: 0 }],
    className: 'zeitplanungsvorlagen-table',
    customDefaultSortIndex: 6,
    sorterOptions: [
      {
        columnKey: 'c3',
        titleAsc: i18n.t('zeit.myZeitHome.tabs.zeitplanungsvorlagen.table.sorters.dauerAsc'),
        titleDesc: i18n.t('zeit.myZeitHome.tabs.zeitplanungsvorlagen.table.sorters.dauerDesc'),
      },
      {
        columnKey: 'c4',
        titleAsc: i18n.t('zeit.myZeitHome.tabs.zeitplanungsvorlagen.table.sorters.erstelltAsc'),
        titleDesc: i18n.t('zeit.myZeitHome.tabs.zeitplanungsvorlagen.table.sorters.erstelltDesc'),
      },
      {
        columnKey: 'c5',
        titleAsc: i18n.t('zeit.myZeitHome.tabs.zeitplanungsvorlagen.table.sorters.bearbeitetAsc'),
        titleDesc: i18n.t('zeit.myZeitHome.tabs.zeitplanungsvorlagen.table.sorters.bearbeitetDesc'),
      },
    ],
  };
}
