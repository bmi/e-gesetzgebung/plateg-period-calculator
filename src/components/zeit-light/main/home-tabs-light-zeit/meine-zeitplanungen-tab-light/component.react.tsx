// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { ZeitplanungTableDTO } from '@plateg/rest-api';
import { EmptyContentComponent, ImageModule, TableComponent, TableComponentProps } from '@plateg/theme';
import { GlobalDI } from '@plateg/theme/src/shares';

import { routes } from '../../../../../shares/routes';
import { ZeitplanungController } from '../../../../zeit/main/home-tabs-zeit/controller';
import { getMeineZeitplanungenTableVals } from './controller.react';

interface MeineZeitplanungenTabComponentInterface {
  zeitplanungenData: ZeitplanungTableDTO[];
}

export function MeineZeitplanungenTabLightComponent(
  props: MeineZeitplanungenTabComponentInterface,
): React.ReactElement {
  const { t } = useTranslation();
  const zeitplanungController = GlobalDI.getOrRegister('zeitplanungController', () => new ZeitplanungController());
  const [zeitplanungenTableValsLight, setZeitplanungenTableValsLight] = useState<
    TableComponentProps<ZeitplanungTableDTO>
  >({
    id: '',
    columns: [],
    content: [],
    expandable: false,
    sorterOptions: [],
    customDefaultSortIndex: 0,
    expandableCondition: undefined,
    className: '',
  });

  useEffect(() => {
    setZeitplanungenTableValsLight(getMeineZeitplanungenTableVals(props.zeitplanungenData));
  }, [props.zeitplanungenData]);

  const emptyContentKey = 'zeit.myZeitHome.tabs.meineZeitplanungen';

  return (
    <>
      {zeitplanungenTableValsLight?.content?.length > 0 && (
        <>
          <TableComponent
            id="period-zeitplanungen-table"
            columns={zeitplanungenTableValsLight.columns}
            content={zeitplanungenTableValsLight.content}
            filteredColumns={zeitplanungenTableValsLight.filteredColumns}
            filterRowsMethod={zeitplanungController.getFilteredRows}
            prepareFilterButtonMethod={zeitplanungController.getFilterButton}
            expandable={zeitplanungenTableValsLight.expandable}
            expandedRowRender={zeitplanungenTableValsLight.expandedRowRender}
            sorterOptions={zeitplanungenTableValsLight.sorterOptions}
            customDefaultSortIndex={zeitplanungenTableValsLight.customDefaultSortIndex}
            className={zeitplanungenTableValsLight.className}
            expandableCondition={zeitplanungenTableValsLight.expandableCondition}
            isLight={true}
            tabKey={routes.MEINE_ZEITPLANUNGEN}
          ></TableComponent>
        </>
      )}
      <EmptyContentComponent
        images={[
          {
            label: t(`${emptyContentKey}.imgText1`),
            imgSrc: require(
              `../../../../../media/empty-content-images/meine-zeitplanung-tab/img1-myZeitplanungen.svg`,
            ) as ImageModule,
            height: 100,
          },
          {
            label: t(`${emptyContentKey}.imgText2`),
            imgSrc: require(
              `../../../../../media/empty-content-images/meine-zeitplanung-tab/img2-myZeitplanungen.svg`,
            ) as ImageModule,
            height: 114,
          },
          {
            label: t(`${emptyContentKey}.imgText3`),
            imgSrc: require(
              `../../../../../media/empty-content-images/meine-zeitplanung-tab/img3-myZeitplanungen.svg`,
            ) as ImageModule,
            height: 119,
          },
        ]}
      >
        <p className="info-text">
          {t(`${emptyContentKey}.text`, {
            interpolation: { escapeValue: false },
          })}
        </p>
      </EmptyContentComponent>
    </>
  );
}
