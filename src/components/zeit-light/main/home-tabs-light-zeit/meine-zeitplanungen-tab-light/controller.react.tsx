// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { ColumnsType } from 'antd/lib/table';
import Text from 'antd/lib/typography/Text';
import i18n from 'i18next';
import React, { ReactElement } from 'react';
import { Link } from 'react-router-dom';

import { ZeitplanungsvorlageTableDTO, ZeitplanungTableDTO } from '@plateg/rest-api';
import { compareDates, getDateTimeString, TableComponentProps, TwoLineText } from '@plateg/theme';
import {
  DropdownMenu,
  DropdownMenuItem,
} from '@plateg/theme/src/components/table-component/table-sub-components/dropdown-button-component/component.react';
import { GlobalDI } from '@plateg/theme/src/shares';
import { Filters } from '@plateg/theme/src/shares/filters';

import { routes } from '../../../../../shares/routes';
import { ZeitplanungController } from '../../../../zeit/main/home-tabs-zeit/controller';

export const getBeschreibungRenderer = (
  record: ZeitplanungTableDTO | ZeitplanungsvorlageTableDTO,
  tab: string,
): ReactElement => (
  <>
    <label htmlFor={'expandable-beschreibung-' + record.id.toString()}>
      {i18n.t(`zeit.myZeitHome.tabs.${tab}.table.beschreibungLabel`)}
    </label>
    <Text id={'expandable-beschreibung-' + record.id.toString()} className="expandable-row-beschreibung">
      {record.beschreibung}
    </Text>
  </>
);

export function getMeineZeitplanungenTableVals(
  content: ZeitplanungTableDTO[],
): TableComponentProps<ZeitplanungTableDTO> {
  const zeitplanungController = GlobalDI.getOrRegister('zeitplanungController', () => new ZeitplanungController());
  const columns: ColumnsType<ZeitplanungTableDTO> = [
    {
      title: i18n.t('zeit.myZeitHome.tabs.meineZeitplanungen.table.menuItemZeitplanung').toString(),
      key: 'c2',
      render: (record: ZeitplanungTableDTO): ReactElement => (
        <Link
          id={`ezeit-zeitplanungsTab-title-${record.id}`}
          to={`/zeit/${routes.MEINE_ZEITPLANUNGEN}/${record.id}/${routes.TABELLENSICHT}`}
        >
          {record.titel}
        </Link>
      ),
    },
    {
      title: i18n.t('zeit.myZeitHome.tabs.meineZeitplanungen.table.menuItemNaechstesElement').toString(),
      key: 'c3',
      sorter: (a: ZeitplanungTableDTO, b: ZeitplanungTableDTO) =>
        compareDates(a.naechstesElement?.beginn, b.naechstesElement?.beginn),
      render: (record: ZeitplanungTableDTO): ReactElement => {
        if (!record.naechstesElement) {
          return <></>;
        }
        return (
          <>
            <Text strong={true} className="limited-width">
              {record.naechstesElement.titel}
            </Text>
            <br />
            <Text>
              {record.naechstesElement.termin === true
                ? zeitplanungController.getDurationString(
                    record.naechstesElement.beginn,
                    record.naechstesElement.faelligIn.toString(),
                  )
                : zeitplanungController.getDurationDatesString(
                    record.naechstesElement.beginn,
                    record.naechstesElement.ende,
                    record.naechstesElement.faelligIn.toString(),
                  )}
            </Text>
          </>
        );
      },
    },
    {
      title: i18n.t('zeit.myZeitHome.tabs.meineZeitplanungen.table.menuItemBeplanterZeitraum').toString(),
      key: 'c4',
      render: (record: ZeitplanungTableDTO): ReactElement => (
        <Text>{`${Filters.dateFromString(record.erstePhaseBeginntAm)} – ${Filters.dateFromString(
          record.letztePhaseEndetAm,
        )}`}</Text>
      ),
    },
    {
      title: i18n.t('zeit.myZeitHome.tabs.meineZeitplanungen.table.menuItemErsteller').toString(),
      key: 'c5',
      render: (record: ZeitplanungTableDTO): ReactElement => (
        <TwoLineText
          firstRow={getDateTimeString(record.erstelltAm)}
          firstRowBold={false}
          secondRow={'Gast'}
          secondRowLight={true}
          secondRowBold={false}
          elementId={`erstellt-${record.id}`}
        />
      ),
    },
    {
      title: i18n.t('zeit.myZeitHome.tabs.meineZeitplanungen.table.menuItemZuletztBearbeitet').toString(),
      key: 'c6',
      sorter: (a: ZeitplanungTableDTO, b: ZeitplanungTableDTO) =>
        compareDates(a.bearbeitetAm || a.erstelltAm, b.bearbeitetAm || b.erstelltAm),
      render: (record: ZeitplanungTableDTO): ReactElement => (
        <TwoLineText
          firstRow={getDateTimeString(record.bearbeitetAm || record.erstelltAm)}
          firstRowBold={false}
          secondRow={'Gast'}
          secondRowLight={true}
          secondRowBold={false}
          elementId={`bearbeitet-${record.id}`}
        />
      ),
    },
    {
      title: i18n.t('zeit.myZeitHome.tabs.meineZeitplanungen.table.menuItemAktionen').toString(),
      key: 'c7',
      render: (record: ZeitplanungTableDTO): ReactElement => {
        const items: DropdownMenuItem[] = [];
        return (
          <DropdownMenu
            openLink={`/zeit/${routes.MEINE_ZEITPLANUNGEN}/${record.id}/${routes.TABELLENSICHT}`}
            items={items}
            elementId={record.id}
          />
        );
      },
    },
  ];

  return {
    id: '',
    expandable: true,
    expandedRowRender: (record) => getBeschreibungRenderer(record, routes.MEINE_ZEITPLANUNGEN),
    expandableCondition: (record: ZeitplanungTableDTO) => (record.beschreibung ? true : false),
    columns,
    content,
    filteredColumns: [
      { name: 'typ', columnIndex: 1 },
      { name: 'erstelltVon', columnIndex: 4 },
    ],
    sorterOptions: [
      {
        columnKey: 'c3',
        titleAsc: i18n.t('zeit.myZeitHome.tabs.sorters.terminAsc'),
        titleDesc: i18n.t('zeit.myZeitHome.tabs.sorters.terminDesc'),
      },
      {
        columnKey: 'c6',
        titleAsc: i18n.t('zeit.myZeitHome.tabs.sorters.bearbeitetAsc'),
        titleDesc: i18n.t('zeit.myZeitHome.tabs.sorters.bearbeitetDesc'),
      },
    ],
    customDefaultSortIndex: 4,
  };
}
