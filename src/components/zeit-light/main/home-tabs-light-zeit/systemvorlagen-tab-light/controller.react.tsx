// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { ColumnsType } from 'antd/lib/table';
import Text from 'antd/lib/typography/Text';
import i18n from 'i18next';
import React, { ReactElement } from 'react';
import { Link } from 'react-router-dom';

import { ZeitplanungsvorlageTableDTO } from '@plateg/rest-api';
import { DropdownMenu, TableComponentProps } from '@plateg/theme';
import { DropdownMenuItem } from '@plateg/theme/src/components/table-component/table-sub-components/dropdown-button-component/component.react';

import { routes } from '../../../../../shares/routes';

export function getZeitplanungsvorlagenTableVals(
  content: ZeitplanungsvorlageTableDTO[],
): TableComponentProps<ZeitplanungsvorlageTableDTO> {
  const systemvorlagenKey = `zeit.myZeitHome.tabs.zeitplanungsvorlagen.table`;
  const columns: ColumnsType<ZeitplanungsvorlageTableDTO> = [
    {
      title: i18n.t(`${systemvorlagenKey}.menuItemVorhabentyp`).toString(),
      key: 'Vorhabentyp',
      fixed: 'left',
      render: (record: ZeitplanungsvorlageTableDTO): ReactElement => {
        return <Text strong>{i18n.t(`${systemvorlagenKey}.vorhabenart.${record.vorhabenart}`).toString()}</Text>;
      },
    },
    {
      title: i18n.t(`${systemvorlagenKey}.menuItemZeitplanungsvorlage`).toString(),
      key: 'Zeitplanungsvorlage',
      render: (record: ZeitplanungsvorlageTableDTO): ReactElement => {
        const target = `/zeit/${routes.SYSTEMVORLAGEN}/${record.id}/${routes.TABELLENSICHT}`;
        return (
          <>
            <Link id={`ezeit-zeitplanungsVorlageTab-${record.id}-link`} to={target}>
              {record.titel}
            </Link>
          </>
        );
      },
    },
    {
      title: i18n.t(`${systemvorlagenKey}.menuItemDauer`).toString(),
      key: 'menuItemDauer',
      sorter: (a, b) => a.dauer - b.dauer,
      render: (record: ZeitplanungsvorlageTableDTO): ReactElement => {
        return <Text>{`${record?.dauer?.toString()} ${i18n.t(`${systemvorlagenKey}.monate`)}`}</Text>;
      },
    },
    {
      title: i18n.t(`${systemvorlagenKey}.menuItemAktionen`).toString(),
      key: 'Aktionen',
      render: (record: ZeitplanungsvorlageTableDTO): ReactElement => {
        const items: DropdownMenuItem[] = [];
        return (
          <DropdownMenu
            openLink={`/zeit/${routes.SYSTEMVORLAGEN}/${record.id}/${routes.TABELLENSICHT}`}
            items={items}
            elementId={record.id}
          />
        );
      },
    },
  ];

  return {
    id: '',
    expandable: false,
    columns,
    content,
    filteredColumns: [{ name: 'vorhabenart', columnIndex: 0 }],
    className: 'zeitplanungsvorlagen-table',
    customDefaultSortIndex: 6,
    sorterOptions: [
      {
        columnKey: 'menuItemDauer',
        titleAsc: i18n.t(`${systemvorlagenKey}.sorters.dauerAsc`),
        titleDesc: i18n.t(`${systemvorlagenKey}.sorters.dauerDesc`),
      },
    ],
  };
}
