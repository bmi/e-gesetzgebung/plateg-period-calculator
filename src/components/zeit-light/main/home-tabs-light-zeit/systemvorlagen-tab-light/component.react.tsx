// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { ZeitplanungsvorlageTableDTO } from '@plateg/rest-api';
import { EmptyContentComponent, GlobalDI, ImageModule, TableComponent, TableComponentProps } from '@plateg/theme';

import { ZeitplanungController } from '../../../../zeit/main/home-tabs-zeit/controller';
import { getZeitplanungsvorlagenTableVals } from './controller.react';

interface ZeitplanungsvorlagenLightInterface {
  zeitplanungsvorlagenData: ZeitplanungsvorlageTableDTO[];
}

export function SystemvorlagenTabLightComponent(props: ZeitplanungsvorlagenLightInterface): React.ReactElement {
  const { t } = useTranslation();
  const [zeitplanungsvorlagenData, setZeitplanungsvorlagenData] = useState(props.zeitplanungsvorlagenData);
  const [zeitplanungsvorlagenTableVals, setZeitplanungsvorlagenTableVals] = useState<
    TableComponentProps<ZeitplanungsvorlageTableDTO>
  >({
    id: '',
    columns: [],
    content: [],
    expandable: false,
    sorterOptions: [],
    customDefaultSortIndex: 0,
    expandableCondition: undefined,
    className: '',
  });

  const zeitplanungController = GlobalDI.getOrRegister('zeitplanungController', () => new ZeitplanungController());

  useEffect(() => {
    setZeitplanungsvorlagenData(props.zeitplanungsvorlagenData);
  }, [props.zeitplanungsvorlagenData]);

  useEffect(() => {
    setZeitplanungsvorlagenTableVals(getZeitplanungsvorlagenTableVals(zeitplanungsvorlagenData));
  }, [zeitplanungsvorlagenData]);

  return (
    <>
      {zeitplanungsvorlagenTableVals?.content?.length > 0 && (
        <>
          <TableComponent
            id="period-zeitplanungsvorlagen-table"
            columns={zeitplanungsvorlagenTableVals.columns}
            content={zeitplanungsvorlagenTableVals.content}
            filteredColumns={zeitplanungsvorlagenTableVals.filteredColumns}
            filterRowsMethod={zeitplanungController.getFilteredRows}
            prepareFilterButtonMethod={zeitplanungController.getFilterButton}
            expandable={zeitplanungsvorlagenTableVals.expandable}
            expandedRowRender={zeitplanungsvorlagenTableVals.expandedRowRender}
            sorterOptions={zeitplanungsvorlagenTableVals.sorterOptions}
            customDefaultSortIndex={zeitplanungsvorlagenTableVals.customDefaultSortIndex}
            className={zeitplanungsvorlagenTableVals.className}
            expandableCondition={zeitplanungsvorlagenTableVals.expandableCondition}
            isLight={true}
          />
        </>
      )}
      <EmptyContentComponent
        noIndex={true}
        images={[
          {
            label: t('zeit.myZeitHome.tabs.zeitplanungsvorlagen.imgText1'),
            imgSrc: require(
              `../../../../../media/empty-content-images/zeitplanungsvorlagen-tab/img1-zeitplanungsvorlagen.svg`,
            ) as ImageModule,
            height: 110,
          },
        ]}
      >
        <p className="info-text">
          {t('zeit.myZeitHome.tabs.zeitplanungsvorlagen.text', {
            interpolation: { escapeValue: false },
          })}
        </p>
      </EmptyContentComponent>
    </>
  );
}
