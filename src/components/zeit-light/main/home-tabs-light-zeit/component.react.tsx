// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button, Col, Row, TabsProps, Typography } from 'antd';
import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory, useRouteMatch } from 'react-router';
import { Subscription } from 'rxjs';

import { AktionType, ZeitplanungsvorlageTableDTO, ZeitplanungTableDTO } from '@plateg/rest-api';
import { BreadcrumbComponent, HeaderController, TabsWrapper, TitleWrapperComponent } from '@plateg/theme';
import { GlobalDI } from '@plateg/theme/src/shares';

import { routes } from '../../../../shares/routes';
import { ZeitHelpLink } from '../../../zeit/component.react';
import { NewZeitplanungModal } from '../../../zeit/main/new-zeitplanung-modal/component.react';
import { NewZeitplanungsvorlageModal } from '../../../zeit/main/new-zeitplanungsvorlage-modal/component.react';
import { ZeitLighBackendController } from '../../controller-backend';
import { ZeitLightCollectionsKeys, ZeitLightLocalController } from '../../controller-local';
import { MeineZeitplanungenTabLightComponent } from './meine-zeitplanungen-tab-light/component.react';
import { SystemvorlagenTabLightComponent } from './systemvorlagen-tab-light/component.react';
import { ZeitplanungsvorlagenTabLightComponent } from './zeitplanungsvorlagen-tab-light/component.react';

export function HomeTabsLightZeit(): React.ReactElement {
  const { t } = useTranslation();
  const history = useHistory();
  const { Title } = Typography;
  const headerController = GlobalDI.getOrRegister('ezeitHeaderController', () => new HeaderController());
  const routeMachter = useRouteMatch<{ tabKey?: string }>('/zeit/:tabKey?');
  const [activeTab, setActiveTab] = useState<string>(
    routeMachter?.params.tabKey != null ? routeMachter?.params.tabKey : routes.MEINE_ZEITPLANUNGEN,
  );
  const newZeitplanungsvorlageModalButtonRef = useRef<HTMLButtonElement>(null);
  const newZeitplanungModalButtonRef = useRef<HTMLButtonElement>(null);
  const [isNewZeitplanungsvorlageModalVisible, setIsNewZeitplanungsvorlageModalVisible] = useState(false);
  const [isNewZeitplanungModalVisible, setIsNewZeitplanungModalVisible] = useState(false);

  const [zeitplanungenData, setZeitplanungenData] = useState<ZeitplanungTableDTO[]>([]);
  const [zeitplanungsvorlagenData, setZeitplanungsvorlagenData] = useState<ZeitplanungsvorlageTableDTO[]>([]);
  const [systemvorlagenData, setSystemvorlagenData] = useState<ZeitplanungsvorlageTableDTO[]>([]);
  const zeitLightLocalController = GlobalDI.getOrRegister(
    'zeitLightLocalController',
    () => new ZeitLightLocalController(),
  );

  const zeitLighBackendController = GlobalDI.getOrRegister(
    'zeitLighBackendController',
    () => new ZeitLighBackendController(),
  );
  useEffect(() => {
    const zeitData = zeitLightLocalController.getZeitplanungenTableData();
    setZeitplanungenData(zeitData);
    const vorhabenData = zeitLightLocalController.getVorlagenTableData();
    setZeitplanungsvorlagenData(vorhabenData);
    const sub: Subscription = zeitLighBackendController.getZeitplanungsvorlagenAlleLightCall().subscribe({
      next: (data) => {
        data.forEach((vorlage) => {
          vorlage.dto.aktionen = [
            ...vorlage.dto.aktionen,
            ...[AktionType.Exportieren, AktionType.NeueVersionErstellen],
          ];
        });
        zeitLightLocalController.syncLocalDrafts(data, ZeitLightCollectionsKeys.Systemvorlagen);
        setSystemvorlagenData(zeitLightLocalController.getVorlagenTableData(ZeitLightCollectionsKeys.Systemvorlagen));
      },
      error: (err) => console.error(err),
    });

    return () => {
      if (sub) {
        sub.unsubscribe();
      }
    };
  }, []);

  useEffect(() => {
    setBreadcrumb(activeTab);
  }, [activeTab]);

  useEffect(() => {
    if (!activeTab && !routeMachter?.params.tabKey) {
      setActiveTab(routes.MEINE_ZEITPLANUNGEN);
    }
  }, [routeMachter?.params]);

  useEffect(() => {
    const tabKey: string = routeMachter?.params?.tabKey as string;
    if (tabKey !== activeTab) {
      setActiveTab(tabKey);
    }
  }, [routeMachter?.params?.tabKey]);

  useEffect(() => {
    newZeitplanungsvorlageModalButtonRef.current?.focus();
  }, [isNewZeitplanungsvorlageModalVisible]);

  useEffect(() => {
    newZeitplanungModalButtonRef.current?.focus();
  }, [isNewZeitplanungModalVisible]);

  const setBreadcrumb = (tabName: string) => {
    const tabText = (
      <span>
        {t(`zeit.header.breadcrumbs.projectName`)} - {t(`zeit.header.breadcrumbs.${tabName}`)}
      </span>
    );
    headerController.setHeaderProps({
      headerLeft: [<BreadcrumbComponent key="breadcrumb" items={[tabText]} />],
      headerCenter: [],
      headerRight: [],
      headerLast: [<ZeitHelpLink key="zeit-header-help" isLight={true} />],
    });
  };
  const tabItems: TabsProps['items'] = [
    {
      key: routes.MEINE_ZEITPLANUNGEN,
      label: t('zeit.myZeitHome.tabs.meineZeitplanungen.tabNav'),
      children: <MeineZeitplanungenTabLightComponent zeitplanungenData={zeitplanungenData} />,
    },
    {
      key: routes.ZEITPLANUNGSVORLAGEN,
      label: t('zeit.myZeitHome.tabs.zeitplanungsvorlagen.tabNav'),
      children: <ZeitplanungsvorlagenTabLightComponent zeitplanungsvorlagenData={zeitplanungsvorlagenData} />,
    },
    {
      key: routes.SYSTEMVORLAGEN,
      label: t('zeit.myZeitHome.tabs.systemvorlagen.tabNav'),
      children: <SystemvorlagenTabLightComponent zeitplanungsvorlagenData={systemvorlagenData} />,
    },
  ];
  return (
    <div className="zeit-home-page">
      <TitleWrapperComponent>
        <Row>
          <Col xs={{ span: 22, offset: 1 }}>
            <div className="heading-holder">
              <Title level={1}>{t('zeit.myZeitHome.title')}</Title>
              <div className="btn-container">
                <Button
                  id="ezeit-newZeitplanungsVorlage-btn"
                  type="default"
                  size={'large'}
                  onClick={() => setIsNewZeitplanungsvorlageModalVisible(true)}
                  ref={newZeitplanungsvorlageModalButtonRef}
                >
                  {t('zeit.header.btnNewZeitPlanungsVorlage')}
                </Button>
                <Button
                  id="ezeit-newZeitplanung-btn"
                  type="primary"
                  size={'large'}
                  onClick={() => setIsNewZeitplanungModalVisible(true)}
                  ref={newZeitplanungModalButtonRef}
                >
                  {t('zeit.header.btnNewZeitPlanung')}
                </Button>
              </div>
            </div>
          </Col>
        </Row>
      </TitleWrapperComponent>
      <Row>
        <Col xs={{ span: 22, offset: 1 }}>
          <NewZeitplanungsvorlageModal
            isVisible={isNewZeitplanungsvorlageModalVisible}
            setIsVisible={setIsNewZeitplanungsvorlageModalVisible}
            isLight={true}
          />

          <NewZeitplanungModal
            isVisible={isNewZeitplanungModalVisible}
            setIsVisible={setIsNewZeitplanungModalVisible}
            isLight={true}
          />

          <TabsWrapper
            activeKey={activeTab}
            className="my-periods-tabs standard-tabs"
            onChange={(key: string) => history.push(`/zeit/${key}`)}
            moduleName={t('zeit.myZeitHome.title')}
            items={tabItems}
          />
        </Col>
      </Row>
    </div>
  );
}
