// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import chai, { expect } from 'chai';
import chaiString from 'chai-string';
import { of } from 'rxjs';
import sinon from 'sinon';

import {
  Configuration,
  ErstellerType,
  RegelungsvorhabenTypType,
  ZeitplanungLightControllerApi,
  ZeitplanungselementEntityDTO,
  ZeitplanungsvorlageLightControllerApi,
  ZeitplanungsvorlagenBauplan,
} from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme';

import { ZeitLightController } from './controller-light';
import { ZeitCollectionInterface, ZeitLightCollectionsKeys, ZeitLightLocalController } from './controller-local';
import { testVorlage, testZeitplanung } from './controller-local.test';

chai.use(chaiString);

describe('deleteZeitvorlageFromZeitplanungsvorlageCallLight', () => {
  const ctrl = new ZeitLightController();
  const ctrlLocal = new ZeitLightLocalController();
  let deleteZeitplanungsvorlagenelementFromZeitplanungsvorlageLightStub: sinon.SinonStub;
  const zeitplanungsvorlageLightControllerApi = GlobalDI.getOrRegister(
    'zeitplanungsvorlageLightControllerApi',
    () => new ZeitplanungsvorlageLightControllerApi(new Configuration()),
  );
  const updatedVorlage = { ...testVorlage };
  updatedVorlage.dto.elemente = [updatedVorlage.dto.elemente[1]];
  beforeEach(() => {
    ctrlLocal.syncLocalDrafts([testVorlage], ZeitLightCollectionsKeys.Zeitplanungsvorlagen);
  });
  before(() => {
    deleteZeitplanungsvorlagenelementFromZeitplanungsvorlageLightStub = sinon
      .stub(zeitplanungsvorlageLightControllerApi, 'deleteZeitplanungsvorlagenelementFromZeitplanungsvorlageLight')
      .returns(of(updatedVorlage));
  });
  after(() => {
    ctrlLocal.syncLocalDrafts([], ZeitLightCollectionsKeys.Zeitplanungsvorlagen);
    deleteZeitplanungsvorlagenelementFromZeitplanungsvorlageLightStub.restore();
  });
  afterEach(() => {
    deleteZeitplanungsvorlagenelementFromZeitplanungsvorlageLightStub.resetHistory();
  });
  it('deleteZeitvorlageFromZeitplanungsvorlageCallLight test call', () => {
    ctrl.deleteZeitvorlageFromZeitplanungsvorlageCallLight('6b443066-5c94-32c3-b5d3-4863ed029114', 'test-vorlage-id');
    sinon.assert.calledOnce(deleteZeitplanungsvorlagenelementFromZeitplanungsvorlageLightStub);
    const draftCollectionString: string =
      global.window.localStorage.getItem(ZeitLightCollectionsKeys.Zeitplanungsvorlagen) || '';
    const draftCollection: ZeitCollectionInterface = JSON.parse(draftCollectionString) as ZeitCollectionInterface;
    expect(draftCollection['test-vorlage-id'].dto.elemente).to.have.lengthOf(3);
  });
});

describe('addDateToZeitplanungsvorlageLight', () => {
  const ctrl = new ZeitLightController();
  const ctrlLocal = new ZeitLightLocalController();
  const zeitplanungsvorlageLightControllerApi = GlobalDI.getOrRegister(
    'zeitplanungsvorlageLightControllerApi',
    () => new ZeitplanungsvorlageLightControllerApi(new Configuration()),
  );
  let addDateToZeitplanungsvorlageLightStub: sinon.SinonStub;
  const newTermin = {
    termin: true,
    wichtig: false,
    bauplan: ZeitplanungsvorlagenBauplan.GAnnahmeUndInterneVerteilungGesetzbeschluss,
    titel: 'New Gesetzes',
    vorherigeElementeIds: ['16454c54-50f7-3537-80bc-efbc78b00e38'],
    dauer: {
      days: 1,
    },
  };
  const updatedVorlage = { ...testVorlage };
  updatedVorlage.dto.elemente.push(newTermin);
  before(() => {
    ctrlLocal.syncLocalDrafts([testVorlage], ZeitLightCollectionsKeys.Zeitplanungsvorlagen);
    addDateToZeitplanungsvorlageLightStub = sinon
      .stub(zeitplanungsvorlageLightControllerApi, 'addDateToZeitplanungsvorlageLight')
      .returns(of(updatedVorlage));
  });
  after(() => {
    addDateToZeitplanungsvorlageLightStub.restore();
  });
  afterEach(() => {
    addDateToZeitplanungsvorlageLightStub.resetHistory();
  });

  it('addDateToZeitplanungsvorlageLight test call', () => {
    ctrl.addDateToZeitplanungsvorlageLight({
      id: 'test-vorlage-id',
      terminvorlageEntityDTO: newTermin,
    });

    sinon.assert.calledOnce(addDateToZeitplanungsvorlageLightStub);
    const draftCollectionString: string =
      global.window.localStorage.getItem(ZeitLightCollectionsKeys.Zeitplanungsvorlagen) || '';
    const draftCollection: ZeitCollectionInterface = JSON.parse(draftCollectionString) as ZeitCollectionInterface;
    expect(draftCollection['test-vorlage-id'].dto.elemente).to.have.lengthOf(3);
  });
});

describe('modifyDateToZeitplanungsvorlageLight', () => {
  const ctrl = new ZeitLightController();
  const ctrlLocal = new ZeitLightLocalController();
  const zeitplanungsvorlageLightControllerApi = GlobalDI.getOrRegister(
    'zeitplanungsvorlageLightControllerApi',
    () => new ZeitplanungsvorlageLightControllerApi(new Configuration()),
  );
  let modifyDateToZeitplanungsvorlageLightStub: sinon.SinonStub;
  const upadatedTermin = {
    termin: false,
    wichtig: false,
    bauplan: ZeitplanungsvorlagenBauplan.GAnnahmeUndInterneVerteilungGesetzbeschluss,
    titel: 'New Termin',
    vorherigeElementeIds: ['16454c54-50f7-3537-80bc-efbc78b00e38'],
    dauer: {
      weeks: 2,
    },
  };
  const updatedVorlage = { ...testVorlage };
  updatedVorlage.dto.elemente[1].dto = upadatedTermin;
  beforeEach(() => {
    ctrlLocal.syncLocalDrafts([testVorlage], ZeitLightCollectionsKeys.Zeitplanungsvorlagen);
  });
  before(() => {
    modifyDateToZeitplanungsvorlageLightStub = sinon
      .stub(zeitplanungsvorlageLightControllerApi, 'modifyDateInZeitplanungsvorlageLight')
      .returns(of(updatedVorlage));
  });
  after(() => {
    modifyDateToZeitplanungsvorlageLightStub.restore();
  });
  afterEach(() => {
    modifyDateToZeitplanungsvorlageLightStub.resetHistory();
  });

  it('modifyDateToZeitplanungsvorlageLight test call', () => {
    ctrl.modifyDateToZeitplanungsvorlageLight({
      elementId: 'ff791ac7-dda4-3178-8acf-1910595063bc',
      terminvorlageEntityDTO: {
        termin: false,
        wichtig: false,
        bauplan: ZeitplanungsvorlagenBauplan.GAnnahmeUndInterneVerteilungGesetzbeschluss,
        titel: 'New Termin',
        vorherigeElementeIds: ['16454c54-50f7-3537-80bc-efbc78b00e38'],
        dauer: {
          weeks: 2,
        },
      },
      vorlageId: 'test-vorlage-id',
    });
    sinon.assert.calledOnce(modifyDateToZeitplanungsvorlageLightStub);
    const draftCollectionString: string =
      global.window.localStorage.getItem(ZeitLightCollectionsKeys.Zeitplanungsvorlagen) || '';
    const draftCollection: ZeitCollectionInterface = JSON.parse(draftCollectionString) as ZeitCollectionInterface;
    expect(draftCollection['test-vorlage-id'].dto.elemente[1].dto.titel).to.eql('New Termin');
  });
});
describe('addPhaseToZeitplanungsvorlageLight', () => {
  const ctrl = new ZeitLightController();
  const ctrlLocal = new ZeitLightLocalController();
  const zeitplanungsvorlageLightControllerApi = GlobalDI.getOrRegister(
    'zeitplanungsvorlageLightControllerApi',
    () => new ZeitplanungsvorlageLightControllerApi(new Configuration()),
  );
  let addPhaseToZeitplanungsvorlageLightStub: sinon.SinonStub;
  const newPhase = {
    termin: false,
    wichtig: false,
    bauplan: ZeitplanungsvorlagenBauplan.GAnnahmeUndInterneVerteilungGesetzbeschluss,
    titel: 'New Phase',
    vorherigeElementeIds: ['16454c54-50f7-3537-80bc-efbc78b00e38'],
    dauer: {
      weeks: 2,
    },
  };
  const updatedVorlage = { ...testVorlage };
  updatedVorlage.dto.elemente.push(newPhase);

  beforeEach(() => {
    ctrlLocal.syncLocalDrafts([testVorlage], ZeitLightCollectionsKeys.Zeitplanungsvorlagen);
  });
  before(() => {
    addPhaseToZeitplanungsvorlageLightStub = sinon
      .stub(zeitplanungsvorlageLightControllerApi, 'addPhaseToZeitplanungsvorlageLight')
      .returns(of(updatedVorlage));
  });
  after(() => {
    addPhaseToZeitplanungsvorlageLightStub.restore();
  });
  afterEach(() => {
    addPhaseToZeitplanungsvorlageLightStub.resetHistory();
  });

  it('addPhaseToZeitplanungsvorlageLight test call', () => {
    ctrl.addPhaseToZeitplanungsvorlageLight({
      id: 'test-vorlage-id',
      phasenvorlageEntityDTO: newPhase,
    });
    sinon.assert.calledOnce(addPhaseToZeitplanungsvorlageLightStub);
    const draftCollectionString: string =
      global.window.localStorage.getItem(ZeitLightCollectionsKeys.Zeitplanungsvorlagen) || '';
    const draftCollection: ZeitCollectionInterface = JSON.parse(draftCollectionString) as ZeitCollectionInterface;
    expect(draftCollection['test-vorlage-id'].dto.elemente).to.have.lengthOf(3);
  });
});

describe('modifyPhaseToZeitplanungsvorlageCallLight', () => {
  const ctrl = new ZeitLightController();
  const ctrlLocal = new ZeitLightLocalController();
  let modifyPhaseToZeitplanungsvorlageCallLightStub: sinon.SinonStub;
  const zeitplanungsvorlageLightControllerApi = GlobalDI.getOrRegister(
    'zeitplanungsvorlageLightControllerApi',
    () => new ZeitplanungsvorlageLightControllerApi(new Configuration()),
  );
  const upadatedPhase = {
    termin: false,
    wichtig: false,
    bauplan: ZeitplanungsvorlagenBauplan.GAnnahmeUndInterneVerteilungGesetzbeschluss,
    titel: 'Updated Phase',
    vorherigeElementeIds: ['16454c54-50f7-3537-80bc-efbc78b00e38'],
    dauer: {
      weeks: 2,
    },
  };
  const updatedVorlage = { ...testVorlage };
  updatedVorlage.dto.elemente[0].dto = upadatedPhase;

  beforeEach(() => {
    ctrlLocal.syncLocalDrafts([testVorlage], ZeitLightCollectionsKeys.Zeitplanungsvorlagen);
  });
  before(() => {
    modifyPhaseToZeitplanungsvorlageCallLightStub = sinon
      .stub(zeitplanungsvorlageLightControllerApi, 'modifyPhaseInZeitplanungsvorlageLight')
      .returns(of(updatedVorlage));
  });
  after(() => {
    modifyPhaseToZeitplanungsvorlageCallLightStub.restore();
  });
  afterEach(() => {
    modifyPhaseToZeitplanungsvorlageCallLightStub.resetHistory();
  });

  it('modifyPhaseToZeitplanungsvorlageCallLightStub test call', () => {
    ctrl.modifyPhaseToZeitplanungsvorlageCallLight({
      elementId: '6b443066-5c94-32c3-b5d3-4863ed029114',
      phasenvorlageEntityDTO: upadatedPhase,
      vorlageId: 'test-vorlage-id',
    });
    sinon.assert.calledOnce(modifyPhaseToZeitplanungsvorlageCallLightStub);
    const draftCollectionString: string =
      global.window.localStorage.getItem(ZeitLightCollectionsKeys.Zeitplanungsvorlagen) || '';
    const draftCollection: ZeitCollectionInterface = JSON.parse(draftCollectionString) as ZeitCollectionInterface;
    expect(draftCollection['test-vorlage-id'].dto.elemente[0].dto.titel).to.eql('Updated Phase');
  });
});

describe('deleteZeitplanungselementFromZeitplanungCallLight', () => {
  const ctrl = new ZeitLightController();
  const ctrlLocal = new ZeitLightLocalController();
  let deleteZeitplanungselementFromZeitplanungCallLightStub: sinon.SinonStub;
  const zeitplanungLightControllerApi = GlobalDI.getOrRegister(
    'zeitplanungLightControllerApi',
    () => new ZeitplanungLightControllerApi(new Configuration()),
  );
  const updatedZeitplanung = { ...testZeitplanung };
  updatedZeitplanung.dto.elemente = updatedZeitplanung.dto.elemente.filter((item) => item.base.id !== 'termin2');
  beforeEach(() => {
    ctrlLocal.updateItem(updatedZeitplanung, ZeitLightCollectionsKeys.Zeitplanung);
  });
  before(() => {
    deleteZeitplanungselementFromZeitplanungCallLightStub = sinon
      .stub(zeitplanungLightControllerApi, 'deleteZeitplanungselementFromZeitplanungLight')
      .returns(of(updatedZeitplanung));
  });
  after(() => {
    deleteZeitplanungselementFromZeitplanungCallLightStub.restore();
  });
  afterEach(() => {
    deleteZeitplanungselementFromZeitplanungCallLightStub.resetHistory();
  });
  it('deleteZeitplanungselementFromZeitplanungCallLight test call', () => {
    ctrl.deleteZeitplanungselementFromZeitplanungCallLight('termin2', 'testZeitplanung', false);
    sinon.assert.calledOnce(deleteZeitplanungselementFromZeitplanungCallLightStub);
    const draftCollectionString: string =
      global.window.localStorage.getItem(ZeitLightCollectionsKeys.Zeitplanung) || '';
    const draftCollection: ZeitCollectionInterface = JSON.parse(draftCollectionString) as ZeitCollectionInterface;
    expect(draftCollection['testZeitplanung'].dto.elemente).to.have.lengthOf(4);
  });
});

describe('addDateToZeitplanungLight ', () => {
  const ctrl = new ZeitLightController();
  const ctrlLocal = new ZeitLightLocalController();
  const zeitplanungLightControllerApi = GlobalDI.getOrRegister(
    'zeitplanungLightControllerApi',
    () => new ZeitplanungLightControllerApi(new Configuration()),
  );
  let addDateToZeitplanungLightStub: sinon.SinonStub;
  const newTermin: ZeitplanungselementEntityDTO = {
    termin: true,
    wichtig: false,
    bauplan: ZeitplanungsvorlagenBauplan.GKabinettbeschluss,
    titel: 'New Termin',
    uebergeordneteElementeId: '',
    vorherigeElementeIds: ['0d2040f0-a9b7-3b75-9b5e-6a6d9a88ae1e'],
    kommentar: '',
    zeitplanungAnpassen: false,
    hinweise: [],
    gesperrt: false,
    datum: '2023-10-04',
    beginn: '2023-10-04',
    ende: '2023-10-04',
  };
  const updatedZeitplanung = { ...testZeitplanung };
  updatedZeitplanung.dto.elemente.push(newTermin);
  before(() => {
    ctrlLocal.updateItem(testZeitplanung, ZeitLightCollectionsKeys.Zeitplanung);
    addDateToZeitplanungLightStub = sinon
      .stub(zeitplanungLightControllerApi, 'addDateToZeitplanungLight')
      .returns(of(updatedZeitplanung));
  });
  after(() => {
    addDateToZeitplanungLightStub.restore();
  });
  afterEach(() => {
    addDateToZeitplanungLightStub.resetHistory();
  });

  it('addDateToZeitplanungLight test call', () => {
    ctrl.addDateToZeitplanungLight({
      id: 'testZeitplanung',
      terminEntityDTO: newTermin,
    });

    sinon.assert.calledOnce(addDateToZeitplanungLightStub);
    const draftCollectionString: string =
      global.window.localStorage.getItem(ZeitLightCollectionsKeys.Zeitplanung) || '';
    const draftCollection: ZeitCollectionInterface = JSON.parse(draftCollectionString) as ZeitCollectionInterface;
    expect(draftCollection['testZeitplanung'].dto.elemente).to.have.lengthOf(4);
  });
});

describe('modifyDateInZeitplanungCallLight', () => {
  const ctrl = new ZeitLightController();
  const ctrlLocal = new ZeitLightLocalController();
  const zeitplanungLightControllerApi = GlobalDI.getOrRegister(
    'zeitplanungLightControllerApi',
    () => new ZeitplanungLightControllerApi(new Configuration()),
  );
  let modifyDateInZeitplanungCallLightStub: sinon.SinonStub;
  const updatedZeitplanung = { ...testZeitplanung };
  updatedZeitplanung.dto.elemente[1].dto.titel = 'New Termin Updated';
  beforeEach(() => {
    ctrlLocal.updateItem(updatedZeitplanung, ZeitLightCollectionsKeys.Zeitplanung);
  });
  before(() => {
    modifyDateInZeitplanungCallLightStub = sinon
      .stub(zeitplanungLightControllerApi, 'modifyDateInZeitplanungLight')
      .returns(of(updatedZeitplanung));
  });
  after(() => {
    modifyDateInZeitplanungCallLightStub.restore();
  });
  afterEach(() => {
    modifyDateInZeitplanungCallLightStub.resetHistory();
  });

  it('modifyDateInZeitplanungCallLight test call', () => {
    ctrl.modifyDateInZeitplanungCallLight({
      elementId: 'termin1',
      terminEntityDTO: {
        termin: true,
        wichtig: false,
        bauplan: ZeitplanungsvorlagenBauplan.GKabinettbeschluss,
        titel: 'New Termin Updated',
        uebergeordneteElementeId: '',
        vorherigeElementeIds: ['0d2040f0-a9b7-3b75-9b5e-6a6d9a88ae1e'],
        kommentar: '',
        zeitplanungAnpassen: false,
        hinweise: [],
        gesperrt: false,
        datum: '2023-10-04',
        beginn: '2023-10-04',
        ende: '2023-10-04',
      },
      zeitplanungId: 'testZeitplanung',
    });
    sinon.assert.calledOnce(modifyDateInZeitplanungCallLightStub);
    const draftCollectionString: string =
      global.window.localStorage.getItem(ZeitLightCollectionsKeys.Zeitplanung) || '';
    const draftCollection: ZeitCollectionInterface = JSON.parse(draftCollectionString) as ZeitCollectionInterface;
    expect(draftCollection['testZeitplanung'].dto.elemente[1].dto.titel).to.eql('New Termin Updated');
  });
});

describe('addPhaseToZeitplanungLight ', () => {
  const ctrl = new ZeitLightController();
  const ctrlLocal = new ZeitLightLocalController();
  const zeitplanungLightControllerApi = GlobalDI.getOrRegister(
    'zeitplanungLightControllerApi',
    () => new ZeitplanungLightControllerApi(new Configuration()),
  );
  let addPhaseToZeitplanungLightStub: sinon.SinonStub;
  const newPhase: ZeitplanungselementEntityDTO = {
    termin: false,
    wichtig: false,
    bauplan: ZeitplanungsvorlagenBauplan.GKabinettbeschluss,
    titel: 'New Phase',
    uebergeordneteElementeId: '',
    vorherigeElementeIds: ['0d2040f0-a9b7-3b75-9b5e-6a6d9a88ae1e'],
    kommentar: '',
    zeitplanungAnpassen: false,
    hinweise: [],
    gesperrt: false,
    datum: '2023-10-04',
    beginn: '2023-10-04',
    ende: '2023-10-20',
  };
  const updatedZeitplanung = { ...testZeitplanung };
  updatedZeitplanung.dto.elemente.push(newPhase);
  before(() => {
    ctrlLocal.updateItem(testZeitplanung, ZeitLightCollectionsKeys.Zeitplanung);
    addPhaseToZeitplanungLightStub = sinon
      .stub(zeitplanungLightControllerApi, 'addPhaseToZeitplanungLight')
      .returns(of(updatedZeitplanung));
  });
  after(() => {
    addPhaseToZeitplanungLightStub.restore();
  });
  afterEach(() => {
    addPhaseToZeitplanungLightStub.resetHistory();
  });

  it('addPhaseToZeitplanungLight test call', () => {
    ctrl.addPhaseToZeitplanungLight({
      id: 'testZeitplanung',
      phaseEntityDTO: newPhase,
    });

    sinon.assert.calledOnce(addPhaseToZeitplanungLightStub);
    const draftCollectionString: string =
      global.window.localStorage.getItem(ZeitLightCollectionsKeys.Zeitplanung) || '';
    const draftCollection: ZeitCollectionInterface = JSON.parse(draftCollectionString) as ZeitCollectionInterface;
    expect(draftCollection['testZeitplanung'].dto.elemente).to.have.lengthOf(4);
  });
});

describe('modifyPhaseInZeitplanungCallLight', () => {
  const ctrl = new ZeitLightController();
  const ctrlLocal = new ZeitLightLocalController();
  const zeitLightController = GlobalDI.getOrRegister('zeitLightController', () => new ZeitLightController());
  const zeitplanungLightControllerApi = GlobalDI.getOrRegister(
    'zeitplanungLightControllerApi',
    () => new ZeitplanungLightControllerApi(new Configuration()),
  );
  let modifyPhaseInZeitplanungCallLightStub: sinon.SinonStub;
  let updateZeitplanungStub: sinon.SinonStub;
  const updatedZeitplanung = { ...testZeitplanung };
  updatedZeitplanung.dto.elemente[0].dto.titel = 'Updated Phase';
  beforeEach(() => {
    ctrlLocal.updateItem(updatedZeitplanung, ZeitLightCollectionsKeys.Zeitplanung);
  });
  before(() => {
    modifyPhaseInZeitplanungCallLightStub = sinon
      .stub(zeitplanungLightControllerApi, 'modifyPhaseInZeitplanungLight')
      .returns(of(updatedZeitplanung));
    updateZeitplanungStub = sinon.stub(ctrl, 'updateZeitplanung');
  });
  after(() => {
    modifyPhaseInZeitplanungCallLightStub.restore();
    updateZeitplanungStub.restore();
  });
  afterEach(() => {
    modifyPhaseInZeitplanungCallLightStub.resetHistory();
    updateZeitplanungStub.resetHistory();
  });

  it('modifyPhaseInZeitplanungCallLight test call', () => {
    ctrl.modifyPhaseInZeitplanungCallLight({
      elementId: 'phase1',
      phaseEntityDTO: {
        termin: false,
        wichtig: false,
        bauplan: ZeitplanungsvorlagenBauplan.GKabinettbeschluss,
        titel: 'Updated Phase',
        uebergeordneteElementeId: '',
        vorherigeElementeIds: ['0d2040f0-a9b7-3b75-9b5e-6a6d9a88ae1e'],
        kommentar: '',
        zeitplanungAnpassen: false,
        hinweise: [],
        gesperrt: false,
        datum: '2023-10-04',
        beginn: '2023-10-04',
        ende: '2023-10-30',
      },
      zeitplanungId: 'testZeitplanung',
    });
    sinon.assert.calledOnce(modifyPhaseInZeitplanungCallLightStub);
    const draftCollectionString: string =
      global.window.localStorage.getItem(ZeitLightCollectionsKeys.Zeitplanung) || '';
    const draftCollection: ZeitCollectionInterface = JSON.parse(draftCollectionString) as ZeitCollectionInterface;
    expect(draftCollection['testZeitplanung'].dto.elemente[0].dto.titel).to.eql('Updated Phase');
  });
});

describe('updateVorlage', () => {
  const ctrl = new ZeitLightController();
  const zeitLightLocalController = GlobalDI.getOrRegister(
    'zeitLightLocalController',
    () => new ZeitLightLocalController(),
  );

  let updateItemStub: sinon.SinonStub;
  before(() => {
    updateItemStub = sinon.stub(zeitLightLocalController, 'updateItem');
  });
  after(() => {
    updateItemStub.restore();
  });
  afterEach(() => {
    updateItemStub.resetHistory();
  });

  it('updateVorlage test call', () => {
    const dto = {
      erstellerType: ErstellerType.Selbst,
      erstelltVon: {
        base: {
          id: 'user-guest',
          erstelltAm: '2022-06-16T12:34:26.538579Z',
          bearbeitetAm: '2022-08-24T11:41:49.951635Z',
        },
        dto: {
          name: 'Gast',
          email: 'Gast',
        },
      },
      titel: 'Draft 1',
      beschreibung: undefined,
      readOnlyAccess: false,
      vorhabenart: RegelungsvorhabenTypType.Gesetz,
      elemente: [],
      dauer: 3,
    };
    ctrl.updateVorlage(testVorlage, dto);
    sinon.assert.calledOnce(updateItemStub);
  });
});

describe('updateZeitplanung', () => {
  const ctrl = new ZeitLightController();
  const zeitLightLocalController = GlobalDI.getOrRegister(
    'zeitLightLocalController',
    () => new ZeitLightLocalController(),
  );

  let updateItemStub: sinon.SinonStub;
  before(() => {
    updateItemStub = sinon.stub(zeitLightLocalController, 'updateItem');
  });
  after(() => {
    updateItemStub.restore();
  });
  afterEach(() => {
    updateItemStub.resetHistory();
  });

  it('updateZeitplanung test call', () => {
    const dto = {
      erstellerType: ErstellerType.Selbst,
      erstelltVon: {
        base: {
          id: 'user-guest',
          erstelltAm: '',
          bearbeitetAm: '',
        },
        dto: {
          name: 'Gast',
          email: 'Gast',
        },
      },
      titel: 'TEST',
      beschreibung: 'asdasd',
      regelungsvorhabenId: '',
      readOnlyAccess: false,
      vorhabenart: RegelungsvorhabenTypType.Gesetz,
      elemente: [
        {
          base: {
            id: 'phase1',
            erstelltAm: '',
            bearbeitetAm: '',
          },
          dto: {
            termin: false,
            wichtig: false,
            bauplan: ZeitplanungsvorlagenBauplan.GVorphase,
            titel: 'Vorphase',
            uebergeordneteElementeId: '',
            vorherigeElementeIds: [],
            kommentar: '',
            zeitplanungAnpassen: false,
            hinweise: [],
            gesperrt: false,
            beginn: '2022-12-06',
            ende: '2023-04-24',
            untergeordneteElemente: [],
            datum: '2023-04-24',
          },
        },
      ],
      naechstesElement: {
        id: 'ccd9501e-05c9-3ead-9871-d48aa278052d',
        beginn: '2023-04-25',
        ende: '2023-05-11',
        titel: 'Hausabstimmung',
        termin: false,
        faelligIn: 140,
      },
      erstePhaseBeginntAm: '2022-12-06',
      letztePhaseEndetAm: '2024-05-12',
    };
    ctrl.updateZeitplanung(testZeitplanung, dto);
    sinon.assert.calledOnce(updateItemStub);
  });
});
