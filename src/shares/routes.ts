// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

export const routes = {
  MEINE_ZEITPLANUNGEN: 'meineZeitplanungen',
  BILLIGUNGSANFRAGEN: 'billigungsanfragen',
  ZEITPLANUNGSVORLAGEN: 'zeitplanungsvorlagen',
  SYSTEMVORLAGEN: 'systemvorlagen',
  ARCHIV: 'archiv',

  NEUEZEITPLANUNG: 'neueZeitplanung',
  REGELUNGSVORHABEN: 'regelungsvorhaben',

  TABELLENSICHT: 'tabellensicht',
  KOMMENTARE: 'kommentare',
  KALENDERSICHT: 'kalendersicht',

  NEUES_ELEMENT: 'neuesElement',
  ELEMENT_BEARBEITEN: 'elementBearbeiten',
  ELEMENT_PRUEFEN: 'pruefen',
  UEBERSICHT: 'uebersicht',

  ERFOLG: 'erfolg',
  FEHLER: 'fehler',

  HILFE: 'hilfe',
};
